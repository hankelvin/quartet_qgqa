import os, glob, shutil, re, time, torch
import pytorch_lightning as pl
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks.early_stopping import EarlyStopping 
from pytorch_lightning.callbacks.progress import TQDMProgressBar
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
from pytorch_lightning.strategies.deepspeed import DeepSpeedStrategy
from pytorch_lightning.utilities.deepspeed import convert_zero_checkpoint_to_fp32_state_dict
from pytorch_lightning.loggers import TensorBoardLogger
from utils.utils import QTTLogger
from utils.modeling_omnibus import load_save_model, save_exp_configs
from utils.utils_omnibus import (deepspeed_config_for_t5, visual_inspect_saveout, 
                                set_savepath, check_save2loadmodel, unlock4loadmodel)
from utils.lightning_omnibus import OmnibusModel 
from utils.lightning_omnibus_data import OmnibusDataModule
torch.use_deterministic_algorithms(True)
os.environ["TOKENIZERS_PARALLELISM"] = "true"


def main(args):
    ### 0. PRELIMINARIES: Setting random seeds for reproducibility
    pl.seed_everything(54506)
    # Ensure that all operations are deterministic on GPU (if used) for reproducibility
    torch.backends.cudnn.determinstic = True
    torch.backends.cudnn.benchmark = False
    os.environ["TOKENIZERS_PARALLELISM"] = "true"
    
    # a. set up savepath, model directory and logging
    SAVEPATH = set_savepath(args)    
    args.savepath = args.test_out_savepath = SAVEPATH
    if not os.path.exists(args.savepath): os.makedirs(args.savepath)    
    args.logger = logger = QTTLogger(args.savepath, main_process = True) 
    logger.print('SAVEPATH: ', args.savepath)
    
    if args.settings['diet_gpu'][0] == 'deepspeed': 
        ds_stage = re.search(r'\d', args.settings['deepspeed_stage']).group()
        ds_config = deepspeed_config_for_t5(ds_stage = int(ds_stage))
        args.ds_config = ds_config

    ### 1. LOAD MODEL
    if args.load_ckpt:
        while True:
            if check_save2loadmodel(args): break
            time.sleep(60*5)
        logger.print('Model state found free... Already locked for loading here...')
        PL_MODEL = load_save_model(*args.load_ckpt, args)
        unlock4loadmodel(args)
    else: PL_MODEL = OmnibusModel(args = args)
    p_lm_args = PL_MODEL.args
    logger.print('MODEL LOADED ....')

    ### 2. LOAD DATA
    datamodule = OmnibusDataModule(args, PL_MODEL.tokenizer, PL_MODEL.model.ipstrat2idx)
    logger.print('DATAMODULE LOADED ....')

    # enforce deterministic again (in case dataset handler sets otherwise)
    torch.use_deterministic_algorithms(True)
    
    ### 3. CHECKS... visually inspect src and tgts
    visual_inspect_saveout(p_lm_args, datamodule, PL_MODEL.tokenizer, args.logger)
    if not args.test_only:
        p_lm_args.training_steps = len(datamodule.train_dataloader())*p_lm_args.settings['max_epochs']
    
    ### 4. SET UP LIGHTNING
    tb_logger = TensorBoardLogger(save_dir = args.savepath, version = 1, name = "lightning_logs")
    print('args.ckpt_every_n_steps: ', args.ckpt_every_n_steps)
    max_epochs = p_lm_args.settings['max_epochs'] if args.max_steps is None else None
    max_steps  = args.max_steps
    
    if args.ckpt_every_n_steps is None:
        save_top_k = 3 if not p_lm_args.save_all_epochs else -1
        if max_epochs is None: save_top_k = 1
        ckpt_callback = ModelCheckpoint(dirpath = args.savepath, 
                        save_top_k = save_top_k, 
                        monitor = "validation_loss_epoch", mode = "min", 
                        every_n_epochs = 1, save_on_train_epoch_end = True)
    else: 
        save_top_k = 3 if not p_lm_args.save_all_epochs else max_epochs
        if max_epochs is None: save_top_k = 1
        ckpt_callback = ModelCheckpoint(dirpath = args.savepath, 
                        save_top_k = save_top_k, 
                        every_n_train_steps = args.ckpt_every_n_steps, 
                        save_on_train_epoch_end = True)
    lr_monitor = LearningRateMonitor(logging_interval='step')
    callbacks = [
                EarlyStopping(monitor = "validation_loss_epoch", mode = "min",  
                min_delta = 0.00, patience = args.patience, check_on_train_epoch_end = False),
                TQDMProgressBar(refresh_rate = 20), 
                ckpt_callback, lr_monitor
                ]
    plugins, strategy = [], 'ddp' if p_lm_args.accelerator == 'gpu' and p_lm_args.devices > 1 else None
    # b. prepare deepspeed config/strat
    if p_lm_args.settings['diet_gpu'][0] == 'deepspeed': 
        p_lm_args.ds_config = ds_config
        print('ds_config', ds_config, '\n')
        strategy = DeepSpeedStrategy(config = ds_config)
    
    ### 5. TRAIN 
    # a. for reduced train set exps, ensure same max_step as full train set
    max_epochs, max_steps = p_lm_args.settings['max_epochs'], -1
    print('max_epochs'.upper(), max_epochs)

    # b. set up Trainer
    c_bf_models = 't5' in p_lm_args.settings['model']
    try: c_bf_deepspeed = p_lm_args.ds_config['bf16']['enabled']
    except: c_bf_deepspeed = False
    if (c_bf_models and c_bf_deepspeed):
        check_fp = 'bf16' 
        torch.backends.cuda.matmul.allow_tf32 = True
    else: check_fp = 16
    print('check_fp', check_fp)
    trainer = Trainer(deterministic = True, strategy = strategy,
                gpus = p_lm_args.devices if p_lm_args.accelerator == 'gpu' else None, 
                num_nodes = p_lm_args.num_nodes, plugins = plugins, accelerator = p_lm_args.accelerator, 
                precision = check_fp, check_val_every_n_epoch = args.check_val_every_n_epoch, 
                max_epochs = max_epochs, max_steps = max_steps, callbacks = callbacks,
                accumulate_grad_batches = args.settings['accumulate_grad_batches'], logger = tb_logger, 
                multiple_trainloader_mode = 'max_size_cycle',
                log_every_n_steps = 25)
    
    # c. only log on main hereafter to avoid clashes
    p_lm_args.logger.main_process = trainer.is_global_zero
    logger.main_process = trainer.is_global_zero
    
    ##### START TRAINING #####
    if not p_lm_args.test_only: 
        # run Trainer fit
        save_exp_configs(p_lm_args, PL_MODEL, logger)
        p_lm_args.logger = PL_MODEL.args.logger = logger # restore logger if loaded from str from saved
        trainer.fit(model = PL_MODEL, datamodule = datamodule, 
                ckpt_path = p_lm_args.load_ckpt[1] if p_lm_args.load_ckpt else None)
    
    ### 6. TEST (on rank 1 only, to avoid uneven ddp data split problem)
    if trainer.is_global_zero:
        # a. Lightning Deepspeed hack
        if not p_lm_args.test_only: 
            ckpt_save_path = ckpt_callback.best_model_path
            ckpt_output_path = f'{args.savepath}best_model.pt'
            trainer.save_checkpoint(f'{args.savepath}final.xkpt') # NOTE: for use in exps where max_steps specified
            logger.print('Trainer.fit() best model path:', ckpt_callback.best_model_path)
            logger.print('Trainer.fit() best model score: ', ckpt_callback.best_model_score )
            if 'deepspeed' in p_lm_args.settings['diet_gpu']: 
                logger.print('Location to save converted deepspeed', ckpt_output_path)
                convert_zero_checkpoint_to_fp32_state_dict(ckpt_save_path, ckpt_output_path) 
            p_lm_args.logger = PL_MODEL.args.logger = logger # restore logger if loaded from str from saved
            PL_MODEL = load_save_model(ckpt_output_path, ckpt_save_path, p_lm_args, test_phase = True)
        
        # c. re-init trainer to remove ddp
        trainer = Trainer(deterministic = True, devices = 1, accelerator = p_lm_args.accelerator, 
                    precision = check_fp, num_nodes = 1, 
                    callbacks = [TQDMProgressBar(refresh_rate = 20), ckpt_callback], 
                    logger = tb_logger, multiple_trainloader_mode = 'max_size_cycle', log_every_n_steps = 25 )
        
        ### BEGIN TESTING ### 
        trainer.test(model = PL_MODEL, datamodule = datamodule)

        ### 7. HOUSEKEEPING
        # a. keep best_model.pt as well as checkpoints (with model and optim states)
        ckpt_fps = glob.glob(f'{p_lm_args.savepath}*.ckpt')
        logger.print('Going into housekeeping...')
        for fp in ckpt_fps: 
            if os.path.basename(fp) == os.path.basename(ckpt_save_path): 
                shutil.copytree(ckpt_save_path, ckpt_save_path.replace('.ckpt', '_tested.ckpt'))
            if p_lm_args.save_all_epochs: continue
            logger.print(f'Saved model checkpoints {fp} being removed from disk...')
            shutil.rmtree(fp)        
        logger.print('Tested model (FINAL) checkpoint saved to disk...')
        logger.print('Housekeeping completed!')

        # c. save experiment settings/configs too
        save_exp_configs(p_lm_args, PL_MODEL, logger)
        

if __name__ == '__main__':
    import argparse
    from utils.utils_omnibus import Settings, backward_compat_args

    parser = argparse.ArgumentParser(description='Arugments for running script.')
    parser.add_argument('--settings_code', help = 'Which experimental setting code to use',
                        default='a01_t5base_qg', type=str)
    parser.add_argument('--model', help = 'If specified, overwrite the model name under settings code',
                        default=None, type=str)
    parser.add_argument('--accelerator', help = 'Which type of accelerator to use (cpu, or gpu), default cpu',
                        default='gpu', type=str)
    parser.add_argument('--devices', help = 'The number of gpus to use for training',
                        default=2, type=int)
    parser.add_argument('--bsz', help = 'Batch size to over utils_omnibus',
                        default = None, type=int)
    parser.add_argument('--num_nodes', help = 'The number of nodes to use for training',
                        default=1, type=int)
    parser.add_argument('--max_epochs', help = 'The epochs to run traning',
                        default=None, type=int)
    parser.add_argument('--max_steps', help = 'The number of steps to run. Overrides max_epochs',
                        default=None, type=int)
    parser.add_argument('--test_only', help = 'If testing only, provide the path to (i) model .pt and \
                        (ii) path to checkpoint .ckpt in settings under "load_ckpt".',
                        default = False, type = bool)
    parser.add_argument('--load_ckpt', help = 'If loading from ckpt, provide path to checkpoint .ckpt here. \
                        input here should be 2-element sequence. first elem is best_model.pt or "", second elem\
                        is the path to the checkpoint to load.',
                        default = [], nargs= "*", type = str)
    parser.add_argument('--datasets', help = 'The dataset(s) to use, overrides all datasets in every task\'s settings',
                        default = [], nargs= "*", type = str)
    parser.add_argument('--datasets_filter', help = 'The filter to apply to dataset(s) specified in --datasets (or under \
                        args.settings. input should be {task}:{dataset}:{filter} as a single str.',
                        default = {}, nargs= "*", type = str)
    parser.add_argument('--load_pkl_dataset', help = 'The path of the pickled dataset(s) to use',
                        default = None, type = str)
    parser.add_argument('--patience', help = 'Number of epochs before early stopping kicks in.',
                        default=3, type=int)
    parser.add_argument('--check_val_every_n_epoch', help = 'Number of epochs before running a val step.',
                        default=1, type=int)
    parser.add_argument('--save_all_epochs', help = 'Whether to save all model epoch checkpoints.',
                        default=False, type=bool)
    parser.add_argument('--exp_code_str', help = 'Additional experiment code string to disambiguate experiments.',
                        default='', type=str)
    parser.add_argument('--linear_warmup_ratio', help = 'Overwrite the learning rate specified in settings template.',
                        default=None, type=float)
    parser.add_argument('--bypass_dqegem', help = 'Set triple linearisation to trp_sep_scheme2.',
                        default=False, type=bool)
    parser.add_argument('--ckpt_every_n_steps', help = 'Switch from every_n_epochs to every_n_steps for model checkpointing \
                        Used in ablation exps to ensure direct #steps comparability.',
                        default=None, type=int)
    parser.add_argument('--textansselector', help = 'Whether to use text answer selector model when doing QG inference.',
                        default=False, type=bool)
    parser.add_argument('--textansselector_ckpt', help = 'Whether to use text answer selector model when doing QG inference.',
                        default='/home/khan/synalp_me/q2kb/results/omnibus_model/textansselect_t5base_gen_ce_allq-cq-webnlg_lrwarm0.1_ep10/epoch=9-step=1830-v1_tested.ckpt', 
                        type=str)
    parser.add_argument('--languages', help = 'The language(s) to train mQTT with. ',
                        default=['en'], nargs = '*', type=str)
    parser.add_argument('--multilingual_ERM', help = 'Whether to ensure training data for multilingual setting excludes (True) QA instances where 2x reader answers do not match and aligned graph answer not equivalent to original En graph answer.',
                        default=False, type=bool)
    parser.add_argument('--multilingual_EQQ', help = 'Whether to ensure training data for multilingual setting excludes (True) C-Q-A instances where context and question backtranslation do not meet BSc and GLEU cutoffs, and 2x reader answer confidence do not meet cutoff.', default=False, type=bool)
    parser.add_argument('--qwebnlg_EFU', help = 'Option in allq_cq loaders for QG to ensure textQG training data for for a given (q,a,t) is mapped to itself (and not just to texts encompassing it)', 
                        default=False, type=bool)
    parser.add_argument('--accumulate_grad_batches', help = 'How many batches to accumulate gradient batches for.',
                        default=None, type=int)    
    args = parser.parse_args()
    args.settings = getattr(Settings(), args.settings_code)
    args.devices = torch.cuda.device_count()

    args = parser.parse_args()
    args.settings = getattr(Settings(), args.settings_code)
    args.devices = torch.cuda.device_count()

    ### SETTINGS CONTROL CHECKS ### 
    # 0. warmup
    if args.linear_warmup_ratio is not None: 
        assert type(args.linear_warmup_ratio) == float
        assert 0 <= args.linear_warmup_ratio <= 0.1
        args.settings['linear_warmup_ratio'] = args.linear_warmup_ratio

    # 1. model, batch size and epochs
    if args.model is not None: 
        if 't5' not in args.model or 't5' not in args.settings['model']:
            raise NotImplementedError
        print('args.settings_code', args.settings_code)
        cur_m_name = re.search(r't5[a-z]+', args.settings_code).group()
        new_m_name = os.path.basename(args.model).replace('-', '').replace('_', '').lower()
        args.settings_code = args.settings_code.replace(cur_m_name, new_m_name)
        args.settings['model'] = args.model
        print(f'Model specified for settings_code being overwritten to {new_m_name}. New: ', args.settings_code)

    if args.bsz is not None: args.settings['bsz'] = args.bsz

    if args.max_epochs is not None: args.settings['max_epochs'] = args.max_epochs
    print('Max epochs:', args.settings['max_epochs'])
    
    # 2. generation arguments
    if 'textqa' in args.settings['tasks'] or 'kbqa' in args.settings['tasks']:
        args.settings['gen_args'].pop('min_length')

    # 3. datasets 
    if args.datasets:
        print('Task datasets being overridden with:', args.datasets)
        for task_name, task_settings in args.settings['tasks'].items():
            args.settings['tasks'][task_name]['datasets'] = args.datasets
    
    if type(args.settings['use_saved_data']) == str: 
        args.settings['use_saved_data']
        datasets = [settings['datasets'] for __, settings in args.settings['tasks'].items()]
        datasets = set(l2 for l in datasets for l2 in l)
        for ds in sorted(datasets): args.settings['use_saved_data'] += f'_{ds}'
    # override if load_pkl_dataset spec-ed
    if args.load_pkl_dataset is not None:
        args.settings['use_saved_data'] = args.load_pkl_dataset
        print(f'loading dataset from: {args.settings["use_saved_data"]}')

    # 4. inference only
    if '_test_only' in args.settings_code or '_infer' in args.settings_code:
        args.test_only = True
        print(f'In {args.settings_code}, setting test_only to True...\n')

    # 5. models and checkpoint
    if args.settings.get('load_ckpt', False):
        assert len(args.settings['load_ckpt']) == 2
        args.load_ckpt = args.settings['load_ckpt']

    if not args.test_only and (args.load_ckpt and args.load_ckpt[1]): 
        prev_max_epochs = int(re.match(r'(?:.+_ep)([0-9]+)(?:.+)', args.load_ckpt[1]).group(1))
        prev_num_ep = int(re.match(r'(?:.+epoch=)([0-9]+)(?:.+)', args.load_ckpt[1]).group(1))
        remainder = prev_max_epochs - (prev_num_ep+1)
        if remainder > 0: pass # use the same NOTE: has impact on lr being resumed
        else: args.max_epochs += (prev_num_ep + 1)
        args.settings['max_epochs'] = args.max_epochs
        print('NEW args.max_epochs', args.max_epochs)

    if 't5small' in args.settings['model']: assert args.settings['model'] == 't5-small'
    elif 't5base' in args.settings['model']: assert args.settings['model'] == 't5-base'

    # 6. prompts and linearisation scheme
    print('bypass_dqegem:', args.bypass_dqegem)
    for task_name, tasksettings in args.settings['tasks'].items():
        backward_compat_sq_cq = ['sqg_t5base_g2sq_infer', 'cqg_t5base_t2cq_g2cq_test_only']
        if task_name in ['cqg', 'sqg', 'kbqa', 'kbqg', 'quartet'] \
            and args.settings_code not in backward_compat_sq_cq:
            if args.bypass_dqegem:
                args.settings['tasks'][task_name]['src_format'] = 'trp_sep_scheme2'
                if 'tsep_scheme2' not in args.settings: args.settings_code += '_tsep_scheme2'

            else: 
                # HACK: allow use of tripend setting
                c_SQGEN = '_dqegem' not in args.settings_code and 'sqg_t5base' in args.settings_code
                c_CQGEN = '_dqegem' not in args.settings_code and 'cqg_t5base' in args.settings_code
                if args.settings['tasks'][task_name]['src_format'] in ['dqe_gem', 'dqe_gem_usetripend'] \
                    or c_SQGEN or c_CQGEN: continue
                    # allow SQGEN and CQGEN to be non-dqegem
                
                args.settings['tasks'][task_name]['src_format'] = 'dqe_gem'
                if 'dqegem' not in args.settings: args.settings_code += '_dqegem'                    
    
    # 7. filtering
    if args.datasets_filter == '': args.datasets_filter = {}
    if args.datasets_filter != {}:
        for f_instruct in args.datasets_filter:
            task, dataset, instruction = f_instruct.split(':')
            if args.settings['tasks'][task]['datasets_filter'] != {}: 
                args.settings['tasks'][task]['datasets_filter'].update({dataset: instruction})
            else: 
                args.settings['tasks'][task]['datasets_filter'] = {dataset: instruction}

    # 8. no linear warmup if loading from checkpoint and continuing
    if args.load_ckpt: args.settings["linear_warmup_ratio"] = 0 

    assert set(args.languages).issubset(set(['en', 'ru', 'ptbr'])), args.languages
    args.multilingual, args.non_english = False, False
    if len(args.languages) > 1: args.multilingual = True
    args.languages = sorted(args.languages)
    non_en = [lang for lang in args.languages if lang != 'en']
    mling_str = ''
    if non_en:
        args.non_english = True
        for lang in args.languages: mling_str += f'_{lang}'
        args.settings_code += mling_str
        if args.multilingual_ERM: args.settings_code += '_ERM'
        if args.multilingual_EQQ: args.settings_code += '_EQQ'
    else: args.multilingual_EQQ_settings = {}
    print('args.languages, args.multilingual', args.languages, args.multilingual)

    if args.qwebnlg_EFU: 
        assert any(['webnlg' in ds or 'dqeablate' in ds for ds in args.datasets])
        args.settings_code += '_qwebnlg_EFU'

    if args.accumulate_grad_batches is not None:
        args.settings['accumulate_grad_batches'] = args.accumulate_grad_batches

    args = backward_compat_args(args)    

    main(args)