import os, json, datasets, re, sys
from collections import Counter, defaultdict
'''
1. This script prepares the data for the following:
a. fine-tuning FiD checkpoint with:
    i) data used for training DQE and QTT for text and graph modality
    ii) load the data from the cross-ans (QA consistency) runs for use on FiD inference
    iii) NOTE: for graph input, we use the same linearisation as Oguz et al 2022 (Unik-QA)
    which has been shown to work well for fine-tuning retrieval-based QA with FiD. 

from https://github.com/facebookresearch/UniK-QA/blob/main/UniK-QA%20on%20WebQSP.ipynb

'''
class Relation:
    def __init__(self, line):
        if line is None:
            self.subj = self.rel = self.obj = None
            return
        e1, rel, e2 = line.strip().split(' | ', 2)
        e1 = self.canonicalize(e1)
        e2 = self.canonicalize(e2)
        self.subj = e1
        self.rel = rel
        self.obj = e2
    
    def __hash__(self):
        return hash((self.subj, self.rel, self.obj))
        
    def _filter_relation(self):
        # same criteria as GraftNet
        relation = self.rel
        if relation == "<fb:common.topic.notable_types>": return False
        domain = relation[4:-1].split(".")[0]
        if domain == "type" or domain == "common": return True
        return False

    def should_ignore(self):
        if self._filter_relation():
            return True
        return False
    
    def canonicalize(self, ent):
        if ent.startswith("<fb:m."):
            return "/m/" + ent[6:-1]
        elif ent.startswith("<fb:g."):
            return "/g/" + ent[6:-1]
        else:
            return ent
    
    def __repr__(self):
        return f"Subj: {self.subj}; Rel: {self.rel}; Obj: {self.obj}"

def remove_underscore_camelcase(triple):
    '''
    helper func to convert DBpedia triples to Wikidata-like format
    '''
    newtriple = re.sub(r"([a-z])([A-Z])", "\g<1> \g<2>", triple)
    newtriple = re.sub(r'_', ' ', newtriple)
    newtriple = re.sub(r'\s+', ' ', newtriple)
    # lower case the property 
    newtriple = newtriple.split(' | ')
    newtriple[1] = newtriple[1].lower()

    return ' | '.join(newtriple)

def convert_relation_to_text(relation, entity_names, freebase = False):
    if isinstance(relation, Relation):
        subj, rel, obj = relation.subj, relation.rel, relation.obj
    else:
        subj, rel, obj = relation
    # subject
    if subj in entity_names:
        subj_surface = entity_names[subj]
    else:
        subj_surface = subj
        
    # object
    if obj in entity_names:
        obj_surface = entity_names[obj]
    else:
        obj_surface = obj
            
    if freebase:
        # relation
        # e.g. <fb:film.film.other_crew>
        # remove bracket
        rel_surface = rel[4:-1]
        # replace '.' and '_' with ' '
        rel_surface = rel_surface.replace('.', ' ')
        # only keep the last two words
        rel_surface = ' '.join(rel_surface.split(' ')[-2:])
        rel_surface = rel_surface.replace('_', ' ')
    else: rel_surface = rel
    
    return ' '.join([subj_surface, rel_surface, obj_surface, '.'])

def prepare_data(): 
    webnlg = datasets.load_dataset('GEM/web_nlg', 'en')
    idx2ctx, idx2graph = {}, {}
    for split in ['train', 'validation', 'test']: 
        for line in webnlg[split]: 
            idx2ctx[line['gem_id']] = line['target']
            idx2graph[line['gem_id']] = line['input']
    squad = datasets.load_dataset('squad')
    idx2ctx_squad, idx2title_squad, idx2split_squad = {}, {}, {}
    for split in ['train', 'validation']: 
        for line in squad[split]:
            idx2ctx_squad[line['id']] = line['context']  
            idx2title_squad[line['id']] = line['title'].replace('_', ' ')
            idx2split_squad[line['id']] = split

    dir = 'results/omnibus_model'
    # NOTE: for direct comparability with DQE and QTT Table 4 results. DO NOT USE TAS (text answer selector)
    dirpath  = f'cross_answerability_evaluation_model3ckpt-epoch8-lr01-entlink_ablation_xadapter_qwebnlg_EFUlr01-entlink_ablation_xadapter_qwebnlg_EFU_remove_inconsistent0.7_runnum0{args.runnum}_unanswerable_zero_sampmode2'
    fp = os.path.join(dir, dirpath,'cross_answerability_scores_gens_tgt.json')
    with open(fp, encoding = 'utf-8') as f: holder = json.load(f)
    save_dirpath = 'qtt_data'
    if not os.path.exists(save_dirpath): os.makedirs(save_dirpath)
    
    for inputformat in ['unikqa_text', 'unikqa_linear_graph']:
        for sysname in ['DQE', 'OUR']:
            sysname_str = sysname if sysname == 'DQE' else 'QTT'
            for filt_str in ['', '_filt0.7']:
                print('WORKING on TEST... :', inputformat, sysname, filt_str)
                for qst_mod in ['text', 'graph']:
                    # 1. load the questions generated from a given System and a give Modality
                    # a. load the "OUR_text" form (it's the same set for all)
                    subholder = holder[str(args.runnum)][f'{sysname}_{qst_mod}_on_OUR_text{filt_str}']
                    # use gem_ids to recover graph/text
                    gem_ids   = subholder['gem_ids_all']
                    srcs      = subholder['src_all']
                    tgts      = subholder['tgt_all']
                    assert len(srcs) == len(gem_ids) == len(tgts), (len(srcs), len(gem_ids), len(tgts))
                    
                    savepath = f'tools/FiD/{save_dirpath}/qstsys-{sysname_str}_qstsrcmod-{qst_mod}_TEST_inpform-{inputformat}{filt_str}_runnum{args.runnum}.jsonl'
                    
                    with open(savepath, encoding='utf-8', mode = 'w+') as f: 
                        for src, gem_id, tgt in zip(srcs, gem_ids, tgts):
                            question, answer = re.search(r'(?:.+\[Sp1\]\s+)(.+)(?:\[INP\])', src).group(1), tgt
                            assert question
                            if inputformat == 'unikqa_text': context = idx2ctx[gem_id] 
                            else: context = ' '.join([
                                    convert_relation_to_text(Relation(remove_underscore_camelcase(t)), 
                                                            entity_names = []) for t in idx2graph[gem_id]])
                            spool = Counter(t.split(' | ')[0].replace('_', ' ') for t in idx2graph[gem_id])
                            title = spool.most_common()[0][0]
                            line = {'id': gem_id, 'question': question, 'target': answer, 
                                    'answers': [answer], 'ctxs': [{"title": title, "text": context }]}

                            f.write(json.dumps(line)+'\n')
                
                if args.test_only: continue
                if filt_str != '': continue
                dirpath, dkey = '/home/khan/synalp_me/q2kb/01_data', 'dqe_webnlg'
                holder_dqe, holder_qtt = defaultdict(list), defaultdict(list) 
                # because DQE data separated into graphsyn and textsquad
                if sysname == 'OUR': 
                    neg_key = cq_rjt =  ''
                    fkey, subobj_str = 'webnlg', '_nosubjobj' 
                    fp = f'{dirpath}/{dkey}/z08_allq_cq_{fkey}{neg_key}_MERGED{cq_rjt}{subobj_str}.jsonl'
                    print('\t\tWORKING on:', fp)
                    with open(fp, encoding = 'utf-8') as f:  
                        holder_qtt['unikqa_linear_graph'] = \
                            holder_qtt['unikqa_text'] = \
                                [json.loads(i) for i in f]
                    
                elif sysname == 'DQE':
                    fp = f'{dirpath}/{dkey}/ABLATION_DATA_graphsynqg_validation-train_ANS_ALIGNED.jsonl'
                    print('\t\tWORKING on:', fp)
                    with open(fp, encoding = 'utf-8') as f:  
                        holder_dqe['unikqa_linear_graph'] = [json.loads(i) for i in f]
                    
                    for split in ['train', 'validation']:
                        fp = f'{dirpath}/{dkey}/ABLATION_DATA_textsquad_{split}.jsonl'
                        print('\t\tWORKING on:', fp)
                        with open(fp, encoding = 'utf-8') as f:
                            holder_dqe['unikqa_text'].extend([json.loads(i) for i in f])

                    
                train, validation = [], []
                if sysname == 'DQE': data_allq  = holder_dqe[inputformat]
                if sysname == 'OUR': data_allq  = holder_qtt[inputformat]
                print('WORKING on TRAIN+VALIDATION... :', inputformat, sysname, filt_str)
                print('\t\tHOLDER (data_allq) SIZE:', len(data_allq))
                for dataline in data_allq:
                    gem_id = dataline['id']
                    c_web_nlg = 'web_nlg' in gem_id
                    if c_web_nlg:
                        spool = Counter(t.split(' | ')[0].replace('_', ' ') for t in idx2graph[gem_id])
                        title = spool.most_common()[0][0]
                        graph_context = ' '.join([
                                        convert_relation_to_text(Relation(remove_underscore_camelcase(t)), 
                                                                entity_names = []) for t in idx2graph[gem_id]])
                        text_context = idx2ctx[gem_id] 
                    else: 
                        graph_context = None 
                        title = idx2title_squad[gem_id]
                        text_context = idx2ctx_squad[gem_id] 
                    
                    for sqcq in ['sq', 'cq']:
                        for qaline in dataline[sqcq]:
                            question = qaline['question']
                            
                            if inputformat == 'unikqa_text': 
                                answerset = set(a[0] for a in qaline['answer_readers'])
                                if qaline['controls'][1] == 'text': 
                                    # this is graph entity if question gen from rdf
                                    # this is text span if question gen from text
                                    answerset.add(qaline['answer_org']) 
                                
                                for answer in answerset: 
                                    line = {'id': gem_id, 'question': question, 'target': answer, 
                                        'answers': [answer], 'ctxs': [{"title": title, "text": text_context }]}
                                    if c_web_nlg:
                                        if 'train' in gem_id: train.append(line) 
                                        elif 'validation' in gem_id: validation.append(line) 
                                    else: locals()[idx2split_squad[gem_id]].append(line)
                            
                            elif inputformat == 'unikqa_linear_graph': 
                                if graph_context is None: continue # DQE squad questions 
                                if qaline['controls'][1] == 'text': continue

                                answer = qaline['answer_org'].replace('_', ' ')
                                line = {'id': gem_id, 'question': question, 'target': answer, 
                                    'answers': [answer], 'ctxs': [{"title": title, "text": graph_context }]}
                                if c_web_nlg:
                                    if 'train' in gem_id: train.append(line) 
                                    elif 'validation' in gem_id: validation.append(line)
                                else: locals()[idx2split_squad[gem_id]].append(line)
                            else: raise ValueError
                
                print('\t\t\tSize of train & validation:', len(train), len(validation))
                if not train: print('\t\t\t\tTRAIN EMPTY!') # may be in unikqa_text, 
                if not validation: print('\t\t\t\VALIDATION EMPTY!') # may be in unikqa_text, 
                for split in ['train', 'validation']:
                    data_split = locals()[split]
                    savepath = f'tools/FiD/{save_dirpath}/{sysname_str}_{split}_{inputformat}{filt_str}.jsonl'
                    print('SAVEPATH', savepath)
                    with open(savepath, encoding='utf-8', mode = 'w+') as f: 
                        for line in data_split: f.write(json.dumps(line)+'\n')

if __name__ == '__main__': 
    import argparse
    parser = argparse.ArgumentParser(description='Arugments for preparing the train, dev, test data to be used on the FiD model.')
    parser.add_argument('--runnum', help = 'Which runnum to produce the evaluation/test data',
                        default=0, type=int)
    parser.add_argument('--test_only', help = 'Whether to produce only the test data ',
                        default=False, type=bool)
    args = parser.parse_args()

    prepare_data()
