# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
# 
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import torch, json, os
import transformers
import numpy as np
from pathlib import Path
import torch.distributed as dist
from torch.utils.data import DataLoader, SequentialSampler
### [CHANGE START] 
import datasets
### [CHANGE END] 

import src.slurm
import src.util
from src.options import Options
import src.data
import src.evaluation
import src.model
from train_reader import calculate_f1_squad

def evaluate(model, dataset, dataloader, tokenizer, opt):
    loss, curr_loss = 0.0, 0.0
    model.eval()
    if hasattr(model, "module"):
        model = model.module
    if opt.write_crossattention_scores:
        model.overwrite_forward_crossattention()
        model.reset_score_storage() 
    total = 0
    exactmatch = []
    ### [CHANGE START] add token f1
    tokenf1 = []
    bertscore = []
    BERTSCORE_OBJ = datasets.load_metric('bertscore', keep_in_memory = True)
    ### [CHANGE END]
    if opt.write_results:
        write_path = Path(opt.checkpoint_dir) / opt.name / 'test_results'
        fw = open(write_path / ('%d.txt'%opt.global_rank), 'a')
    with torch.no_grad():
        for i, batch in enumerate(dataloader):
            (idx, _, _, context_ids, context_mask) = batch

            if opt.write_crossattention_scores:
                model.reset_score_storage()

            outputs = model.generate(
                input_ids=context_ids.cuda(),
                attention_mask=context_mask.cuda(),
                max_length=50,
            )

            if opt.write_crossattention_scores:
                crossattention_scores = model.get_crossattention_scores(context_mask.cuda())

            for k, o in enumerate(outputs):
                ans = tokenizer.decode(o, skip_special_tokens=True)
                example = dataset.data[idx[k]]
                if 'answers' in example:
                    score = src.evaluation.ems(ans, example['answers'])
                    exactmatch.append(score)
                    ### [CHANGE START] add token f1, add bertscore
                    # compute token f1 against all answers, take the max
                    tokenf1.append(max([calculate_f1_squad(ans, g) for g in example['answers']]))
                    bertscore.append(max([np.mean(calculate_BERTScore_rescaled([ans], [g], BERTSCORE_OBJ, 
                                unanswerable_zero = True, unanswerable_str = 'unanswerable', 
                                bertscore_model = 'bert-base-multilingual-cased',
                                bertscore_rescale_with_baseline = True)) for g in example['answers']]))
                    ### [CHANGE END]

                if opt.write_results:
                    ### [CHANGE START] also saving reference answers
                    fw.write(str(example['id']) + "\t" + ans + "\t" \
                             + '¶'.join(example['answers']) if 'answers' in example else ''  + '\n')
                    ### [CHANGE END]
                if opt.write_crossattention_scores:
                    for j in range(context_ids.size(1)):
                        example['ctxs'][j]['score'] = crossattention_scores[k, j].item()

                total += 1
            if (i + 1) % opt.eval_print_freq == 0:
                log = f'Process rank:{opt.global_rank}, {i+1} / {len(dataloader)}'
                if len(exactmatch) == 0:
                    log += '| no answer to compute scores'
                else:
                    log += f' | average = {np.mean(exactmatch):.3f}'
                logger.warning(log)

    logger.warning(f'Process rank:{opt.global_rank}, total {total} \
                   | average EM  = {np.mean(exactmatch):.3f}\
                   | average F1 = {np.mean(tokenf1):.3f}')
    if opt.is_distributed:
        torch.distributed.barrier()
    score_em, total = src.util.weighted_average(np.mean(exactmatch), total, opt)
    score_f1, total = src.util.weighted_average(np.mean(tokenf1), total, opt)
    ### [CHANGE START]
    score_bsc, total = src.util.weighted_average(np.mean(bertscore), total, opt)
    ### [CHANGE END]

    return score_em, score_f1, score_bsc, total

### CHANGE START 
def calculate_BERTScore_rescaled(predictions, references, bertscore_obj, 
                                unanswerable_zero, unanswerable_str, 
                                bertscore_model = 'bert-base-multilingual-cased',
                                bertscore_rescale_with_baseline = True):
    '''
    similar to DQE's calculate_BERTScore function, except allowing rescale_with_baseline
    to be set (defaults to True). Also allows unanswerables to be set to BSC = 0.0
    '''
    # bertscore.add_batch(predictions, references)
    bscores = bertscore_obj.compute(predictions = predictions, references = references,
                        model_type = bertscore_model, lang = 'en', 
                        rescale_with_baseline = bertscore_rescale_with_baseline, 
                        idf = False, device = torch.device('cuda') \
                        if torch.cuda.is_available() else torch.device('cpu'))['f1']
    if unanswerable_zero: return [0.0 if p == unanswerable_str else bsc for bsc, p in zip(bscores, predictions)]
    else: return bscores

### CHANGE END

if __name__ == "__main__":
    options = Options()
    options.add_reader_options()
    options.add_eval_options()
    opt = options.parse()
    src.slurm.init_distributed_mode(opt)
    src.slurm.init_signal_handler()
    opt.train_batch_size = opt.per_gpu_batch_size * max(1, opt.world_size)

    dir_path = Path(opt.checkpoint_dir)/opt.name
    directory_exists = dir_path.exists()
    if opt.is_distributed:
        torch.distributed.barrier()
    dir_path.mkdir(parents=True, exist_ok=True)
    if opt.write_results:
        (dir_path / 'test_results').mkdir(parents=True, exist_ok=True)
    logger = src.util.init_logger(opt.is_main, opt.is_distributed, Path(opt.checkpoint_dir) / opt.name / 'run.log')
    if not directory_exists and opt.is_main:
        options.print_options(opt)


    tokenizer = transformers.T5Tokenizer.from_pretrained('t5-base', return_dict=False)

    collator_function = src.data.Collator(opt.text_maxlength, tokenizer)
    eval_examples = src.data.load_data(
        opt.eval_data, 
        global_rank=opt.global_rank, #use the global rank and world size attibutes to split the eval set on multiple gpus
        world_size=opt.world_size
    )
    eval_dataset = src.data.Dataset(
        eval_examples, 
        opt.n_context, 
    )

    eval_sampler = SequentialSampler(eval_dataset) 
    eval_dataloader = DataLoader(
        eval_dataset, 
        sampler=eval_sampler, 
        batch_size=opt.per_gpu_batch_size,
        num_workers=20, 
        collate_fn=collator_function
    )
    
    model_class = src.model.FiDT5
    model = model_class.from_pretrained(opt.model_path)
    model = model.to(opt.device)

    logger.info("Start eval")
    ### [CHANGE START]
    exactmatch, tokenf1, bertscore, total = evaluate(model, eval_dataset, eval_dataloader, tokenizer, opt)
    ### [CHANGE END]

    logger.info(f'EM {100*exactmatch:.2f}, Total number of example {total}')
    ### [CHANGE START]
    logger.info(f'F1 {100*tokenf1:.2f}, Total number of example {total}')
    logger.info(f'BSC {100*bertscore:.2f}, Total number of example {total}')
    ### [CHANGE END]

    if opt.write_results and opt.is_main:
        glob_path = Path(opt.checkpoint_dir) / opt.name / 'test_results'
        write_path = Path(opt.checkpoint_dir) / opt.name / 'final_output.txt'
        src.util.write_output(glob_path, write_path) 
        if not os.path.exists(glob_path): os.makedirs(glob_path)
        with open(glob_path.as_posix()+'/scores.json', 'w+') as f:
            json.dump({'EM': exactmatch, 'F1': tokenf1, 'BSC': bertscore}, f)
            print('RESULTS SAVED TO: ', glob_path.as_posix()+'/scores.json')
    if opt.write_crossattention_scores:
        src.util.save_distributed_dataset(eval_dataset.data, opt)

