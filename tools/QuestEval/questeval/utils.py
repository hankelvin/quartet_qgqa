from typing import Dict, List, Optional, Tuple
import string, re, unidecode, collections
from collections import defaultdict
import torch
import hashlib
from transformers import (
    T5ForConditionalGeneration,
    T5Tokenizer,
)

### CHANGE START
def text2hash(
    string: str, 
    quartet_format: bool = False, 
    quartet_model_num: int = None,
    quartet_fullfinetune: bool = False, 
    quartet_tas_model: bool = False,
    language: str = '',
    unanswerable_zero: bool = False,
    remove_inconsistent: Tuple = (False, None),
    system_name = None,
    ckpt_num = '',
    ) -> str:
    hash_object = hashlib.sha512(string.encode('utf-8'))
    hex_dig = hash_object.hexdigest()
    assert system_name is not None
    hex_dig += f'_{system_name}'
    if quartet_format: 
        assert quartet_model_num is not None
        hex_dig += f'_QUARTET{str(quartet_model_num)}'
    hex_dig += ckpt_num
    if unanswerable_zero: hex_dig += '_UNANSZERO'
    if remove_inconsistent[0]: hex_dig += f'_RMVINCONST{str(remove_inconsistent[1])}'
    if quartet_fullfinetune: hex_dig += '_FULLFINETUNE'
    if quartet_tas_model: hex_dig += '_TASMODEL'
    if language: hex_dig += language
    return hex_dig
### CHANGE END: added differentiator to hex for QUARTET

def split_on_punct(doc):
    """
    From one spacy doc to a List of (sentence_text, (start, end))
    """
    start = 0
    seen_period = False
    start_idx = 0
    for i, token in enumerate(doc):
        if seen_period and not token.is_punct:
            yield doc[start: token.i].text, (start_idx, token.idx)
            start = token.i
            start_idx = token.idx
            seen_period = False
        elif token.text in [".", "!", "?"]:
            seen_period = True
    if start < len(doc):
        yield doc[start: len(doc)].text, (start_idx, len(doc.text))


def sentencize(
    text: str, spacy_pipeline
) -> List:
    preprocessed_context = spacy_pipeline(text)
    return [sentence_tuple[0] for sentence_tuple in split_on_punct(preprocessed_context)]

### CHANGE START
class Args:
    def __init__(self, settings_code, settings):
        self.settings_code = settings_code
        self.settings = settings 
### CHANGE END: dummy args obj so as to launch model

class API_T2T:
    def __init__(
        self,
        pretrained_model_name_or_path: str,
        max_source_length: int,
        model_batch_size: int,
        keep_score_idx: int,  # Note: will work only if beamsize == 1
        device: str = "cuda",
        ### CHANGE START
        use_quartet_model: bool = False,
        quartet_model_num: int = None,
        quartet_fullfinetune: bool = False, 
        load_ckpt: list = [],
        language: str = '',
        mDQE_module_dir_ckpt_path: str = '',
        ### CHANGE END
    ) -> None:
        self.pretrained_model_name_or_path = pretrained_model_name_or_path
        ### CHANGE START
        self.c_textnxfacts = 'textnxfacts' in pretrained_model_name_or_path
        self.c_textansselect = 'textansselect' in pretrained_model_name_or_path
        self.ipstrats = ['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa','data2text', 'text2data']
        self.ipstrat2idx = {v:i for i,v in enumerate(self.ipstrats)}
        self.use_quartet_model = use_quartet_model
        self.quartet_fullfinetune = quartet_fullfinetune
        self.language = language
        self.mDQE_module_dir_ckpt_path = mDQE_module_dir_ckpt_path
        if 'QUARTET' in pretrained_model_name_or_path:
            import sys, os
            PARENTDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            print('PARENTDIR', PARENTDIR)
            sys.path.append('../../utils')
            from utils.lightning_omnibus import OmnibusModel
            from utils.utils_omnibus import Settings
            from utils.modeling_omnibus import load_save_model
            assert use_quartet_model and quartet_model_num is not None
            
            model_key = f'_model{quartet_model_num}{"_ablation_xadapters" if quartet_fullfinetune else ""}' 
            if 'textqa' in pretrained_model_name_or_path: 
                settings_code = f'quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10{model_key}'
                args = Args(settings_code = settings_code, settings = getattr(Settings(), settings_code))
            elif 'kbqa' in pretrained_model_name_or_path: 
                settings_code = f'quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10{model_key}'
                args = Args(settings_code = settings_code, settings = getattr(Settings(), settings_code))
            elif 'textqg' in pretrained_model_name_or_path \
                or 'kbqg' in pretrained_model_name_or_path: 
                settings_code = f'quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10{model_key}'
                args = Args(settings_code = settings_code, settings = getattr(Settings(), settings_code))
            elif self.c_textnxfacts: #'textnxfacts' in pretrained_model_name_or_path: 
                # settings_code = 'quartet_textnxfacts_t5small_infer_dqewebnlgtest_10'
                if self.language == 'en': settings_code = 'textnxfacts_t5small_infer_dqewebnlgtest_10'
                else: settings_code = f'textnxfacts_mt5small_infer_dqewebnlgtest_10_{self.language}'
                args = Args(settings_code = settings_code, settings = getattr(Settings(), settings_code))
            elif self.c_textansselect: #'textansselect' in pretrained_model_name_or_path: 
                # settings_code = 'textansselect_t5base_gen_ce_infer'
                if self.language == 'en': settings_code = 'textansselect_t5base_gen_ce_uncased_infer'
                else: settings_code = 'textansselect_t5base_gen_ce_EPR_uncased_infer'
                args = Args(settings_code = settings_code, settings = getattr(Settings(), settings_code))
            print('CHECK: pretrained_model_name_or_path: \t', pretrained_model_name_or_path)

            # ensure that multilingual model setting is used. 
            if language != 'en': args.multilingual = True
            
            if self.c_textansselect:
                import sys
                sys.path.append('../../utils')
                from utils import TextAnsSelector
                # NOTE: special case, in order to keep easy access to predict_answerset
                # args.load_ckpt = args.settings['load_ckpt'] # ensure ckpt is set
                args.textansselector_ckpt = args.settings['load_ckpt'][1]
                self.model = TextAnsSelector(args)
                self.tokenizer = self.model.tokenizer
            else: 
                if self.c_textnxfacts: 
                    # use the ckpt spec-ed in settings
                    args.load_ckpt = load_ckpt = args.settings['load_ckpt']
                else: args.load_ckpt = load_ckpt
                
                c_load_ckpt = load_ckpt and len(load_ckpt) == 2 and load_ckpt[1] != ''
                # for using load checkpoint, hits load_save_model
                if c_load_ckpt:
                    args.test_only = True 
                    args.ds_config = {'bf16':{'enabled': True}} # HACK
                    print('\n\n HERE')
                    print('pretrained_model_name_or_path', pretrained_model_name_or_path)
                    print('settings_code', settings_code)
                    print('load_ckpt', load_ckpt)
                    if self.language != 'en': args.non_english = True
                    # NOTE: at load_save_model, ckpt exp_configs will be loaded and set up will ensure 
                    # the same (add) as training (i.e. match the naming in the ckpt state_dict) is used
                    __m = load_save_model(load_ckpt[0], load_ckpt[1], args, test_phase = True, load4dqe = True)
                else: 
                    __m = OmnibusModel(args, test_phase = True)

                __m.model.active_head = 'lm_head'
                __m.model.eval()
                self.model = __m.model
                self.tokenizer = __m.tokenizer
                # NOTE: important else HF T5 default max_length truncates generation
                self.gen_args = args.settings['gen_args'] 
        else: 
        ### CHANGE END
            ### CHANGE START : allow mDQE model loading 
            if self.language != 'en':
                
                import sys, json, os, glob
                assert self.mDQE_module_dir_ckpt_path
                sys.path.append(self.mDQE_module_dir_ckpt_path['module_path'])
                from train_dqe_xprime_utils import load_DQE_model
                __ = self.mDQE_module_dir_ckpt_path['ckpt_dir']
                ckpt_dir = os.path.join(__, pretrained_model_name_or_path)
                with open(os.path.join(ckpt_dir, 'exp_configs.json'), 
                          encoding = 'utf-8') as f: args_dict = json.load(f)
                class Argser(): pass
                args = Argser()
                for k,v in args_dict.items(): setattr(args, k, v)
                args.test_only = True

                args.load_ckpt_path = os.path.join(ckpt_dir, 'best_model.pt')
                args.savepath = ''
                m, *__ = load_DQE_model(args, bypass_trainer = True)
                self.tokenizer  = m.tokenizer
                self.model      = m.model
                ### CHANGE END
            else: 
                self.tokenizer = T5Tokenizer.from_pretrained(
                    pretrained_model_name_or_path=pretrained_model_name_or_path
                )

                self.model = T5ForConditionalGeneration.from_pretrained(
                    pretrained_model_name_or_path=pretrained_model_name_or_path
                )

        self.keep_score_idx = keep_score_idx

        if device == "cuda":
            # NOTE: special case, in order to keep easy access to predict_answerset
            if self.c_textansselect: self.model.model.cuda() 
            else: self.model.cuda()
        self.max_source_length = max_source_length
        self.model_batch_size = model_batch_size

    def predict(
        self,
        sources: List[str],
        ### CHANGE START
        ipstrats: List[str] = None, # for prefix mask
        ### CHANGE END
    ):
        # sources should be question <s> context

        gen_texts = []
        keep_score_idx_scores = []

        for i in range(0, len(sources), self.model_batch_size):
            inputs = self.tokenizer(
                sources[i: i+self.model_batch_size],
                max_length=self.max_source_length,
                padding="max_length",
                truncation=True,
                return_tensors="pt",
                verbose=False,
            )
            with torch.no_grad():
                ### CHANGE START
                # 1. keep special tokens for QUARTET tasks (except textnxfacts)
                # 2. also passing gen_args to set higher max length
                if self.use_quartet_model and (not self.c_textnxfacts and not self.c_textansselect):
                    source_ids, source_mask = inputs["input_ids"], inputs["attention_mask"]
                    dict_generated_ids = self.model.generate(
                        input_ids=source_ids.to(self.model.device),
                        attention_mask=source_mask.to(self.model.device),
                        use_cache=True,
                        # decoder_start_token_id=None,
                        num_return_sequences=1,
                        do_sample=False,
                        output_scores=True,
                        return_dict_in_generate=True,
                        # NOTE: important else HF T5 default max_length truncates generation
                        **self.gen_args)

                    gen_text = self.tokenizer.batch_decode(
                        dict_generated_ids['sequences'],
                        skip_special_tokens=False,
                        clean_up_tokenization_spaces=False)
                     
                else: 
                ### CHANGE END
                    source_ids, source_mask = inputs["input_ids"], inputs["attention_mask"]
                    dict_generated_ids = self.model.generate(
                            input_ids=source_ids.to(self.model.device),
                            attention_mask=source_mask.to(self.model.device),
                            use_cache=True,
                            decoder_start_token_id=None,
                            num_beams=1,
                            num_return_sequences=1,
                            do_sample=False,
                            output_scores=True,
                            return_dict_in_generate=True)

                    gen_text = self.tokenizer.batch_decode(
                        dict_generated_ids['sequences'],
                        skip_special_tokens=True,
                        clean_up_tokenization_spaces=True)

                gen_texts += gen_text

                keep_score_idx_score = (1 - dict_generated_ids['scores'][0].softmax(-1)[:, self.keep_score_idx])
                if len(gen_text) != 1:
                    keep_score_idx_score = keep_score_idx_score.squeeze()
                keep_score_idx_scores += keep_score_idx_score.tolist()

        # Note: self.model.additional_scores_idx keep in memory probs only if beam == 1;
        #   it is useful only when T5 is used as a classifier so far.
        return keep_score_idx_scores, gen_texts


def calculate_f1_squad(
    a_gold: str,
    a_pred: str
) -> float:
    def normalize_answer(s):
        """Lower text and remove punctuation, articles and extra whitespace."""

        def remove_articles(text):
            regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
            return re.sub(regex, ' ', text)

        def white_space_fix(text):
            return ' '.join(text.split())

        def remove_punc(text):
            exclude = set(string.punctuation)
            return ''.join(ch for ch in text if ch not in exclude)

        def lower(text):
            return text.lower()

        return white_space_fix(remove_articles(remove_punc(lower(s))))

    def get_tokens(s):
        if not s: return []
        return normalize_answer(s).split()

    gold_toks = get_tokens(a_gold)
    pred_toks = get_tokens(a_pred)
    common = collections.Counter(gold_toks) & collections.Counter(pred_toks)
    num_same = sum(common.values())
    if len(gold_toks) == 0 or len(pred_toks) == 0:
        # If either is no-answer, then F1 is 1 if they agree, 0 otherwise
        return int(gold_toks == pred_toks)
    if num_same == 0:
        return 0
    precision = 1.0 * num_same / len(pred_toks)
    recall = 1.0 * num_same / len(gold_toks)
    f1 = (2 * precision * recall) / (precision + recall)
    return f1


def calculate_BERTScore(
    model_predictions: List[str],
    gold_references: List[str],
    metric_BERTScore,
    device: str,
    ### CHANGE START
    unanswerable_zero: bool = False, 
    bsc_ans_lc: bool = False,
    language: str = 'en', 
    ### CHANGE END
) -> List[float]:

    if len(model_predictions) == 0:
        return []
    ### CHANGE START
    metric_BERTScore.add_batch(predictions=[i.lower() for i in model_predictions] if bsc_ans_lc else model_predictions, 
                                references=[i.lower() for i in gold_references] if bsc_ans_lc else gold_references)
    ### CHANGE END
    final_score = metric_BERTScore.compute(model_type='bert-base-multilingual-cased', device=device,
                                           ### CHANGE START
                                           lang = language,
                                           ### CHANGE END
                                           )

    # """
    if unanswerable_zero:
        # set all unanswerable scores to 0
        for i, (pred) in enumerate(model_predictions):
            if pred == "unanswerable" or pred == "":
                final_score['f1'][i] = 0.0
    # """
    return [f1 for f1 in final_score['f1']]


def extract_table_answers(
    text: str
) -> List[str]:

    asws = []

    asw_toks = []
    is_asw = False
    for tok in text.split():

        if tok == ']':
            asws.append(' '.join(asw_toks))
            is_asw = False
            asw_toks = []

        if is_asw:
            asw_toks.append(tok)

        if tok == '[':
            is_asw = True
    return asws

### CHANGE START
def extract_table_answers_QUARTET(
    spacy_pipeline,
    text_orig: List[str]
) -> List[str]:

    asws = []

    for triple in text_orig:
        # keep the subject and object only
        for elem in triple[::2]:
            elem = ' '.join([t.text for t in \
                        spacy_pipeline(Triple.clean_obj(elem.strip(), lc=False))])
            asws.append(elem)

    return asws

def strip_special_tokens(text, ctrl_sep = '[Sp1]', kqa_task = 'kbqa task', tqa_task = 'textqa task', 
                                eos = '</s>', pad = '<pad>'):
        return text.replace(ctrl_sep, '').replace(kqa_task, '')\
                    .replace(tqa_task, '').replace(eos, '')\
                        .replace(pad, '').strip()
### CHANGE END: add answer selector for graph input, strip special char for QUARTER outputs

class WrongE2EFormat(
    Exception
):
    def __init__(self, obj):
        err = """
            It seems you passed an objected weirdly formatted.
            For E2E, please give a Meaning Representation as a string, 
            formatted as below:
                input = 'name[The Eagle], eatType[coffee shop], food[Japanese]'
            Your object was: {}
        """
        super().__init__(err.format(obj))


def linearize_e2e_input(
    input: str,
    format: str ='gem'
) -> str:
    """
    Linearize an E2E input for QuestEval.
    Input must be a string, in standard E2E format.
    Example:
        'name[The Eagle], eatType[coffee shop], food[Japanese]'
    lowercase=True indicates that you want all tokens to be lowercased.
    """
    if format != 'gem':
        raise ValueError(f'Unsupported format for now: {format}')

    if not isinstance(input, str):
        raise WrongE2EFormat(input)

    items = dict([s.strip()[:-1].split('[') for s in input.split(',')])

    return ' , '.join([
        f'{key} [ {value} ]'
        for key, value in items.items()
    ])


class LinearizeWebnlgInput():

    def __init__(
        self,
        spacy_pipeline,
        lowercase=False,
        format: str ='gem',
    ):
        """
        Linearize a WebNLG input for QuestEval.
        Input must be a list of triples, each being a string with two "|".
        Example:
            [
                "(15788)_1993_SB | discoverer | Donal_O'Ceallaigh",
                "(15788)_1993_SB | epoch | 2006-03-06"
            ]
        lowercase=True indicates that you want all strings to be lowercased.
        """

        self.lowercase = lowercase
        self.format = format
        self.spacy_pipeline = spacy_pipeline

    def __call__(
        self,
        input: List[str]
    )-> str:

        if self.format != 'gem':
            raise ValueError(f'Unsupported format for now: {self.format}')

        if not isinstance(input, list):
            raise WrongWebNlgFormat(input)

        triples = [Triple(triple,
                          spacy_pipeline=self.spacy_pipeline,
                          lower=self.lowercase)
                   for triple in input]

        table = dict()
        for triple in triples:
            table.setdefault(triple.sbj, list())
            table[triple.sbj].append((triple.obj, triple.prp))

        ret = list()
        for entidx, (entname, entlist) in enumerate(table.items(), 1):
            ret.append(f'entity [ {entname} ]')
            for values, key in entlist:
                ret.append(f'{key} [ {values} ]')

        return ' , '.join(ret)


class Triple:
    def __init__(
        self,
        raw_text: str,
        spacy_pipeline,
        lower: bool = False,
    ):
        sbj, prp, obj = self.safe_split(raw_text)
        obj = ' '.join([t.text for t in spacy_pipeline(self.clean_obj(obj.strip(), lc=lower))])
        prp = self.clean_prp(prp.strip())
        sbj = ' '.join([t.text for t in spacy_pipeline(self.clean_obj(sbj.strip(), lc=lower))])
        if prp == 'ethnicgroup':
            obj = obj.split('_in_')[0]
            obj = obj.split('_of_')[0]

        self.sbj = sbj
        self.obj = obj
        self.prp = prp

    @staticmethod
    def safe_split(
        raw_text
    ) -> List[str]:

        if not isinstance(raw_text, str):
            raise TypeError('A triple must be a string with two "|"'
                            f'but you gave: {raw_text}')

        split = raw_text.strip().split('|')
        if not len(split) == 3:
            raise TypeError('A triple must be a string with two "|"'
                            f'but you gave: {raw_text}')

        return split

    def __repr__(self):
        return f'{self.sbj} | {self.prp} | {self.obj}'

    @staticmethod
    def clean_obj(
        s,
        lc: bool = False
    ):
        s = unidecode.unidecode(s)
        if lc: s = s.lower()
        s = re.sub('^"|"$', "", s)  # remove useless quotesigns
        s = re.sub('_', ' ', s)  # turn undescores to spaces
        return s

    @staticmethod
    def clean_prp(
        s: str,
        lc: bool=False
    ) -> str:
        s = unidecode.unidecode(s)
        if lc: s = s.lower()
        s = re.sub('^"|"$', "", s)  # remove useless quotesigns
        s = re.sub('\s+', '_', s)  # turn spaces to underscores
        s = re.sub('\s+\(in metres\)', '_m', s)
        s = re.sub('\s+\(in feet\)', '_f', s)
        s = re.sub('\(.*\)', '', s)
        return s.strip()


class WrongWebNlgFormat(Exception):
    def __init__(self, obj):
        err = """
            It seems you passed an objected weirdly formatted.
            For webnlg, please give a list of triplets, where each
            triplet is a string with two '|'.
            For instance:
                input = [
                    "(15788)_1993_SB | discoverer | Donal_O'Ceallaigh",
                    "(15788)_1993_SB | epoch | 2006-03-06"
                ]
            Your object was: {}
        """
        super().__init__(err.format(obj))

### CHANGE START 
class Linearize_CQ_KELM_Input():

    def __init__(
        self,
        spacy_pipeline,
        lowercase=False,
        format: str ='gem',
        c_dqebaseline: bool = False,
        tripend_char: str = None,
    ):
        """
        Linearize a CQ-KELM input for QuestEval.
        lowercase=True indicates that you want all strings to be lowercased.
        """

        self.lowercase = lowercase
        self.format = format
        self.spacy_pipeline = spacy_pipeline
        ### Modificaton start 
        self.c_dqebaseline = c_dqebaseline
        # defaults to ' , ', which is the separator used in DQE
        # intended use is to specify the tripend separator 
        self.tripend_char = tripend_char
        ### Modificaton end  

    def __call__(
        self,
        triples_name: List[List[str]],
    )-> str:

        if self.format != 'gem':
            raise ValueError(f'Unsupported format for now: {self.format}')

        if not isinstance(triples_name, (list, tuple)):
            raise WrongWebNlgFormat(triples_name)

        triples = [Triple_CQ_KELM_Input(triple,
                          spacy_pipeline=self.spacy_pipeline,
                          lower=self.lowercase, c_dqebaseline = self.c_dqebaseline)
                   for triple in triples_name]

        # [ADDITION] match qual_objs to the right triple
        qual_match, qual_nomatch = defaultdict(list), defaultdict(list)
        for idx_qual_t, triple in enumerate(triples):
            if triple.qual_obj is not None:
                s, o = triple.sbj, triple.obj
                found = False
                for idx_t, triple in enumerate(triples): 
                    if s == triple.sbj and o == triple.obj and triple.qual_obj is None: 
                        found  = True
                        break
                if found: qual_match[idx_t].append(idx_qual_t)
                else: qual_nomatch[(s,o)].append(idx_qual_t)

        table = dict()
        for idx_t, triple in enumerate(triples):
            table.setdefault(triple.sbj, list())
            if triple.qual_obj is not None: continue
            
            obj_line, prp_line = triple.obj, triple.prp
            ### CHANGE START
            # adding qual_objs to their S-O 'parent'
            if idx_t in qual_match:
                for idx_qual_t in qual_match[idx_t]:
                    qual_trip = triples[idx_qual_t]
                    qual_line = f' [ {qual_trip.prp} [ {qual_trip.qual_obj} ] ]'     
                    obj_line += qual_line
            ### CHANGE END
                
            table[triple.sbj].append((obj_line, prp_line))
        
        ### CHANGE START
        # for certain KELM entries, qual-ed statements do not come with their S-O parent
        # adding placeholder '##F_PRP' for these 
        for s_o, idx_qual_ts in qual_nomatch.items():
            obj_line = triple.obj
            prp_line = '##F_PRP'
            qual_line = ''
            for idx_qual_t in idx_qual_ts:
                triple = triples[idx_qual_t]
                qual_line += f' [ {triple.prp} [ {triple.qual_obj} ] ]'
            
            obj_line += qual_line
            table[triple.sbj].append((obj_line, prp_line))
        ### CHANGE END

        ret = list()
        for entidx, (entname, entlist) in enumerate(table.items(), 1):
            ret.append(f'entity [ {entname} ]')
            for values, key in entlist:
                ret.append(f'{key} [ {values} ]')
            
            ### CHANGE START
            # add tend to the last element of the set of facts about current entity
            if self.tripend_char is not None: ret[-1] += self.tripend_char
            ### CHANGE END

        return ' , '.join(ret)


class Triple_CQ_KELM_Input:
    def __init__(
        self,
        triples_name: list,
        spacy_pipeline,
        lower: bool = False,
        c_dqebaseline: bool = True,
    ):
        if len(triples_name) == 4: sbj, obj, prp, qual_obj = triples_name
        elif len(triples_name) == 3: (sbj, prp, obj), qual_obj = triples_name, None
        else: raise NotImplementedError(f"{triples_name} provided...")
        if qual_obj: 
            qual_obj = ' '.join([t.text for t in spacy_pipeline(self.clean_obj(qual_obj.strip(), lc=lower))])
        else: qual_obj = None
        obj = ' '.join([t.text for t in spacy_pipeline(self.clean_obj(obj.strip(), lc=lower))])
        prp = self.clean_prp(prp.strip(), c_dqebaseline = c_dqebaseline)
        sbj = ' '.join([t.text for t in spacy_pipeline(self.clean_obj(sbj.strip(), lc=lower))])
        if prp == 'ethnicgroup':
            obj = obj.split('_in_')[0]
            obj = obj.split('_of_')[0]

        self.sbj = sbj
        self.obj = obj
        self.prp = prp.replace('_', ' ')
        self.qual_obj = qual_obj


    @staticmethod
    def clean_obj(
        s,
        lc: bool = False
    ):
        s = unidecode.unidecode(s)
        if lc: s = s.lower()
        s = re.sub('^"|"$', "", s)  # remove useless quotesigns
        s = re.sub('_', ' ', s)     # turn undescores to spaces
        return s

    @staticmethod
    def clean_prp(
        s: str,
        lc: bool=False,
        c_dqebaseline: bool=False,
    ) -> str:
        s = unidecode.unidecode(s)
        if lc: s = s.lower()
        s = re.sub('^"|"$', "", s)  # remove useless quotesigns
        ### [MODIFICATION] START: 
        if c_dqebaseline:
            s = re.sub('\s+', '_', s)  # turn spaces to underscores
        else:
            # turn spaces to single whitespace
            s = re.sub('\s+', ' ', s) 
            # remove camelCase (lower caps, introduce whitespace)
            # align with how we represent triples in Wikidata (closer to NL)  
            s = re.sub(r"([a-z])([A-Z])", "\g<1> \g<2>", s).lower()
        ### [MODIFICATION] END: 
        s = re.sub('\s+\(in metres\)', '_m', s)
        s = re.sub('\s+\(in feet\)', '_f', s)
        s = re.sub('\(.*\)', '', s)
        return s.strip()

def check_connected(subgraph_name):
    '''
    helper func to check if subgraph is connected. returns False if not 
    used for cases where we loop through combinations of subgraph to obtain subsets 
    and applies to case where subgraph triples do not necessarily 
    all share the same SBJ (e.g. case of WebNLG)
    '''
    for idx, t in enumerate(subgraph_name):
        if len(t) > 3: raise NotImplementedError
        other_ts = subgraph_name[:idx] + subgraph_name[idx+1:]
        other_ents = [t3 for t2 in [t[::2] for t in other_ts] for t3 in t2]
        # check that curr triple's set of ents and other ents is not empty set
        if len(set(t[::2]).intersection(other_ents)) == 0: return False
    return True

### CHANGE END