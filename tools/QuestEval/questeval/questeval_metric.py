from cmath import log
from typing import List, Tuple, Dict, Callable, Set
import os, json, itertools, logging, copy, tqdm, re, glob, string
import numpy as np
from datasets import load_metric
import spacy, torch
from questeval import DIR, __version__
from questeval.utils import (
    API_T2T,
    sentencize,
    calculate_f1_squad,
    calculate_BERTScore,
    extract_table_answers,
    extract_table_answers_QUARTET,
    strip_special_tokens,
    text2hash,
    check_connected, 
    Linearize_CQ_KELM_Input,
)


HF_ORGANIZATION = "ThomasNLG"

class QuestEval:
    def __init__(
        self,
        task: str = "text2text",
        language: str = "en",
        answer_types: Tuple = ('NER', 'NOUN'),
        list_scores: Tuple = ('answerability', 'bertscore', 'f1'),
        src_preproc_pipe = None,
        do_weighter: bool = False,
        do_consistency: bool = False,
        qg_batch_size: int = 36,
        clf_batch_size: int = 48,
        limit_sent: int = 5,
        reduction_multi_refs: Callable = max,
        no_cuda: bool = False,
        use_cache: bool = False,
        ### CHANGE START
        use_quartet_model: bool = False,
        quartet_model_num: int = None,              # 1,2 or 3. 3 is the ensemble method
        quartet_fullfinetune: bool = False, 
        quartet_tas_model: bool = False,
        tend_sep: str = None,
        ctrl_sep: str = None,
        qst_sep: str = None,
        ans_sep: str = None,
        inp_sep: str = None,
        log_dir: str = None,
        unanswerable_zero: bool = False,            # setting unanswerable to zero instead of BERTScore
        remove_inconsistent: Tuple = (False, None), # removing inconsistent qs that can't be answered by their source input
        unanswerable_str: str = 'unanswerable',     # the generated string for unanswerable 
        print_removals: bool = False,
        system_name: str = None,                    # when MRs are paired with multiple system texts, logs get mixed up
                                                    # when cache is used, the computed scores will be affected.
        bsc_ans_lc: bool = False,
        load_ckpt: list = [],
        mDQE_module_dir_ckpt_path: dict = \
            {'module_path': '../multilingual_dqe/02_baseline',
             'ckpt_dir':   '../multilingual_dqe/02_baseline'}, 
        ### CHANGE END

    ) -> None:
        """
        Main class for the QuestEval metric

        Args:
            task (:str):
                the task to evaluate with QuestEval

        Return:
            :obj:`torch.optim.lr_scheduler.LambdaLR` with the appropriate schedule.
        """
        """
        format for the json logs:
            hash(txt) #json file name
                {
                'type': type
                'text': text or pointer if image
                'self': {answer_type_1: {'answers': [answers],
                                         {'model_QG_1': {'questions': [questions],
                                                         'model_Weighter_1': [weights]
                                                        }
                                         }
                        }
                'asked': {question_1: {model_QA_1:  'answer': answer,
                                                    'answerability': score
                                                    'ground_truth': {answer_1: {'f1': score,
                                                                              'bertscore: score}
                                    }
                        }
                }
        """
        ### CHANGE START
        self.AVAILABLE_LANGUAGES = ("en","ru","ptbr")  # todo: "multi"
        ### CHANGE END
        self.AVAILABLE_TASKS = ("text2text", "summarization", "text_simplification", "data2text")

        if task not in self.AVAILABLE_TASKS:
            logging.warning(f"Task {task} is not known. Setting the default text2text task. ")
            task = "text2text"

        if language not in self.AVAILABLE_LANGUAGES:
            raise (
                f"Language {language} is not implemented. The list of available languages are: {self.AVAILABLE_LANGUAGES}."
            )

        if task == 'summarization' and do_weighter is False:
            logging.warning(
                "Task is summarization but the weighter is deactivate. Set do_weighter=True to activate it when loading QuestEval."
            )

        if log_dir is None: self.log_dir = os.path.join(DIR, 'logs')
        else: 
            self.log_dir = os.path.join(log_dir, 'logs')
            if not os.path.exists(self.log_dir): os.makedirs(self.log_dir)
        self.hash_files = set(os.listdir(self.log_dir))
        self.use_cache = use_cache

        self.task = task
        ### CHANGE START
        assert language in ['en', 'ru', 'ptbr'], 'Only English, Portuguese and Russian currently supported'
        self.trans_table = ''.maketrans('', '', string.punctuation+' ')
        ### CHANGE END
        self.language = language

        self.answer_types = answer_types
        self.src_preproc_pipe = src_preproc_pipe
        self.limit_sent = limit_sent
        self.sep = "</s>"
        self.qg_prefix = None
        self.qg_batch_size = qg_batch_size
        self.clf_batch_size = clf_batch_size
        self.device = 'cuda' if (torch.cuda.is_available() and not no_cuda) else 'cpu'

        self.reduction_multi_refs = reduction_multi_refs
        self.do_consistency = do_consistency
        self.do_weighter = do_weighter
        self.list_scores = list_scores
        if 'bertscore' in self.list_scores:
            self.metric_BERTScore = load_metric("bertscore")

        if language in ['en', 'ptbr', 'ru']:
            ### CHANGE START 
            model_map = {'en': 'en_core_web_sm', 'ptbr': 'pt_core_news_sm', 'ru':'ru_core_news_sm'}
            try:
                self.spacy_pipeline = spacy.load(model_map[language])
            except OSError:
                logging.warning("Downloading language model for the spaCy model.")
                from spacy.cli import download
                download(model_map[language])
                self.spacy_pipeline = spacy.load(model_map[language])
            ### CHANGE END

        if self.src_preproc_pipe is None:
            if task == 'data2text':
                """
                structured tables should be linearized for our QA/QG format this way:
                'name [ The Eagle ] , eatType [ coffee shop ] , food [ French ] , priceRange [ £ 2 0 - 2 5 ] , customer rating [ 3 out of 5 ] , area [ city centre ] , familyFriendly [ yes ] , near [ Burger King ]'
                we handle by default the preprocessing of the table for webnlg given the GEM format (https://gem-benchmark.com/)
                if your tables are in an other format, please pass a custom function for src_preproc_pipe
                """
                from questeval.utils import LinearizeWebnlgInput
                self.src_preproc_pipe = LinearizeWebnlgInput(spacy_pipeline=self.spacy_pipeline)

        logging.info("Loading the models, it can take time to download at first time.")

        ### CHANGE START
        self.use_quartet_model = use_quartet_model
        self.quartet_model_num = quartet_model_num
        self.quartet_fullfinetune = quartet_fullfinetune
        self.quartet_tas_model = quartet_tas_model
        if self.use_quartet_model:
            assert self.quartet_model_num in [1,2,3,4,5]
            for sep in [tend_sep, ctrl_sep, qst_sep, ans_sep, inp_sep]: assert sep is not None
            spacy_pipeline = spacy.load('en_core_web_sm')
            self.src_preproc_pipe = Linearize_CQ_KELM_Input(spacy_pipeline = spacy_pipeline,
                                        tripend_char = tend_sep)
        self.tend_sep, self.ctrl_sep, self.qst_sep = tend_sep, ctrl_sep, qst_sep
        self.ans_sep, self.inp_sep = ans_sep, inp_sep
        self.unanswerable_zero = unanswerable_zero
        self.remove_inconsistent, self.inconsistent_cutoff = remove_inconsistent
        if self.remove_inconsistent: 
            assert self.inconsistent_cutoff is not None and 0.0 <= self.inconsistent_cutoff <= 1.0
        print('\t\tself.remove_inconsistent, self.inconsistent_cutoff', 
                self.remove_inconsistent, self.inconsistent_cutoff)
        self.unanswerable_str = unanswerable_str
        self.print_removals = print_removals
        self.global_ans_scores_holder = \
            {type_log: {k: [] for k in self.list_scores} for type_log in ['src', 'hyp', 'ref']}
        assert system_name is not None
        self.system_name = system_name
        self.bsc_ans_lc = bsc_ans_lc
        self.load_ckpt = load_ckpt
        self.ckpt_num = ""
        if self.load_ckpt and len(self.load_ckpt) == 2: 
            assert len(self.load_ckpt[1]) > 0
            if 'final.xkpt' in self.load_ckpt[1]: self.ckpt_num = 'xkpt-epochfinal'
            else: self.ckpt_num = 'ckpt-epoch' + re.search(r'(?:epoch=)([0-9]+)', self.load_ckpt[1]).group(1)
        self.mDQE_module_dir_ckpt_path = mDQE_module_dir_ckpt_path
        ### CHANGE END
        self.models = self._load_all_models()

    def _load_all_models(self) -> Dict:
        
        # Textual hypothesis
        models = {"hyp": {}}
        if self.language == 'en':
            ### CHANGE START
            if self.use_quartet_model:
                models['hyp']['QA']  = f'QUARTET/t5-textqa_allq-cq-webnlg-squad2-en'
                models['hyp']['QG']  = f'QUARTET/t5-textqg_allq-cq-webnlg-en'
                models['hyp']['NxF'] = f'QUARTET/t5-textnxfacts_cq-kelm_gem-webnlg'
                models['hyp']['TAS'] = None
                if self.quartet_tas_model: 
                    models['hyp']['TAS'] = f'QUARTET/t5-textansselect_cq-kelm_gem-webnlg'
            else: 
            ### CHANGE END
                models['hyp']['QA'] = f'{HF_ORGANIZATION}/t5-qa_squad2neg-en'
                models['hyp']['QG'] = f'{HF_ORGANIZATION}/t5-qg_squad1-en'
        else:
            ### CHANGE START
            ckpt_dir, lang = self.mDQE_module_dir_ckpt_path['ckpt_dir'], self.language
            if self.use_quartet_model: 
                models['hyp']['QA']  = f'QUARTET/mt5-textqa_allq-cq-webnlg-squad2-MULTILINGUAL-{lang}'
                models['hyp']['QG']  = f'QUARTET/mt5-textqg_allq-cq-webnlg-MULTILINGUAL-{lang}'
                models['hyp']['NxF'] = f'QUARTET/mt5-textnxfacts_cq-kelm_gem-webnlg-MULTILINGUAL-{lang}'
                models['hyp']['TAS'] = None
                if self.quartet_tas_model: 
                    models['hyp']['TAS'] = f'QUARTET/mt5-textansselect_cq-kelm_gem-webnlg-MULTILINGUAL-{lang}'
            else: 
                models['hyp']['QA'] = f'{ckpt_dir}/train_qa/train_qa_{lang}_mt5-small'
                models['hyp']['QG'] = f'{ckpt_dir}/train_qg/train_qg_{lang}_mt5-small'
                ### CHANGE END
                # raise("Multilingual evaluation not handled yet.")

        # (if) multimodal sources
        if self.task == "data2text":
            models['src'] = dict()
            ### CHANGE START
            if self.use_quartet_model:
                if self.language == 'en': 
                    models['src']['QA'] = f'QUARTET/t5-kbqa_allq-cq-webnlg_synth-en'
                    models['src']['QG'] = f'QUARTET/t5-kbqg_allq-cq-webnlg_synth-en'
                else: 
                    lang = self.language
                    models['src']['QA'] = f'QUARTET/mt5-kbqa_allq-cq-webnlg_synth-MULTILINGUAL-{lang}'
                    models['src']['QG'] = f'QUARTET/mt5-kbqg_allq-cq-webnlg_synth-MULTILINGUAL-{lang}'
            else: 
                ### CHANGE END
                if self.language == 'en': 
                    models['src']['QA'] = f'{HF_ORGANIZATION}/t5-qa_webnlg_synth-en'
                    models['src']['QG'] = f'{HF_ORGANIZATION}/t5-qg_webnlg_synth-en'
                ### CHANGE START
                else:
                    ckpt_dir, lang = self.mDQE_module_dir_ckpt_path['ckpt_dir'], self.language
                    models['src']['QA'] = f'{ckpt_dir}/train_qa/train_qa_{lang}_mt5-small_dqe_syn_models'
                    models['src']['QG'] = f'{ckpt_dir}/train_qg/train_qg_{lang}_mt5-small_dqe_syn_models'
                ### CHANGE END

        print('\t\tCHECK MODELS: \t', models)
        # Loading all the different models
        for modality in models.keys():
            for task in models[modality].keys():
                if not type(models[modality][task]) == str:
                    continue
                models[modality][task]= self.get_model(model_name=models[modality][task])

        # Loading the weighter
        models['Weighter'] = None
        if self.do_weighter:
            models['Weighter'] = self.get_model(model_name=f'{HF_ORGANIZATION}/t5-weighter_cnndm-en')

        # Linking already loaded models for the other keys
        for k in ["src", "ref"]:
            if models.get(k) == None:
                models[k] = dict()
                models[k]['QA'] = models['hyp']['QA']
                models[k]['QG'] = models['hyp']['QG']

        return models

    def corpus_questeval(
        self,
        hypothesis: List[str],
        sources: List[str] = None,
        list_references: List[List[str]] = None,
        batch_size: int = 512
    ) -> Dict:

        assert hypothesis is not None
        ### CHANGE START 
        if self.use_quartet_model: 
            having_sources = (
            sources is not None
            and all([isinstance(s, (list, tuple)) for s in sources])  # Only list or tuple allowed for QUARTET linearization
        )
        
        else: 
            ### CHANGE END 
            having_sources = (
            sources is not None
            ### CHANGE START 
            and all([isinstance(s2, str) for s in sources for s2 in s])  # Only str allowed
            and all([isinstance(s, list) for s in sources])
            ### CHANGE END: check for list of lists of strings
            )
        print('\t\tHAVING SOURCES: ', having_sources)
        having_references = (
            list_references is not None
            and all([isinstance(r, str) for rs in list_references for r in rs])  # Only str allowed
            and len(set([len(rs) for rs in list_references])) == 1  # Same number of refs per ex
        )
        print('\t\tHAVING REFERENCES: ', having_references)
        assert having_sources or having_references, "You need to provide at least correct sources or correct references."
        if having_references:
            assert len(list_references) == len(hypothesis)
        if having_sources:
            assert len(sources) == len(hypothesis)

        scores = []
        for ex_idx in tqdm.tqdm(range(0, len(hypothesis), batch_size)):
            logging.info(f"Total examples: {len(hypothesis)}. Proceeding the examples {ex_idx}")
            batch_sources, batch_list_references = None, None
            if having_sources:
                batch_sources = sources[ex_idx:ex_idx + batch_size]
            if having_references:
                batch_list_references = list_references[ex_idx:ex_idx + batch_size]
            scores += self._batch_questeval(
                hypothesis=hypothesis[ex_idx:ex_idx + batch_size],
                sources=batch_sources,
                list_references=batch_list_references,
            )
            torch.cuda.empty_cache()
        result = {'corpus_score': np.average(scores), 'ex_level_scores': scores}
        return result

    def _batch_questeval(
        self,
        hypothesis: List[str],
        sources: List[str] = None,
        list_references: List[List[str]] = None,
    ) -> List[float]:

        list_compared_logs = []
        d_loaded_logs = dict()

        # Hypothesis
        print('\t\tWorking on HYPOTHESES... text2logs')
        hyp_logs, hyp_hashes, modified_logs = self._texts2logs(hypothesis, type_logs='hyp', d_loaded_logs=d_loaded_logs)
        if modified_logs:
            self._serialize_logs(hyp_logs, hyp_hashes)

        # Source
        if sources is not None:
            print('\t\tWorking on SOURCES... text2logs')
            src_logs, src_hashes, modified_logs = self._texts2logs(sources, type_logs='src', d_loaded_logs=d_loaded_logs)
            ### CHANGE START
            if self.remove_inconsistent and modified_logs:
                self._serialize_logs(src_logs, src_hashes)
            ### CHANGE END: ensure update question set is updated to saved logs
            # Asking the questions on the compared text
            print('\t\tWorking on SOURCES... QA src-on-hyp')
            modified_logs = max(self._compute_question_answering(src_logs, hyp_logs, 'src', 'hyp'), modified_logs)
            print('\t\tWorking on SOURCES... QA hyp-on-src')
            modified_logs = max(self._compute_question_answering(hyp_logs, src_logs, 'hyp', 'src'), modified_logs)
            # Compute the similarity scores
            print('\t\tWorking on SOURCES... answer similarity')
            modified_logs = max(self._compute_answer_similarity_scores(src_logs, type_logs='src'), modified_logs)
            # Serialise logs
            if modified_logs:
                self._serialize_logs(src_logs, src_hashes)
                self._serialize_logs(hyp_logs, hyp_hashes)
            list_compared_logs.append(src_logs)

        # Reference
        if list_references is not None:
            len_refs = [len(refs) for refs in list_references]
            assert min(len_refs) == max(len_refs), \
                "The number of references used to compute the score among the example should  be consistant."
            for i_ref in range(len_refs[0]):
                references = [refs[i_ref] for refs in list_references]
                print('\t\tWorking on REFERENCES... text2logs')
                ref_logs, ref_hashes, modified_logs = self._texts2logs(references, type_logs='ref', d_loaded_logs=d_loaded_logs)
                ### CHANGE START
                if self.remove_inconsistent and modified_logs:
                    self._serialize_logs(ref_logs, ref_hashes)
                ### CHANGE END: ensure update question set is updated to saved logs
                # Asking the questions on the compared text
                print('\t\tWorking on REFERENCES... QA src-on-hyp')
                modified_logs = max(self._compute_question_answering(ref_logs, hyp_logs, 'ref', 'hyp'), modified_logs)
                print('\t\tWorking on REFERENCES... QA hyp-on-src')
                modified_logs = max(self._compute_question_answering(hyp_logs, ref_logs, 'hyp', 'ref'), modified_logs)
                # Compute the similarity scores
                print('\t\tWorking on REFERENCES... answer similarity')
                modified_logs = max(self._compute_answer_similarity_scores(ref_logs, type_logs='ref'), modified_logs)
                # Serialise logs
                if modified_logs:
                    self._serialize_logs(ref_logs, ref_hashes)
                    self._serialize_logs(hyp_logs, hyp_hashes)
                list_compared_logs.append(ref_logs)

        # Compute the similarity scores for hyp
        print('\t\tWorking on HYPOTHESES answer similiarity... ')
        modified_logs = self._compute_answer_similarity_scores(hyp_logs, type_logs='hyp')
        # Serialise hyp logs
        if modified_logs:
            self._serialize_logs(hyp_logs, hyp_hashes)

        list_compared_logs = [
            [
                list_compared_logs[i][j]
                for i in range(len(list_compared_logs))
            ]
            for j in range(len(list_compared_logs[0]))
        ]

        # Calculate Score
        scores = []
        for hyps_log, compared_logs in zip(hyp_logs, list_compared_logs):
            scores.append(self._calculate_score_from_logs(hyps_log, compared_logs))

        return scores

    def _texts2logs(
        self,
        texts: List[str],
        type_logs: str,
        d_loaded_logs: Dict
    ):
        modified_logs = False

        # Preprocessing
        ### CHANGE START 
        texts_orig = None
        if type_logs == 'src' and self.src_preproc_pipe is not None:
            texts_orig = texts 
            ### CHANGE END 
            texts = [self.src_preproc_pipe(source) for source in texts]
            
        ### CHANGE START 
        print('\t\tLoading logs')
        logs, logs_hashes = self._load_logs(texts, type_logs, d_loaded_logs, texts_orig, 
                            quartet_format=self.use_quartet_model)
                 
        ### CHANGE END
        # Selecting the answers
        modified_logs = max(self._compute_answer_selection(logs, type_logs), modified_logs) 
        print('\t\tComputed answer selection, logs mod: ', modified_logs)
        #  Generating the questions
        modified_logs = max(self._compute_question_generation(logs, type_logs), modified_logs)
        print('\t\tComputed question generation, logs mod: ', modified_logs)
        ### CHANGE START: quartet usage modifies logs during QG step, reserialise here
        if self.use_quartet_model and modified_logs:
            # NOTE: we added to each log, no new logs added ... we can reuse logs_hashes
            self._serialize_logs(logs, logs_hashes) 
        ### CHANGE END
        # Asking the questions on itself (Round trip consistency)
        ### CHANGE START
        if self.do_consistency or self.remove_inconsistent:
            modified_logs = max(self._compute_question_answering(logs, logs, type_logs, type_logs), modified_logs)
            if self.remove_inconsistent: 
                modified_logs = max(self._compute_answer_similarity_scores(logs, type_logs), modified_logs)
                modified_logs = max(self._remove_inconsistent_qa_pairs(logs, type_logs), modified_logs) 
                self._serialize_logs(logs, logs_hashes) 
            ### CHANGE END: added max
        # Weighter
        if type_logs == 'src' and self.do_weighter:
            modified_logs = max(self._compute_weighter(logs, type_logs='src'), modified_logs)

        return logs, logs_hashes, modified_logs

    def _load_logs(
        self,
        texts: List,
        type_logs: str,
        d_loaded_logs: Dict,
        ### CHANGE START
        texts_orig: List = None,
        quartet_format: bool = False, 
        ### CHANGE END
    ) -> Tuple[List[Dict], List[str]]:
        logs, log_hashs = [], []

        ### CHANGE START
        if texts_orig is None: iterable = [(t, None) for t in texts] 
        else: 
            assert len(texts) == len(texts_orig)
            iterable = zip(texts, texts_orig)
        for text, text_orig in iterable:
            log_hash = text2hash(text, 
            ### CHANGE START
                                quartet_format = quartet_format, 
                                quartet_model_num = self.quartet_model_num,
                                quartet_fullfinetune = self.quartet_fullfinetune,
                                quartet_tas_model = self.quartet_tas_model,
                                language = self.language, 
                                unanswerable_zero = self.unanswerable_zero,
                                remove_inconsistent = (self.remove_inconsistent, self.inconsistent_cutoff),
                                system_name = self.system_name,
                                ckpt_num = self.ckpt_num)
            ### CHANGE END
            if log_hash not in d_loaded_logs:
                ctrls = None
                
                if quartet_format:
                    ctrls = []
                    if type_logs == 'src': 
                        num_facts = len(text_orig)
                        # 1. add CQ controls (n_facts between 2 and 4)
                        for n_facts in range(min(2, num_facts), min(num_facts, 4)+1):
                            if n_facts <= 1: continue
                            combis = itertools.combinations(range(len(text_orig)), n_facts)
                            for c in combis:
                                cut = tuple(t for i, t in enumerate(text_orig) if i in c)
                                if not check_connected(cut): continue
                                ctrls.append({'src_type': ('cqg', 'rdf'), 'n_facts': n_facts, 
                                'text_cut': self.src_preproc_pipe(cut)}) 
                        # 2. add SQ controls... input to SQ is the original full graph
                        ctrls.append({'src_type': ('sqg', 'rdf'), 'n_facts': '1', 
                                'text_cut': self.src_preproc_pipe(text_orig)})
                   
                    elif type_logs == 'hyp':
                        nxfact_model = self.models['hyp']['NxF']
                        # 1. utilise the num_facts prediction model
                        # predict return 2-tuple (confidence score across all vocab, and generated text)
                        # NOTE: here it is one text at each step, so index 1-0
                        # NOTE: being conservative -1 
                        try: num_facts = max(1, int(nxfact_model.predict([text])[1][0]) - 1) 
                        except: 
                            print(nxfact_model.predict([text]))
                            print('FAILURE NUMFACTS', text)
                            raise ValueError
                        # add CQ controls
                        if num_facts > 4: num_facts = 4
                        __ = [{'src_type': ('cqg', 'text'), 'n_facts': str(n_facts), 'text_cut': None} \
                                for n_facts in range(min(2, num_facts), min(num_facts, 4)+1) if n_facts > 1]
                        ctrls.extend(__)
                        # add SQ controls
                        ctrls.append({'src_type': ('sqg', 'text'), 'n_facts': '1', 'text_cut': None})
                        
                log = {'type': type_logs, 'text': text, 'text_orig': text_orig, 
                        'ctrls': ctrls, 'self': dict(), 'asked': dict()}
                ### CHANGE END
                if not (self.use_cache and log_hash in self.hash_files and text != ""):
                    temp=1
                if self.use_cache and log_hash in self.hash_files and text != "":
                    cached_path = os.path.join(self.log_dir, log_hash)
                    try:
                        with open(cached_path, 'r') as f_log:
                            tmp  = json.load(f_log)
                            ### CHANGE START
                            assert all([k in log for k in ['type', 'text', 'text_orig', 
                                            'ctrls', 'self', 'asked']])
                            assert isinstance(log['type'], str)
                            assert isinstance(log['text'], str)
                            assert isinstance(log['ctrls'], list) or log['ctrls'] is None
                            assert isinstance(log['text_orig'], str) or log['text_orig'] is None
                            ### CHANGE END
                            assert isinstance(log['self'], dict)
                            assert isinstance(log['asked'], dict)
                            log = tmp
                    except json.decoder.JSONDecodeError:
                        self.hash_files.remove(log_hash)
                        os.remove(cached_path)
                    except AssertionError:
                        self.hash_files.remove(log_hash)
                        os.remove(cached_path)
                d_loaded_logs[log_hash] = log

            logs.append(d_loaded_logs[log_hash])
            log_hashs.append(log_hash)

        return logs, log_hashs

    def _serialize_logs(
        self,
        logs: List[Dict],
        hashes: List[str]
    ) -> None:
        for log, hash in zip(logs, hashes):
            with open(os.path.join(self.log_dir, hash), 'w') as outfile:
                json.dump(log, outfile, indent=2)

    def open_log_from_text(
        self, 
        text: str,
        ### CHANGE START
        quartet_format: bool = False,
        system_name: str = None,
        ### CHANGE END
        ) -> Dict:
        """
        Function to open a serialised log and analyse it.
        """
        log_hash = text2hash(text, 
        ### CHANGE START
                            quartet_format = quartet_format, 
                            quartet_model_num = self.quartet_model_num,
                            quartet_fullfinetune = self.quartet_fullfinetune,
                            quartet_tas_model = self.quartet_tas_model,
                            language = self.language, 
                            unanswerable_zero = self.unanswerable_zero, 
                            remove_inconsistent = (self.remove_inconsistent, self.inconsistent_cutoff),
                            system_name = self.system_name if system_name is None else system_name,
                            ckpt_num = self.ckpt_num)
        
        ### CHANGE END
        with open(os.path.join(self.log_dir, log_hash), 'r') as f_log:
            log = json.load(f_log)
        return log

    def _compute_answer_selection(
        self,
        logs: List[Dict],
        type_logs: str
    ) -> None:
        for answer_type in self._get_answer_types(type_logs):
            ### CHANGE START
            to_do_exs, to_do_exs_text_orig, to_do_exs_idxs = [], [], []
            for idx, log in enumerate(logs):
                if answer_type not in log['self'] and log['text'] != '':
                    log['self'][answer_type] = dict()
                    to_do_exs.append(log['text'])
                    to_do_exs_text_orig.append(log['text_orig'])
                    ### CHANGE END: added to_do_exs_text_orig
                    to_do_exs_idxs.append(idx)

            if len(to_do_exs) != 0:
                ### CHANGE START
                if self.quartet_tas_model and answer_type == 'NOUN': list_answers = []  # else duplicated
                else: list_answers = self._predict_self_answers(to_do_exs, to_do_exs_text_orig, answer_type)
                ### CHANGE END: added to_do_exs_text_orig
                for i in range(len(list_answers)):
                    logs[to_do_exs_idxs[i]]['self'][answer_type]['answers'] = list_answers[i]

        return len(to_do_exs) != 0

    def _compute_question_generation(
        self,
        logs: List[Dict],
        type_logs: str
    ) -> None:
        name_model_qg = self._get_qg_hash(type_logs)

        to_do_exs, to_do_exs_idxs, to_do_exs_types = [], [], []
        for idx, log in enumerate(logs):
            if log['text'] == '':
                continue
            for answer_type in self._get_answer_types(type_logs):
                if name_model_qg not in log['self'][answer_type]:
                    log['self'][answer_type][name_model_qg] = {'questions': []}

                    ### CHANGE START
                    if self.use_quartet_model:
                        
                        # 1. start with as many entries as answers x ctrls
                        prods = list(itertools.product(log['self'][answer_type]['answers'], log['ctrls']))
                        
                        # 2. wipe answers, we will rebuilt below
                        # copy_ans = copy.deepcopy(logs[idx]['self'][answer_type]['answers'])
                        logs[idx]['self'][answer_type]['answers'] = []

                        if type_logs == 'src':
                            # 3. for KBQA... filter out entries where answer isn't in cut
                            # NOTE: here 'text_cut' is actually graph_cut
                            # p is (ans, ctrl)... i.e. p[0] is answer
                            prods = [p for p in prods if p[0] in str(p[1]['text_cut'])] 
                        
                        # NOTE: using i to control answer insertion in next step 
                        to_do_exs += [(p[0], log['text'], p[1], i) for i, p in enumerate(prods)]
                        to_do_exs_idxs += [idx] * len(prods)
                        to_do_exs_types += [answer_type] * len(prods)
                        # replace answer list with the now-extended answer list
                        logs[idx]['self'][answer_type]['answers'] += [p[0] for p in prods]
                        
                    else:
                        ### CHANGE END
                        to_do_exs += [(a, log['text']) for a in log['self'][answer_type]['answers']]
                        to_do_exs_idxs += [idx] * len(log['self'][answer_type]['answers'])
                        to_do_exs_types += [answer_type] * len(log['self'][answer_type]['answers'])

        if len(to_do_exs) != 0:
            question_texts = self._predict_questions(to_do_exs, type_logs)
            ### CHANGE START 
            # reverse direction, so that insertion into answer_list doesn't throw everything off
            for i in reversed(range(len(question_texts))): 
                
                idx = to_do_exs_idxs[i]
                answer_type = to_do_exs_types[i]
                
                ### CHANGE START
                answer_list = copy.deepcopy(logs[idx]['self'][answer_type]['answers'])
                if self.use_quartet_model:
                    eos_token = self.models[type_logs]['QG'].tokenizer.eos_token
                    pad_token = self.models[type_logs]['QG'].tokenizer.pad_token
                    # get the answer, in the event we need to add (i.e. lines with more than 1 questions generated)
                    answer = to_do_exs[i][0]
                    answer_idx = to_do_exs[i][-1]
                    __ = question_texts[i]
                    questions = __.split(self.ans_sep)[-1].split(self.qst_sep)[1:] # 1: because 0 is the answer
                    questions = [q.replace(eos_token, '').replace(pad_token, '').strip() for q in questions]
                    if not questions: logs[idx]['self'][answer_type][name_model_qg]['questions'].insert(0, '')
                    for ix, question in enumerate(questions): 
                        logs[idx]['self'][answer_type][name_model_qg]['questions'].insert(0, question)
                        if ix > 0: 
                            answer_list.insert(answer_idx+1, answer) # insert behind, i.e. do not throw off next elem's 

                    # set answer_list back 
                    logs[idx]['self'][answer_type]['answers'] = answer_list
                else: 
                ### CHANGE END
                    question = question_texts[i]
                    ### CHANGE START
                    logs[idx]['self'][answer_type][name_model_qg]['questions'].insert(0, question)
                    ### CHANGE END: switch to insert from append

        return len(to_do_exs) != 0

    def _compute_question_answering(
        self,
        logs_1: Dict,
        logs_2: Dict,
        type_logs_1: str,
        type_logs_2: str
    ) -> None:
        """
        asking questions from logs_2 on text from logs_1
        """
        assert len(logs_1) == len(logs_2)

        name_model_qg = self._get_qg_hash(type_logs_2)
        name_model_qa = self._get_qa_hash(type_logs_1)
        if type_logs_1 == type_logs_2:
            print('\t\tRUNNING CONSISTENCY on: ', type_logs_2)
            print('\t\t consistency name_model_qg: ', name_model_qg)
            print('\t\t consistency name_model_qa: ', name_model_qa)

        to_do_exs, to_do_exs_types, to_do_exs_idxs, to_do_gold_asws = [], [], [], []
        for idx, (log_1, log_2) in enumerate(zip(logs_1, logs_2)):
            if log_1['text'] == '' or log_2['text'] == '':
                continue
            for answer_type in self._get_answer_types(type_logs_2):
                questions = log_2['self'][answer_type][name_model_qg]['questions']
                gold_answers = log_2['self'][answer_type]['answers']
                assert len(questions) == len(gold_answers), (questions, gold_answers)
                for question, gold_answer in zip(questions, gold_answers):
                    if question not in log_1['asked']:
                        log_1['asked'][question] = dict()

                    if name_model_qa not in log_1['asked'][question]:
                        to_do_exs += [(question, log_1['text'])]
                        to_do_exs_idxs += [idx]
                        to_do_gold_asws += [gold_answer]

                    # if already in the logs, we need to add the gold_answers if it hasnt been yet
                    elif gold_answer not in log_1['asked'][question][name_model_qa]['ground_truth']:
                        log_1['asked'][question][name_model_qa]['ground_truth'][gold_answer] = {}

        if len(to_do_exs) != 0:
            answerability_scores, qa_texts = self._predict_answers(to_do_exs, type_logs_1)

            assert len(to_do_exs) == len(qa_texts) == len(to_do_gold_asws) == len(answerability_scores)
            for i in range(len(to_do_exs)):

                question = to_do_exs[i][0]
                idx = to_do_exs_idxs[i]
                assert to_do_exs[i][1] == logs_1[idx]['text']

                if name_model_qa not in logs_1[idx]['asked'][question]:
                    logs_1[idx]['asked'][question][name_model_qa] = {'answer': qa_texts[i],
                                                                     'answerability': answerability_scores[i],
                                                                     'ground_truth': dict()
                                                                     }
                logs_1[idx]['asked'][question][name_model_qa]['ground_truth'][to_do_gold_asws[i]] = {}

        return len(to_do_exs) != 0

    ### CHANGE START
    def _get_ans_bs(self, ans_dict):
        try: gt = [(k, v['bertscore']) for k,v in  ans_dict['ground_truth'].items()]
        except: raise ValueError(ans_dict)
        # there may be more than 1 ground truth answer
        return [(ans_dict['answer'], gt2) for gt2 in gt]

    def _remove_inconsistent_qa_pairs(
        self,
        logs: Dict,
        type_logs: str,
    ) -> None:
        name_model_qg = self._get_qg_hash(type_logs)
        name_model_qa = self._get_qa_hash(type_logs)
        
        if self.print_removals:
            print('\t\tREMOVING INCONSISTENCIES FOR:', type_logs)
            print('\t\t remove inconst name_model_qg:', name_model_qg)
            print('\t\t remove inconst name_model_qa:', name_model_qa)
        mod_logs = False 
        for idx, log in enumerate(logs):
            to_remove = set()
            # checked asked. look for questions there with the name_model_qa
            # if 'unanswerable' or bert_score with ground_truth < inconsistent_cutoff, discard 
            for q, ans_dict in log['asked'].items():
                if name_model_qa not in ans_dict: continue 
                # ans: QA predicted answer, gt: ground truth, bs: bertscore
                agtbs_list = self._get_ans_bs(ans_dict[name_model_qa])
                for ans, (gt, bs) in agtbs_list:
                    if (ans == self.unanswerable_str and gt != self.unanswerable_str) \
                        or bs < self.inconsistent_cutoff: 
                        if self.print_removals: print('\t\tREMOVING', (q, gt, bs))
                        to_remove.add((q, gt))
                
            # begin removal of q, a pair from every answer_type
            for answer_type in self._get_answer_types(type_logs):
                qa_dict = log['self'][answer_type]
                assert len(qa_dict['answers']) == len(qa_dict[name_model_qg]['questions'])
                to_remove_idx = []
                zipped = zip(qa_dict[name_model_qg]['questions'], qa_dict['answers'])
                for idx_qa, (q, a) in enumerate(zipped):
                    if (q, a) in to_remove: 
                        if self.print_removals: print('\t\t\REMOVED', (q, a))
                        to_remove_idx.append(idx_qa)
                
                if self.print_removals: 
                    print('\t\tLength BEFORE', len(qa_dict[name_model_qg]['questions']), len(qa_dict['answers']))
                qa_dict[name_model_qg]['questions'] = [q for i, q in \
                            enumerate(qa_dict[name_model_qg]['questions']) if i not in to_remove_idx]
                qa_dict['answers'] = [a for i, a in \
                            enumerate(qa_dict['answers']) if i not in to_remove_idx]
                if self.print_removals:
                    print('\t\tLength AFTER', len(qa_dict[name_model_qg]['questions']), len(qa_dict['answers']))

                assert len(qa_dict['answers']) == len(qa_dict[name_model_qg]['questions'])
                logs[idx]['self'][answer_type][name_model_qg]['questions'] = qa_dict[name_model_qg]['questions']
                logs[idx]['self'][answer_type]['answers'] = qa_dict['answers']

                # wiped 'asked' else self-consistency questions get counted in final score 
                logs[idx]['asked'] = {}

            mod_logs = True
                
        return mod_logs

    ### CHANGE END

    def _compute_answer_similarity_scores(
        self,
        logs: Dict,
        type_logs: str
    ) -> None:
        """
        filling the similarity scores
        """

        modified_logs = False
        name_model_qa = self._get_qa_hash(type_logs)

        for type_score in self.list_scores:

            # no need for comparison for answerabiliy, it is calculated directly in compute_question_answering
            if type_score == 'answerability':
                continue

            to_do_exs_idxs, to_do_questions, to_do_pred_asws, to_do_gold_asws = [], [], [], []
            for idx, log in enumerate(logs):
                if log['text'] == '':
                    continue
                for question in log['asked']:
                    d_answer = log['asked'][question][self._get_qa_hash(log['type'])]
                    for gold_answer in d_answer['ground_truth']:
                        if type_score not in d_answer['ground_truth'][gold_answer]:
                            to_do_exs_idxs += [idx]
                            to_do_questions += [question]
                            to_do_pred_asws += [d_answer['answer']]
                            to_do_gold_asws += [gold_answer]

            if len(to_do_exs_idxs) != 0:

                modified_logs = True

                if type_score == 'f1':
                    sim_scores = [calculate_f1_squad(pred_asw, gold_asw) for pred_asw, gold_asw in
                                  zip(to_do_pred_asws, to_do_gold_asws)]
                elif type_score == 'bertscore':
                    sim_scores = calculate_BERTScore(to_do_pred_asws, to_do_gold_asws, self.metric_BERTScore,
                                                     device=self.device, 
                                                     ### CHANGE START
                                                     unanswerable_zero=self.unanswerable_zero,
                                                     bsc_ans_lc=self.bsc_ans_lc,
                                                     language = self.language)
                                                     ### CHANGE END
                else:
                    raise NotImplementedError(f"{type_score} not implemented")

                assert len(to_do_exs_idxs) == len(sim_scores)
                for i in range(len(to_do_exs_idxs)):
                    idx = to_do_exs_idxs[i]
                    q = to_do_questions[i]
                    a = to_do_gold_asws[i]
                    logs[idx]['asked'][q][name_model_qa]['ground_truth'][a][type_score] = sim_scores[i]

        return modified_logs

    def _compute_weighter(
        self,
        logs: Dict,
        type_logs: str
    ) -> None:
        """
        weighting the probability that a question is asking about important content or not (see https://arxiv.org/abs/2103.12693)
        """

        name_model_weighter = self._get_weighter_hash()
        name_model_qg = self._get_qg_hash(type_logs)

        to_do_exs, to_do_exs_types, to_do_exs_idxs, to_do_gold_asws = [], [], [], []
        for idx, log in enumerate(logs):
            if log['text'] == '':
                continue
            for answer_type in self._get_answer_types(type_logs):
                if name_model_weighter not in log['self'][answer_type][name_model_qg]:
                    log['self'][answer_type][name_model_qg][name_model_weighter] = []

                    questions = log['self'][answer_type][name_model_qg]['questions']
                    answers = log['self'][answer_type]['answers']
                    assert len(questions) == len(answers)
                    to_do_exs += [f"{asw} {self.sep} {question} {self.sep} {log['text']}"
                                  for asw, question in zip(answers, questions)]

                    to_do_exs_idxs += [idx] * len(answers)
                    to_do_exs_types += [answer_type] * len(answers)

        if len(to_do_exs) != 0:
            weighter_scores = self._predict_weighter(to_do_exs)
            assert len(to_do_exs) == len(weighter_scores)
            for i in range(len(to_do_exs)):
                idx = to_do_exs_idxs[i]
                answer_type = to_do_exs_types[i]
                logs[idx]['self'][answer_type][name_model_qg][name_model_weighter].append(weighter_scores[i])

        return len(to_do_exs) != 0

    def _get_answer_types(self, type_logs: str) -> str:
        return ('TABLE', ) if type_logs == 'src' and self.task == 'data2text' else self.answer_types

    def _predict_self_answers(
        self,
        texts: List,
        ### CHANGE START
        texts_orig: List,
        answer_type: str,
        nominal_deprels: Set = set(['nsubj', 'obj', 'iobj', 'pobj',
                    'obl', 'vocative', 'expl', 'dislocated',
                    'nmod', 'appos','nummod']),
        ### CHANGE END
    ) -> List[str]:
        if self.limit_sent is not None:
            list_sentences = [sentencize(text, self.spacy_pipeline) for text in texts]
            texts = [' '.join(sentences[:self.limit_sent]) for sentences in list_sentences]

        list_answers = []
        if answer_type in ['NER', 'NOUN']:
            if self.quartet_tas_model:
                tas_model = self.models['hyp']['TAS']
                list_answers = [list(tas_model.model.predict_answerset(text, lang = self.language)) for text in texts]
            else:
                if answer_type == 'NER':
                    list_answers = [[a.text for a in self.spacy_pipeline(text).ents] for text in texts]
                else: # answer_type == 'NOUN':
                    ### CHANGE START
                    if self.language in ['ru', 'ptbr']: 
                        list_answers = []
                        for text in texts:
                            list_answers_1 = []
                            for sent in self.spacy_pipeline(text).sents:
                                for word in sent:
                                    # look for head words holding nominal deprels
                                    if word.dep_ in nominal_deprels: 
                                        # get subtree of these nominal head words 
                                        ans_cand, subtree, first = [], list(word.subtree), False
                                        for i, tok in enumerate(subtree):
                                            # 1. exclude determiners from answer span. 
                                            if (i == 0 or first) and tok.pos_ in ['PUNCT', 'ADP', 'DET', 'PRON']: 
                                                first = True
                                                continue                                            
                                            first = False
                                            # 2. keep first part of adpos spans (later parts will be picked by 
                                            # nom deprel and treated again)
                                            if tok.pos_ == 'VERB' and i+1<len(subtree) \
                                                        and subtree[i+1].pos_ == 'ADP': break
                                            # 3. separate conjunctions (exclude connective word)
                                            if tok.pos_ in ['SCONJ', 'CCONJ']:
                                                list_answers_1.append(' '.join(ans_cand))
                                                ans_cand = []                        
                                            else: ans_cand.append(tok.text)
                                        ans_cand = ' '.join(ans_cand)
                                        # remove answer candidates that are only punct and/or whitespace
                                        if not ans_cand.translate(self.trans_table): continue
                                        list_answers_1.append(ans_cand)
                            list_answers.append(list_answers_1)
                    ### CHANGE END
                    else: list_answers = [[a.text for a in self.spacy_pipeline(text).noun_chunks] for text in texts]
            
        elif answer_type == 'SPANER':
            pass  # todo not implemented
        elif answer_type == 'TABLE':
            ### CHANGE START
            if self.use_quartet_model:
                # loop through ent/values
                # apply clean_obj to align with how triplesets are linearized/processed
                list_answers = [extract_table_answers_QUARTET(self.spacy_pipeline, t_o) for t_o in texts_orig]
            ### CHANGE END
            else:
                list_answers = [extract_table_answers(text) for text in texts]

        return list_answers

    def _predict_questions(
        self,
        to_do_exs: List[tuple],
        type_logs: str
    ) -> List[str]:
        model_QG = self.models[type_logs]['QG']
        ### CHANGE START
        ipstrats = None
        if self.use_quartet_model:            
            formated_inputs, ipstrats = [], []
            for asw, context, ctrl, asw_idx in to_do_exs: 
                qg_type, src_type = ctrl['src_type'] 
                # in the case of g2cq, we provided the cut graph instead of the original full graph
                if ctrl['text_cut'] is not None: 
                    assert src_type in ['rdf'] and qg_type in ['cqg', 'sqg']
                    context = ctrl['text_cut'] 
                line = f'{qg_type} task{self.ctrl_sep} {src_type} {ctrl["n_facts"]}{self.ans_sep} {asw}{self.inp_sep} {context}' 
                formated_inputs.append(line)
                _srckey = 't' if src_type == 'text' else 'g'
                ipstrats.append(f'{_srckey}2{qg_type[:2]}')
        else:
        ### CHANGE END
            str_prefix = f'{self.qg_prefix} {self.sep} ' if self.qg_prefix is not None else ''
            formated_inputs = [f'{str_prefix}{asw} {self.sep} {context}' for asw, context in to_do_exs]
        
        ### CHANGE START
        _, question_texts = model_QG.predict(formated_inputs, ipstrats)
        ### CHANGE END: added ipstrats
        return question_texts

    def _predict_answers(
        self,
        to_do_exs: List[tuple],
        type_logs: str
    ) -> Tuple[List[float], List[str]]:
        model_QA = self.models[type_logs]['QA']
        ### CHANGE START
        ipstrats = None
        if self.use_quartet_model:
            formated_inputs, ipstrats = [], []
            for question, context in to_do_exs: 
                qa_type = 'kbqa' if type_logs == 'src' else 'textqa'
                # kbqa task[Sp1]  Where is the Al Khor Sports Club based?[INP]
                line = f'{qa_type} task{self.ctrl_sep} {question}{self.inp_sep} {context}' 
                formated_inputs.append(line)
                ipstrats.append(f'{qa_type}')
        else:
        ### CHANGE END
            formated_inputs = [f'{question} {self.sep} {context}' for question, context in to_do_exs]
        ### CHANGE START
        qa_scores, qa_texts = model_QA.predict(formated_inputs, ipstrats)
        ### CHANGE END: added ipstrats

        ### CHANGE START
        # remove task prefix and eos, pad tokens
        if self.use_quartet_model:
            qa_texts = [strip_special_tokens(qat, ctrl_sep = self.ctrl_sep, 
                        kqa_task = 'kbqa task', tqa_task = 'textqa task', 
                        eos = model_QA.tokenizer.eos_token, pad = model_QA.tokenizer.pad_token) 
                        for qat in qa_texts]
        ### CHANGE END

        return qa_scores, qa_texts

    def _predict_weighter(self, to_do_exs: List[str]) -> List[float]:
        if self.models['Weighter'] is None:
            # Neutral Policy
            probs = [1.0 for _ in to_do_exs]

        else:
            probs, texts = self.models['Weighter'].predict(to_do_exs)
            assert len(probs) == len(to_do_exs)

        return probs

    def _calculate_score_from_logs(
        self,
        hyp_log: List[Dict],
        compared_logs: List[List[Dict]]
    ) -> float:

        scores = []
        for compared_log in compared_logs:
            if compared_log['text'] == '' or hyp_log['text'] == '':
                score = 0
            else:
                hyp_score = self._base_score(hyp_log, compared_log)
                compared_score = self._base_score(compared_log, hyp_log)
                score = np.average([hyp_score, compared_score])
            scores.append(score)
        return self.reduction_multi_refs(scores)

    def _base_score(
        self,
        questioned_log: Dict,
        compared_log: Dict
    ) -> float:
        regularizer = lambda list_score, list_reg: np.multiply(scores, list_reg).tolist()
        list_borned = lambda a_list: [max(min(1, x), 0) for x in a_list]

        if self.do_consistency:
            consistencies = self._get_scores(compared_log, compared_log, 'f1')

        if self.do_weighter and compared_log['type'] == 'src':
            name_model_qg = self._get_qg_hash(compared_log['type'])
            name_model_weighter = self._get_weighter_hash()
            weighter_probs = [
                w for answer_type in self._get_answer_types(questioned_log['type'])
                for w in compared_log['self'][answer_type][name_model_qg][name_model_weighter]
            ]

        list_scores = []
        for type_score in self.list_scores:
            scores = self._get_scores(questioned_log, compared_log, type_score)

            # if no questions, return a score set to 0; could be improved though ?
            if len(scores) == 0:
                ### CHANGE START
                self.global_ans_scores_holder[compared_log['type']][type_score].append([0])
                ### CHANGE END
                return 0

            # sometimes the answers scores return a value ~1.000000X which is superior to 1
            scores = list_borned(scores)

            if self.do_consistency:
                assert consistencies is not None, "consistencies is None. Please compute the score with ques_consists activate."
                scores = regularizer(scores, consistencies)

            if self.do_weighter and compared_log['type'] == 'src':
                assert weighter_probs is not None, "weighter_probs is None. Please compute the weighter probs with do_weighter activate."
                scores = regularizer(scores, weighter_probs)
            ### CHANGE START
            self.global_ans_scores_holder[compared_log['type']][type_score].append(scores)
            ### CHANGE END
            list_scores += scores

        final_score = np.average(list_scores)
        assert 0 <= final_score <= 1, "score should be in [0-1] "
        return final_score

    def _get_scores(
        self,
        questioned_log: List[Dict],
        compared_log: List[Dict],
        type_score: str
    ) -> List[float]:

        name_model_qg = self._get_qg_hash(compared_log['type'])
        asked_questions = [q for answer_type in self._get_answer_types(compared_log['type'])
                           for q in compared_log['self'][answer_type][name_model_qg]['questions']
                           ]

        name_model_qa = self._get_qa_hash(questioned_log['type'])
        if type_score == 'answerability':
            scores = [questioned_log['asked'][q][name_model_qa]['answerability']
                      for q in asked_questions]

        else:  # F1 or BERTScore
            asked_answers = [a for answer_type in self._get_answer_types(compared_log['type'])
                             for a in compared_log['self'][answer_type]['answers']]

            assert len(asked_answers) == len(asked_questions)

            [questioned_log['asked'][q][name_model_qa]['ground_truth'][a][type_score]
             for q, a in zip(asked_questions, asked_answers)]
            scores = [questioned_log['asked'][q][name_model_qa]['ground_truth'][a][type_score]
                      for q, a in zip(asked_questions, asked_answers)]

        return scores

    def get_model(self, model_name: str,):
        keep_score_idx = None
        print('\t\t\t model_name:', model_name)
        if 't5' in model_name.lower():

            if "qa" in model_name.lower():
                # 73 is the index for the token unanswerable in T5 vocabulary
                keep_score_idx = 73
            if 'weighter' in model_name.lower():
                # 1176 is the index for the token true in T5 vocabulary
                keep_score_idx = 1176
            ### CHANGE START 
            self.qg_prefix = None
            ### CHANGE END
            ### CHANGE START: sv1 was added for mDQE QG models
            ckpt_dir, lang = self.mDQE_module_dir_ckpt_path['ckpt_dir'], self.language
            if model_name in [
                              f"{HF_ORGANIZATION}/t5-qg_squad1-en", 
                              f'{ckpt_dir}/train_qg/train_qg_{lang}_mt5-small',
                              f'{ckpt_dir}/train_qg/train_qg_{lang}_mt5-small_dqe_syn_models',
                              ### CHANGE END
                              ]:
                # the default models were trained with this prefix 'sv1' and 'nqa' prefix on the two datasets
                self.qg_prefix = 'sv1'

            # batch size
            model_batch_size = self.qg_batch_size if "qg" in model_name.lower() else self.clf_batch_size

            model = API_T2T(
                pretrained_model_name_or_path=model_name,
                keep_score_idx=keep_score_idx,
                max_source_length=512,
                model_batch_size=model_batch_size,
                device=self.device,
                ### CHANGE START
                use_quartet_model=self.use_quartet_model,
                quartet_model_num=self.quartet_model_num,
                quartet_fullfinetune = self.quartet_fullfinetune,
                load_ckpt=self.load_ckpt,
                language = self.language,
                mDQE_module_dir_ckpt_path = self.mDQE_module_dir_ckpt_path,
                ### CHANGE END
            )

        else:
            raise NotImplementedError(f'Model Name Not Handled: the model name should contain t5 ({model_name}).')

        return model

    def set_model(
        self,
        key: str,
        task: str,
        model_name: str,
    ) -> None:

        assert key in [None, 'hyp', 'src', 'ref']
        assert task in ['weighter', 'QG', 'QG']

        model = self.get_model(model_name=model_name)

        if key is None:
            self.models[task] = model
        else:
            self.models[key][task] = model

    def _get_answer_hash(self) -> str:
        # TODO: self.spacy_pipeline
        msg = f"LimitSent={self.limit_sent}" \
              f"_models={'_'.join(self.answer_types)}"

        return msg

    def _get_qg_hash(self, type_log: str) -> str:
        model = self.models[type_log]['QG']
        msg = f'QG_hash={model.pretrained_model_name_or_path}'

        return msg

    def _get_qa_hash(self, type_log: str) -> str:
        model = self.models[type_log]['QA']
        msg = f'QA_hash={model.pretrained_model_name_or_path}'

        return msg

    def _get_weighter_hash(self) -> str:
        msg = 'W_hash='
        tmp = 'None'
        if self.do_weighter:
            model = self.models['Weighter']
            tmp = f'{model.pretrained_model_name_or_path}'
        msg += tmp
        return msg

    def __hash__(self) -> str:
        ### HACK: version
        version = 'v0.2.4_modQTT'
        msg = f"QuestEval_version={version}" \
              f"_task={self.task}_lang={self.language}_preproc={self.src_preproc_pipe}" \
              f"_consist={self.do_consistency}_scores={self.list_scores}" \
              f"{self._get_weighter_hash()}" \
              f"_hyp_{self._get_qa_hash('hyp')}_ref_{self._get_qa_hash('ref')}_src_{self._get_qa_hash('src')}" \
              f"_hyp_{self._get_qg_hash('hyp')}_ref_{self._get_qg_hash('ref')}_src_{self._get_qg_hash('src')}" \
              f"_quartet={self.use_quartet_model}_qttnum={self.quartet_model_num}" \
              f"_rmvinconst={self.remove_inconsistent}_rmvinconstbsc={self.inconsistent_cutoff}" \
              f"_fullfinetune={self.quartet_fullfinetune}" \
              f"_tas_model={self.quartet_tas_model}" \
              f"_unanszero={self.unanswerable_zero}" \
              f"_bsc_ans_lc={self.bsc_ans_lc}" \
              f"_ckpt_num={self.ckpt_num}" \
            ### CHANGE START from _quartet...
        return msg

    ### CHANGE START
    def clear_logs(self,):
        quartet_format = self.use_quartet_model
        
        suffix = ''
        f'_{self.system_name}'

        if quartet_format: 
            assert self.quartet_model_num is not None
            suffix += f'_QUARTET{str(self.quartet_model_num)}'
        suffix += self.ckpt_num
        if self.unanswerable_zero: suffix += '_UNANSZERO'
        remove_inconsistent = (self.remove_inconsistent, self.inconsistent_cutoff)
        if remove_inconsistent[0]: suffix += f'_RMVINCONST{str(remove_inconsistent[1])}'

        path = os.path.join(self.log_dir, "*"+suffix)
        to_delete_files = glob.glob(path)
        for fp in to_delete_files: 
            os.remove(fp)

    ### CHANGE END