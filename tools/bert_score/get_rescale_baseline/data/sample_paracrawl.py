import random, os, tqdm
        
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Get me some random paracrawl")
    parser.add_argument(
        "--lang", type=str, required=True, help="language to compute baseline with"
    )
    args = parser.parse_args()
    
    read_fp = f'en-{args.lang}.txt'
    write_fp = f'paracrawl/rand_{args.lang}.txt'
    with open(read_fp, encoding='utf-8') as f: lines = f.readlines()
    # picks = sorted(random.sample(range(len(lines)), 1_500_000))
    if not os.path.exists(os.path.dirname(write_fp)): os.mkdirs(write_fp)
    counter = 0
    with open(write_fp, encoding='utf-8', mode='w+') as f:
        for i, l in enumerate(tqdm.tqdm(lines)):
            if random.random() > 0.5: continue
            if i > 1_500_000: break
            pick_line = lines[i].strip().split('\t')[-1]
            if not pick_line: continue
            f.write(pick_line+'\n')
            counter+=1
