import json, os, re, tqdm
PARENTDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print('PARENTDIR', PARENTDIR)
from itertools import product
from questeval.questeval_metric import QuestEval
from questeval.utils import LinearizeWebnlgInput, Linearize_CQ_KELM_Input
import sys
sys.path.append(f'{PARENTDIR}/evaluation')
from evaluation_humaneval_correlation_utils import (_parse_mr_spacy, sacrebleu_sent,
                                                    give_webnlg_scores_en)
import spacy, datasets
from scipy.stats import pearsonr, spearmanr
sys.path.append(f'{PARENTDIR}/tools/parent')
from parent import parent
from nltk import word_tokenize
from evaluation_cross_answerability import remove_underscore_camelcase, calculate_BERTScore_rescaled, give_linearizers

os.environ["CUBLAS_WORKSPACE_CONFIG"]=":4096:8" # solve RuntimeError: Deterministic behavior was enabled

'''
This script introduces the computation for the QuestEval scores on the 9 systems from WebNLG 2017
- models used are original DQE and QUARTET
- the logs are extracted and saved out results/omnibus_model
'''

D2W = {}
d2w_dirpath = f'{PARENTDIR}/datasets/dqe_webnlg'
with open(f'{d2w_dirpath}/z00_webnlg2020_dpb2wkdt_triples.json', encoding = 'utf-8') as f: d2w = json.load(f)
# NOTE: WebNLG 2020 test has triples that have not been converted to Wikidata form in our earlier SQ work (WebNLG 2017)...
# we use a similar approach as the WebNLG 2020 eval scipt to remove _ and camel case (except no lowercase)
for dp_trip, wkdt_trip in d2w.items():
    c_underscore  = '_' in wkdt_trip
    c_camel = re.search(r'\w+[A-Z]\w+', 'wkdt_trip')
    if c_underscore or c_camel: 
        D2W[dp_trip] = remove_underscore_camelcase(wkdt_trip)

def main(args): 
    args.ckpt_num = ''
    if args.load_ckpt and len(args.load_ckpt)==2: 
        assert len(args.load_ckpt[1]) > 0
        if 'final.xkpt' in args.load_ckpt[1]: args.ckpt_num = 'xkpt-epochfinal'
        else: args.ckpt_num = 'ckpt-epoch' + re.search(r'(?:epoch=)([0-9]+)', args.load_ckpt[1]).group(1)

    args.mling_str = ''
    if args.multilingual: 
        raise NotImplementedError
    else:
        spacy_pipeline = spacy.load('en_core_web_sm') 
        webnlg_scores = give_webnlg_scores_en(D2W)
    
    # 3. add PARENT score, using Rebuffel's optimized implementation
    print('\t\t working on PARENT')
    webnlg_scores['mr_parsed'] = webnlg_scores.apply(lambda x: _parse_mr_spacy(x.mr, spacy_pipeline, print_check=False), axis=1)
    # PARENT takes: (1) predictions, (2) references, (3) table
    *__, parentf1 = parent(
        [word_tokenize(text) for text in webnlg_scores['text']],
        [[word_tokenize(ref) for ref in refset] for refset in webnlg_scores[['r0', 'r1', 'r2']].values.tolist()],
        [mr for mr in webnlg_scores['mr_parsed']],
        avg_results = False, n_jobs = 32, use_tqdm = True)
    assert len(webnlg_scores) == len(parentf1)
    webnlg_scores['parent'] = parentf1

    # 4. add SacreBLEU to check 
    print('\t\t working on SACREBLEU')
    webnlg_scores['sacrebleu'] = webnlg_scores.apply(sacrebleu_sent, axis=1)

    # 4. add BERTScore to check 
    print('\t\t working on BERTSCORE')
    UNANSWERABLE_STR = 'unanswerable'
    BERTSCORE_OBJ = datasets.load_metric('bertscore', keep_in_memory = True)    
    webnlg_scores['bertscore'] = calculate_BERTScore_rescaled(webnlg_scores['text'].values.tolist(), 
                                webnlg_scores[['r0', 'r1', 'r2']].values.tolist(), 
                                BERTSCORE_OBJ,  args.unanswerable_zero, UNANSWERABLE_STR, 
                                bert_score_model = 'bert-base-multilingual-cased',
                                bert_score_rescale_with_baseline = True)
    BERTSCORE_OBJ = None

    # 4. begin scoring team by team
    print('\t\t working on Data-QuestEval')
    gb_webnlg_scores = webnlg_scores.groupby('team')
    for team in webnlg_scores['team'].unique():
        if team not in args.teams: continue
        print(f'Working on team: {team}')
        grp = gb_webnlg_scores.get_group(team)
        hyps = grp['text'].values.tolist()
        idxes  = grp['id'].values.tolist()
        if args.multilingual: # TODO
            correctness_scores = grp['Correctness'].values.tolist()
            datacoverage_scores = grp['DataCoverage'].values.tolist()
            relevance_scores = grp['Relevance'].values.tolist()
            assert len(correctness_scores) == len(datacoverage_scores) == len(relevance_scores), \
                (len(correctness_scores), len(datacoverage_scores), len(relevance_scores))
            # NOTE: definition of correctness (c) is wrt to datacoverage (dc), allowing 
            # c*dc to give a score equivalent to semantic. 
            sem_scores = [c+dc+r for c, dc, r \
                          in zip(correctness_scores, datacoverage_scores, relevance_scores)]
            fluency_scores = grp['Fluency'].values.tolist()
            
            textstructure_scores = grp['TextStructure'].values.tolist()
        else: 
            fluency_scores = grp['fluency'].values.tolist()
            grammar_scores = grp['grammar'].values.tolist()
            sem_scores = grp['semantics'].values.tolist()
            bleu_scores = grp['bleu'].values.tolist()
        sacrebleu_scores = grp['sacrebleu'].values.tolist()
        parent_scores = grp['parent'].values.tolist()
        bertscore_scores = grp['bertscore'].values.tolist()

        for dqe_appr in ['OUR', 'DQE']:
            print(f'\t\tWorking on: {dqe_appr}{args.quartet_model_num}')
            src = grp[f'src_{dqe_appr}'].values.tolist()

            if dqe_appr == 'DQE':
                questeval = QuestEval(task = 'data2text', no_cuda = False, qg_batch_size = args.bsz, 
                                list_scores = ('bertscore', ),
                                use_quartet_model = False, quartet_model_num = None, log_dir = args.log_dir.format(PARENTDIR),
                                unanswerable_zero = args.unanswerable_zero, 
                                remove_inconsistent = (args.remove_inconsistent, args.inconsistent_cutoff),
                                system_name = team, language = args.language_eval)
                # if args.multilingual: linearizer, __, __ = give_linearizers(args.language_eval)
                # else: linearizer = LinearizeWebnlgInput(spacy_pipeline = spacy_pipeline)
                linearizer = LinearizeWebnlgInput(spacy_pipeline = spacy_pipeline)
            
            else: 
                sep_dict = {'tend_sep': '[TEND]', 'ctrl_sep': '[Sp1]', 'qst_sep': '[QST]', 'ans_sep': '[ANS]', 'inp_sep': '[INP]'}
                questeval = QuestEval(task = 'data2text', no_cuda = False, qg_batch_size = args.bsz, 
                                list_scores = ('bertscore',),
                                use_quartet_model = True, quartet_model_num = args.quartet_model_num, 
                                quartet_fullfinetune = args.quartet_fullfinetune, quartet_tas_model = args.quartet_tas_model, log_dir = args.log_dir.format(PARENTDIR), unanswerable_zero = args.unanswerable_zero, 
                                remove_inconsistent = (args.remove_inconsistent, args.inconsistent_cutoff),
                                system_name = team, language = args.language_eval,
                                bsc_ans_lc = args.bsc_ans_lc, load_ckpt = args.load_ckpt, **sep_dict)
                if args.multilingual: __, linearizer, __ = give_linearizers(args.language_eval)
                else:  linearizer = Linearize_CQ_KELM_Input(spacy_pipeline = spacy_pipeline, tripend_char = '[TEND]')
        
            # 1. run on DQE
            # a. obtain global scores
            score = questeval.corpus_questeval(hypothesis = hyps, sources = src)

            # b. write to file
            DIRPATH = f'{PARENTDIR}/results/quartet_human_eval'
            if not os.path.exists(DIRPATH): os.makedirs(DIRPATH)
            anszero = '_unanswerable_zero' if args.unanswerable_zero else ''
            rmvinconst = f'_remove_inconsistent{args.inconsistent_cutoff}' if args.remove_inconsistent else ''
            fullfinetune = '_fullfinetune' if args.quartet_fullfinetune else ""
            tas_model = '_tas_model' if args.quartet_tas_model else ""
            bsc_ans_lc = '_bsc_ans_lc' if args.bsc_ans_lc else ''
            model_key = f'_model{args.quartet_model_num}' 
            wnlg_str = 'webnlg2020' if args.multilingual else 'webnlg2017'
            SAVEPATH = f'{DIRPATH}/DQE_QUARTET_{wnlg_str}/{team}_team_{dqe_appr}{model_key}{args.ckpt_num}{anszero}{rmvinconst}{bsc_ans_lc}{fullfinetune}{tas_model}{args.mling_str}_logs.jsonl'
            if not os.path.exists(os.path.dirname(SAVEPATH)): os.makedirs(os.path.dirname(SAVEPATH))
            with open(SAVEPATH, encoding = 'utf-8', mode = 'w+') as f:
                if args.multilingual:             
                    assert len(src) == len(hyps) == len(idxes) == len(fluency_scores) == len(correctness_scores) 
                    assert len(src) == len(sem_scores) == len(datacoverage_scores) == len(relevance_scores) == len(textstructure_scores)
                    assert len(src) == len(sacrebleu_scores) == len(parent_scores) == len(bertscore_scores)
                    zipped = zip(src, hyps, idxes, sem_scores, sacrebleu_scores, parent_scores, 
                                fluency_scores, correctness_scores, datacoverage_scores, relevance_scores,
                                textstructure_scores, bertscore_scores)
                    qe_hash = questeval.__hash__()
                    for __, (s,h,i, sem, sbl, par, flue, cor, dc, relv, txs, bsc) in enumerate(tqdm.tqdm(zipped)):
                        slog = questeval.open_log_from_text(questeval.src_preproc_pipe(s), 
                                    quartet_format = True if dqe_appr == 'OUR' else False, system_name = team)
                        hlog = questeval.open_log_from_text(h, 
                                    quartet_format = True if dqe_appr == 'OUR' else False, system_name = team)
                        line = json.dumps({'id_key': f'{team}_{i}', 'src_log': slog, 
                                            'src': s, 'hyp_log': hlog, 'hyp': h, 
                                            'semantic': sem,  'sacrebleu': sbl, 'parent': par, 
                                            'bertscore': bsc, 'fluency': flue, 
                                            'correctness': cor, 'datacoverage': dc, 'relevance': relv,
                                            'textstructure': txs, 'qe_hash': qe_hash})
                        f.write(line + '\n')   
                else: 
                    assert len(src) == len(hyps) == len(idxes) == len(sem_scores) == len(bleu_scores) 
                    assert len(src) == len(sacrebleu_scores) == len(parent_scores)
                    assert len(src) == len(fluency_scores) == len(grammar_scores) == len(bertscore_scores)
                    zipped = zip(src, hyps, idxes, sem_scores, bleu_scores, sacrebleu_scores, parent_scores, 
                                fluency_scores, grammar_scores, bertscore_scores)
                    qe_hash = questeval.__hash__()
                    for __, (s,h,i, sem, bl, sbl, par, flue, gram, bsc) in enumerate(tqdm.tqdm(zipped)):
                        slog = questeval.open_log_from_text(linearizer(s), 
                                    quartet_format = True if dqe_appr == 'OUR' else False, system_name = team)
                        hlog = questeval.open_log_from_text(h, 
                                    quartet_format = True if dqe_appr == 'OUR' else False, system_name = team)
                        line = json.dumps({'id_key': f'{team}_{i}', 'src_log': slog, 
                                            'src': s, 'hyp_log': hlog, 'hyp': h, 
                                            'semantic': sem, 'bleu': bl, 'sacrebleu': sbl, 'parent': par, 
                                            'bertscore': bsc, 'fluency': flue, 'grammar': gram, 'qe_hash': qe_hash})
                        f.write(line + '\n')

            SAVEPATH_SCORE = f'{DIRPATH}/DQE_QUARTET_{wnlg_str}/{team}_team_{dqe_appr}{model_key}{args.ckpt_num}{anszero}{rmvinconst}{bsc_ans_lc}{fullfinetune}{tas_model}{args.mling_str}_scores.json'
            print()
            score['qe_hash'] = qe_hash
            score['global_ans_scores_holder'] = questeval.global_ans_scores_holder
            dqe_scores = score['ex_level_scores'] # called by locals()
            if args.multilingual:             
                snames = ['fluency_scores', 'correctness_scores', 'sem_scores', 
                    'datacoverage_scores', 'relevance_scores', 'textstructure_scores', 
                    'sacrebleu_scores', 'parent_scores', 'bertscore_scores', 'dqe_scores']
            else: snames = ['fluency_scores', 'grammar_scores', 'sem_scores', 'bleu_scores', 
                    'sacrebleu_scores', 'parent_scores', 'bertscore_scores', 'dqe_scores']
            for sname1, sname2 in product(snames, snames):
                if sname1 == sname2: continue
                autoscore1 = locals()[sname1]
                autoscore2 = locals()[sname2]
                pearson_corr = pearsonr(autoscore1, autoscore2)
                # following Shimorina 2020, since sampling approach gives rise to non-normal dist in data
                spearman_corr = spearmanr(autoscore1, autoscore2)
                print('\t\t\tCorrelation on score-pair: ', sname1, sname2, pearson_corr, spearman_corr)
                score[f'pearson_corr_{sname1}_ON_{sname2}'] = pearson_corr
                score[f'spearman_corr_{sname1}_ON_{sname2}'] = spearman_corr
                score[f'{sname1}'] = autoscore1
                score[f'{sname2}'] = autoscore2
            
            if not os.path.exists(os.path.dirname(SAVEPATH_SCORE)): os.makedirs(os.path.dirname(SAVEPATH_SCORE))
            with open(SAVEPATH_SCORE, 'w+') as f: json.dump(score, f)
            print('Correlation computed and saved... \n\n')
            if dqe_appr == 'OUR': 
                questeval.clear_logs()
                print('Logs cleared... \n\n')


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description='Arugments for running script.')
    parser.add_argument('--teams', help = 'Which teams to run the QuestEval models on',
                        default=['baseline'], nargs= "*", type = str)
    parser.add_argument('--log_dir', help = 'Where to save the QuestEval logs',
                        default='{}/results/quartet_human_eval/', type = str)
    parser.add_argument('--bsz', help = 'batch size',
                        default=64, type=int)
    parser.add_argument('--unanswerable_zero', help = 'Whether to set QA "unanswerable" predictions to bertscore 0 ',
                        default=False, type=bool)
    parser.add_argument('--remove_inconsistent', help = 'Whether to run consistency on QG and remove unanswerable/low BERTScore',
                        default=False, type=bool)
    parser.add_argument('--inconsistent_cutoff', help = 'Whether to run consistency on QG and remove unanswerable/low BERTScore',
                        default=None, type=float)
    parser.add_argument('--quartet_model_num', help = 'Which QUARTET model to use. 1, 2, 3',
                        default=1, type=int)
    parser.add_argument('--quartet_fullfinetune', help = 'Whether to use the fullfinetuning version of the model.',
                        default=False, type=bool)
    parser.add_argument('--quartet_tas_model', help = 'Whether to use the text answer selector model instead of the spacy pipeline.',
                        default=False, type=bool)
    parser.add_argument('--bsc_ans_lc', help = 'Whether to lowercase answer when comparing BERTScore against g-truth answer.',
                        default=False, type=bool) 
    parser.add_argument('--load_ckpt', help = 'If loading from ckpt, provide path to checkpoint .ckpt here. \
                        input here should be 2-element sequence. first elem is best_model.pt or "", second elem\
                        is the path to the checkpoint to load.',
                        default = [], nargs= "*", type = str)
    parser.add_argument('--language_eval', help = 'The language to use on QTT/mQTT. ',
                        default='en', type=str)
    parser.add_argument('--exp_code_str', help = 'Additional experiment code string to disambiguate experiments.',
                        default='', type=str)

    args = parser.parse_args()
    args.unanswerable_zero = True
    assert args.quartet_model_num in [3]
    args.multilingual = False
    if args.language_eval not in ['en']: args.multilingual = True
    main(args)
