import re, json, datasets, os
from collections import defaultdict
import numpy as np
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0,parentdir) 
from utils.utils_evaluate import evaluate


# functions here: 
# 1. loaders for outputs
# a. a loader to read in our gen outputs. each gem-webnlg test set input is paried with 
# its corresponding set of questions. NOTE: questions generated from text T2Q are separated 
# from the questions generated from graph G2Q. questions from the SQG and CQG models are merged. 
# b. a loader to take in the outputs of the DQE model for (i) T2Q, (ii) G2Q 

def load_our_outputs(args):
    model_key = f'_model{args.quartet_model_num}' 
    sq_dir = args.sq_dir.format(model_key, args.lrwarm, args.ckpt_num)
    cq_dir = args.cq_dir.format(model_key, args.lrwarm, args.ckpt_num)
    dirpath = args.dirpath
    args.sq_dir_used, args.cq_dir_used = sq_dir, cq_dir
    
    gens_holder_text = defaultdict(dict)
    gens_holder_graph = defaultdict(dict)

    gensholder = {'text': gens_holder_text, 'rdf': gens_holder_graph}

    for __dir in [sq_dir, cq_dir]:
        print('working on: ', __dir)
        with open(f'{dirpath}{__dir}/tested_gens.txt', encoding='utf-8') as f:
            glines = [l.strip() for l in f.readlines()]
        with open(f'{dirpath}{__dir}/tested_src.txt', encoding='utf-8') as f:
            slines = [l.strip() for l in f.readlines()]
        print('loading done for: ', __dir)
        
        assert len(glines) == len(slines)
        for gl, sl in zip(glines, slines): 
            ggem_id, gline = gl.split('\t')
            sgem_id, sline = sl.split('\t')
            
            # remove any whitespaces
            gline = gline.strip()
            sline = sline.strip()

            qg_ctrl_vals = detect_qsource(sline)

            assert ggem_id == sgem_id

            # web_nlg_en-test-1778	 text[ANS]  the Pacific Daylight[QST] [QST]  Where is the Pacific Zone located?</s>
            try: src_type = re.search(r'(text|rdf)(?:.*\[ANS\].+)', gline).group(1).strip()        
            except: 
                print('gline FAIL to retrieve [src type] from generated... SKIPPING:', gline)
                continue
            if src_type not in ['text', 'rdf']: raise ValueError(src_type, gline)

            # obtain answer from src (what the generation was conditioned on)
            answer_cond = get_answer_from_our_srcgen(sline, line_type = 's')
            if answer_cond is None: print('sline FAIL to retrieve [answer span] from source', sline)
            answer_gen = get_answer_from_our_srcgen(gline, line_type = 'g')
            if answer_gen is None: 
                print('gline FAIL to retrieve [answer span] from generated... SKIPPING:', gline)
                continue            
            
            questions = get_questions_our_from_gen(gline)
            answer_cond = f'{answer_cond}<SEP>{" ".join(qg_ctrl_vals)}'

            if answer_cond in gensholder[src_type][ggem_id]:
                gensholder[src_type][ggem_id][(answer_cond)].update(questions)
            else: gensholder[src_type][ggem_id].update({answer_cond: questions})

    return gens_holder_text, gens_holder_graph

def get_answer_from_our_srcgen(line, line_type = 's'):
    mapping = {'s':'INP', 'g': 'QST'}
    answer = re.search(rf'(?:\[ANS\]\s*)(.+?)(?:\[{mapping[line_type]}\].+)', line)
    if answer: return answer.group(1)
    else: return None

def get_questions_our_from_gen(gline):
    questions = re.search(r'(?:.+)(\[QST\].+)', gline).group(1)
    questions = questions.replace('</s>', '').split('[QST]')
    # lowercase to match on content, not case
    questions = set(q.strip() for q in questions if q) 
    return questions

def load_dqe_outputs(args):
    mling_code_str = '' if args.language_eval == 'en' else f'_{f"MULTILINGUAL-{args.language_eval}"}'
    filepath = args.dqe_filepath.format(mling_code_str)
    print('DQE outputs being loaded from:', filepath)
    with open(filepath, encoding='utf-8') as f: lines = [json.loads(i) for i in f]
    
    gens_holder_text = defaultdict(dict)
    gens_holder_graph = defaultdict(dict)

    for line in lines:
        ggem_id = line['gem_id']

        graph, text = line['src_log']['self'], line['tgt_log']['self']
        # (dummy val for DQE)
        qg_ctrl_vals = ['None', '', '']
        
        # work on T2Q
        qa_pair_dict = text
        for k, v in qa_pair_dict.items():
            qa_pair_dict[k]['questions'] = []
            for k2, v2 in v.items():
                if 'QG_hash' in k2: qa_pair_dict[k]['questions'].extend(v2['questions'])

            questions, answers = qa_pair_dict[k]['questions'], qa_pair_dict[k]['answers']
            assert len(questions) == len(answers)
            
            for question, answer_cond in zip(questions, answers):
                answer_cond, question = answer_cond, question.strip()

                answer_cond = f'{answer_cond}<SEP>{" ".join(qg_ctrl_vals)}'

                if answer_cond in gens_holder_text[ggem_id]:
                    gens_holder_text[ggem_id][answer_cond].add(question)
                else: gens_holder_text[ggem_id].update({answer_cond: set([question])})

        # work on G2Q
        qa_pair_dict = graph
        for k, v in qa_pair_dict.items():
            qa_pair_dict[k]['questions'] = []
            for k2, v2 in v.items():
                if 'QG_hash' in k2: qa_pair_dict[k]['questions'].extend(v2['questions'])

            questions, answers = qa_pair_dict[k]['questions'], qa_pair_dict[k]['answers']
            assert len(questions) == len(answers)
            
            for question, answer_cond in zip(questions, answers):
                answer_cond, question = answer_cond, question.strip()

                answer_cond = f'{answer_cond}<SEP>{" ".join(qg_ctrl_vals)}'

                if answer_cond in gens_holder_graph[ggem_id]:
                    gens_holder_graph[ggem_id][answer_cond].add(question)
                else: gens_holder_graph[ggem_id].update({answer_cond: set([question])})

    return gens_holder_text, gens_holder_graph

def compute_quantities(holder, language_eval = 'en'):
    '''
    compare the number of questions generated by us vs dqe (either T2Q or G2Q)
    on the following measurements:
    - overall-questions:    overall across the entire test set
    - overall-answers:      overall across the entire test set
    - gsize-questions:      #q by graph size of entries, in order to return an average 
    - gsize-answers:        #a by graph size of entries, in order to return an average 
    - entry-answers:        #q by entry in test set, in order to return an average /entry
    - entry-questions:      #a by entry in test set, in order to return an average /entry
    - answers:              the number of answers for a given input 
    - q/a:                  the number of questions for a given answer
    # NOTE: there are more T2Q entries than G2Q entries
    '''
    num_entries, num_a, num_q = 0, 0, 0
    by_entry_q, by_entry_a, by_ans_q = [], [], []
    by_gsize_q, by_gsize_a = defaultdict(list),  defaultdict(list)
    if language_eval == 'en':
        webnlg_data = datasets.load_dataset('GEM/web_nlg', 'en')['test']
    else: raise NotImplementedError
    
    for entry in webnlg_data:
        num_entries+=1
        gem_id, gsize = entry['gem_id'], len(entry['input'])

        aset = set(holder[gem_id].keys())
        by_entry_a.append(len(aset))
        by_gsize_a[gsize].append(len(aset))
        __qctr = 0
        for answer_cond, questions in holder[gem_id].items():
            num_a += 1
            __qctr += len(set(q.lower() for q in questions))
            by_ans_q.append(len(set(q.lower() for q in questions)))
        num_q += __qctr
        by_entry_q.append(__qctr)
        by_gsize_q[gsize].append(__qctr)

    assert num_a == len(by_ans_q)
 
    stats_dict ={
        'num_entries': num_entries, 
        'num_ans_overall': num_a, 
        'avg_num_ans_per_entry': round(num_a/num_entries, 2), 
        'min_max_ans_per_entry': (min(by_entry_a), max(by_entry_a)), 
        'num_qst_overall': num_q, 
        'avg_num_qst_per_entry': round(num_q/num_entries, 2), 
        'min_max_qst_per_entry': (min(by_entry_q), max(by_entry_q)), 
        'avg_num_qst_per_answer': round(np.mean(by_ans_q), 2), 
        'min_max_qst_per_answer': (min(by_ans_q), max(by_ans_q)), 
    }

    __ = {'ans': by_gsize_a, 'qst': by_gsize_q}
    for key, coll in __.items():
        for gsize in sorted(coll.keys()):
            counts = coll[gsize]
            stats_dict[f'avg_num_{key}_per_gsize_{gsize}'] = round(np.mean(counts), 2)
            stats_dict[f'min_max_{key}_per_gsize_{gsize}'] = (min(counts), max(counts))
    
    return stats_dict

def produce_refs_gens(holder_ref, holder_gen, language_eval = 'en'):
    '''
    helper func to set ref and gen set correctly. 
    '''
    references, generated = [], []
    for gem_id in holder_ref.keys():
        # empty ref errors encountered (rouge), when system has no questons for a given input
        ref_set, gen_set = holder_ref[gem_id].values(), holder_gen[gem_id].values()
        # ref sys empty, set ref sys to gen sys
        if not ref_set and gen_set: 
            ref_set = gen_set
        # gen sys empty, set gen sys to single empty string
        elif ref_set and not gen_set: gen_set = ['']
        else: pass
        
        references.append([q for qs in ref_set for q in set(qs)])
        generated.append([q for qs in gen_set for q in set(qs)])
    return references, generated
    
def compute_cross_autoscores(holder_dqe, holder_our, language_eval = 'en'):
    
    # DQE as ref (we align by answer)
    references, generated = produce_refs_gens(holder_dqe, holder_our, language_eval = language_eval)
    score_DQE_ref = evaluate(references, generated, tgt_strategy = 'dqe_compare', 
                             gentypes = ['normal'], reftypes = ['multi'], pfx = 'DQE_ref',
                            auto_scores = ['bertscore'],
                            bert_score_model = 'bert-base-multilingual-cased', 
                            bert_score_rescale_with_baseline = False,
                            eos_token = '</s>')
    
    # OUR as ref
    references, generated = produce_refs_gens(holder_our, holder_dqe, language_eval = language_eval)
    score_OUR_ref = evaluate(references, generated, tgt_strategy = 'dqe_compare', 
                             gentypes = ['normal'], reftypes = ['multi'], pfx = 'OUR_ref',
                            auto_scores = ['bertscore'],
                            bert_score_model = 'bert-base-multilingual-cased', 
                            bert_score_rescale_with_baseline = False,
                            eos_token = '</s>')

    return score_DQE_ref, score_OUR_ref

def detect_qsource(sline):
    '''
    helper func to retrieve the QG control values from src line
    '''
    qgtype = re.search(r'(?:.*)(s|c)(?:qg task.+)', sline).group(1)
    qgsrc_nfcount = re.search(r'(?:.*\[Sp1\]\s+)(.+)(?:\[ANS\].+)', sline).group(1)
    qgsrc, nfcount = qgsrc_nfcount.split()
    
    return qgtype, qgsrc[0], nfcount

if __name__  == '__main__':
    import argparse, datetime, os
    parser = argparse.ArgumentParser(description='Arugments for running the coverage/autoscores eval.')
    parser.add_argument('--sq_dir', help = 'path for OUR SQ outputs',
                        default='quartet_sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10{}_noprmpt_dqe-webnlg-test_only{}_ep10{}', 
                        type=str)
    parser.add_argument('--cq_dir', help = 'path for OUR CQ outputs',
                        default='quartet_cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10{}_noprmpt_dqe-webnlg-test_only{}_ep10{}', 
                        type=str)
    parser.add_argument('--dirpath', help = 'path for OUR CQ outputs',
                        default='results/omnibus_model/', 
                        type=str)
    parser.add_argument('--dqe_filepath', help = 'path for DQE outputs',
                        default='datasets/dqe_webnlg/dqe_outputs_test{}.jsonl', 
                        type=str)
    parser.add_argument('--languages_trained', help = 'The language(s) used to train QTT/mQTT with. ',
                        default=['en'], nargs = '*', type=str)
    parser.add_argument('--language_eval', help = 'The language to use on QTT/mQTT. ',
                        default='en', type=str)
    parser.add_argument('--quartet_model_num', help = 'Which QUARTET model to use. 1, 2 or 3',
                        default='1', type=str)
    parser.add_argument('--ckpt_num', help = 'The checkpoint num for the SQ/CQ inference (e.g. ckpt-epoch5).',
                        default='', type=str)  
    parser.add_argument('--lrwarm', help = 'The lrwarmup ratio for the model (fixed); may differ ('') for legacy models.',
                        default='_lrwarm0', type=str)  
    parser.add_argument('--trp_sep_scheme2', help = 'If TS2 linearisation was used.',
                        default=False, type=bool)  
    parser.add_argument('--ablation', help = 'Which ablation setting to use (1) is without adapter.',
                        default=0, type=int)  
    

    args = parser.parse_args()
    assert args.quartet_model_num in ['3']
    model_key = f'_model{args.quartet_model_num}' 
    assert set(args.languages_trained).issubset(set(['en'])), args.languages_trained
    args.multilingual_trained, args.non_english = False, False
    if len(args.languages_trained) > 1: args.multilingual_trained = True
    args.languages_trained = sorted(args.languages_trained)
    assert args.languages_trained and args.language_eval, (args.languages_trained, args.language_eval)
    print('LANGUAGES TRAINED ON/LANGUAGE FOR EVAL:', args.languages_trained, args.language_eval)
    non_en = [lang for lang in args.languages_trained if lang != 'en']
    mling_str = ''
    if non_en: args.non_english = True
    print('args.languages_trained, args.language_eval, args.multilingual_trained, args.non_english', 
        args.languages_trained, args.language_eval, args.multilingual_trained, args.non_english)
    
    if (args.multilingual_trained or args.non_english): 
        key1, replace_key1 = '_t5', '_mt5'
        key2, replace_key2 = 'dqe-webnlg-test_only', 'gem-webnlg-test_only'
        args.sq_dir = args.sq_dir.replace(key1, replace_key1).replace(key2, replace_key2)
        args.cq_dir = args.cq_dir.replace(key1, replace_key1).replace(key2, replace_key2)

    if args.trp_sep_scheme2:
        key, replace_key = '_noprmpt', '_noprmpt_tsep_scheme2'
        args.sq_dir = args.sq_dir.replace(key, replace_key)
        args.cq_dir = args.cq_dir.replace(key, replace_key)
    if args.ablation >0:
        if args.ablation >3: raise ValueError
        if args.ablation == 1:
            key = '_noprmpt'
            replace_key = '_ablation_xadapter' 
            if args.non_english: 
                replace_key += f'_{args.language_eval}'       
            if '{}' in args.sq_dir: args.sq_dir = args.sq_dir.replace(key, replace_key)
            if '{}' in args.cq_dir: args.cq_dir = args.cq_dir.replace(key, replace_key)

    dirpath = 'results/omnibus_model/'
    if not args.non_english: args.mling_str = ''
    else: 
        args.mling_str = f'-MULTILINGUAL'
        for lang in args.languages_trained: args.mling_str += f'-{lang}'
        args.mling_str += f'-on-{args.language_eval}'
    savepath = f'{dirpath}sqg_cqg_webnlg_coverage_evaluation{model_key}{args.ckpt_num}{args.mling_str}/'
    if not os.path.exists(savepath): os.makedirs(savepath)
    exp_config = vars(args)
    with open(f'{savepath}exp_configs.json', 'w+') as f: json.dump(exp_config, f)
    print('Exp config saved!!')


    ### Loading OUR data
    gens_holder_text_OUR, gens_holder_graph_OUR = load_our_outputs(args)
    print('NUM TEXT: ', len(gens_holder_text_OUR))
    print('NUM GRAPH: ', len(gens_holder_graph_OUR))
    key1 = list(gens_holder_text_OUR)[0]
    key2 = list(gens_holder_text_OUR)[-1]
    print(f'SAMPLE TEXT ({key1}), ({key2}): ', gens_holder_text_OUR[key1], '\n', gens_holder_text_OUR[key2])
    print(f'SAMPLE GRAPH ({key1}), ({key2}): ', gens_holder_graph_OUR[key1], '\n', gens_holder_graph_OUR[key2])

    ### Loading DQE data 
    gens_holder_text_DQE, gens_holder_graph_DQE = load_dqe_outputs(args)
    print('NUM TEXT: ', len(gens_holder_text_DQE))
    print('NUM GRAPH: ', len(gens_holder_graph_DQE))

    key1 = list(gens_holder_text_DQE)[0]
    key2 = list(gens_holder_text_DQE)[-1]
    print(f'SAMPLE TEXT ({key1}), ({key2}): ', gens_holder_text_DQE[key1], '\n', gens_holder_text_DQE[key2])
    print(f'SAMPLE GRAPH ({key1}), ({key2}): ', gens_holder_graph_DQE[key1], '\n', gens_holder_graph_DQE[key2])

    ### Running coverage compute, running cross-autoscores
    print('\n', '#'*100)
    # if not args.multilingual: # NOTE/TODO: need to update compute_quantities to load non-En WebNLG
    stats_dict_text_DQE = compute_quantities(gens_holder_text_DQE, language_eval = args.language_eval)
    stats_dict_text_OUR = compute_quantities(gens_holder_text_OUR, language_eval = args.language_eval)
    print('DQE TEXT stats \n', stats_dict_text_DQE)
    print('^'*100)
    print('OUR TEXT stats \n', stats_dict_text_OUR)
    with open(f'{savepath}coverage_stats_text_DQE.json', 'w+') as f: json.dump(stats_dict_text_DQE, f)
    with open(f'{savepath}coverage_stats_text_OUR.json', 'w+') as f: json.dump(stats_dict_text_OUR, f)
    print('^'*100)
    scores_text_DQE_ref, scores_text_OUR_ref = compute_cross_autoscores(gens_holder_text_DQE, 
                                                gens_holder_text_OUR, language_eval = args.language_eval)
    print('CROSS-AUTO TEXT stats... ')
    print(scores_text_DQE_ref)
    print(scores_text_OUR_ref)
    with open(f'{savepath}cross_autoscores_text_DQE.json', 'w+') as f: json.dump(scores_text_DQE_ref, f)
    with open(f'{savepath}cross_autoscores_text_OUR.json', 'w+') as f: json.dump(scores_text_OUR_ref, f)

    print('#'*100, '\n')
    print('\n', '#'*100)
    # if not args.multilingual: # NOTE/TODO: need to update compute_quantities to load non-En WebNLG
    stats_dict_graph_DQE = compute_quantities(gens_holder_graph_DQE, language_eval = args.language_eval)
    stats_dict_graph_OUR = compute_quantities(gens_holder_graph_OUR, language_eval = args.language_eval)
    print('DQE GRAPH stats \n', stats_dict_graph_DQE)
    print('^'*100)
    print('OUR GRAPH stats \n', stats_dict_graph_OUR)
    with open(f'{savepath}coverage_stats_graph_DQE.json', 'w+') as f: json.dump(stats_dict_graph_DQE, f)
    with open(f'{savepath}coverage_stats_graph_OUR.json', 'w+') as f: json.dump(stats_dict_graph_OUR, f)
    print('^'*100)
    scores_graph_DQE_ref, scores_graph_OUR_ref = compute_cross_autoscores(gens_holder_graph_DQE, 
                                                gens_holder_graph_OUR, language_eval = args.language_eval)
    print('CROSS-AUTO GRAPH stats... ')
    print(scores_graph_DQE_ref)
    print(scores_graph_OUR_ref)
    with open(f'{savepath}cross_autoscores_graph_DQE.json', 'w+') as f: json.dump(scores_graph_DQE_ref, f)
    with open(f'{savepath}cross_autoscores_graph_OUR.json', 'w+') as f: json.dump(scores_graph_OUR_ref, f)
    print('#'*100, '\n')

    #### SAVE OUTPUTS ####
    for dictname in ['gens_holder_text_DQE', 'gens_holder_graph_DQE', 
                    'gens_holder_text_OUR', 'gens_holder_graph_OUR']:
        holder = globals()[dictname]
        for gem_id, coll in holder.items(): 
            for ans, qset in coll.items(): holder[gem_id][ans] = list(qset)
        with open(f'{savepath}{dictname}.json', 'w+') as f: json.dump(holder, f)

