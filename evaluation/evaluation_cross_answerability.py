import json, datasets, random, tqdm, datetime, os, time, copy, math, re, hashlib, torch
from functools import partial
from collections import defaultdict
import numpy as np
from torch.utils.data import DataLoader
PARENTDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0,PARENTDIR) 
from data_utils.shared import Entry, make_trip_elems
from utils.lightning_omnibus import OmnibusModel
from utils.modeling_omnibus import load_save_model
from utils.utils_evaluate import evaluate
from utils.utils_omnibus import Settings
from utils.utils import QTTLogger
from questeval.utils import (LinearizeWebnlgInput, Linearize_CQ_KELM_Input)
from concurrent.futures import ProcessPoolExecutor
import spacy
os.environ["TOKENIZERS_PARALLELISM"] = "false"
from transformers import T5Tokenizer, T5ForConditionalGeneration, logging
logging.set_verbosity_error() # silence transformers' 'weights not used' logging when loading BERTScore 

N_qa, RUNS = 2, 1 # N: number of questions per entry, M: number of runs through data
UNANSWERABLE_STR = 'unanswerable'

DEVICE = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
print('DEVICE BEING USED:', DEVICE) 
os.environ["CUBLAS_WORKSPACE_CONFIG"]=":4096:8" # solve RuntimeError: Deterministic behavior was enabled
BERTSCORE_OBJ = datasets.load_metric('bertscore', keep_in_memory = True)
'''
This script takes the set of questions produced by (i) DQE and (ii) our model on the WebNLG 2020 test set.
'''
def main(args):
    BSZ = args.bsz
    rnd_seed = 54506 + args.run_num
    model_key = f'_model{args.quartet_model_num}' 
    args.ckpt_num = ''
    if args.load_ckpt: 
        if args.multilingual_trained or args.non_english: args.ckpt_num += f"{args.exp_code_str}"
        else: 
            assert len(args.load_ckpt[1]) > 0
            args.ckpt_num = 'ckpt-epoch' + '8'
            if 'celosstasks' in args.load_ckpt[1]: args.ckpt_num += '-celosses'
            args.ckpt_num += f"-{args.exp_code_str}"
    rmv_key = f'_remove_inconsistent{args.inconsistent_cutoff}' if args.remove_inconsistent else ''
    dirpath = f'{PARENTDIR}/results/omnibus_model/'
    ans_zero = '_unanswerable_zero' if args.unanswerable_zero else ''
    bsc_ans_lc = '_bsc_ans_lc' if args.bsc_ans_lc else ''
    sampmode = f'_sampmode{args.sampling_mode}'
    if not args.non_english: args.mling_str = '' 
    else: 
        args.mling_str = f'-MULTILINGUAL'
        for lang in args.languages_trained: args.mling_str += f'-{lang}'
        args.mling_str += f'-on-{args.language_eval}'
    args.savepath = savepath = \
        f'{dirpath}cross_answerability_evaluation{model_key}{args.ckpt_num}{args.exp_code_str}{rmv_key}_runnum{str(args.run_num).zfill(2)}{ans_zero}{bsc_ans_lc}{sampmode}{args.mling_str}/'
    if args.trial_run: savepath = savepath[:-1] + '_trial_run/'
    if not os.path.exists(savepath): os.makedirs(savepath)    
    print('SAVEPATH:', savepath)

    ### 0. load WebNLG data, prep input for DQE and OUR
    if args.language_eval == 'en': 
        data_webnlg = datasets.load_dataset('GEM/web_nlg', 'en')['test']
    else: raise NotImplementedError 

    id2graph = {entry['gem_id']: entry['input'] for entry in data_webnlg}
    id2text = {entry['gem_id']: entry['target'] for entry in data_webnlg}
    # if bypass_dqegem True, we will use this to map DBpedia triples to Wikidata format (used in the training for trp_sep_scheme2 exps)
    d2w_dirpath = f'{PARENTDIR}/datasets/dqe_webnlg'
    with open(f'{d2w_dirpath}/z00_webnlg2020_dpb2wkdt_triples.json', encoding = 'utf-8') as f: d2w = json.load(f)
    # NOTE: WebNLG 2020 test has triples that have not been converted to Wikidata form in our earlier SQ work (WebNLG 2017)...
    # we use a similar approach as the WebNLG 2020 eval scipt to remove _ and camel case (except no lowercase)
    for dp_trip, wkdt_trip in d2w.items():
        c_underscore  = '_' in wkdt_trip
        c_camel = re.search(r'\w+[A-Z]\w+', 'wkdt_trip')
        if c_underscore or c_camel: 
            d2w[dp_trip] = remove_underscore_camelcase(wkdt_trip)
    assert len(id2graph) == len(id2text) == len(data_webnlg), (len(id2graph), len(id2text), len(data_webnlg))
    # for multilingual case, triples might be in different form (WebNLG 2.0)
    for graph in id2graph.values():
        for t in graph: 
            if t not in d2w: d2w[t] = remove_underscore_camelcase(t)

    ### 1. load DQE and OUR models
    MODELS_DQE, TOKENIZERS_DQE, MODELS_OUR, TOKENIZERS_OUR, OUR_ARGS = \
                    load_model_cross_ans(model_key, savepath, main_args = args)
    print('ALL MODELS AND TOKENIZERS LOADED...')

    ### 2. load the QA data for evaluation  
    # a. collect q and a
    global_gens_holder = {}
    gen_holders_savedpath = \
        f'{PARENTDIR}/results/omnibus_model/sqg_cqg_webnlg_coverage_evaluation{model_key}{args.ckpt_num}{args.mling_str}/'
    args.gen_holders_savedpath = gen_holders_savedpath
    print('gen_holders_savedpath', gen_holders_savedpath)
    for key in ['gens_holder_text_DQE', 'gens_holder_graph_DQE', 'gens_holder_text_OUR', 'gens_holder_graph_OUR']:
        with open(f'{gen_holders_savedpath}{key}.json', encoding = 'utf-8') as f: 
            global_gens_holder[key] = json.load(f)

    # create tokenized versions of datasets, in the 2 formats 
    global_tokenized_holder = {'tokenized_holder_text_DQE': {}, 'tokenized_holder_graph_DQE': {},
                               'tokenized_holder_text_OUR': {}, 'tokenized_holder_graph_OUR': {}}
    DIRPATH = f'{PARENTDIR}/results/quartet_human_eval'
    bypass_dqegem_str = "_bypass_dqegem" if args.bypass_dqegem else ""
    trial_run_str = "_trial" if args.trial_run else ""
    args.tok_holders_dirpath = f'{DIRPATH}/cross_ans_tokenized_holder'
    if not os.path.exists(args.tok_holders_dirpath): os.makedirs(args.tok_holders_dirpath)
    for origin in ['DQE', 'OUR']:
        for qst_src_type in ['text', 'graph']:
            print('WORKING ON DATA: ', origin, qst_src_type)

            gen_holder = global_gens_holder[f'gens_holder_{qst_src_type}_{origin}']
            tok_holder = global_tokenized_holder[f'tokenized_holder_{qst_src_type}_{origin}']
            if origin == 'DQE': # model_key and args.ckpt_num differentiation not applicable for DQE
                json_path = f'{DIRPATH}/cross_ans_tokenized_holder/tokenized_holder_{qst_src_type}_{origin}{bypass_dqegem_str}{trial_run_str}{args.mling_str}.jsonl'
            else: 
                json_path = f'{DIRPATH}/cross_ans_tokenized_holder/tokenized_holder_{qst_src_type}_{origin}{model_key}{args.ckpt_num}{bypass_dqegem_str}{trial_run_str}{args.exp_code_str}{args.mling_str}.jsonl'
            if os.path.exists(json_path):
                print('SAVED version of tokenized holders found... proceeding to load from here:', json_path)
                tok_holder = defaultdict(dict)
                with open(json_path, encoding = 'utf-8') as f:    
                    for l in f: 
                        line = json.loads(l)
                        gem_id = line.pop('gem_id')
                        form = line.pop('form')
                        entry = Cross_QA_Entry(**line)
                        if form not in tok_holder[gem_id]:
                            tok_holder[gem_id].update({form: [entry]})
                        else: 
                            tok_holder[gem_id][form].append(entry)
                if not args.trial_run: 
                    if args.language_eval == 'en': test_set_size = 1779
                    else: raise NotImplementedError                    
                    assert len(tok_holder) == test_set_size, (len(tok_holder), origin, qst_src_type)
                assert all([len(v)==4 for v in tok_holder.values()])
                assert all([len(v2)>=1 for v in tok_holder.values() for v2 in v.values()])
                print('NUM ENTRIES', sum([len(v2) for v in tok_holder.values() for v2 in v.values()]))
                global_tokenized_holder[f'tokenized_holder_{qst_src_type}_{origin}'] = tok_holder
            else:
                print('SAVED version of tokenized holders NOT found... proceeding to create.')
                max_workers = num_chunks = 8
                split_sz = math.ceil(len(gen_holder)/num_chunks)
                all_keys = list(gen_holder.keys())
                chunk_keys  = [all_keys[split_sz*(nc):split_sz*(1+nc)] for nc in range(num_chunks)]
                proc_chunks = [{k:gen_holder[k] for k in ck } for ck in chunk_keys]
                proc_chunks = [copy.deepcopy(i) for i in proc_chunks]
                func_batch_create = partial(batch_proc, args = copy.deepcopy(args),
                                            id2graph = copy.deepcopy(id2graph), 
                                            id2text = copy.deepcopy(id2text), 
                                            tokenizers_dqe = copy.deepcopy(TOKENIZERS_DQE), 
                                            tokenizers_our = copy.deepcopy(TOKENIZERS_OUR), 
                                            d2w = copy.deepcopy(d2w))

                with ProcessPoolExecutor(max_workers = max_workers) as executor:     
                    __data = executor.map(func_batch_create, proc_chunks)

                for chunk in __data: 
                    print('chunk', len(chunk))
                    for elem in chunk:
                        gem_id = elem.pop('gem_id')
                        tok_holder[gem_id] = {'DQE_text_form': [], 'DQE_graph_form': [], 
                                              'OUR_text_form': [], 'OUR_graph_form': []}
                        for f_s_t, entries in elem.items():
                            tok_holder[gem_id][f_s_t].extend(entries)
                
                # check if system has no QA pairs generated for a given g/t
                # NOTE: question set to empty string, answer set to unanswerable.
                linearizer_DQE, linearizer_OUR, c_l_cq_kelm_i = give_linearizers(args.language_eval,
                                                                args.bypass_dqegem, getattr(args, 'entry_obj', None))
                for _gem_id in id2graph.keys(): 
                    for (_origin, _src_type) in [['DQE', 'text'], ['DQE', 'graph'], 
                                                 ['OUR', 'text'], ['OUR', 'graph']]:
                        if not tok_holder[_gem_id][f'{_origin}_{_src_type}_form']:
                            print(f'NOTE: MISSING gem_ids for [{origin}_{qst_src_type}_on_{_origin}_{_src_type}]:', gem_id)
                            if _origin == 'DQE':
                                entry = prep4input_DQE('', 'unanswerable', _src_type, _gem_id, linearizer_DQE, 
                                    id2graph, id2text, TOKENIZERS_DQE, check_cq_kelm_linearizer = c_l_cq_kelm_i)
                            elif _origin == 'OUR':
                                entry = prep4input_OUR('', 'unanswerable', _src_type, _gem_id, linearizer_OUR, 
                                    id2graph, id2text, TOKENIZERS_OUR, prmpt_sep = '[Sp1]', inp_sep = '[INP]', 
                                    bypass_dqegem_settings = (False, None))
                        
                            tok_holder[_gem_id][f'{_origin}_{_src_type}_form'].append(entry)

                global_tokenized_holder[f'tokenized_holder_{qst_src_type}_{origin}'] = tok_holder
                
                with open(json_path, encoding = 'utf-8', mode = 'w+') as f:
                    for gem_id, coll in tok_holder.items(): 
                        for form, entries in coll.items():
                            for entry in entries:
                                entry = entry.__dict__
                                entry['gem_id'] = gem_id
                                entry['form'] = form
                                f.write(f'{json.dumps(entry)}\n')
    print('ALL DATA LOADED...')

    ### 4. set up procedure for sampling Nx questions per WebNLG entry
    print('BEGINNING INFERENCE...')
    outputs_dict = defaultdict(dict)
    # pool_sizes: for use in sampling_mode == 2 (avoid running ans_self_remove_inconsistent_from_coll on other repeatedly)
    pool_sizes, pool_sizes_filt = {'text': {}, 'graph': {} }, {'text': {}, 'graph': {} }     
    for run in range(0+args.run_num, RUNS+args.run_num):
        print('WORKING ON RUN: ', run)
        # set random state
        random.seed(rnd_seed)
        # sample Nx questions from each
        for origin in ['DQE', 'OUR']:
            for src_type in ['text', 'graph']:
                tok_holder, other_origin, other_tok_holder = \
                        get_prelimaries_holders(src_type, origin, global_tokenized_holder)
                sample_tok_holder, sample_tok_holder_filt = defaultdict(dict), defaultdict(dict)
                no_questions, no_questions_filt, ctr_empty_qa = [], [], 0
                for __, (gem_id, coll) in enumerate(tqdm.tqdm(tok_holder.items())):
                    if args.trial_run and __> 20: continue 
                    coll_copy = copy.copy(coll)
                    other_coll_copy = copy.copy(other_tok_holder[gem_id])

                    models, tokenizers, other_models, other_tokenizers, gen_args = \
                        get_prelimaries_model_tokenizer(origin, src_type, MODELS_DQE, MODELS_OUR,
                                                TOKENIZERS_DQE, TOKENIZERS_OUR, OUR_ARGS)
                    
                    coll_copy_filt, ans_self_filt, ans_self, ctr_empty_qa = \
                        ans_self_remove_inconsistent_from_coll(coll_copy, gem_id, origin, src_type, 
                                                    models, tokenizers, args, gen_args, DEVICE, ctr_empty_qa)
                    if coll_copy_filt is None: 
                        print('\t\t NOTE: -- coll_copy_filt is None for:', gem_id, coll)
                        print(f'\t\t       -- origin | other_origin: {f"{origin}_{src_type}"} | {other_origin}')
                    
                    # Example: tok_holder[gem_id][f'OUR_{form_src_type}_form'].append(entry_our) 
                    assert len(set(len(i) for i in coll_copy_filt.values())) == 1 # check all same length
                    
                    qs_idxes = list(range(len(coll_copy[f'{origin}_{src_type}_form'])))
                    assert len(ans_self) == len(qs_idxes), (len(ans_self), len(qs_idxes))
                    qs_idxes_filt = list(range(len(coll_copy_filt[f'{origin}_{src_type}_form'])))
                    assert len(ans_self_filt) == len(qs_idxes_filt)
                    
                    pick_size, pick_size_filt, pool_sizes, pool_sizes_filt = \
                        get_pick_size(args, N_qa, gem_id, origin, src_type, pool_sizes, pool_sizes_filt,
                                      other_coll_copy, other_origin, other_models, other_tokenizers, 
                                      gen_args, qs_idxes, qs_idxes_filt)

                    ### UNFILTERED
                    c_add_sth, sample_tok_holder = fill_sample_tok_holder(qs_idxes, pick_size, 
                                                        gem_id, coll_copy, sample_tok_holder, ans_self)
                    if not c_add_sth: no_questions.append(gem_id)
                    
                    ### FILTERED
                    c_add_sth_filt, sample_tok_holder_filt = fill_sample_tok_holder(qs_idxes_filt, pick_size_filt, 
                                                    gem_id, coll_copy_filt, sample_tok_holder_filt, ans_self_filt)
                    if not c_add_sth_filt: no_questions_filt.append(gem_id)
                print(f'\t\t{"ctr_empty_qa".upper()}', gem_id, origin, src_type, ctr_empty_qa)
                forms = ['DQE_text', 'DQE_graph', 'OUR_text', 'OUR_graph']
                for filtstr, s_t_holder, no_qs in \
                    (('', sample_tok_holder, no_questions), 
                    (f'_filt{args.inconsistent_cutoff}', sample_tok_holder_filt, no_questions_filt)):
                    for form in forms:
                        savepath_scores = f'{savepath}cross_answerability_scores_gens_tgt.json'
                        c_bypass_dqe_on_dqe = args.bypass_dqe_on_dqe and ((origin == 'DQE' ) and ('DQE' in form))
                        if os.path.exists(savepath_scores):
                            with open(savepath_scores, encoding = 'utf-8') as f: 
                                outputs_dict = json.load(f)
                            c_key_present = f'{origin}_{src_type}_on_{form}{filtstr}' in outputs_dict[str(run)]
                            if (c_key_present and outputs_dict[str(run)][f'{origin}_{src_type}_on_{form}{filtstr}']) \
                                or c_bypass_dqe_on_dqe: 
                                print(f'Either (i) found key in cached results and non-empty \
                                      [{c_key_present}, {origin}_{src_type}_on_{form}{filtstr}], \
                                      or (ii) bypass_dqe_on_dqe is True [{c_bypass_dqe_on_dqe}]... skipping...')
                                continue

                        if c_bypass_dqe_on_dqe:
                            print(f'BYPASSS activated. {origin}_{src_type} on FORM: ', form, 'not being computed.')
                            outputs_dict[str(run)].update({f'{origin}_{src_type}_on_{form}{filtstr}': {}})
                            continue
                        
                        print(f'WORKING ON {origin}_{src_type} on FORM: ', form, 'FILTERED' if filtstr else 'UNFILTERED')
                        # extract the right format... flatten

                        data2use, ans_self2use = [], []
                        for i in s_t_holder.values():
                            for i2 in i[f'{form}_form']:
                                data2use.append(i2[0])
                                ans_self2use.append(i2[1])
                        assert len(data2use) == len(ans_self2use), (len(data2use), len(ans_self2use))
                        print(f'\t\t>>> {form.upper()} data2use size', len(data2use), 
                              'FILTERED' if filtstr else 'UNFILTERED')
                        test_score_gt, test_score_crossmod_selfsys, gens_all, tgt_all, src_all, gem_ids_all, runtime, \
                            test_score_gt_finer, test_score_crossmod_selfsys_finer = \
                            run_inference(data2use, ans_self2use, form = form, 
                                        unanswerable_zero = args.unanswerable_zero,
                                        models_dqe = MODELS_DQE, models_our = MODELS_OUR, 
                                        tokenizer_dqe = TOKENIZERS_DQE, tokenizer_our = TOKENIZERS_OUR, 
                                        our_args = OUR_ARGS, bsz = BSZ, bsc_ans_lc = args.bsc_ans_lc,
                                        finer_grained = args.finer_grained, ablate_key = args.ablate_key)
                        
                        outputs_dict[str(run)].update({f'{origin}_{src_type}_on_{form}{filtstr}': 
                                        {'test_score_groundtruth': test_score_gt, 
                                        'test_score_crossmod_selfsys': test_score_crossmod_selfsys,
                                        'test_score_groundtruth_finer': test_score_gt_finer,  
                                        'test_score_crossmod_selfsys_finer': test_score_crossmod_selfsys_finer,
                                        'runtime': runtime, 'gens_all': gens_all, 'tgt_all': tgt_all, 
                                        'src_all': src_all, 'ans_self': ans_self2use, 'gem_ids_all': gem_ids_all,
                                        'no_questions': no_qs}})

                        with open(savepath_scores, 'w+') as f: 
                            json.dump(outputs_dict, f)
                        print(f'\t\tScores and outputs saved for {origin}_{src_type} on FORM: ', 
                                form, 'FILTERED' if filtstr else 'UNFILTERED')

    print('ALL RUNS AND SCORING COMPLETED...')
    print('STARTING SAVE...')
    
    for src_type in ['text', 'graph']:
        exp_config = vars(OUR_ARGS[src_type])
        exp_config['logger'] = 'removed'
        with open(f'{savepath}{src_type}_exp_configs.json', 'w+') as f: json.dump(exp_config, f)
    print('Exp config saved!!')
    with open(f'{savepath}cross_answerability_scores_gens_tgt.json', 'w+') as f: json.dump(outputs_dict, f)
    print('All scores and outputs saved!!')
    with open(f'{savepath}success.txt', 'w+') as f: f.write('all origins and src_types done!')

def get_prelimaries_holders(src_type, origin, global_tokenized_holder):
    tok_holder = global_tokenized_holder[f'tokenized_holder_{src_type}_{origin}']
    other_origin = 'DQE' if origin == 'OUR' else 'OUR'
    other_tok_holder = global_tokenized_holder[f'tokenized_holder_{src_type}_{other_origin}']
    
    return tok_holder, other_origin, other_tok_holder

def get_prelimaries_model_tokenizer(origin, src_type, models_dqe, models_our,
                                tokenizers_dqe, tokenizers_our, our_args):
    models = models_dqe if origin == 'DQE' else models_our
    tokenizers = tokenizers_dqe if origin == 'DQE' else tokenizers_our
    other_models = models_dqe if origin == 'OUR' else models_our
    other_tokenizers = tokenizers_dqe if origin == 'OUR' else tokenizers_our
    gen_args = our_args[src_type].settings['gen_args']
    
    return models, tokenizers, other_models, other_tokenizers, gen_args

def get_pick_size(args, N_qa, gem_id, origin, src_type, pool_sizes, pool_sizes_filt,
                  other_coll_copy, other_origin, other_models, other_tokenizers, 
                  gen_args, qs_idxes, qs_idxes_filt):
    if args.sampling_mode == 1: pick_size = N_qa
    elif args.sampling_mode == 2: 
        # determine the minimum of the sizes of both sides (DQE and OUR)
        
        ### UNFILTERED
        if gem_id in pool_sizes[src_type]: pick_size = pool_sizes[src_type][gem_id]
        else:
            pick_size = min(len(other_coll_copy[f'{origin}_{src_type}_form']), len(qs_idxes))
            pool_sizes[src_type][gem_id] = pick_size
        
        ### FILTERED
        if gem_id in pool_sizes_filt[src_type]: pick_size_filt = pool_sizes_filt[src_type][gem_id]
        else:
            other_coll_copy_filt, *__ = ans_self_remove_inconsistent_from_coll(other_coll_copy, 
                                    gem_id, other_origin, src_type, other_models, other_tokenizers, 
                                    args, gen_args, DEVICE, None)
            pick_size_filt = min(len(other_coll_copy_filt[f'{origin}_{src_type}_form']), len(qs_idxes_filt))
            pool_sizes_filt[src_type][gem_id] = pick_size_filt
    else: raise NotImplementedError
    
    return pick_size, pick_size_filt, pool_sizes, pool_sizes_filt

def fill_sample_tok_holder(qs_idxes, pick_size, gem_id, coll_copy, sample_tok_holder, ans_self):
    if len(qs_idxes) >= pick_size: pick_idxes = random.sample(qs_idxes, pick_size)
    elif len(qs_idxes) == 0: 
        return False, sample_tok_holder
    else: 
        if args.sampling_mode == 2: 
            raise ValueError('Check set up. Sample mode 2 pick size should match or be larger')
        else: 
            pick_idxes = qs_idxes
            while len(pick_idxes) < pick_size:
                pick_idxes += pick_idxes[: pick_size - len(qs_idxes)]
    # drawn from pool for each form, store in sample_tok_holder
    assert len(pick_idxes) == pick_size
    for key in coll_copy.keys(): 
        sample_tok_holder[gem_id][key] = \
            [(e, ans_self[i]) for i, e in enumerate(coll_copy[key]) if i in pick_idxes]
    
    return True, sample_tok_holder

def ans_self_remove_inconsistent_from_coll(coll, gem_id, origin, src_type, models, tokenizers, 
                        args, gen_args, device, ctr_empty_qa = None): 
    '''
    given a set of entries containing QA pairs, do the following:
    1. answer the question on its own source context (e.g. Q_t on T)
    2. check whether 
    (i) the questions can be answered from the original input; 
    (ii) whether the answer has a BERTScore of over inconsistent_cutoff (e.g. 0.7) against the ground truth
    if cutoff is set such that all questions would get rejected, no filtering is done (i.e. the entire 
    original set is returned) to avoid BSC errors.
    '''
    c_our = origin == 'OUR'
    # 2. get the right form to use 
    coll_selfform = coll[f'{origin}_{src_type}_form']

    if not c_our and args.bypass_dqe_on_dqe:
        # check if saved version exists... 
        unanszero_str = "_unanszero" if args.unanswerable_zero else ""
        trial_str = "_trial" if args.trial_run else ""
        fp_dqe_filtered_cache = \
            os.path.join(args.tok_holders_dirpath, 
            f'filtered_cache_{origin}_{src_type}_rmv{args.inconsistent_cutoff}{unanszero_str}{trial_str}{args.mling_str}.json')
        if os.path.exists(fp_dqe_filtered_cache):
            with open(fp_dqe_filtered_cache, encoding = 'utf-8') as f: dqe_filtered_cache = json.load(f)
            if gem_id in dqe_filtered_cache:
                cached_dict = dqe_filtered_cache[gem_id]
                hash_keys = [e.hash_key for e in coll_selfform]
                # check set of saved keys and current keys match
                assert set(hash_keys) == set(cached_dict['coll_hashes']), \
                            (hash_keys, cached_dict['coll_hashes'])
                cached_coll_filtered_hashes = set(cached_dict['coll_filtered_hashes'])
                cached_ans_self_dict = cached_dict['ans_self_dict']

                # use the hash_key to ensure the same order
                coll_filtered = {k: [] for k in coll}
                for form, entries_set in coll.items():
                    coll_filtered[form] = [e for e in entries_set if e.hash_key in cached_coll_filtered_hashes]
                ans_self_filtered = [cached_ans_self_dict[e.hash_key] for e in coll_selfform \
                                    if e.hash_key in cached_coll_filtered_hashes]
                ans_self = [cached_ans_self_dict[e.hash_key] for e in coll_selfform]
                return coll_filtered, ans_self_filtered, ans_self, ctr_empty_qa
            else: pass
        else: pass
   
    # 1. get the right model
    model_self, tokenizer_self = models[src_type], tokenizers[src_type]
    model_self.to(device)
    model_self.eval()
    collate_fn = Collate(pad_token_id = tokenizer_self.pad_token_id, 
                        ipstrat2idx = getattr(model_self, 'ipstrat2idx', None)).collate_fn
    dataloader = DataLoader(coll_selfform, shuffle = False, batch_size = len(coll_selfform), 
                            collate_fn = collate_fn)

    # NOTE: there is only 1 batch
    assert len(dataloader) == 1, (len(dataloader), len(coll_selfform), )
    for batch in dataloader: 
        src, __, src_self, __, tgt, __, ipstrat_enc, __, hash_key_enc = batch
        tgt_self = tgt.clone().detach()
        
        with torch.no_grad():

            gens_self = model_self.generate(src.to(device), **gen_args)

    decode_func = decode_OUR if c_our else decode_DQE
    gens_self, tgt_self, __, __ = decode_func(gens_self, tgt_self, None, None, 
                                            tokenizer_self, args.unanswerable_zero, args.bsc_ans_lc)

    assert args.inconsistent_cutoff is not None and 0.0 < args.inconsistent_cutoff < 1.0
    gens_self_bscore = calculate_BERTScore_rescaled(gens_self, tgt_self, BERTSCORE_OBJ,
                        args.unanswerable_zero, unanswerable_str = UNANSWERABLE_STR,
                        lang = args.language_eval, bert_score_rescale_with_baseline = True,)
    # 1. remove if bsc below threshold
    to_remove = set(i for i, x in enumerate(gens_self_bscore) if x < args.inconsistent_cutoff)
    # 2. remove if 'unanswerable' (i.e. inconsistent)... 
    # a. == '' applies if unanswerable_zero set to True (decode_func will replace UNANSWERABLE_STR with '')
    # b. other case applicable if unanswerable_zero == False 
    to_remove.update(set(i for i, x in enumerate(gens_self) if x == '' or x == UNANSWERABLE_STR))

    # if no QA pair for a given g/t remains after filtering, keep last (else error)
    if len(to_remove) == len(gens_self): 
        print('\t\t EMPTY QA set (using last): gem_id, origin, src_type', gem_id, origin, src_type, f'{origin}_{src_type}_form')
        if ctr_empty_qa is not None: ctr_empty_qa += 1
        to_remove = set(list(to_remove)[:-1])

    ans_self_filtered = [g for i, g in enumerate(gens_self) if i not in to_remove]

    coll_filtered = {k: [] for k in coll}
    for form, entries_set in coll.items():
        coll_filtered[form] = [e for i, e in enumerate(entries_set) if i not in to_remove]
    
    ans_self = gens_self

    if not c_our and args.bypass_dqe_on_dqe:
        hash_keys = [tokenizer_self.decode(hk) for hk in hash_key_enc]
        hash_keys_filtered = [hk for i, hk in enumerate(hash_keys) if i not in to_remove]
        assert len(hash_keys) == len(ans_self) and len(hash_keys_filtered) == len(ans_self_filtered), \
            (len(hash_keys), len(ans_self), len(hash_keys_filtered), len(ans_self_filtered))
        # print('Saving cached version of filtered DQE questions')
        if os.path.exists(fp_dqe_filtered_cache):
            with open(fp_dqe_filtered_cache, encoding = 'utf-8') as f: dqe_filtered_cache = json.load(f)
        else: dqe_filtered_cache = defaultdict(dict)
        dqe_filtered_cache[gem_id] = {'coll_hashes': hash_keys, 
                                'coll_filtered_hashes': hash_keys_filtered,
                                'ans_self_dict': {hk: a_s for hk, a_s in zip(hash_keys, ans_self)}}
        
        with open(fp_dqe_filtered_cache, encoding = 'utf-8', mode = 'w+') as f:
                json.dump(dqe_filtered_cache, f)
        # print('Cached version of filtered DQE questions saved to: ', fp_dqe_filtered_cache)
    return coll_filtered, ans_self_filtered, ans_self, ctr_empty_qa

class Cross_QA_Entry:
    def __init__(self, src, src_self, tgt, id, id_enc, pad_token_id, hash_key, hash_key_enc,
                ipstrat = None, qg_ctrl_vals = 'None'):
        # src_self, being format to ask the question on the input it was conditioned on 
        self.src, self.src_self, self.tgt, self.id, self.id_enc = src, src_self, tgt, id, id_enc
        self.pad_token_id = pad_token_id
        self.hash_key, self.hash_key_enc = hash_key, hash_key_enc
        self.ipstrat = ipstrat
        self.qg_ctrl_vals = qg_ctrl_vals

# b. prep4input func for DQE
def prep4input_DQE(question, answer, src_type, id, linearizer_DQE, id2graph, id2text,
                    tokenizers_dqe, inp_sep = '</s>', check_cq_kelm_linearizer = False):
    '''
    NOTE: Linearizer_CQ_KELM_Input requires triples to be a list of [s, p, o]. 
    Linearizer_WebNLG_Input requires triples to be string of 's | p |o'
    '''
    context = id2graph[id] if src_type == 'graph' else id2text[id]
    context_self = id2text[id] if src_type == 'graph' else id2graph[id]
    # NOTE: added 22 Nov (vals retrieved at evaluation_coverage)
    __ = answer.split('<SEP>')
    if len(__) == 2: answer, qg_ctrl_vals = __ 
    else: qg_ctrl_vals = 'None'

    if src_type == 'text':
        # https://github.com/ThomasScialom/QuestEval/blob/main/questeval/questeval_metric.py
        src = f'{question} {inp_sep} {context}'
        context_self = linearizer_DQE([t.split(' | ') for t in context_self] \
                                      if check_cq_kelm_linearizer else context_self)
        src_self = f'{question} {inp_sep} {context_self}'
    
    elif src_type == 'graph':
        context = linearizer_DQE([t.split(' | ') for t in context] \
                                      if check_cq_kelm_linearizer else context)
        src = f'{question} {inp_sep} {context}'
        src_self = f'{question} {inp_sep} {context_self}'
    
    else: raise ValueError('Check src_type')
    
    hash_key = hashlib.sha512(f'{id}{question}{answer}'.encode('utf-8')).hexdigest()

    tokenizer = tokenizers_dqe[src_type]
    return Cross_QA_Entry(src = tokenizer.encode(src, truncation = True, max_length = args.tokenizer_maxlength), 
                        src_self = tokenizer.encode(src_self, truncation = True, max_length = args.tokenizer_maxlength), 
                        tgt = tokenizer.encode(answer, truncation = True, max_length = args.tokenizer_maxlength),
                        id = id, id_enc = tokenizer.encode(id),
                        pad_token_id = tokenizer.pad_token_id,
                        ipstrat = None, hash_key = hash_key, 
                        hash_key_enc =  tokenizer.encode(hash_key, add_special_tokens = False), 
                        qg_ctrl_vals = tokenizer.encode(qg_ctrl_vals, add_special_tokens = False),)

# b. prep4input func for OUR 
def prep4input_OUR(question, answer, src_type, id, linearizer_OUR, id2graph, id2text,
                    tokenizers_our, prmpt_sep = '[Sp1]', inp_sep = '[INP]', bypass_dqegem_settings = (False, None)):
    c_bypass_dqegem, d2w = bypass_dqegem_settings
    context = id2graph[id] if src_type == 'graph' else id2text[id]
    context_self = id2text[id] if src_type == 'graph' else id2graph[id]

    # NOTE: added 22 Nov (vals retrieved at evaluation_coverage)
    __ = answer.split('<SEP>')
    if len(__) == 2: answer, qg_ctrl_vals = __ 
    else: qg_ctrl_vals = 'None'

    if src_type == 'text':
        prmpt = 'textqa task'
        src = f'{prmpt}{prmpt_sep} {question}{inp_sep} {context}'
        if c_bypass_dqegem: context_self = [d2w[t] for t in context_self]
        context_self = linearizer_OUR([t.split(' | ') for t in context_self])
        src_self = f'{prmpt}{prmpt_sep} {question}{inp_sep} {context_self}'
    
    elif src_type == 'graph':
        prmpt = 'kbqa task'
        if c_bypass_dqegem: context = [d2w[t] for t in context]
        context = linearizer_OUR([t.split(' | ') for t in context])
        src = f'{prmpt}{prmpt_sep} {question}{inp_sep} {context}'
        src_self = f'{prmpt}{prmpt_sep} {question}{inp_sep} {context_self}'
    
    else: raise ValueError('Check src_type')

    hash_key = hashlib.sha512(f'{id}{question}{answer}'.encode('utf-8')).hexdigest()

    tokenizer = tokenizers_our[src_type]
    return Cross_QA_Entry(src = tokenizer.encode(src, truncation = True, max_length = args.tokenizer_maxlength), 
                        src_self = tokenizer.encode(src_self, truncation = True, max_length = args.tokenizer_maxlength), 
                        tgt = tokenizer.encode(answer, truncation = True, max_length = args.tokenizer_maxlength),
                        id = id, id_enc = tokenizer.encode(id),
                        pad_token_id = tokenizer.pad_token_id,
                        ipstrat = 'textqa' if src_type == 'text' else 'kbqa', hash_key = hash_key, 
                        hash_key_enc = tokenizer.encode(hash_key, add_special_tokens = False), 
                        qg_ctrl_vals = tokenizer.encode(qg_ctrl_vals, add_special_tokens = False))

def give_linearizers(language_eval, bypass_dqegem = False, entry_obj = None):
    if language_eval == 'en': 
        spacy_pipeline = spacy.load('en_core_web_sm')
        linearizer_OUR = Linearize_CQ_KELM_Input(spacy_pipeline = spacy_pipeline, tripend_char = '[TEND]', lowercase = False)
        linearizer_DQE = LinearizeWebnlgInput(spacy_pipeline = spacy_pipeline, lowercase = False)
        c_l_cq_kelm_i = False
    else: raise NotImplementedError
    
    return linearizer_DQE, linearizer_OUR, c_l_cq_kelm_i

def batch_proc(batch_gen_holder, args, id2graph, id2text, tokenizers_dqe, tokenizers_our, d2w = None):
    linearizer_DQE, linearizer_OUR, c_l_cq_kelm_i = give_linearizers(args.language_eval,
                                                    args.bypass_dqegem, getattr(args, 'entry_obj', None))

    holder_list = []
    for __, (gem_id, qa_pairs) in enumerate(tqdm.tqdm(batch_gen_holder.items())):

        if args.trial_run and __> 20: break 

        holder = {'DQE_text_form': [], 'DQE_graph_form': [], 
                  'OUR_text_form': [], 'OUR_graph_form': [],
                  'gem_id': gem_id}
                            
        for __2, (answer, qset) in enumerate(qa_pairs.items()):
            answer = answer.strip()
            for question in qset:
                if question == '': continue
                for form_src_type in ['text', 'graph']:
                    # create DQE form
                    entry_dqe = prep4input_DQE(question, answer, form_src_type, gem_id, linearizer_DQE, 
                                id2graph, id2text, tokenizers_dqe, check_cq_kelm_linearizer = c_l_cq_kelm_i)
                    holder[f'DQE_{form_src_type}_form'].append(entry_dqe)
                    if __ == 0 and __2 == 0: 
                        tter = tokenizers_dqe[form_src_type]
                        print('SAMPLE: <DQE>', 
                        '\n\t SRC', tter.decode(entry_dqe.src), 
                        '\n\t src_self', tter.decode(entry_dqe.src_self), 
                        '\n\t TGT', tter.decode(entry_dqe.tgt), '\n')
                    # create OUR form 
                    entry_our = prep4input_OUR(question, answer, form_src_type, gem_id, linearizer_OUR, 
                                id2graph, id2text, tokenizers_our, 
                                bypass_dqegem_settings = (args.bypass_dqegem, d2w))
                    holder[f'OUR_{form_src_type}_form'].append(entry_our)
                    if __ == 0 and __2 == 0: 
                        tter = tokenizers_our[form_src_type]
                        print('SAMPLE: <OUR>', 
                        '\n\t SRC', tter.decode(entry_our.src), 
                        '\n\t SRC SELF', tter.decode(entry_our.src_self), 
                        '\n\t TGT', tter.decode(entry_our.tgt), '\n')
        holder_list.append(holder)
    return holder_list


def _strip_special_tokens(text, kqa_task = 'kbqa task', tqa_task = 'textqa task', eos = '</s>', pad = '<pad>'):
    return text.replace(kqa_task, '').replace(tqa_task, '').replace(eos, '').replace(pad, '').strip()


def decode_DQE(gens, tgt, src, id_enc, tokenizer, unanswerable_zero, bsc_ans_lc = False):
    gens = tokenizer.batch_decode(gens, skip_special_tokens = True, clean_up_tokenization_spaces = True)
    tgt = tokenizer.batch_decode(tgt, skip_special_tokens = True, clean_up_tokenization_spaces = True)

    if src is not None: 
        src = tokenizer.batch_decode(src, skip_special_tokens = True, clean_up_tokenization_spaces = True)
    if id_enc is not None:
        id_enc = tokenizer.batch_decode(id_enc, skip_special_tokens = True, clean_up_tokenization_spaces = True)

    if unanswerable_zero: ['' if g == UNANSWERABLE_STR else g for g in gens]
    if bsc_ans_lc: gens, tgt = [i.lower() for i in gens], [i.lower() for i in tgt]
    return gens, tgt, src, id_enc


def decode_OUR(gens, tgt, src, id_enc, tokenizer, unanswerable_zero, bsc_ans_lc = False):
    gens = [i.split('[Sp1]')[1].replace('</s>', '').replace('<pad>', '').strip() \
            if '[Sp1]' in i else _strip_special_tokens(i) for i in tokenizer.batch_decode(gens)]
    tgt = [_strip_special_tokens(i) for i in tokenizer.batch_decode(tgt)]
    if src is not None: 
        src = [i.replace('</s>', '').replace('<pad>', '').strip() for i in tokenizer.batch_decode(src)]
    if id_enc is not None:
        id_enc = tokenizer.batch_decode(id_enc, skip_special_tokens=True)
    if unanswerable_zero: ['' if g == UNANSWERABLE_STR else g for g in gens]
    if bsc_ans_lc: gens, tgt = [i.lower() for i in gens], [i.lower() for i in tgt]
    return gens, tgt, src, id_enc

class Args:
    def __init__(self, settings_code, settings):
        self.settings_code = settings_code
        self.settings = settings 

def load_model_cross_ans(model_key, savepath, main_args): 
    ablate_key = main_args.ablate_key
    multilingual_trained, languages_trained = main_args.multilingual_trained, main_args.languages_trained
    language_eval, load_ckpt, non_english = main_args.language_eval , main_args.load_ckpt, main_args.non_english
    ### 1. load DQE and OUR model
    # a. DQE
    lang = language_eval
    if lang in ['en']:
        models_dqe, tokenizers_dqe = {}, {}
        tokenizers_dqe['text']  = T5Tokenizer.from_pretrained("ThomasNLG/t5-qa_squad2neg-en")
        models_dqe['text']      = T5ForConditionalGeneration.from_pretrained("ThomasNLG/t5-qa_squad2neg-en")
        tokenizers_dqe['graph'] = T5Tokenizer.from_pretrained("ThomasNLG/t5-qa_webnlg_synth-en")
        models_dqe['graph']     = T5ForConditionalGeneration.from_pretrained("ThomasNLG/t5-qa_webnlg_synth-en")
    else: raise NotImplementedError

    # b. OUR - QUARTET
    models_our, tokenizers_our = {}, {}
    args = {}
    for src_type in ['text', 'graph']:
        if src_type == 'text': 
            settings_code = f'quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10{model_key}{ablate_key}'
            m_args = Args(settings_code = settings_code, settings = getattr(Settings(), settings_code))
        elif src_type == 'graph': 
            settings_code = f'quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10{model_key}{ablate_key}'
            m_args = Args(settings_code = settings_code, settings = getattr(Settings(), settings_code))
        try: m_args.settings['gen_args'].pop('min_length')
        except: pass
        if multilingual_trained or non_english: 
            m_args.multilingual = multilingual_trained
            m_args.model = m_args.settings['model'] = 'google/mt5-small'

        m_args.non_english = non_english
        m_args.multilingual_trained = multilingual_trained
        m_args.languages_trained = languages_trained
        m_args.settings['gen_args']['max_length'] = 150 # avoid OOM. no reason for > 150... if so likely degenerate
        m_args.savepath = savepath
        m_args.logger = QTTLogger(m_args.savepath, main_process = True) 
        args[src_type] = m_args
        
        m_args.load_ckpt = load_ckpt
        c_load_ckpt = load_ckpt and len(load_ckpt) == 2 and load_ckpt[1] != ''
        if c_load_ckpt: # for using load checkpoint, hits load_save_model
            m_args.test_only = True 
            m_args.ds_config = {'bf16':{'enabled': True}} # NOTE: hardcode to True, unlikely to use False for T5
            
            # NOTE: at load_save_model, the ckpt exp_configs will be loaded and set up will ensure 
            # the same (add) as training (i.e. match the naming in the ckpt state_dict) is used
            __m = load_save_model(load_ckpt[0], load_ckpt[1], m_args, test_phase = True)
        else: 
            __m = OmnibusModel(args = m_args, test_phase = True) 
        
        if not ablate_key: # not needed for ablation_xadapters 
            __m.model.active_head == 'lm_head'
            print('ACTIVE HEAD', __m.model.active_head)
            print('ACTIVE ADAPTER', __m.model.active_adapters)
        models_our[src_type] = __m.model
        tokenizers_our[src_type] = __m.tokenizer

    return models_dqe, tokenizers_dqe, models_our, tokenizers_our, args

# 4. funcs to run QA, evaluate against stored answers
def run_inference(data, ans_self, form, unanswerable_zero, models_dqe, models_our, 
                    tokenizer_dqe, tokenizer_our, our_args, bsz, bsc_ans_lc, finer_grained = True,
                    ablate_key = ''):
    modelgrp, src_type = form.split('_')
    if 'DQE' == modelgrp:
        model, tokenizer = models_dqe[src_type], tokenizer_dqe[src_type]
    elif 'OUR' == modelgrp: 
        model, tokenizer = models_our[src_type], tokenizer_our[src_type]
    collate_fn = Collate(pad_token_id = tokenizer.pad_token_id, 
                        ipstrat2idx = getattr(model, 'ipstrat2idx', None)).collate_fn
    # NOTE: shuffle == False so ans_self stays in same order
    dataloader = DataLoader(data, shuffle = False, batch_size = bsz, collate_fn = collate_fn)

    gen_args = our_args[src_type].settings['gen_args']
    bert_score_model = our_args[src_type].settings['bert_score_model']
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    gens_all, tgt_all, src_all, gem_ids_all, qg_ctrl_vals_all = [], [], [], [], []
    model.to(device)
    model.eval()
    print(f'\t\t>>> RUNNING THROUGH: {form}')
    runtimes = []
    for idx, batch in enumerate(tqdm.tqdm(dataloader)):
        src, __, src_self, __, tgt, id_enc, ipstrat_enc, qg_ctrl_vals_enc, hash_key_enc = batch
        starttime = time.time()  
        with torch.no_grad():
            c_our = modelgrp == 'OUR'
            gens = model.generate(src.to(device), **gen_args)    
        # get avg run time for 1 qa-pair to be answered.
        runtimes.append((time.time()-starttime)/src.shape[0])
        
        decode_func = decode_DQE if 'DQE' == modelgrp else decode_OUR
        gens, tgt, src, id_enc = decode_func(gens, tgt, src, id_enc, 
                                                tokenizer, unanswerable_zero, bsc_ans_lc)

        gens_all.extend(gens)
        tgt_all.extend(tgt)
        src_all.extend(src)
        gem_ids_all.extend(id_enc)
        if 'OUR' == modelgrp and finer_grained == True: 
            qg_ctrl_vals_all.extend([tokenizer.decode(val) for val in qg_ctrl_vals_enc])
    
    test_score_gt = evaluate(tgt_all, gens_all,  gentypes = ['normal'], 
                             reftypes = ['single'], tgt_strategy = 'NA',
                            auto_scores = ['bertscore'],
                            bert_score_model = bert_score_model,
                            lang = args.language_eval, 
                            bert_score_rescale_with_baseline = True,
                            eos_token = tokenizer.eos_token)
    assert len(gens_all) == len(ans_self), (len(gens_all), len(ans_self))
    test_score_crossmod_selfsys = evaluate(ans_self, gens_all, gentypes = ['normal'], 
                            reftypes = ['single'], tgt_strategy = 'NA',
                            auto_scores = ['bertscore'],
                            bert_score_model = bert_score_model,
                            lang = args.language_eval, 
                            bert_score_rescale_with_baseline = True,
                            eos_token = tokenizer.eos_token)

    print('test_score_gt', test_score_gt)
    print('test_score_crossmod_selfsys', test_score_crossmod_selfsys)

    # NOTE: added 22 Nov (finer grained evaluation)
    test_score_gt_finer, test_score_crossmod_selfsys_finer = {}, {}
    if 'OUR' == modelgrp and finer_grained == True: 
        print('WORKING ON FINER-GRAINED EVALUATION FOR <OUR>...')
        template = {}
        key_set = ['gens_all', 'tgt_all', 'src_all', 'ans_self']
        for key in key_set: template[key] = []
        
        finer_holder = defaultdict()
        # zip the outputs 
        zipped = zip(gens_all, tgt_all, src_all, ans_self, qg_ctrl_vals_all)
        for g, t, s, a_s, ctrl_val in zipped:
            if ctrl_val in finer_holder: __ = finer_holder[ctrl_val]
            else: __ = copy.deepcopy(template)
            __['gens_all'].append(g) 
            __['tgt_all'].append(t)
            __['src_all'].append(s)
            __['ans_self'].append(a_s)
            finer_holder[ctrl_val] = __

        # 2. compute SQ
        # NOTE: ctrl_val is of form '{qgtype} {qgsrc} {nfcount}' e.g. "s g 1"
        split_holder = {'r': {}, 't': {}}
        for src_type in ['r', 't']:
            for key in key_set: split_holder[src_type][key] = []
        for ctrl_val, inputs in finer_holder.items(): 
            if '1' in ctrl_val:
                for src_type in ['r', 't']:
                    if src_type in ctrl_val:
                        for key in key_set: split_holder[src_type][key].extend(inputs[key])
        # a. by T/G split 
        for src_type in ['t', 'r']:
            inputs = split_holder[src_type]
            assert len(inputs['tgt_all']) == len(inputs['ans_self']) == len(inputs['gens_all']) == len(inputs['src_all']), \
                ('FAIL... counts are:', len(inputs['tgt_all']), len(inputs['ans_self']), 
                                        len(inputs['gens_all']), len(inputs['src_all']))
            if not inputs['tgt_all']: continue
            print(f'\t\t WORKING ON FINER-GRAINED EVALUATION FOR <OUR><{src_type}><{key}> (SQ)...')
            t_s_g, t_s_c_s = evaluate_finer_wrapper(inputs['tgt_all'], inputs['ans_self'], 
                                    inputs['gens_all'], tokenizer, bert_score_model)
            
            for key in key_set: # store subset of g,s,t,a used
                t_s_g[key] = inputs[key]
                t_s_c_s[key] = inputs[key]

            test_score_gt_finer[f's {src_type} 1'] = t_s_g
            test_score_crossmod_selfsys_finer[f's {src_type} 1'] = t_s_c_s
            
            print(f'\t\ttest_score_gt (finer) SRC <{src_type}>',
                                {k:v for k,v in t_s_g.items() if k not in key_set})
            print(f'\t\ttest_score_crossmod_selfsys (finer) SRC <{src_type}>',
                                {k:v for k,v in t_s_c_s.items() if k not in key_set})

        # 2. compute CQ (everything with 1 < nf <5)
        # a. fine-grained CQ
        print(f'\t\t WORKING ON FINER-GRAINED EVALUATION FOR <OUR><{key}> (CQ)...')
        for nf in range(2, 5):
            nf = str(nf)
            split_holder = {'r': {}, 't': {}}
            for src_type in ['r', 't']:
                for key in key_set: split_holder[src_type][key] = []
            for ctrl_val, inputs in finer_holder.items(): 
                if nf in ctrl_val: 
                    for src_type in ['r', 't']:
                        if src_type in ctrl_val:
                            for key in key_set: split_holder[src_type][key].extend(inputs[key])
            # a. by T/G split 
            for src_type in ['t', 'r']:
                inputs = split_holder[src_type]
                assert len(inputs['tgt_all']) == len(inputs['ans_self']) == len(inputs['gens_all']), \
                    ('FAIL... counts are:', len(inputs['tgt_all']), len(inputs['ans_self']), len(inputs['gens_all']))
                if not inputs['tgt_all']: continue
                print(f'\t\t WORKING ON FINER-GRAINED EVALUATION FOR <OUR><{src_type}><{key}> (CQ-{nf})...')
                t_s_g, t_s_c_s = evaluate_finer_wrapper(inputs['tgt_all'], inputs['ans_self'], 
                                        inputs['gens_all'], tokenizer, bert_score_model)

                for key in key_set: # store subset of g,s,t,a used
                    t_s_g[key] = inputs[key]
                    t_s_c_s[key] = inputs[key]

                test_score_gt_finer[f'c {src_type} {nf}'] = t_s_g
                test_score_crossmod_selfsys_finer[f'c {src_type} {nf}'] = t_s_c_s

                print(f'\t\ttest_score_gt (finer) SRC <{src_type}><{key}>',
                                {k:v for k,v in t_s_g.items() if k not in key_set})
                print(f'\t\ttest_score_crossmod_selfsys (finer) SRC <{src_type}><{key}>',
                                {k:v for k,v in t_s_c_s.items() if k not in key_set})
        
    runtime = round(np.mean(runtimes), 4)
    return test_score_gt, test_score_crossmod_selfsys, gens_all, tgt_all, src_all, gem_ids_all, runtime, \
            test_score_gt_finer, test_score_crossmod_selfsys_finer

def evaluate_finer_wrapper(tgt_all, ans_self, gens_all, tokenizer, bert_score_model):
    torch.cuda.empty_cache()
    test_score_gt = evaluate(tgt_all, gens_all, 
                    gentypes = ['normal'], reftypes = ['single'], tgt_strategy = 'NA',
                    auto_scores = ['bertscore'],
                    bert_score_model = bert_score_model,
                    lang = args.language_eval, bert_score_rescale_with_baseline = True,
                    eos_token = tokenizer.eos_token)
    test_score_crossmod_selfsys = evaluate(ans_self, gens_all, references2 = None, generated2 = None, 
                    gentypes = ['normal'], reftypes = ['single'], tgt_strategy = 'NA',
                    auto_scores = ['bertscore'],
                    bert_score_model = bert_score_model,
                    lang = args.language_eval, bert_score_rescale_with_baseline = True,
                    eos_token = tokenizer.eos_token)
    return test_score_gt, test_score_crossmod_selfsys

def trp_sep_scheme2_linearizer(triples_name, entry_obj):
    context = ''
    for t in triples_name:
        t = make_trip_elems(t)
        context += entry_obj.create_triple(t)
    return context

def calculate_BERTScore_rescaled(predictions, references, bertscore_obj, 
                                unanswerable_zero, unanswerable_str, lang = 'en',
                                bert_score_model = 'bert-base-multilingual-cased',
                                bert_score_rescale_with_baseline = True,
                                baseline_dirpath = 'tools/bert_score/get_rescale_baseline/rescale_baseline/'):
    '''
    similar to DQE's calculate_BERTScore function, except allowing rescale_with_baseline
    to be set (defaults to True). Also allows unanswerables to be set to BSC = 0.0
    '''
    bsc_baseline_avail = ['cs', 'de', 'en', 'en-sci', 'es', 'et', 'fi', 'fr', 'it', 'lv', 'pt', 'zh']
    baseline_path = f'{baseline_dirpath}/{lang}/{bert_score_model}.tsv' if lang not in bsc_baseline_avail else None
    bscores = bertscore_obj.compute(predictions = predictions, references = references,
                        model_type = bert_score_model, lang = lang, 
                        rescale_with_baseline = bert_score_rescale_with_baseline, 
                        baseline_path = baseline_path if bert_score_rescale_with_baseline else None,
                        idf = False, device = torch.device('cuda') \
                        if torch.cuda.is_available() else torch.device('cpu'))['f1']
    if unanswerable_zero: return [0.0 if p == unanswerable_str else bsc for bsc, p in zip(bscores, predictions)]
    else: return bscores

class Collate:
    '''
    Custom collate function for dataloading in this script.
    '''
    def __init__(self, bos_token_id = None, pad_token_id = None, eos_token_id = None,
                ipstrat2idx = None):
        '''
        kbemb, an instance of PretrainedKBemb containing the embeddings for 
        the relations of the KB.
        '''
        self.bos_token_id, self.eos_token_id = bos_token_id, eos_token_id
        self.pad_token_id = pad_token_id
        self.ipstrat2idx = ipstrat2idx

    def pad2max(self, batch_elem, make_mask = False):
        '''
        helper function to pad all of a batch element to the max length within
        '''
        max_len = max([len(s) for s in batch_elem])
        new_batch_elem, batch_elem_mask = [], []
        for s in batch_elem: 
            orig_len, pad_size = len(s), max_len - len(s)
            s.extend([self.pad_token_id] * pad_size)
            new_batch_elem.append(s)
            if make_mask:
                batch_elem_mask.append([1] * orig_len + [0] * pad_size)

        return new_batch_elem, batch_elem_mask 

    def collate_fn(self, batch):
        idxes = [(entry.id) for entry in batch]
        # 1. prepare tgt
        tgt_pad = self.pad_token_id
        if self.ipstrat2idx is not None and batch[0].ipstrat is not None:
            ipstrat_enc = torch.LongTensor([self.ipstrat2idx[entry.ipstrat] for entry in batch])
        else: ipstrat_enc = torch.LongTensor([0 for entry in batch])
        
        tgt = [entry.tgt for entry in batch]
        # check case of kbqa with token_indices
        if type(tgt[0]) == tuple: assert all(len(tt) for tt in tgt)
        elif type(tgt[0]) == int: pass # for textnxfacts
        else: tgt, __ = self.pad2max(tgt, make_mask = False)
        tgt = torch.LongTensor(tgt)
        tgt[(tgt==self.pad_token_id)] = tgt_pad 
        

        src, src_mask = [entry.src for entry in batch], []

        # 2. prepare src 
        # pad to maxlen in batch, also create mask
        src, src_mask = self.pad2max(src, make_mask = True)
        src, src_mask = torch.LongTensor(src), torch.LongTensor(src_mask)

        src_self, src_self_mask = [entry.src_self for entry in batch], []

        # 2. prepare src_self 
        # pad to maxlen in batch, also create mask
        src_self, src_self_mask = self.pad2max(src_self, make_mask = True)
        src_self, src_self_mask = torch.LongTensor(src_self), torch.LongTensor(src_self_mask)
        
        id_enc = [entry.id_enc for entry in batch]
        id_enc, __ = self.pad2max(id_enc, make_mask = False)
        id_enc = torch.LongTensor(id_enc)

        qg_ctrl_vals_enc = [entry.qg_ctrl_vals for entry in batch]

        hash_key_enc = [entry.hash_key_enc for entry in batch]
        
        return src, src_mask, src_self, src_self_mask, tgt, id_enc, ipstrat_enc, qg_ctrl_vals_enc, hash_key_enc

def remove_underscore_camelcase(triple):
    '''
    helper func to convert DBpedia triples to Wikidata-like format
    '''
    newtriple = re.sub(r"([a-z])([A-Z])", "\g<1> \g<2>", triple)
    newtriple = re.sub(r'_', ' ', newtriple)
    newtriple = re.sub(r'\s+', ' ', newtriple)
    # lower case the property 
    newtriple = newtriple.split(' | ')
    newtriple[1] = newtriple[1].lower()

    return ' | '.join(newtriple)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Arugments for running the cross-answerability script.')
    parser.add_argument('--run_num', help = 'Which run to execute (controls random seed, controls sampling)',
                        default=0, type=int)
    parser.add_argument('--unanswerable_zero', help = 'Whether to set QA "unanswerable" predictions to bertscore 0 ',
                        default=False, type=bool)
    parser.add_argument('--trial_run', help = 'Whether to run script on a small trial set',
                        default=False, type=bool)
    parser.add_argument('--bsz', help = 'batch size',
                        default=1024, type=int)
    parser.add_argument('--quartet_model_num', help = 'Which QUARTET model to use. 1 or 2',
                        default='1', type=str)
    parser.add_argument('--remove_inconsistent', help = 'Whether to run consistency on QG and remove unanswerable/low BERTScore',
                        default=False, type=bool)
    parser.add_argument('--inconsistent_cutoff', help = 'The cutoff to apply for removing questions that cannot be answered by its source input.',
                        default=None, type=float)
    parser.add_argument('--bsc_ans_lc', help = 'Whether to lowercase answer when comparing BERTScore against g-truth answer.',
                        default=False, type=bool)      
    parser.add_argument('--sampling_mode', help = 'Which mode to use for sampling (1): pick N_qa from both sides, (2): pick min of both sides',
                        default=2, type=int)     
    parser.add_argument('--load_ckpt', help = 'If loading from ckpt, provide path to checkpoint .ckpt here. \
                        input here should be 2-element sequence. first elem is best_model.pt or "", second elem\
                        is the path to the checkpoint to load.',
                        default = [], nargs= "*", type = str)
    parser.add_argument('--exp_code_str', help = 'Additional experiment code string to disambiguate experiments.',
                        default='', type=str)
    parser.add_argument('--finer_grained', help = 'Whether to run scoring at finer level too (e.g. SQ/CQ, T/G, nf_size).',
                        default=True, type=bool)
    parser.add_argument('--tokenizer_maxlength', help = 'Max length to use for truncation (set to model max length).',
                        default=512, type=int)
    parser.add_argument('--bypass_dqegem', help = 'Max length to use for truncation (set to model max length).',
                        default=False, type=bool)
    parser.add_argument('--ablate_key', help = 'Use "_ablation_xadapter" to select ablation setting',
                        default='', type=str)
    parser.add_argument('--bypass_dqe_on_dqe', help = 'Bypass computation of DQE origin on DQE format.',
                        default=False, type=bool)
    parser.add_argument('--languages_trained', help = 'The language(s) used to train QTT/mQTT with. ',
                        default=['en'], nargs = '*', type=str)
    parser.add_argument('--language_eval', help = 'The language to use on QTT/mQTT. ',
                        default='en', type=str)
    parser.add_argument('--qwebnlg_EFU', help = 'Option in allq_cq loaders for QG to ensure textQG training data for for a given (q,a,t) is mapped to itself (and not just to texts encompassing it)', 
                        default=False, type=bool)

    args = parser.parse_args()
    
    assert set(args.languages_trained).issubset(set(['en'])), args.languages_trained
    args.multilingual_trained, args.non_english = False, False
    if len(args.languages_trained) > 1: args.multilingual_trained = True
    args.languages_trained = sorted(args.languages_trained)
    assert args.languages_trained and args.language_eval, (args.languages_trained, args.language_eval)
    print('LANGUAGES TRAINED ON/LANGUAGE FOR EVAL:', args.languages_trained, args.language_eval)
    non_en = [lang for lang in args.languages_trained if lang != 'en']
    mling_str = ''
    args.non_english = False
    if non_en: args.non_english = True
    print('args.languages_trained, args.language_eval, args.multilingual_trained, args.non_english', 
          args.languages_trained, args.language_eval, args.multilingual_trained, args.non_english)

    if args.load_ckpt and 'tsep_scheme2' in args.load_ckpt[1]:
        args.bypass_dqegem = True

        fp_exp_config = os.path.join(os.path.dirname(args.load_ckpt[1]), 'exp_configs.json')
        with open(fp_exp_config) as f: exp_configs = json.load(f)

        args.trp_sep_scheme = exp_configs['settings']['trp_sep_scheme']
        args.trp_seps = exp_configs['trp_seps']
        args.triple_repr = 'spo'
        setattr(args, 'multilingual_trained', exp_configs.get('multilingual', False))
        setattr(args, 'languages_trained', exp_configs.get('languages', args.language_eval))
        
        args.entry_obj = Entry(args)
    if args.ablate_key == '0': args.ablate_key = ''
    start_time = time.time()
    main(args)
    print('TIME TAKEN:', time.time() - start_time)