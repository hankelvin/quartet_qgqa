import glob, os, json, re
PARENTDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
import pandas as pd, numpy as np
'''
Collect the scores for each setting+run for the downstream FiD exps
construct a csv file and save out to file.
'''

dirpath = '../tools/FiD/checkpoint/'
folders = sorted(glob.glob(os.path.join(dirpath, 'TEST_*')))

lines_coll = []
for folder in folders:
    fname = os.path.basename(folder)
    with open(f'{folder}/test_results/scores.json', encoding = 'utf-8') as f: 
            sdict = json.load(f)
    # e.g. fname = 'TEST_TQA_trgdata-DQE-graph____qstsys-DQE_qstsrcmod-graph_TEST_inpform-unikqa_linear_graph_filt0.7_runnum0'
    trg_setting = re.search(r'(?:TEST_TQA_trgdata-)(.+)(?:____.+)', fname).group(1)
    qstsrc_setting = re.search(r'(?:.+qstsys-)(.+)(?:_qstsrcmod.+)', fname).group(1) + \
                     re.search(r'(?:.+_qstsrcmod)(.+)(?:_TEST.+)', fname).group(1)
    inpform = re.findall(r'unikqa_linear_graph|unikqa_text', fname)[0]
    runnum = re.search(r'(?:.+_runnum)([0-9]+)', fname).group(1)
    filt_setting = 'filt0.7' in fname

    line = {'trg_setting': trg_setting, 'qstsrc_setting': qstsrc_setting, 
    'inpform': inpform, 'filt0.7_setting': filt_setting, 
    'runnum': int(runnum),'fname': fname, **sdict}
    lines_coll.append(line)

df = pd.DataFrame(lines_coll)
# remove runnum from fname. use this to group across runs to compute mean/std scores
df['fname_cut'] = df.apply(lambda x: x.fname.replace(f'_runnum{x.runnum}', ''), axis = 1)
df.reset_index(inplace=True, drop=True)
savepath = f'{PARENTDIR}/results/FiD'
if not os.path.exists(savepath): os.makedirs(savepath)
df.to_csv(os.path.join(savepath, f'fid_scores.csv'), index=False)

gb = df.groupby('fname_cut')
for setting in df.fname_cut.unique():
    group = gb.get_group(setting)
    trg_setting = list(set(group.trg_setting))[0]
    qstsrc_setting = list(set(group.qstsrc_setting))[0]
    inpform = 'graph' if 'graph' in list(set(group.inpform))[0] else 'text'
    print(f'trg_setting: {trg_setting} \t qstsrc_setting: {qstsrc_setting} \t inpform: {inpform}')
    print('Mean/STD BSc: \t', round(np.mean(group['BSC'])*100, 2), '\tiny{(',round(np.std(group['BSC'])*100, 2), ')}')
    print()