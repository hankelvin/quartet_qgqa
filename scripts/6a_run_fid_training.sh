# finetuning params https://github.com/facebookresearch/UniK-QA/issues/1
# FiD TriviaQA
conda deactivate
conda activate fid

cd tools/FiD

##### DQE GRAPH 
CUDA_VISIBLE_DEVICES=0 python train_reader.py \
  --name model_tqa_DQE_graph \
  --checkpoint_dir checkpoint \
  --train_data qtt_data/DQE_train_unikqa_linear_graph.jsonl \
  --eval_data qtt_data/DQE_validation_unikqa_linear_graph.jsonl \
  --model_path download/tqa_reader_base \
  --model_size base \
  --lr 0.00001 \
  --optim adamw \
  --scheduler linear \
  --weight_decay 0.01 \
  --text_maxlength 250 \
  --per_gpu_batch_size 16 \
  --n_context 1 \
  --eval_freq 5000 \
  --total_step 15000 \
  --warmup_step 1500 

##### DQE TEXT 
CUDA_VISIBLE_DEVICES=1 python train_reader.py \
  --name model_tqa_DQE_text \
  --checkpoint_dir checkpoint \
  --train_data qtt_data/DQE_train_unikqa_text.jsonl \
  --eval_data qtt_data/DQE_validation_unikqa_text.jsonl \
  --model_path download/tqa_reader_base \
  --model_size base \
  --lr 0.00001 \
  --optim adamw \
  --scheduler linear \
  --weight_decay 0.01 \
  --text_maxlength 250 \
  --per_gpu_batch_size 16 \
  --n_context 1 \
  --eval_freq 5000 \
  --total_step 15000 \
  --warmup_step 1500 


##### QTT GRAPH 
CUDA_VISIBLE_DEVICES=2 python train_reader.py \
  --name model_tqa_QTT_graph \
  --checkpoint_dir checkpoint \
  --train_data qtt_data/QTT_train_unikqa_linear_graph.jsonl \
  --eval_data qtt_data/QTT_validation_unikqa_linear_graph.jsonl \
  --model_path download/tqa_reader_base \
  --model_size base \
  --lr 0.00001 \
  --optim adamw \
  --scheduler linear \
  --weight_decay 0.01 \
  --text_maxlength 250 \
  --per_gpu_batch_size 16 \
  --n_context 1 \
  --eval_freq 2000 \
  --total_step 6500 \
  --warmup_step 650 

##### QTT TEXT 
CUDA_VISIBLE_DEVICES=3 python train_reader.py \
  --name model_tqa_QTT_text \
  --checkpoint_dir checkpoint \
  --train_data qtt_data/QTT_train_unikqa_text.jsonl \
  --eval_data qtt_data/QTT_validation_unikqa_text.jsonl \
  --model_path download/tqa_reader_base \
  --model_size base \
  --lr 0.00001 \
  --optim adamw \
  --scheduler linear \
  --weight_decay 0.01 \
  --text_maxlength 250 \
  --per_gpu_batch_size 16 \
  --n_context 1 \
  --eval_freq 5000 \
  --total_step 15000 \
  --warmup_step 1500 