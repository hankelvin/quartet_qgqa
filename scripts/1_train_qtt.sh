conda deactivate
conda activate quartet_qgqa

# BASE MODEL 
python omnibus.py --settings_code quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter --datasets allq-cq-webnlg-negsamp4-hardnegs --max_steps 383445 --ckpt_every_n_steps 150000 --bsz 32 --patience 3 --model t5-small --datasets_filter quartet:allq-cq-webnlg-negsamp4-hardnegs:nosubjobj --linear_warmup_ratio 0.1 --qwebnlg_EFU True

# ABLATION 2: No-Adapter, using the DQE SynQG and SQuADv2 data
# use this code for data pkl prep: quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate
python omnibus.py --settings_code quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate --datasets allq-cq-dqeablate-negsamp4-hardnegs --max_steps 383445 --ckpt_every_n_steps 150000 --bsz 32 --patience 3 --model t5-small --datasets_filter quartet:allq-cq-dqeablate-negsamp4-hardnegs:nosubjobj --linear_warmup_ratio 0.1 --exp_code_str '_loadxdqeablate' --qwebnlg_EFU True 

# ABLATION 3: No-Adapter, TS2 linearisation 
# use this code for data pkl prep: quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw
python omnibus.py --settings_code quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw --datasets allq-cq-webnlg-negsamp4-hardnegs --max_steps 383445 --ckpt_every_n_steps 150000 --bsz 32 --patience 3 --model t5-small --datasets_filter quartet:allq-cq-webnlg-negsamp4-hardnegs:nosubjobj --bypass_dqegem True --linear_warmup_ratio 0.1 --exp_code_str '_loadtsep_scheme2' --qwebnlg_EFU True

# ABLATION 4: No-Adapter, without any AuxTask
# use this code for data pkl prep: quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks
python omnibus.py --settings_code quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks --datasets allq-cq-webnlg-negsamp4-hardnegs --max_steps 383445 --ckpt_every_n_steps 150000 --bsz 32 --patience 3 --model t5-small --datasets_filter quartet:allq-cq-webnlg-negsamp4-hardnegs:nosubjobj --linear_warmup_ratio 0.1 --exp_code_str '_loadxauxtask' --qwebnlg_EFU True 

