ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd results/quartet_human_eval/logs
for i in `perl -e '$,=" ";print +(0..9)'`
    do rm -f  $i*
done

for i in `perl -e '$,=" ";print +(a..z)'`
    do  rm -f  $i*
done

cd ../../..
PYTORCH_CUDA_ALLOC_CONF=max_split_size_mb:1000 python evaluation/evaluation_humaneval_correlation.py --teams $4 --unanswerable_zero True --remove_inconsistent True --inconsistent_cutoff $1 --quartet_model_num $2 --bsc_ans_lc True --load_ckpt "" $3 --quartet_fullfinetune True
