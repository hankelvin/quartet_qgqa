conda deactivate
conda activate questeval

# Base (without TAS)
for i in {0..5}
    do ./scripts/run_cross_ans.sh $i 3 0.7 results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_qwebnlg_EFU_allq-cq-webnlg-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10/final.xkpt 2 lr01-entlink_ablation_xadapter_qwebnlg_EFU 250 "_ablation_xadapters"; done

# Base (with TAS)
for i in {0..5}
    do ./scripts/run_cross_ans.sh $i 3 0.7 results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_qwebnlg_EFU_allq-cq-webnlg-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10/final.xkpt 2 lr01-entlink_ablation_xadapter_qwebnlg_EFU_TAS 250 "_ablation_xadapters"; done

# using the DQE SynQG and SQuADv2 data 
for i in {0..5}
    do ./scripts/run_cross_ans.sh $i 3 0.7 results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_qwebnlg_EFU_allq-cq-dqeablate-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10_loadxdqeablate/final.xkpt 2 lr01_ablation_xadapter_qwebnlg_EFU_loadxdqeablate_TAS 250 "_ablation_xadapters"; done

# TS2 linearisation 
for i in {0..5}
    do ./scripts/run_cross_ans.sh $i 3 0.7 results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_tsep_scheme2_qwebnlg_EFU_allq-cq-webnlg-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10_loadtsep_scheme2/final.xkpt 2 lr01-entlink_ablation_xadapter_qwebnlg_EFU_loadtsep_scheme2_TAS 250 "_ablation_xadapters"; done

# No AuxTasks
for i in {0..5}
    do ./scripts/run_cross_ans.sh $i 3 0.7 results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_qwebnlg_EFU_allq-cq-webnlg-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10_loadxauxtask/final.xkpt 2 lr01_ablation_xadapter_qwebnlg_EFU_loadxauxtask_TAS 250 "_ablation_xadapters"; done

