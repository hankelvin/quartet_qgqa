conda deactivate
conda activate questeval

./scripts/run_human_xadapter_teams.sh 0.7 3 "results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_qwebnlg_EFU_allq-cq-webnlg-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10/final.xkpt" "adapt baseline melbourne pkuwriter tilburg-nmt"
  
./scripts/run_human_xadapter_teams.sh 0.7 3 "results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_qwebnlg_EFU_allq-cq-webnlg-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10/final.xkpt" "tilburg-pipe tilburg-smt upf-forge vietnam webnlg" 
  
# Running DQE 
./scripts/run_human_xadapter_teams.sh 3 "results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_qwebnlg_EFU_allq-cq-webnlg-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10/final.xkpt" "adapt baseline melbourne pkuwriter tilburg-nmt"
  
./scripts/run_human_xadapter_teams.sh 3 "results/omnibus_model/quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter_qwebnlg_EFU_allq-cq-webnlg-negsamp4-hardnegs_nosubjobj_lrwarm0.1_ep10/final.xkpt" "tilburg-pipe tilburg-smt upf-forge vietnam webnlg" 