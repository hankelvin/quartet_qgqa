import datasets, json, sys, tqdm, time, math, random, torch
from data_utils.shared import _strip_punct_ws, trim_entry_obj
from data_utils.data_utils_omnibus import SQEntry
from data_utils.data_utils_kbqa import KBQAEntry
from data_utils.data_utils_textqa import TextQAEntry
from data_utils.data_utils_textnxfacts import TextNxFactsEntry
from dataset_handlers.utils import (Triple, _start_gem_formatter, get_NE_NP,
        QTypesPredictor, make_one_omnibus_entry, dqe_triple_preprocessing, check_connected,
        _semtypes_distri as wkdtqg_semtypes_distri)
from collections import defaultdict
from itertools import combinations

# NOTE: using the same preprocessing treatment for triples in DQE 
# see https://github.com/ThomasScialom/QuestEval/blob/main/questeval/utils.py


def load_webnlg(args, tokenizer, task_name, task_settings, dataset_version = 'dqe-webnlg', 
                splits = ['train', 'validation', 'test']):
    # if only doing inference (for SQ and CQ) (Step 3)
    c_test_only_step3 = any(['dqe-webnlg-test_only' in ds or 'gem-webnlg-test_only' \
                        in ds for ds in task_settings['datasets']])
    print('c_test_only_step3'.upper(), ' is set to: ', c_test_only_step3)
    if c_test_only_step3: 
        return load_webnlg_QG_inference(args, tokenizer, task_name, task_settings, dataset_version, splits)
    if task_name == 'sqgcqg':
        __ = load_webnlg_normal(args, tokenizer, 'sqg', task_settings, dataset_version, splits)
        __ += load_webnlg_normal(args, tokenizer, 'cqg', task_settings, dataset_version, splits)
        return __
    else: 
        return load_webnlg_normal(args, tokenizer, task_name, task_settings, dataset_version, splits)


def load_webnlg_QG_inference(args, tokenizer, task_name, task_settings, dataset_version = 'dqe-webnlg', 
                splits = ['train', 'validation', 'test']):
    if args.multilingual: raise NotImplementedError
    determ_state = torch.are_deterministic_algorithms_enabled()
    src_types = task_settings['input_strategies']
    args.logger.print('TASK INPUT STRATEGIES', task_settings['input_strategies'])
    sub_tok, obj_tok = args.trp_seps['s'], args.trp_seps['o'] # for SQ use 

    ### 0. Load preliminaries... including qtype predictor for SQ
    data, c_dqebaseline, *__, webnlg_full_train = \
            preliminaries(args, tokenizer, task_name, task_settings, dataset_version = dataset_version,  splits = splits)
    gemformatter, spacy_pipeline = _start_gem_formatter(args, task_settings, bypass = True, c_dqebaseline = c_dqebaseline,
                                                        language = 'en')
    
    qt_predictor, types_distri = None, None
    if task_name == 'sqg': 
        # a. load qt predictor
        # in case Lightning set up starts deterministic, set to False for  QTypesPredictor
        if determ_state: torch.use_deterministic_algorithms(False)
        qt_predictor = QTypesPredictor(args, topk = 3)
   
    fp_template = 'datasets/wikidataqg/trial_20210501_swz-wkdt_pretrain-seen/large_seen/triplerdfs/{}.csv'   
    # b. load wikidataqg types distribution
    types_distri = wkdtqg_semtypes_distri(fp_template)
    args.logger.print('types_distri done')  

    # DBpedia to Wikidata 
    with open('datasets/dqe_webnlg/z00_webnlg2020_dpb2wkdt_triples.json', 
        encoding = 'utf-8') as f:
        webnlg2020_dpb2wkdt = json.load(f)
    # collect the new entities mapping too (all so as to be used by get_asemtypes later)
    dbp2wkdt_entmapping = defaultdict(set)
    for d, w in webnlg2020_dpb2wkdt.items():
        ds, __, do = d.split(' | ')
        ds = ' '.join([t.text for t in spacy_pipeline(Triple.clean_obj(ds.strip(), lc=False))])  
        do = ' '.join([t.text for t in spacy_pipeline(Triple.clean_obj(do.strip(), lc=False))])  
        ws, __, wo = w.split(' | ')
        ws = ' '.join([t.text for t in spacy_pipeline(Triple.clean_obj(ws.strip(), lc=False))])  
        wo = ' '.join([t.text for t in spacy_pipeline(Triple.clean_obj(wo.strip(), lc=False))])  
        dbp2wkdt_entmapping[ds].add(ws) # there may be more than 1 mapping (see Andrew White ( musician ))
        dbp2wkdt_entmapping[do].add(wo)

    # c. load wikidata entity type info 
    with open('datasets/dqe_webnlg/z02_webnlg2020_enttypes.json', 
        encoding = 'utf-8') as f: __webnlg2020_enttypes = json.load(f)
    # certain settings use DQE-processed form of the triples, where the
    # original entities/values have been put through clean_obj... 

    webnlg2020_enttypes = {} # for asemtypes
    for k,v in __webnlg2020_enttypes.items():
        # get wkdt form too 
        new_k = ' '.join([t.text for t in spacy_pipeline(Triple.clean_obj(k.strip(), lc=False))])
        webnlg2020_enttypes[k] = v
        webnlg2020_enttypes[new_k] = v
        try:
            # add the Wikidata form of the entity/value
            wkdt_ks = dbp2wkdt_entmapping[new_k]
            for wkdt_k in wkdt_ks: 
                webnlg2020_enttypes[wkdt_k] = v
        except: print(f'\t\twkdt form of ({k})/({new_k}) not retrievable from dbpedia form')

    ### 1. start processing 
    data_coll = defaultdict(list)
    start = time.time()
    for src_type in src_types:
        seen_triples, seen_text = set(), set()
        for split in splits:
            
            if src_type is None: raise ValueError('Check strategy...')
            if split in ['test']: raise NotImplementedError('WebNLG test should not used to generate training data')

            for idx, entry in enumerate(tqdm.tqdm(data[split])):

                # extract the needed info from gem-webnlg or dqe-webnlg
                graph_orig, ref_sentence, __ = \
                    return_entry_elems(dataset_version, c_dqebaseline, entry, 
                        spacy_pipeline, webnlg2020_dpb2wkdt)
                
                gem_id = entry['gem_id']
                if task_name == 'sqg':
                    for triple_name in graph_orig:
                        
                        # DEDUP: WebNLG has multiple refs for 1 tripleset. avoid duplicates
                        if tuple(triple_name) in seen_triples: continue
                        seen_triples.add(tuple(triple_name))
                        
                        for apos_s_o, e_idx in [(sub_tok, 0), (obj_tok, -1)]: # [SUB] and [OBJ]
                            en = triple_name[e_idx]
                            a_semtypes = get_asemtypes(en, webnlg2020_enttypes, types_distri)
                            qtypes_set = qt_predictor.predict(a_semtypes, apos_s_o)
                            for qtype in qtypes_set:
                                xx = SQEntry(args = args, tokenizer = tokenizer, 
                                    question = '', answer = apos_s_o if args.settings['subobj_tok'] else en, 
                                    triples_name = [triple_name], triples_code = None, 
                                    qtypes = [qtype], a_semtypes = a_semtypes,
                                    task_name = task_name, task_settings = task_settings, 
                                    id = gem_id, gemformatter = gemformatter,
                                    context_f = ref_sentence)

                                # HACK: to ensure same format for dqebaseline
                                if c_dqebaseline: xx.input_sep = "</s>"

                                ##### PREP for INPUT #####
                                p4i_pack = {'task_settings': task_settings, 'src_type': src_type, 'n_facts': None}
                                try: xx.prep4input(**p4i_pack)
                                except: 
                                    args.logger.print(f'WebNLG for {task_name.upper()}: Discarding {f"{gem_id}"}, \
                                            not preppable for {task_name.upper()} format, likely that answer was unalignable.')
                                    continue
                                
                                # NOTE: all entries should go into test
                                data_coll['test'].append(trim_entry_obj(xx))

                elif task_name == 'cqg':
                    t_size = len(graph_orig)
                    min_n_fact, max_n_facts = 2, min(t_size, 4) # CQ-KELM is between 2 and 5 triples

                    entries_coll = []
                    if src_type == 't2cq':
                        
                        # DEDUP: avoid duplicates
                        if ref_sentence in seen_text: continue
                        seen_text.add(ref_sentence)

                        answers = set(ent for ent in spacy_pipeline(ref_sentence).ents)
                        answers.update(ent for ent in spacy_pipeline(ref_sentence).noun_chunks)
                        # filter answers for pronouns (e.g. she, he, it, they). 
                        # assume their coref appears in D2T generated text.
                        answers = set(e.text for e in answers if (len(list(e.subtree)) > 1 \
                                        or list(e.subtree)[0].pos_ != 'PRON'))
                        
                        for n_facts in range(min_n_fact, max_n_facts+1):
                            for answer in answers:
                                xx = make_one_omnibus_entry(args = args, tokenizer = tokenizer, 
                                        context_f = ref_sentence, question = '', answer = '', 
                                        triples_code_prime = None, triples_name_prime = None, 
                                        triples_code = None, triples_name = None, ans_pos = None, 
                                        qtypes = None, a_semtypes = '', cq_id = gem_id, 
                                        task_name = task_name, task_settings = task_settings, 
                                        gemformatter = gemformatter)
                                xx.cq_entry.answer = answer # slot answer in place 
                                p4i_pack = {'task_settings': task_settings, 'src_type': src_type, 'n_facts': n_facts}
                                entries_coll.append((xx, p4i_pack))
                    
                    elif src_type == 'g2cq':
                        for n_facts in range(min_n_fact, max_n_facts+1):
                            combis = list(combinations(range(t_size), r=n_facts))

                            for c in combis:
                                subgraph_name = [t for i, t in enumerate(graph_orig) if i in c]
                                
                                # DEDUP: avoid duplicates if subgraph seen elsewhere
                                tuple_subgraph_name = tuple(tuple(t) for t in sorted(subgraph_name))
                                # check connected, check not already seen 
                                if not check_connected(tuple_subgraph_name): continue 
                                if tuple_subgraph_name in seen_triples: continue
                                seen_triples.add(tuple_subgraph_name)
                                
                                # answers: ent_names, answer: ent_name
                                answers = [t[0::2] for t in subgraph_name]
                                answers = set(e2 for e in answers for e2 in e)

                                for answer in answers:
                                    xx = make_one_omnibus_entry(args = args, tokenizer = tokenizer, 
                                        context_f = None, question = '', answer = None, 
                                        triples_code_prime = None, triples_name_prime = None, 
                                        triples_code = None, triples_name = None, ans_pos = None, 
                                        qtypes = None, a_semtypes = None, cq_id = gem_id, 
                                        task_name = task_name, task_settings = task_settings, 
                                        gemformatter = gemformatter)

                                    a_semtypes = get_asemtypes(answer, webnlg2020_enttypes, types_distri)
                                    xx.cq_entry.answer = answer
                                    xx.cq_entry.triples_name = subgraph_name
                                    xx.cq_entry.a_semtypes = [a_semtypes]
                                    p4i_pack = {'task_settings': task_settings, 'src_type': src_type, 'n_facts': n_facts}
                                    entries_coll.append((xx, p4i_pack))

                    for __, p4i_pack in entries_coll:
                        # HACK: to ensure same format for dqebaseline
                        if c_dqebaseline: __.input_sep = "</s>"

                        ##### PREP for INPUT #####
                        try: __.prep4input(**p4i_pack)
                        except: 
                            args.logger.print(f'WebNLG for {task_name.upper()}: Discarding {f"{gem_id}"}, \
                                    not preppable for {task_name.upper()} format, likely that answer was unalignable.')
                            continue
                        
                        # NOTE: all entries should go into test
                        data_coll['test'].append(trim_entry_obj(__))

    args.logger.print(f'Time taken to prep {task_name.upper()} data:', time.time()-start)    
    args.logger.print(f'NUMBER OF{task_name.upper()} triple-answer pairs accepted for SQ task: ', 
                            sum(len(v) for v in data_coll.values())) 

    torch.use_deterministic_algorithms(determ_state)
    return data_coll, webnlg_full_train

# for use when running test_only too
def load_webnlg_normal(args, tokenizer, task_name, task_settings, dataset_version = 'dqe-webnlg', 
                splits = ['train', 'validation', 'test']):
    '''
    params:
    - task_name (str):
    - dataset_version (str): whether to load 'gem-webnlg', original WebNLG 2017 version (via GEM challenge)
    or the version from DataQuestEval with additional generated QA-pairs from the references. 
    '''
    
    src_types = task_settings['input_strategies']    
    data_coll = defaultdict(list)
    # graph is in English, so gem_formatter and spacy (used by gem_formater) stays in English
    c_dqebaseline = 'dqebaseline' in args.settings_code
    gemformatter, spacy_pipeline_en = _start_gem_formatter(args, task_settings, bypass = True, 
                                                            c_dqebaseline = c_dqebaseline, language = 'en')
    for lang in args.languages:
        data, c_dqebaseline, trip_start_token_id, input_sep_token_id, src_format, tgt_format, webnlg_full_train = \
                preliminaries(args, tokenizer, task_name, task_settings, 
                              dataset_version = dataset_version, splits = splits,lang = lang)
        if args.textansselector:
            import sys
            sys.path.append('../utils')
            from utils import TextAnsSelector
            textansselector = TextAnsSelector(args)
        # load correct spacy_pipeline for get_NE_NP()
        __, spacy_pipeline = _start_gem_formatter(args, task_settings, bypass = True, 
                                                            c_dqebaseline = c_dqebaseline, language = lang)
        start = time.time()
        for split in splits:
            args.logger.print(f'Starting prep for {split.upper()} on the {task_name.upper()} task...')
            for idx, entry in enumerate(tqdm.tqdm(data[split])):
                
                # extract the needed info from gem-webnlg or dqe-webnlg
                graph_orig, ref_sentence, qa_pair_dict = \
                    return_entry_elems(dataset_version, c_dqebaseline, entry, spacy_pipeline_en)
                
                # do once for KBQA
                if task_name == 'kbqa' and 'dqe_gem' in task_settings['src_format']: 
                    graph_orig = gemformatter(graph_orig)
                
                gem_id = entry['gem_id']
                seen_gem_id = set() # for the case of sqg and cqg (we ignore the q-a pairs)
                for adetect, qa_pair in qa_pair_dict.items():

                    assert len(qa_pair['questions']) == len(qa_pair['answers'])
                    for idx_qa, (q, a) in enumerate(zip(qa_pair['questions'], qa_pair['answers'])):
                        
                        # 0. certain answers from DQE outputs are poorly extracted ans (e.g. '-' when answer is a date)
                        # for all QA/QG tasks, reject such QA-pairs.
                        # NOTE: this is not applicable for test_only (when inferring QTT SQ and CQ, for multilingual mQTT case)
                        if task_name not in ['textnxfacts'] and not (args.test_only and args.non_english): 
                            if not _strip_punct_ws(a.lower()): 
                                args.logger.print(f'REJECTED for low quality answer... ID: [{gem_id}] \t Q: [{q}] \t A: [{a}].')
                                continue
                        
                        entries_coll = []
                        # 1. begin working on tasks
                        if task_name == 'kbqa':
                            # if cond_src_type == 'text': continue
                            p4i_pack = {'src_format': src_format, 'tgt_format': tgt_format, 
                                        'multilingual': getattr(args, 'multilingual_trained', getattr(args, 'multilingual'))}
                            context_f = graph_orig
                            triples_name = graph_orig
                            
                            xx = KBQAEntry(args = args, tokenizer = tokenizer, question = q, answer = a, 
                                            context_f = context_f, triples_name = triples_name, ans_pos = None,
                                            task_name = task_name, task_settings = task_settings, 
                                            trip_start_token_id = trip_start_token_id, id = f'{gem_id}_{idx_qa}')
                            xx.lang = lang
                            entries_coll.append((xx, p4i_pack))
                        
                        elif task_name == 'textqa':
                            if a != 'unanswerable' and a not in ref_sentence: continue
                            p4i_pack = {'tgt_format': tgt_format, 
                                        'multilingual': getattr(args, 'multilingual_trained', getattr(args, 'multilingual'))}
                            xx = TextQAEntry(args = args, tokenizer = tokenizer, question = q, answer = a, 
                                            context_f = ref_sentence, task_name = task_name, task_settings = task_settings, 
                                            input_sep_token_id = input_sep_token_id, id = f'{gem_id}_{idx_qa}')
                            xx.lang = lang
                            if xx.id in ['web_nlg_en-train-22943_1', 'web_nlg_en-train-26299_13',
                                        'web_nlg_en-train-32944_3', 'web_nlg_en-train-34557_12']: 
                                args.logger.print('\t\t NOTE: Skipping unalignable QA-pairs in TRAIN split: ', xx.id)
                                continue
                            entries_coll.append((xx, p4i_pack))
                        
                        elif task_name == 'textnxfacts':
                            p4i_pack = {'multilingual': getattr(args, 'multilingual_trained', getattr(args, 'multilingual'))}
                            num_facts = len(graph_orig)
                            xx = TextNxFactsEntry(args = args, tokenizer = tokenizer, 
                                            context_f = ref_sentence, num_facts = num_facts, 
                                            task_name = task_name, task_settings = task_settings, id = gem_id)
                            xx.lang = lang
                            entries_coll.append((xx, p4i_pack))
                        
                        elif task_name == 'sqg':
                            # only usable here for test_only case
                            if not args.test_only: raise NotImplementedError
                            if gem_id in seen_gem_id: continue
                            seen_gem_id.add(gem_id)
                            
                            triples_name = graph_orig
                            context_f = ref_sentence
                            for src_type in src_types:
                                # HACK: to allow sqcq task for WebNLG
                                if src_type in ['t2cq', 'g2cq']: continue
                                # if src_type == 'g2sq' and cond_src_type == 'text': continue
                                # elif src_type == 't2sq' and cond_src_type == 'rdf': continue
                                p4i_pack = {'src_type': src_type, 
                                            'multilingual': getattr(args, 'multilingual_trained', getattr(args, 'multilingual'))}
                                
                                if src_type == 't2sq':
                                    if args.textansselector:
                                        answers = textansselector.predict_answerset(ref_sentence, lang = lang)
                                    else: 
                                        # answers = set(ent for ent in spacy_pipeline(ref_sentence).ents)
                                        # answers.update(ent for ent in spacy_pipeline(ref_sentence).noun_chunks)
                                        answers = get_NE_NP(ref_sentence, spacy_pipeline, language = lang)
                                        
                                elif src_type == 'g2sq':
                                    # ens: ent_names, en: ent_name
                                    answers = [t[0::2] for t in graph_orig]
                                    answers = set(e2 for e in answers for e2 in e)
                                else: raise NotImplementedError
                                
                                for answer in answers:
                                    xx = SQEntry(args = args, tokenizer = tokenizer, 
                                        question = '', answer = answer, 
                                        triples_name = triples_name if src_type == 'g2sq' else None,  
                                        triples_code = None, qtypes = None, a_semtypes = None,
                                        task_name = task_name, task_settings = task_settings, 
                                        id = gem_id, gemformatter = gemformatter, 
                                        context_f = context_f if src_type == 't2sq' else None)
                                    xx.lang = lang
                                    entries_coll.append((xx, p4i_pack))
                            
                        elif task_name == 'cqg':
                            # only usable here for test_only case
                            if not args.test_only: raise NotImplementedError
                            if gem_id in seen_gem_id: continue
                            seen_gem_id.add(gem_id)
                            
                            
                            triples_name = graph_orig
                            context_f = ref_sentence
                            for src_type in src_types:
                                # HACK: to allow sqcq task for WebNLG
                                if src_type in ['t2sq', 'g2sq']: continue
                                t_size = len(graph_orig)
                                min_n_fact, max_n_facts = 2, min(t_size, 4) 

                                for n_facts in range(min_n_fact, max_n_facts+1):
                                
                                    if src_type == 't2cq':
                                        if args.textansselector:
                                            answers = textansselector.predict_answerset(ref_sentence, lang = lang)
                                        else: 
                                            answers = get_NE_NP(ref_sentence, spacy_pipeline, language = lang)
                                        
                                        for answer in answers:
                                            p4i_pack = {'task_settings': task_settings, 
                                                        'src_type': src_type, 'n_facts': n_facts,
                                                        'multilingual': getattr(args, 'multilingual_trained', getattr(args, 'multilingual'))}
                                            xx = make_one_omnibus_entry(args = args, tokenizer = tokenizer, 
                                                    # NOTE: question = ""... q may not be aligned with n_facts control here 
                                                    context_f = context_f, question = '', answer = answer, 
                                                    triples_code_prime = None, triples_name_prime = None, 
                                                    triples_code = None, triples_name = None, 
                                                    ans_pos = None, qtypes = None, a_semtypes = '', 
                                                    cq_id = gem_id, 
                                                    task_name = task_name, task_settings = task_settings, 
                                                    gemformatter = gemformatter)
                                            xx.lang = lang
                                            entries_coll.append((xx, p4i_pack))
                                    
                                    elif src_type == 'g2cq':

                                        combis = list(combinations(range(t_size), r = n_facts))
                                        for c in combis:
                                            subgraph_name = [t for i, t in enumerate(graph_orig) if i in c]
                                            # ensure remaining subgraph is connected
                                            if not check_connected(subgraph_name): continue 

                                            p4i_pack = {'task_settings': task_settings, 
                                                    'src_type': src_type, 'n_facts': len(subgraph_name),
                                                    'multilingual': getattr(args, 'multilingual_trained', getattr(args, 'multilingual'))}

                                            # ens: ent_names, en: ent_name
                                            answers = [t[0::2] for t in subgraph_name]
                                            answers = set(e2 for e in answers for e2 in e)

                                            for answer in answers:
                                                xx = make_one_omnibus_entry(args = args, tokenizer = tokenizer, 
                                                    # NOTE: question = ""... q may not be aligned with subgraph_name here 
                                                    context_f = None, question = '', answer = answer, 
                                                    triples_code_prime = None, triples_name_prime = subgraph_name, 
                                                    triples_code = None, triples_name = None, ans_pos = None, 
                                                    qtypes = None, a_semtypes = None, cq_id = gem_id, 
                                                    task_name = task_name, task_settings = task_settings, 
                                                    gemformatter = gemformatter)
                                                xx.lang = lang
                                                entries_coll.append((xx, p4i_pack))
                                    
                                    else: raise ValueError('Check src_types settings')
                            

                        else: raise NotImplementedError
                        for __, p4i_pack in entries_coll:

                            # HACK: to ensure same format for dqebaseline
                            if c_dqebaseline: __.input_sep = "</s>"

                            ##### PREP for INPUT #####
                            try: __.prep4input(**p4i_pack)
                            except: 
                                args.logger.print(f'WebNLG for {task_name.upper()}: Discarding {f"{gem_id}_{idx_qa}"}, \
                                        not preppable for {task_name.upper()} format, likely that answer was unalignable.')
                                continue
                        
                            data_coll[split].append(trim_entry_obj(__))
                        
    args.logger.print('Time taken to prep WebNLG data:', time.time()-start)
    return data_coll, webnlg_full_train

def filter_webnlg_train(instruction = 'all-1.0'): 
    strategy, ratio = instruction.split('-')
    ratio = float(ratio)

    train_data = datasets.load_dataset('GEM/web_nlg', 'en', 
        download_mode='REUSE_CACHE_IF_EXISTS'.lower())['train'].shuffle(seed=54506)
    train_data = train_data.map(get_graph_size)

    if strategy == '1t': 
        train_data = train_data.filter(lambda x: x['size'] == 1)
    elif strategy == 'all': pass
    else: raise NotImplementedError
    
    if ratio < 1: 
        size = train_data.num_rows
        size = math.ceil(size*ratio)
        
        if strategy == '1t':
            keep_gem_ids = random.sample(train_data[:]['gem_id'], size)
        
        elif strategy == 'all':
            # always ensure even distribution of categories
            keep_gem_ids = []
            cats = set(train_data[:]['category'])

            gem_ids_by_cat = \
                {cat: train_data.filter(lambda x: x['category']==cat)[:]['gem_id'] for cat in cats}

            left = size
            while left > 0: 
                for cat in cats:
                    try: 
                        keep_gem_ids.append(gem_ids_by_cat[cat].pop())
                        left -= 1
                    except: pass
                     
    else: keep_gem_ids = train_data[:]['gem_id']

    return keep_gem_ids

def get_graph_size(entry):
    return {'size': len(entry['input'])}

def preliminaries(args, tokenizer, task_name, task_settings, dataset_version = 'dqe-webnlg', 
                lang = 'en', splits = ['train', 'validation', 'test']): 
    # PRELIMINARIES:
    # - loading relevant items for kbqa task
    src_format, tgt_format = task_settings['src_format'], task_settings['tgt_format']

    # HACK: for ensuring same input format is used when running DQE baselines 
    # see textqa_dqebaseline_gen_ce_dqegem and kbqa_dqebaseline_gen_ce_dqegem in utils_omnibus.py
    c_dqebaseline = 'dqebaseline' in args.settings_code
    
    trip_start_token_id, input_sep_token_id = None, None
    if task_name == 'kbqa' and not c_dqebaseline: 
        args.logger.print('args.trp_seps', args.trp_seps)
        trip_start_token_id = tokenizer.encode(args.trp_seps['s'], add_special_tokens = False)
        assert len(trip_start_token_id) == 1
        trip_start_token_id = trip_start_token_id[0]
    elif task_name == 'textqa' and not c_dqebaseline:
        input_sep_token_id = tokenizer.encode(args.trp_seps['input'], add_special_tokens = False)
        assert len(input_sep_token_id) == 1
        input_sep_token_id = input_sep_token_id[0]

    # 1. load data 
    # a. filter data if spec-ed
    c_filter = task_settings['datasets_filter'].get(dataset_version, False)
    if c_filter: 
        args.logger.print('FILTERING WEBNLG', c_filter)
        keep_gem_ids = filter_webnlg_train(instruction = c_filter)
        args.logger.print('FILTERING WEBNLG remaining size', len(keep_gem_ids))
    
    # b. proceed to load (and filter)... (c_filter check inside if... elif)
    webnlg_full_train = 0
    if 'gem-webnlg' in dataset_version:
        if lang == 'ru':
            data = datasets.load_dataset('GEM/web_nlg', lang)
        elif lang == 'ptbr':
            from dataset_handlers.utils import webnlg_pt_test
            data = webnlg_pt_test()
        elif lang == 'en':
            data = datasets.load_dataset('GEM/web_nlg', 'en', 
                        download_mode='REUSE_CACHE_IF_EXISTS'.lower())
        else: raise NotImplementedError
            
        if c_filter: 
            webnlg_full_train = data['train'].num_rows
            data['train'] = data['train'].filter(lambda x: x['gem_id'] in keep_gem_ids)
            args.logger.print('TRAIN set filter specifed for WebNLG. Before/After: ', 
                    webnlg_full_train, data['train'].num_rows)

    elif 'dqe-webnlg' in dataset_version: 
        data = {}
        for split in splits:
            fp = f'datasets/dqe_webnlg/dqe-SRC_outputs_{split}.jsonl'
            with open(fp, encoding = 'utf-8') as f: 
                
                if split == 'train' and c_filter: 
                    webnlg_full_train = 0
                    data[split] = []
                    for i in f: 
                        line, webnlg_full_train = json.loads(i), webnlg_full_train+1
                        if line['gem_id'] in keep_gem_ids: data[split].append(line)
                    args.logger.print('TRAIN set filter specifed for WebNLG. Before/After: ', 
                            webnlg_full_train, len(data[split]))
                else: data[split] = [json.loads(i) for i in f]

    return data, c_dqebaseline, trip_start_token_id, \
        input_sep_token_id, src_format, tgt_format, webnlg_full_train

def return_entry_elems(dataset_version, c_dqebaseline, entry, spacy_pipeline, webnlg2020_dpb2wkdt = None): 
    '''
    webnlg2020_dpb2wkdt (dict): will be set to None when using load_webnlg_normal; during load_webnlg_inference
    it will be used to convert from DBpedia representation to Wikidata representation.
    
    '''
    if 'gem-webnlg' in dataset_version:
        qa_pair_dict = {'NA': {'questions': [''], 'answers': ['']}}
        graph_orig = entry['input']
        # NOTE: convert to Wikidata representation in order to use SQG/CQG model (trained on KELM)
        if webnlg2020_dpb2wkdt is not None:
            graph_orig = [webnlg2020_dpb2wkdt[t] for t in graph_orig]
        ref_sentence = entry['target'] 

    elif 'dqe-webnlg' in dataset_version: 
        slog, tlog = entry['src_log'], entry['tgt_log']
        qa_pair_dict = tlog['self']
        # pull out questioned nested under keys such as 'QG_hash=ThomasNLG/t5-qg_squad1-en'
        for k, v in qa_pair_dict.items():
            qa_pair_dict[k]['questions'] = []
            for k2, v2 in v.items():
                if 'QG_hash' in k2: qa_pair_dict[k]['questions'].extend(v2['questions'])
        
        graph_orig = entry['src']
        # NOTE: convert to Wikidata representation in order to use SQG/CQG model (trained on KELM)
        if webnlg2020_dpb2wkdt is not None:
            graph_orig = [webnlg2020_dpb2wkdt[t] for t in graph_orig]
        ref_sentence = tlog['text']
        # if not 'dqe_gem', linearisation happens in prep4input
    
    graph_orig = dqe_triple_preprocessing(graph_orig, spacy_pipeline, c_dqebaseline)    
        
    return graph_orig, ref_sentence, qa_pair_dict

def get_asemtypes(ent_name, webnlg2020_enttypes, types_distri):
    # pick 1st, pick only wikidata labels (i.e. drop Q-codes)
    try: a_semtypes = webnlg2020_enttypes[ent_name][0]
    except KeyError: 
        try: a_semtypes = webnlg2020_enttypes[ent_name.replace(' ', '_')][0]
        except: 
            a_semtypes = None
            print(f'\t\t\t\t a_semtypes for {ent_name} failing')
    # retrieve only the names, leave out the Q-codes
    if a_semtypes is not None and a_semtypes != []: 
        
        a_semtypes = a_semtypes[1::2]
        if len(a_semtypes) > 1:
            a_semtypes = [i2.replace('_', ' ') for i in a_semtypes for i2 in i.split('; ')]
            cts = [types_distri[a_st] for a_st in a_semtypes]
            pick_idx = cts.index(max(cts))
            a_semtypes = a_semtypes[pick_idx]
        else: a_semtypes = a_semtypes[0]
    
    else: a_semtypes = ''

    return a_semtypes
