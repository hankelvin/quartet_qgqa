import copy, json, tqdm, time, math, re, os, glob, random
import itertools, datasets
from collections import defaultdict, Counter
from data_utils.data_utils_kbqa import KBQAEntry
from data_utils.data_utils_textqa import TextQAEntry
from data_utils.data_utils_textnxfacts import TextNxFactsEntry, TextAnswerSelectorEntry, EntLinkEntry
from data_utils.data_utils_data2text import Data2TextEntry
from data_utils.data_utils_omnibus import OmnibusCQ, CQ_Entry, SQEntry 
from data_utils.shared import trim_entry_obj
from dataset_handlers.utils import (_start_gem_formatter, chunk_checker, 
        dqe_triple_preprocessing, check_connected, Triple)
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from functools import partial
from transformers import AutoTokenizer
from nltk.metrics import edit_distance


def load_allq_cq(args, task_name, task_settings):
    assert len(task_settings['datasets']) == 1
    dataset = task_settings['datasets'][0]
    if 'webnlg' in dataset: dataset_key = 'webnlg'
    elif 'dqeablate' in dataset: dataset_key = 'dqeablate'
    else: raise NotImplementedError
    args.dataset_key = dataset_key

    start = time.time()
    data_merge = load_data_merge(args, task_settings,  negatives = False)
    c_filter = task_settings['datasets_filter'].get(f'allq-cq-{args.dataset_key}', False)
    if c_filter: 
        strategy, ratio = c_filter.split('-')
        ratio = float(ratio)
        args.logger.print('ALLQ-CQ filtering for ratio specified... proceeding to trim...') 
        args.logger.print(f'PRE TRIM # of {args.dataset_key.upper()} entries: ', len(data_merge))
        pick = random.sample(list(data_merge.keys()), math.ceil(len(data_merge) * ratio))
        data_merge = {k: data_merge[k] for k in pick}
        args.logger.print('POST TRIM # of entries: ', len(data_merge))
    
    args.logger.print(f'Loading for {task_name.upper()}')
    if task_name in ['sqg', 'cqg']: 
        data_allq = loader_allq_cq_QG(data_merge, args, task_name, task_settings)

    elif task_name in ['sqgcqg']: 
        data_allq = loader_allq_cq_QG(data_merge, args, 'sqg', task_settings)
        # NOTE: sqg is much smaller than cqg for allq-cq-webnlg (29k vs 490k)
        # and model could be skewed towards generating cqg given single triple
        if 'allq-cq-webnlg' in dataset: data_allq += data_allq 
        data_allq += loader_allq_cq_QG(data_merge, args, 'cqg', task_settings)
    
    elif task_name in ['quartet']: 
        itmd_pkls_dict, c_kelm = {}, args.dataset_key == 'kelm'
        data_allq = []
        src_types = set(task_settings['input_strategies'])
        args.logger.print('DATA: QUARTET using these tasks \t\t', src_types)
        c_allmix = \
            src_types == set(['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa', 'data2text', 'text2data']) or \
            src_types == set(['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa', 'data2text', 'text2data', 'g2el', 't2el']) 
        c_1stphase = src_types == set(['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa']) 
        c_2ndphase = \
            src_types == set(['data2text', 'text2data']) or \
            src_types == set(['data2text', 'text2data', 'g2el', 't2el']) 

        
        if c_allmix or c_1stphase:
            ### START QG 
            __pstve_sqg = loader_allq_cq_QG(data_merge, args, 'sqg', task_settings, use_mp = True)
            data_allq += __pstve_sqg
            args.logger.print('Current size of data_allq after adding SQG (+ve):', len(data_allq))
            if 'allq-cq-webnlg' in dataset or 'allq-cq-dqeablate' in dataset:  
                for i in range(1, 3): data_allq += __pstve_sqg
                args.logger.print('Current size of data_allq after upsampling SQG (+ve):', len(data_allq))
            del __pstve_sqg
                    
            data_allq += loader_allq_cq_QG(data_merge, args, 'cqg', task_settings, use_mp = True)
            args.logger.print('Current size of data_allq after adding CQG (+ve):', len(data_allq))

            ### START QA
            c_negsamp  = 'negsamp'  in dataset # base negative sampling 
            c_negsamp2 = 'negsamp2' in dataset # a. include SQUAD data, 
                                               # b. add negative 'unanswerable 'samples by randomly swapping 
                                               # c. include adverserial negatives (i.e. 2x reader returned unans or low conf score)
            c_negsamp3 = 'negsamp3' in dataset # same as 2, except exclude SQUAD data 
            c_negsamp4 = 'negsamp4' in dataset # same as 3, except exclude adverserial negatives
            negsamp_codes = [c_negsamp, c_negsamp2, c_negsamp3, c_negsamp4]
            negsamp_codes = ''.join([str(int(i)) for i in negsamp_codes])
            
            ### TEXT QA
            __pstve_textqa = loader_allq_cq_QA(data_merge, args, 'textqa', task_settings, 
                                        use_mp = True, c_negsamp = c_negsamp)                 
            if c_negsamp: 
                __ngtve_textqa_negsamp_size = 0 
                __pstve_textqa, __ngtve_textqa_negsamp = make_negative_QA_examples(__pstve_textqa, args, task_settings, TextQAEntry, hardnegs = 'hardnegs' in dataset)
                data_allq += __ngtve_textqa_negsamp
                __ngtve_textqa_negsamp_size += len(__ngtve_textqa_negsamp)
                args.logger.print('Current size of data_allq after adding TextQA (-ve negsamp):', len(data_allq))
                del __ngtve_textqa_negsamp
            data_allq += __pstve_textqa # add after all set
            args.logger.print('Current size of data_allq after adding TextQA (+ve):', len(data_allq))
            __pstve_textqa_size = len(__pstve_textqa)
            del __pstve_textqa

            ### KBQA
            __pstve_kbqa = loader_allq_cq_QA(data_merge, args, 'kbqa', task_settings, 
                                    use_mp = False if args.dataset_key == 'kelm' else True, c_negsamp = c_negsamp)    

            args.logger.print('Current size of data_allq after adding KBQA (+ve):', len(data_allq))
            if len(__pstve_kbqa) < __pstve_textqa_size:
                max_ct, min_ct = __pstve_textqa_size, len(__pstve_kbqa)
                __pstve_kbqa, upsampby = upsampler(max_ct, min_ct, __pstve_kbqa, src_type = 'kbqa', neg_condition = False)
                args.logger.print(f'Upsamp between src_types... min: {min_ct}; max: {max_ct}, upsamp {"KBQA (+ve)"} by: {upsampby}')

            if c_negsamp:
                __ngtve_kbqa_negsamp_size = 0 
                __pstve_kbqa, __ngtve_kbqa_negsamp = make_negative_QA_examples(__pstve_kbqa, args, task_settings, 
                                                            KBQAEntry, hardnegs = 'hardnegs' in dataset)
                __ngtve_kbqa_negsamp_size += len(__ngtve_kbqa_negsamp)

                if len(__ngtve_kbqa_negsamp) < __ngtve_textqa_negsamp_size:
                    max_ct, min_ct = __ngtve_textqa_negsamp_size, len(__ngtve_kbqa_negsamp)
                    __ngtve_kbqa_negsamp, upsampby = upsampler(max_ct, min_ct, __ngtve_kbqa_negsamp, src_type = 'kbqa', neg_condition = True)
                    args.logger.print(f'Upsamp between src_types... min: {min_ct}; max: {max_ct}, upsamp {"KBQA (-ve)"} by: {upsampby}')
            
                data_allq += __ngtve_kbqa_negsamp # add after all set
                args.logger.print('Current size of data_allq after adding KBQA (-ve negsamp):', len(data_allq))
                del __ngtve_kbqa_negsamp
                data_allq += __pstve_kbqa # add after all set
                args.logger.print('Current size of data_allq after adding KBQA (+ve):', len(data_allq))
                del __pstve_kbqa

            # "adverserial" negatives
            if not c_negsamp4:
                data_merge_negs = load_data_merge(args, task_settings,  negatives = True)
                __ngtve_textqa = loader_allq_cq_QA(data_merge_negs, args, 'textqa', task_settings, 
                                check_larger_factsets = False, use_mp = True)   
                # set negative attr to True (not included for constrastive loss)
                for __ in __ngtve_textqa: __.negative = True 
                data_allq += __ngtve_textqa
                args.logger.print('Current size of data_allq after adding TextQA (-ve adverserial):', len(data_allq))
                del __ngtve_textqa
                
                __ngtve_kbqa = loader_allq_cq_QA(data_merge_negs, args, 'kbqa', task_settings, 
                                check_larger_factsets = False, use_mp = True)   
                # set negative attr to True (not included for constrastive loss)
                for __ in __ngtve_kbqa: __.negative = True 
                data_allq += __ngtve_kbqa
                args.logger.print('Current size of data_allq after adding KBQA (-ve adverserial):', len(data_allq))
                del __ngtve_kbqa, data_merge_negs
            
        if c_allmix or c_2ndphase:
            # add Data2Text task 
            # for dqeablate... use only the WebNLG instances (i.e. no SQuAD, since no graph there)
            if args.dataset_key == 'dqeablate':
                data_merge_D2TT2D = {k:v for k,v in data_merge.items() if 'web_nlg' in k}
            else: data_merge_D2TT2D = data_merge
            directions = [st for st in src_types if st in ['data2text', 'text2data']]
            for direction in directions:
                dm_d2t = groupbyinput_D2T(data_merge_D2TT2D, direction)
                __d2tt2d = loader_allq_cq_TextSupport(data_merge_D2TT2D, dm_d2t, args, direction, task_settings, use_mp = True)
                print(f'There are these many {direction}: ', len(__d2tt2d))
                data_allq += __d2tt2d
                if 'allq-cq-webnlg' in dataset or 'allq-cq-dqeablate' in dataset:  
                    for i in range(1, 3): data_allq += __d2tt2d # upsample 2x
                args.logger.print(f'Current size of data_allq after adding {direction.upper()} (+ve):', len(data_allq))
            del __d2tt2d, dm_d2t
            
            # add EntLink task 
            if 'g2el' in src_types and 't2el' in src_types: 
                __pstve_entlink = loader_allq_cq_EntLink(data_merge, args, 'entlink' , task_settings, use_mp = True)
                print(f'There are these many EntLink entries: ', len(__pstve_entlink))
                data_allq += __pstve_entlink
                if 'allq-cq-webnlg' in dataset or 'allq-cq-dqeablate' in dataset: 
                    for i in range(1, 3): data_allq += __pstve_entlink # upsample 2x
                args.logger.print(f'Current size of data_allq after adding EntLink (+ve):', len(data_allq))
                del __pstve_entlink
    
    elif task_name in ['kbqa', 'textqa']:
        data_allq = loader_allq_cq_QA(data_merge, args, task_name, task_settings)    
    
    elif task_name in ['textnxfacts', 'textansselect']:
        data_allq = loader_allq_cq_TextSupport(data_merge, None, args, task_name, task_settings)

    else: raise NotImplementedError(f'TASK NAME: {task_name}')
    args.logger.print('Time taken to prep ALLQ-CQ data:', time.time()-start)
    return data_allq

def upsampler(max_ct, min_ct, data_allq, src_type, neg_condition, ratio = 3, bypass = False):
    '''helper to upsample a subset to a certain size'''   
    if bypass: pool = data_allq
    else: pool = [i for i in data_allq if i.ipstrat == src_type and i.negative == neg_condition]      
    if max_ct - min_ct <= min_ct:
        upsampby = max_ct - min_ct
        data_allq += random.sample(pool, upsampby)
    else:
        # if min is less than 'ratio' of max, upsamp to 'ratio' of max (avoid overfitting on min)
        # NOTE: applies to WebNLG g2cq case (20k vs 200k)
        if max_ct/min_ct > ratio: upsampby = __upsampby = int(max_ct/ratio - min_ct)
        else: upsampby = __upsampby = int(max_ct - min_ct)
        for i in range(math.floor(__upsampby/min_ct)): 
            data_allq += pool
            __upsampby  -= min_ct
        data_allq += random.sample(pool, __upsampby)
    return data_allq, upsampby


def batch_make_negative_QA_examples(data_list, args, task_settings, hardnegs, obj_class, dataset_key):
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)
    tgt_format, src_format = task_settings['tgt_format'], task_settings['src_format']
    new_data_list = []
    zeroinfo_rate, common_ents_ctr = [], Counter()
    
    for lang in args.languages:
        data_list_pool = [e for e in data_list if e.lang == lang]
        if hardnegs:
            print(f'Working on ent2entry Phase #1, lang: {lang}...')
            ent2entry = defaultdict(set)
            for idx, entry in enumerate(tqdm.tqdm(data_list_pool)):
                for ent in entry.triples_ents: 
                    ent2entry[ent].add(idx)
            print(f'Completed ent2entry phases, lang: {lang}...')
        for __, entry in enumerate(tqdm.tqdm(data_list_pool)):
            met = False
            c_strategy_zeroinfo = random.random() < 0.5
            c_hardnegs_xzerofinfo = hardnegs and not c_strategy_zeroinfo
            if c_hardnegs_xzerofinfo:
                pool = list(set(i for ent in entry.triples_ents for i in ent2entry[ent]))
                tries = min(3 if args.dataset_key  == 'kelm' else 10, len(pool))
            while not met:
                if c_hardnegs_xzerofinfo: 
                    if pool and tries > 0: 
                        pick_idx = random.choice(pool)
                        pick_entry = data_list_pool[pick_idx]
                        pool.remove(pick_idx)
                        tries -= 1
                    else: # could be (i) no tries left, or (ii) case ent/val appears in 1 entry only globally
                        c_strategy_zeroinfo = not c_strategy_zeroinfo 
                        c_hardnegs_xzerofinfo = not c_hardnegs_xzerofinfo
                        continue
                else: pick_entry = random.choice(data_list_pool)
                
                common_ents = pick_entry.triples_ents.intersection(entry.triples_ents)
                num_common_ents = len(common_ents)
                if not hardnegs and num_common_ents == 0: pass
                elif hardnegs and num_common_ents <= 1: 
                    # use c_strategy_zeroinfo to control ratio of each type of hardnegs
                    # type 1: completely no overlap in entity/value info between pos and neg (zeroinfo)
                    # type 2: a single piece of information (1x entity/value, i.e. 1/3 fact if not subj) overlaps 
                    # NOTE: not checking for property info here. (not likely very helpful)
                    # NOTE: hardnegs here differ from adverserial (i.e. not c_negsamp4 cases). the negatives there
                    # were not answerable by 2x QA readers (i.e. could be problem with the questions themselves)
                    # here (with negsamp4 selected), the non-zeroinfo negatives are likely well-formed Qs, 
                    # but whose contexts are missing the required information by design.
                    if c_strategy_zeroinfo:
                        if num_common_ents > 0: continue
                        
                    else:
                        # 1. if pick answer is in any of triples_ents or vice versa, don't proceed
                        # this handles cases of answers selected from text.
                        pick_ans_lwr = set(pick_entry.answer.lower().split())
                        for ent in entry.triples_ents:
                            ent_lwr = set(ent.lower().split())
                            if pick_ans_lwr.intersection(ent_lwr): continue
                        # 2. if answers are the same, don't proceed
                        # a. if answer appears in entry's context_f (whether text or graph), don't proceed
                        ans_lwr = set(entry.answer.lower().split())
                        # normal use case: context_f will be str
                        # under bypass_dqegem: context_f will be a tuple
                        if type(entry.context_f) == str: 
                            ans_lwr.update(entry.context_f.lower().split())
                        else: 
                            __ = ' '.join(' '.join(tup) for tup in entry.context_f)
                            ans_lwr.update(__.lower().split())
                        if pick_ans_lwr.intersection(ans_lwr): continue
                    
                    zeroinfo_rate.append(c_strategy_zeroinfo)
                    common_ents_ctr.update(common_ents)
                else: continue

                new_entry = obj_class(**entry.__dict__)
                new_entry.negative = True
                new_entry.question = pick_entry.question
                new_entry.answer = 'unanswerable'
                new_entry.lang = pick_entry.lang
                new_entry.tokenizer = tokenizer
                new_entry.prep4input(src_format = src_format, tgt_format = tgt_format, kelm = 'kelm' in dataset_key,
                                     multilingual = getattr(args, 'multilingual_trained', getattr(args, 'multilingual')))
                new_data_list.append(trim_entry_obj(new_entry))
                met = True

    print('QA NEGS, zeroinfo_rate'.upper(), sum(zeroinfo_rate)/len(zeroinfo_rate) if zeroinfo_rate else 0.0)
    print('QA NEGS, common_ents_ctr'.upper(), len(common_ents_ctr), '\n', common_ents_ctr.most_common(1000))
    print()
    
    return new_data_list
    

def make_negative_QA_examples(data_list, args, task_settings, obj_class, hardnegs = True, use_mp = True):
    '''
    create negative QA examples by randomly picking another example from entire set.
    only use a picked pair as if they have no intersecting entity/values 
    (property overlap is fine).
    # all the info about entry is kept, except question (replaced by pick_entry's),
    # and answer (replaced with 'unanswerable')
    '''
    ### NOTE: allow dqeablate, where squad is mixed with DQE's SynQG
    dqeablate_squad = []
    if args.dataset_key == 'dqeablate':
        dqeablate_squad = [m for m in data_list if 'squad' in m.id]
        data_list = [m for m in data_list if 'squad' not in m.id]
        print(f'\t\tDQEABLATE setting... inside negative QA creation... \
            there are {len(dqeablate_squad)} dqeablate_squad instances')
        print(f'\t\tDQEABLATE setting... inside negative QA creation... \
            there are {len(data_list)} data_list instances')

    print('making negative QA examples by sampling and swopping')
    num_chunks, split_sz, max_workers = chunk_checker(use_mp = use_mp, data_obj = data_list, 
                                max_workers = 4)
    if num_chunks > 1:  
        proc_chunks = [data_list[split_sz*(nc):split_sz*(1+nc)] for nc in range(num_chunks)]
        print('LEN proc_chunks', len(proc_chunks))
        print('num_chunks, split_sz, max_workers', num_chunks, split_sz, max_workers)
        del data_list # allow memory to be freed up as proc_chunks get pickled to go into PPE 
        func_batch_create = partial(batch_make_negative_QA_examples, args = copy.deepcopy(args), 
                                    task_settings = copy.deepcopy(task_settings), 
                                    hardnegs = copy.deepcopy(hardnegs),
                                    obj_class = copy.deepcopy(obj_class),
                                    dataset_key = copy.deepcopy(args.dataset_key))
        
        with ThreadPoolExecutor(max_workers = max_workers) as executor:     
            __data = executor.map(func_batch_create, proc_chunks)#, chunksize = 1000)
        new_data_list = []
        for chunk in __data: new_data_list.extend(chunk)
        del __data
        data_list = [trim_entry_obj(entry) for chunk in proc_chunks for entry in chunk]
        del proc_chunks    
    else: 
        new_data_list = \
            batch_make_negative_QA_examples(data_list, args, task_settings, hardnegs, 
                                        obj_class, args.dataset_key)
        data_list = [trim_entry_obj(entry) for entry in data_list]    

    if dqeablate_squad: 
        dqeablate_squad, new_dqeablate_squad = make_negative_QA_examples_squad(dqeablate_squad)
        data_list += dqeablate_squad
        new_data_list += new_dqeablate_squad
        print('\t\tdqeablate_squad negatives produced and added to rest...')

    return data_list, new_data_list

def make_negative_QA_examples_squad(data_list):
    '''
    create negative QA examples by randomly picking another example from entire set.
    only use a picked pair as if they have no intersecting entity/values 
    (property overlap is fine).

    '''
    new_data_list = []
    print('making negative QA examples by sampling and swopping')
    for __, entry in enumerate(tqdm.tqdm(data_list)):
        met = False
        while not met:
            pick_entry = random.choice(data_list)
            if pick_entry.context_f != entry.context_f:
                new_entry = TextQAEntry(**entry.__dict__)
                new_entry.negative = True
                new_entry.question = pick_entry.question
                new_entry.answer = 'unanswerable'
                new_entry.prep4input()
                new_data_list.append(trim_entry_obj(new_entry))
                met = True

    data_list = [trim_entry_obj(entry) for entry in data_list]

    return data_list, new_data_list


##### CQG on ALLQ-CQ #####
def loader_allq_cq_QG(data_merge, args, task_name, task_settings, use_mp = False):
    args.logger.print('TASK INPUT STRATEGIES', task_settings['input_strategies'])
    src_types = task_settings['input_strategies']

    data_allq = []
    start = time.time()

    # NOTE: to allow sqcq task 
    if task_name == 'sqg': _src_types = [i for i in src_types if i in ['t2sq', 'g2sq']] 
    elif task_name == 'cqg': _src_types = [i for i in src_types if i in ['t2cq', 'g2cq']]
    else: raise ValueError('Check task_name')
    
    for src_type in _src_types: 
        if task_name == 'cqg': 
            FUNCT, GBI_FUNCT = batch_create_CQG, groupbyinput_CQ
        elif task_name == 'sqg': 
            FUNCT, GBI_FUNCT = batch_create_SQG, groupbyinput_SQ            
        else: raise NotImplementedError
        data_merge_X = GBI_FUNCT(args, data_merge, src_type)

        num_chunks, split_sz, max_workers = chunk_checker(use_mp, data_merge, 
                                            max_workers = 2 if args.dataset_key == 'kelm' else 4)
        args.logger.print('num_chunks, split_sz: ', num_chunks, split_sz)
        if num_chunks > 1:  
            all_keys = list(data_merge_X.keys())
            chunk_keys = [all_keys[split_sz*(nc):split_sz*(1+nc)] for nc in range(num_chunks)]
            proc_chunks = [{k:data_merge_X[k] for k in ck } for ck in chunk_keys]
            func_batch_create = partial(FUNCT, args = copy.deepcopy(args), 
                            task_name = task_name, task_settings = copy.deepcopy(task_settings), 
                            src_type = src_type)
            
            with ProcessPoolExecutor(max_workers = max_workers) as executor:     
                __data = executor.map(func_batch_create, proc_chunks)
            
            for chunk in __data: data_allq.extend(chunk)
        else: 
            __data = FUNCT(data_merge_X, args, task_name, task_settings, src_type = src_type)
            data_allq.extend(__data)
        del __data
        
    # rebalance (upsample) between src_type
    src_type_ctr = Counter([i.ipstrat for i in data_allq])
    print('src_type_ctr'.upper(), src_type_ctr)
    max_type, max_ct = src_type_ctr.most_common()[0]
    for src_type in _src_types:
        if src_type == max_type: continue
        min_ct = src_type_ctr[src_type]
        data_allq, upsampby = upsampler(max_ct, min_ct, data_allq, src_type, neg_condition = False)
        args.logger.print(f'Upsamp between src_types... min: {min_ct}; max: {max_ct}, upsamp {src_type} by: {upsampby}')
        
    del data_merge_X

    args.logger.print(f'Time taken to prep ALLQ data:', time.time()-start)
    args.logger.print(f'NUMBER OF ALLQ triple-answer pairs accepted for {task_name} task: ', len(data_allq)) 
    return data_allq

def batch_create_SQG(sorted_by_input, args, task_name, task_settings, src_type = 'g2sq', q_per_line = 3):
    '''
    helper to build entry for SQG from text, 
    '''
    # instantiate a new tokenizer
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)
    gemformatter, spacy_pipeline = _start_gem_formatter(args, task_settings)
    qst_sep = args.trp_seps['qst']
    __data, ctr_lang = [], defaultdict(int)
    for __, (input2model, qa_pairs) in enumerate(tqdm.tqdm(sorted_by_input.items())):
        if not qa_pairs: continue
        # NOTE: take the first qa_pair (may not be same for all in set) 
        instance_idx = qa_pairs[0]['id'] 
        for lang in args.languages:
            # group questions by the answers
            ans_qst_dict = defaultdict(set)
            for qa_pair in qa_pairs:
                # for text, the gen_sentence for the lang has to be retrieved from qa_pairs
                gen_sentence_lang = qa_pair.get(f'gen_sentence_{lang}', '')
                if not gen_sentence_lang or f'question_{lang}' not in qa_pair: continue
                question = qa_pair[f'question_{lang}'].strip()
                cond_src_type = qa_pair['controls'][1]
                
                answers = []
                if src_type == 'g2sq': 
                    # bypass if the question generated was conditioned on an answer from text
                    # there will be a mismatch between answers extractable from the question subsequently.
                    if cond_src_type == 'text': continue
                    else: 
                        answers = [qa_pair[f'answer_org_{lang}'].strip()]
                else: 
                    # NOTE: for multilingual (i.e. non En)... text answer stays in X 
                    answers = set(__[0].strip() for __ in qa_pair[f'answer_readers_{lang}'])
                    # [['Philocoprella', 0.9784736037254333, [0, 13], 'roberta'],
                    # ['Philocoprella', 0.998862236738205, [0, 13], 'deberta']]
                    
                for answer in answers: ans_qst_dict[answer].add(question)
            
            for answer, question_set in ans_qst_dict.items():
                # remove empty questions (already in set, i.e. duplicate removed above)
                question_set = [q for q in question_set if q]
                # if no questions left or answer is '', skip
                if not answer or not question_set: continue
                # pad to multiples of q_per_line
                question_set += question_set[:q_per_line-len(question_set)%q_per_line]
                
                # loop through batches of questions.
                for start in range(0, len(question_set), q_per_line):
                    # check for duplicates again, in case an entry has < q_per_line q in total for a given answer
                    qset = set(question_set[start:start+q_per_line]) 
                    
                    xx = SQEntry(args = args, tokenizer = tokenizer, 
                                # join with [QST] separator
                                question = qst_sep.join([q for q in qset if q]), 
                                answer = answer, 
                                # NOTE: g2sq... rdf placed here
                                triples_name = input2model if src_type == 'g2sq' else None, 
                                triples_code = None, 
                                qtypes = None, a_semtypes = None,
                                task_name = task_name, 
                                task_settings = task_settings, 
                                id = instance_idx, gemformatter = gemformatter,
                                # NOTE: t2sq... text placed here 
                                context_f = gen_sentence_lang if src_type == 't2sq' else None)
                    xx.lang = lang
                    
                    try: xx.prep4input(src_type = src_type, 
                            multilingual = getattr(args, 'multilingual_trained', getattr(args, 'multilingual')))
                    except:
                        args.logger.print(f'ALLQ for KBQG-SQ: Discarding {instance_idx}, not preppable for KBQG-SQ format.')
                        continue
                    if len(xx.src) > 500 or len(xx.tgt) > 500:  
                        args.logger.print('SKIPPING', instance_idx)
                        continue 
                    __data.append(trim_entry_obj(xx))
                    ctr_lang[lang]+=1
                    if ctr_lang[lang] == 1: _inspect_entry_obj(lang, tokenizer, xx)
                        
    print('LEN __data', len(__data))
    return __data

def batch_create_CQG(sorted_by_input, args, task_name, task_settings, src_type = 'g2cq', q_per_line = 3):
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)
    # since graphs are kept in En, spacy and gemformatter should be in 'en
    gemformatter, spacy_pipeline = _start_gem_formatter(args, task_settings, language = 'en')
    qst_sep = args.trp_seps['qst']

    __data, ctr_lang = [], defaultdict(int)
    for __, (__input2model, qa_pairs) in enumerate(tqdm.tqdm(sorted_by_input.items())):
        for lang in args.languages: 
            if src_type == 'g2cq': 
                instance_idx, input2model = qa_pairs[0]['id'], __input2model
            else: 
                instance_idx, input2model, gs_lang = __input2model.split(' ^&^ ')
                # we are looping through args.languages, so only use input2model if it matches lang
                if f'[{lang}]' != gs_lang: continue
            # group questions by 
            # 1. the answers
            # 2. the triple set size
            ans_qst_dict = {2: defaultdict(set), 3: defaultdict(set), 
                            4: defaultdict(set), '5more': defaultdict(set)}
            
            for qa_pair in qa_pairs:
                if f'question_{lang}' not in qa_pair: continue
                question = qa_pair[f'question_{lang}'].strip()
                # ALLQ's CQ questions were conditioned on text or rdf
                # if text or rdd, always use the control 
                # (triples_name is trackng the triples for the whole gen_sentence)
                n_facts = int(qa_pair['controls'][0])
                cond_src_type = qa_pair['controls'][1]
                
                if n_facts == 0: 
                    args.logger.print('REJECTING! rdf_size == 0', qa_pair)
                    continue
                elif n_facts > 4: n_facts = '5more'
                
                answers = []
                if src_type == 'g2cq': 
                    if cond_src_type == 'text': continue
                    else: answers = [qa_pair[f'answer_org_{lang}'].strip()]
                else: 
                    # NOTE: for multilingual (i.e. non En)... text answer stays in X 
                    answers = set(__[0].strip() for __ in qa_pair[f'answer_readers_{lang}'])
                    # [['Philocoprella', 0.9784736037254333, [0, 13], 'roberta'],
                    # ['Philocoprella', 0.998862236738205, [0, 13], 'deberta']]
                    
                for answer in answers: ans_qst_dict[n_facts][answer].add(question)

            for n_facts, aq_set in ans_qst_dict.items():
                for answer, question_set in aq_set.items():
                    # remove empty questions (already in set, i.e. duplicate removed above)
                    question_set = [q for q in question_set if q]
                    # if no questions left or answer is '', skip
                    if not answer or not question_set: continue
                    # pad to multiples of q_per_line
                    question_set += question_set[:q_per_line-len(question_set)%q_per_line]
                    
                    # loop through batches of questions.
                    for start in range(0, len(question_set), q_per_line):
                        # check for duplicates again, in case an entry has < q_per_line q in total for a given answer
                        qset = set(question_set[start:start+q_per_line]) 

                        ### 2. Create CQ_Entry
                        __cq_entry = CQ_Entry(
                            args = args, 
                            question = qst_sep.join([q for q in qset if q]), 
                            answer = answer, 
                            triples_code = None,  
                            triples_name = input2model if src_type == 'g2cq' else None, 
                            # ALLQ's CQs were built with joint T2CQ-G2CQ controls. 
                            ans_pos = None, 
                            qtypes = None, 
                            a_semtypes = None, 
                            id = instance_idx, )

                        ### 3. Instantiate OmnibusCQ
                        xx = OmnibusCQ(args, tokenizer, __cq_entry, 
                                context_f = input2model if src_type == 't2cq' else None, 
                                triples_code = None, 
                                triples_name = None,
                                task_name = task_name, task_settings = task_settings,
                                id = instance_idx, gemformatter = gemformatter)
                        xx.lang = lang
                        try: xx.prep4input(src_type = src_type, n_facts = n_facts, 
                                multilingual = getattr(args, 'multilingual_trained', getattr(args, 'multilingual')))
                        except:
                            args.logger.print(f'ALLQ for KBQG-CQ: Discarding {instance_idx}, not preppable for KBQG-CQ format.')
                            continue
                        if len(xx.src) > 500 or len(xx.tgt) > 500:  
                            args.logger.print('SKIPPING', instance_idx)
                            continue 
                        __data.append(trim_entry_obj(xx))
                        ctr_lang[lang]+=1
                        if ctr_lang[lang] == 1: _inspect_entry_obj(lang, tokenizer, xx)        
    return __data


def loader_allq_cq_QA(data_merge, args, task_name, task_settings, use_mp = False, 
                    check_larger_factsets = True, c_negsamp = False): 
    args.logger.print('TASK INPUT STRATEGIES', task_settings['input_strategies'])
    
    if task_name == 'kbqa': FUNCT = batch_create_KBQA 
    elif task_name == 'textqa': FUNCT = batch_create_TextQA 
    else: raise NotImplementedError

    # for instantiating 'larger' factsets answerable by the same question 
    larger_factsets = None
    if check_larger_factsets: 
        larger_factsets, sorted2unsorted_dict = \
            collect_larger_factsets(data_merge, task_name, args, task_settings)

    if args.dataset_key == 'kelm':
        if task_name == 'kbqa': max_workers = 2 
        else: max_workers = 1 # 'textqa' was stalled with mp
    else: max_workers = 4

    data_allq = []
    start = time.time()
    num_chunks, split_sz, max_workers = chunk_checker(use_mp, data_merge, 
                                        max_workers = max_workers)
    args.logger.print('num_chunks, split_sz: ', num_chunks, split_sz)
    if num_chunks > 1:  
        all_keys = list(data_merge.keys())
        chunk_keys = [all_keys[split_sz*(nc):split_sz*(1+nc)] for nc in range(num_chunks)]
        proc_chunks = [{k:data_merge[k] for k in ck } for ck in chunk_keys]
        func_batch_create = partial(FUNCT, args = copy.deepcopy(args), 
                    task_name = task_name, task_settings = copy.deepcopy(task_settings),
                    larger_factsets = copy.deepcopy(larger_factsets), c_negsamp = c_negsamp,
                    sorted2unsorted_dict = copy.deepcopy(sorted2unsorted_dict))
        
        with ProcessPoolExecutor(max_workers = max_workers) as executor:     
            __data = executor.map(func_batch_create, proc_chunks)
            
        for chunk in __data: data_allq.extend(chunk)
    else: 
        __data = FUNCT(data_merge_batch = data_merge, args = args, task_name = task_name, 
                        task_settings = task_settings, larger_factsets = larger_factsets,  
                        c_negsamp = c_negsamp, 
                        sorted2unsorted_dict = sorted2unsorted_dict)
        data_allq.extend(__data)
    del __data

    args.logger.print(f'Time taken to prep ALLQ data:', time.time()-start)
    args.logger.print(f'NUMBER OF ALLQ triple-answer pairs accepted for {task_name} task: ', len(data_allq)) 
    del larger_factsets

    return data_allq


def loader_allq_cq_TextSupport(data_merge, dm_d2t, args, task_name, task_settings, 
                                use_mp = False): 
    c_d2tt2d = task_name in ['data2text', 'text2data']
    sorted2unsorted_dict = None
    if task_name == 'textnxfacts': FUNCT = batch_create_TextNumFacts 
    elif task_name == 'textansselect': FUNCT = batch_create_TextAnsSelect 
    elif c_d2tt2d: 
        FUNCT = batch_create_Data2Text 
        # to get sorted2unsorted_dict
        __, sorted2unsorted_dict = collect_larger_factsets(data_merge, 'kbqa', args, task_settings, 
                                    d2t = True) 
    else: raise NotImplementedError

    data_allq = []
    start = time.time()
    num_chunks, split_sz, max_workers = chunk_checker(use_mp, dm_d2t if c_d2tt2d else data_merge, 
                                        max_workers = 2 if args.dataset_key == 'kelm' else 4)
    args.logger.print('num_chunks, split_sz: ', num_chunks, split_sz)
    if num_chunks > 1:  
        all_keys = list(dm_d2t.keys()) if c_d2tt2d else list(data_merge.keys())
        chunk_keys = [all_keys[split_sz*(nc):split_sz*(1+nc)] for nc in range(num_chunks)]
        proc_chunks = [{k:dm_d2t[k] for k in ck } for ck in chunk_keys] \
                        if c_d2tt2d else [{k:data_merge[k] for k in ck } for ck in chunk_keys]
        func_batch_create = partial(FUNCT, args = copy.deepcopy(args), 
                        task_name = task_name, task_settings = copy.deepcopy(task_settings),
                        sorted2unsorted_dict = copy.deepcopy(sorted2unsorted_dict))
        
        with ProcessPoolExecutor(max_workers = max_workers) as executor:     
            __data = executor.map(func_batch_create, proc_chunks)
            
        for chunk in __data: data_allq.extend(chunk)
    else: 
        __data = FUNCT(data_merge_batch = dm_d2t if c_d2tt2d else data_merge, 
                    args = args, task_name = task_name, task_settings = task_settings, 
                    sorted2unsorted_dict = sorted2unsorted_dict)
        data_allq.extend(__data)
    del __data

    args.logger.print(f'Time taken to prep ALLQ data:', time.time()-start)
    args.logger.print(f'NUMBER OF ALLQ-CQ entries accepted for {task_name} task: ', len(data_allq)) 
    return data_allq

def loader_allq_cq_EntLink(data_merge, args, task_name, task_settings, use_mp = True): 
    if args.non_english: raise NotImplementedError
    dqeablate_bypass = args.dataset_key == 'dqeablate'
    ms_dict, enttypes_dict = load_enttypes_supp(args)
    # attach ms info to each MergeALLQ instance 
    # NOTE: enttypes_dict will be copied into batch funct to reduce mem req
    if args.dataset_key in ['webnlg', 'dqeablate']:
        # overwrite data_merge and restart with WebNLG 2020 in datasets in order to:
        # 1. retrieve the DBpedia to Wikidata version of triples in order to 
        # 2. retrieve the Wikidata codes for entities, and the enttypes
        dirpath = 'datasets//dqe_webnlg'
        webnlg_data = datasets.load_dataset('GEM/web_nlg', 'en', download_mode='REUSE_CACHE_IF_EXISTS'.lower())
        with open(f'{dirpath}/z00_webnlg2020_dpb2wkdt_triples.json') as f: d2w = json.load(f)
        with open(f'{dirpath}/z02_webnlg2020_entcodes.json') as f: entcodes = json.load(f)
        # with open(f'{dirpath}/z02_webnlg2020_enttypes.json') as f: enttypes = json.load(f)
        if args.dataset_key == 'webnlg': data2use = {} 
        else: data2use = {k: v for k,v in data_merge.items() if 'web_nlg' not in k} # leave others (e.g. SQuAD in)
        for split in ['train', 'validation']:
            for l in webnlg_data[split]:
                ts = l['input']
                wkdt_ts_name = [d2w[t].split(' | ') for t in ts]
                wkdt_ts_code = []
                for t in ts: 
                    spo = t.split(' | ')
                    
                    # construct wkdt tcode
                    s_ec = entcodes[spo[0]]
                    # handle cases such as A.F.C._Fylde ['Q18280', 'Q16835361', '##F_Literal']
                    if len(s_ec)>1: s_ec = [ec for ec in s_ec if ec[:3] != '##F']
                    s_ec = '##F_Literal' if len(s_ec)>1 else s_ec[0]
                    o_ec = entcodes[spo[2]]
                    if len(o_ec)>1: o_ec = [ec for ec in o_ec if ec[:3] != '##F']
                    o_ec = '##F_Literal' if len(o_ec)>1 else o_ec[0]
                    p = f'##R_{spo[1]}'
                    wkdt_ts_code.append((s_ec, p, o_ec))
                    
                data2use[l['gem_id']] = MergeALLQ(
                    id = l['gem_id'],
                    gen_sentence = l['target'],
                    cq = None, sq = None, 
                    triples_name = wkdt_ts_name,
                    triples_code = wkdt_ts_code)
    
    elif args.dataset_key == 'kelm': data2use = data_merge 
    fctr, actr = 0, 0
    for idx, ma_entry in data2use.items():
        if dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name:
            actr += 1 
            try: setattr(ma_entry, 'mentionspans', ms_dict[idx])
            except: 
                setattr(ma_entry, 'mentionspans', []) # i.e. no ms
                fctr += 1
                print(f'\t\t SQUAD {ma_entry.id}, could not add to ms_dict.')
        else: setattr(ma_entry, 'mentionspans', ms_dict[idx])
    if actr>0: print(f'SQUAD ms dict failed attachments {fctr}/{actr}')
    
    FUNCT = batch_create_EntLink
    data_allq = []
    start = time.time()
    num_chunks, split_sz, max_workers = chunk_checker(use_mp, data2use, 
                                        max_workers = 2 if args.dataset_key == 'kelm' else 4)
    args.logger.print('num_chunks, split_sz: ', num_chunks, split_sz)
    if num_chunks > 1:  
        all_keys = list(data2use.keys())
        chunk_keys = [all_keys[split_sz*(nc):split_sz*(1+nc)] for nc in range(num_chunks)]
        proc_chunks = [{k:data2use[k] for k in ck } for ck in chunk_keys]
        func_batch_create = partial(FUNCT, args = copy.deepcopy(args), 
                        task_name = task_name, task_settings = copy.deepcopy(task_settings),
                        enttypes_dict = copy.deepcopy(enttypes_dict))
        
        with ProcessPoolExecutor(max_workers = max_workers) as executor:     
            __data = executor.map(func_batch_create, proc_chunks)
            
        for chunk in __data: data_allq.extend(chunk)
    else: 
        __data = FUNCT(data_merge_batch = data2use, 
                    args = args, task_name = task_name, task_settings = task_settings,
                    enttypes_dict = enttypes_dict)
        data_allq.extend(__data)
    del __data

    args.logger.print(f'Time taken to prep ALLQ data:', time.time()-start)
    args.logger.print(f'NUMBER OF ALLQ-CQ entries accepted for {task_name} task: ', len(data_allq)) 
    return data_allq

def load_enttypes_supp(args, grain = 'fine'):
    '''
    load the BLINK mentionspans and enttypes info for WebNLG
    '''
    ms_dict, enttypes_dict = None, None
    # each batch opens z11 files (mentionspans, enttypes)
    if args.dataset_key == 'kelm': pass
    elif args.dataset_key in ['webnlg', 'dqeablate']: 
        dskeys = []
        if args.dataset_key == 'webnlg': dskeys = ['dqe_gemwebnlg']
        # add the SQuAD data into ms_dict, enttypes_dict
        if args.dataset_key == 'dqeablate': dskeys = ['squadv2', 'dqe_gemwebnlg']
        
        # NOTE: use only train and validation data
        ms_dict, enttypes_dict = {}, {}
        for dskey in dskeys: 
            for split in ['train', 'validation']:
                fp = 'datasets//dqe_webnlg/z11_{}_blinktext_{}.jsonl'
                with open(fp.format(dskey, split), encoding = 'utf-8') as f:
                    for l in f: 
                        line = json.loads(l)
                        idx = line.pop('id')
                        # NOTE: blinktext for squad was done on set of contexts,
                        # each 'id' there is prefaced with 'train'/'validation' 
                        # all the idxes sharing the same context are joined by '_'
                        if dskey == 'squadv2': 
                            idxes = idx.split('_')[1:]
                            idxes = [f'squad_{i}' for i in idxes]
                        else: idxes = [idx]
                        
                        # replace tuples of (code, ent_name) into a dict
                        wkdt_code = line.pop('wkdt_code')
                        n2c = {tup[1]: tup[0] for tup in wkdt_code}

                        # attach prediction (i.e. entity/value) to mentionspan
                        mentionspans = line['mentionspans']
                        predictions = line['predictions']
                        assert len(predictions) == len(mentionspans), {'mentionspans': mentionspans,
                                                                        'predictions':predictions}
                        for i, p in enumerate(predictions): mentionspans[i]['pred'] = p 
                        
                        # leave out all SEL predictions 
                        mentionspans = [ms for ms in line['mentionspans'] if ms['detect'] != 'sel']
                        
                        # attach entity code (using pred) to mentionspan
                        for i, ms in enumerate(mentionspans):
                            if ms['detect'] == 'blink': 
                                try: ms['ent_code'] = n2c[ms['pred']]
                                except: ms['ent_code'] = '##F_Literal'
                            else: ms['ent_code'] = '##F_Literal'

                        for idx in idxes: ms_dict[idx] = mentionspans

            fp = 'datasets//dqe_webnlg/z11_{}_blinktext_enttypes.json'
            with open(fp.format(dskey), encoding = 'utf-8') as f: enttypes_dict.update(json.load(f))
    else: raise NotImplementedError

    # 1. pre-processing of enttypes_dict
    types_counter = Counter()
    for k, v in enttypes_dict.items():
        # a. keep only the enttypes (i.e. drop type's Q-codes)
        if v is None: continue
        if grain == 'coarse': __ = [i2 for i in v[1::2] for i2 in i.split('; ') ]
        else: __ = v[1::2]
        types_counter.update(__)
        enttypes_dict[k] = __
        
    # 2. if multiple pick the most common ent type across entire data
    for k, v in enttypes_dict.items():
        if v is None: continue
        if len(v) > 1:
            ctr = {types_counter[enttype]: enttype for enttype in v}
            pick = ctr[max(ctr)]
            enttypes_dict[k] =  pick
        else: enttypes_dict[k] = v[0]

    return ms_dict, enttypes_dict

def proc_ent(ent_name, spacy_pipeline):
    '''
    helper func to place ent_name in gemformat if spec-ed
    '''
    return ' '.join([t.text for t in spacy_pipeline(Triple.clean_obj(ent_name.strip(), lc=False))])  

def batch_create_EntLink(data_merge_batch, args, task_name, task_settings, enttypes_dict): 
    dqeablate_bypass = args.dataset_key == 'dqeablate'
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)
    gemformatter, spacy_pipeline = _start_gem_formatter(args, task_settings)
    input_strategies = [i for i in task_settings['input_strategies'] if i in ['t2el', 'g2el']]
    
    __data = []
    for __, (instance_idx, ma_entry) in enumerate(tqdm.tqdm(data_merge_batch.items())): 
        # 0. PRELIMINARIES
        ### NOTE: 'web_nlg' in instance_idx check for dqeablate case where SQuAD will be in the mix.
        c_dqeablate = dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name
        if c_dqeablate: g_eset = []
        else:
             # a. reject entry if BLINK+Duckling ms has more (+-2) entities than in G
            # NOTE: +-2 to allow possiblity of implicit types explicitly realised in text
            g_eset_name = [t[::2] if len(t) == 3 else t[:2] + t[-1] for t in ma_entry.triples_name]
            g_eset_name = [e2 if gemformatter is None else proc_ent(e2, spacy_pipeline) \
                            for e in g_eset_name for e2 in e]
            
            g_eset_code = [t[::2] if len(t) == 3 else t[:2] + t[-1] for t in ma_entry.triples_code]
            g_eset_code = [e2 for e in g_eset_code for e2 in e]
            g_eset = set(zip(g_eset_name, g_eset_code))
            ms_eset = set(ms['pred'] for ms in ma_entry.mentionspans)
            diff = len(g_eset) - len(ms_eset)
            if (diff < -2 or diff > 2): 
                args.logger.print(f'ALLQ-CQ for EntLink: Discarding {instance_idx}, \
                    +- 2 entities across text/graph ({diff}).')
                continue
        
        # b. retrieve subset of enttypes for ma_entry
        new_ms = []
        for i, ms in enumerate(ma_entry.mentionspans): 
            if ms['detect'] == 'blink':
                try: 
                    etype = enttypes_dict[ms['ent_code']]
                    # exclude all types such as:
                    # 'Wikimedia disambiguation page'/'MediaWiki main-namespace page; Wikimedia internal item'
                    if 'MediaWiki' in etype or 'Wikimedia' in etype or 'disambiguation' in etype: continue
                    else: ms['enttype'] = etype
                except: continue
            elif ms['detect'] == 'duckling':
                ms['enttype'] = ms['type'].replace(' | ', '; ').replace('_', ' ')
            new_ms.append(ms)
        ma_entry.mentionspans = new_ms

        graph_mentionspans = {}
        # 1. for G, those without Wikidata Q-codes (i.e. not in WikidataQG or values)
        # >> align with Duckling predictions (check token overlap, as well as isalphanumeric/isnumeric)
        # str() as some duckling preds are digits and loaded as ints
        ducklings = {str(ms['pred']).lower(): ms['enttype'] for ms in ma_entry.mentionspans if ms['detect'] == 'duckling'}
        for g_en, g_ec in g_eset:
            # bypass if no duckling mentions, nothing for matching
            if g_ec[:3] == '##F' and ducklings:
                c_al, c_num = False, False
                for c in g_en: 
                    c_al, c_num = c.isalpha, c.isnumeric()
                    if c_al and c_num: break
                if not (c_al and c_num): continue
                # find the duckling pred that is the least edit distance to unmatched
                # a. normalise by graph ent_name length
                cands = {edit_distance(g_en.lower(), p)/len(g_en) : et for p, et in ducklings.items()}
                # b. accept only if edits < 50% of graph ent_name
                if min(cands) < 0.5: graph_mentionspans[g_en] = cands[min(cands)] 
                else: continue
            else:  
                try: 
                    etype = enttypes_dict[g_ec]
                    if 'MediaWiki' in etype or 'Wikimedia' in etype or 'disambiguation' in etype: continue
                    else: graph_mentionspans[g_en] = etype
                except: continue
                
        for src_type in input_strategies:
            if c_dqeablate and (src_type == 'g2el' or not ma_entry.mentionspans): continue
            xx = EntLinkEntry(args, tokenizer, context_f = ma_entry.gen_sentence_en, # NOTE: only En currently
                        triples_name = ma_entry.triples_name,
                        text_mentionspans = ma_entry.mentionspans, 
                        graph_mentionspans = graph_mentionspans,
                        id = instance_idx, gemformatter = gemformatter,
                        task_name = task_name, task_settings = task_settings)

            try: xx.prep4input(src_type = src_type)
            except: 
                args.logger.print(f'ALLQ-CQ for EntLink: Discarding {instance_idx} for {src_type}, \
                    not preppable for EntLink format, likely that answer was unalignable.')
                continue
            if len(xx.src) > 500 or len(xx.tgt) > 500:  
                args.logger.print('SKIPPING', instance_idx)
                continue 
            __data.append(trim_entry_obj(xx)) 
            
    return __data

def batch_create_KBQA(data_merge_batch, args, task_name, task_settings, larger_factsets, c_negsamp, 
                     sorted2unsorted_dict):
    dqeablate_bypass = args.dataset_key == 'dqeablate'
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)
    __, spacy_pipeline = _start_gem_formatter(args, task_settings)
    trip_start_token_id = None 
    tgt_format, src_format = task_settings['tgt_format'], task_settings['src_format']

    __data, ctr_lang = [], defaultdict(int)
    for __, (instance_idx, ma_entry) in enumerate(tqdm.tqdm(data_merge_batch.items())):
        if dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name: continue
        triples_name_orig = ma_entry.triples_name
        rdf_tripleset_sorted = tuple(tuple((t)) for t in sorted(triples_name_orig))
        try: rdf_tripleset_unsorted = sorted2unsorted_dict[rdf_tripleset_sorted]
        except: 
            args.logger.print(f'SKIPPING {rdf_tripleset_sorted}, sorted2unsorted_dict search failed.') 
            continue
        
        context_fs = [rdf_tripleset_unsorted] 
        rtu_origs = [triples_name_orig]
        if larger_factsets is not None:
            # NOTE: larger_factsets values wuld already be in gemformat if required
            for rtu in larger_factsets[rdf_tripleset_sorted]:
                # retrieve the right form (i.e. gemformat spec-ed)
                try: rdf_tripleset_unsorted = sorted2unsorted_dict[tuple(sorted(rtu))]
                except: 
                    args.logger.print(f'SKIPPING {rdf_tripleset_sorted} for context_fs, sorted2unsorted_dict search failed.') 
                    continue
                rtu_origs.append(rtu) # important rtu aligns with rdf_tripleset_unsorted (for triples_ents collection)
                context_fs.append(rdf_tripleset_unsorted)        
        else: pass

        for lang in args.languages:
            lang_key = f'_{lang}' if lang != 'en' else ''
            for sqcq in [f'sq_{lang}', f'cq_{lang}']:
                for idx, qa_pair in enumerate(getattr(ma_entry, sqcq, [])):
                    if qa_pair is None: continue # could be filtered (set to None) for non-En QA pair

                    # NOTE: remove questions created from text. 
                    # i.e. their answers we have are spans in the gen_sentence 
                    # and may not fully relate to a entity/value in the subgraph.
                    if qa_pair['controls'][1] == 'text': continue

                    ### 1. Create KBQAEntry
                    if f'question{lang_key}' not in qa_pair or f'answer_org{lang_key}' not in qa_pair: continue 
                    q, a, ans_pos = qa_pair[f'question{lang_key}'].strip(), qa_pair[f'answer_org{lang_key}'].strip(), None
                    # remove empty qa pairs
                    if not q or not a: continue
                    if args.dataset_key == 'kelm' and tgt_format == 'dqe_generate':
                        a = ' '.join([t.text for t in spacy_pipeline(Triple.clean_obj(a.strip(), lc=False))])

                    for idx, context_f in enumerate(context_fs):

                        xx = KBQAEntry(args = args, tokenizer = tokenizer, question = None, answer = None, 
                                context_f = context_f, triples_name = rtu_origs[idx], ans_pos = None,
                                task_name = task_name, task_settings = task_settings, 
                                trip_start_token_id = trip_start_token_id, id = None)
                        xx.question, xx.answer = q, a
                        xx.ans_pos, xx.id = ans_pos, f'{instance_idx}_{sqcq}{idx}'
                        xx.lang = lang

                        try: xx.prep4input(src_format = src_format, 
                                tgt_format = tgt_format, kelm= args.dataset_key == 'kelm',
                                multilingual = getattr(args, 'multilingual_trained', getattr(args, 'multilingual')))
                        except: 
                            args.logger.print(f'ALLQ-CQ for KBQA: Discarding {instance_idx}, \
                                not preppable for KBQA format, likely that answer was unalignable.')
                            continue
                        if len(xx.src) > 500 or len(xx.tgt) > 500:  
                            args.logger.print('SKIPPING', instance_idx)
                            continue 
                        if c_negsamp: __data.append(xx) # leave attrs for post-negative creation 
                        else: __data.append(trim_entry_obj(xx)) 
                        ctr_lang[lang] += 1
                        if ctr_lang[lang] == 1: _inspect_entry_obj(lang, tokenizer, xx)
    return __data


def batch_create_TextQA(data_merge_batch, args, task_name, task_settings, larger_factsets, c_negsamp, **kwargs):
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)
    input_sep_token_id = None 

    __data, ctr_lang = [], defaultdict(int)
    for __, (instance_idx, ma_entry) in enumerate(tqdm.tqdm(data_merge_batch.items())):
        for lang in args.languages: 
            lang_key = f'_{lang}' if lang != 'en' else '' # CHECKED 
            gen_sentence = getattr(ma_entry, f'gen_sentence_{lang}', None)
            if gen_sentence is None: continue
            if ma_entry.triples_name is not None: # for e.g. SQuAD has no triples_name
                triples_ents = set()
                for t in ma_entry.triples_name:
                    if len(t) == 3: triples_ents.update(t[::2])
                    elif len(t) == 4: triples_ents.update(t[0:1] + t[2:])
            else: triples_ents = None
            context_fs = [gen_sentence] 
            if larger_factsets is not None:
                add_context = [getattr(_ma_e, f'gen_sentence_{lang}', '') \
                               for _ma_e in larger_factsets[ma_entry.gen_sentence_en]]
                context_fs += [c for c in add_context if c]
            else: pass

            for sqcq in [f'sq_{lang}', f'cq_{lang}']:
                for idx, qa_pair in enumerate(getattr(ma_entry, sqcq, [])):
                    ### 1. Create TextEntry
                    if qa_pair is None: continue # could be filtered (set to None) for non-En QA pair
                    q = qa_pair.get(f'question{lang_key}', '').strip()
                    if not q: continue
                    
                    answers = set(a[0].strip() for a in qa_pair[f'answer_readers{lang_key}'])
                    
                    for a in answers:        
                        if not a: continue
                        for context_f in context_fs:
                            # reject if ans is not in context (not extractable)
                            if a != 'unanswerable' and a not in context_f: continue
                            xx = TextQAEntry(args = args, tokenizer = tokenizer, question = q, answer = a, 
                                        context_f = context_f, task_name = task_name, 
                                        task_settings = task_settings, input_sep_token_id = input_sep_token_id, 
                                        id = f'{instance_idx}_{sqcq}{idx}', triples_ents = triples_ents)
                            xx.lang = lang
                            if a == 'unanswerable': xx.negative = True # for the case of SQUAD, where negs are mixed in
                        
                            try: xx.prep4input(tgt_format = task_settings['tgt_format'], 
                                               multilingual = getattr(args, 'multilingual_trained', getattr(args, 'multilingual')))
                            except: 
                                args.logger.print(f'ALLQ-CQ for TextQA: Discarding {instance_idx}, \
                                    not preppable for TextQA format, likely that answer was unalignable.')
                                continue
                            if len(xx.src) > 500 or len(xx.tgt) > 500: 
                                args.logger.print('SKIPPING', instance_idx)
                                continue 
                            if c_negsamp: __data.append(xx)
                            else: __data.append(trim_entry_obj(xx)) # leave attrs for post-negative creation 
                            ctr_lang[lang]+=1
                            if ctr_lang[lang] == 1: _inspect_entry_obj(lang, tokenizer, xx)
    return __data


def batch_create_Data2Text(data_merge_batch, args, task_name, task_settings, sorted2unsorted_dict):
    if args.non_english: raise NotImplementedError
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)
    gemformatter = None 
    tgt_format, src_format = task_settings['tgt_format'], task_settings['src_format']
    c_trp_sep_scheme2 = 'trp_sep_scheme2' in src_format

    __data = []
    for __, (src, tgt_set) in enumerate(tqdm.tqdm(data_merge_batch.items())):
        if task_name == 'data2text': 
            rdf_tripleset_sorted = src
            try: rdf_tripleset_unsorted = sorted2unsorted_dict[rdf_tripleset_sorted]
            except: 
                args.logger.print(f'SKIPPING {rdf_tripleset_sorted}, sorted2unsorted_dict search failed.') 
                continue

            for context_f, instance_idx in tgt_set:
                xx = Data2TextEntry(args = args, tokenizer = tokenizer, 
                        context_f = context_f, triples_name = rdf_tripleset_unsorted, 
                        task_name = task_name, task_settings = task_settings, 
                        gemformatter = gemformatter, id = instance_idx, gemformat_done = True)
            
                try: xx.prep4input(direction = task_name)
                except: 
                    args.logger.print(f'ALLQ-CQ for Data2Text: Discarding {instance_idx}, \
                        not preppable for Data2Text format, likely that answer was unalignable.')
                    continue
                if len(xx.src) > 500 or len(xx.tgt) > 500:  
                    args.logger.print('SKIPPING', instance_idx)
                    continue 
                __data.append(trim_entry_obj(xx))
        
        elif task_name == 'text2data': 
            context_f = src
            for rdf_tripleset_sorted, instance_idx in tgt_set:
                try: rdf_tripleset_unsorted = sorted2unsorted_dict[rdf_tripleset_sorted]
                except: 
                    args.logger.print(f'SKIPPING {rdf_tripleset_sorted}, sorted2unsorted_dict search failed.') 
                    continue
                xx = Data2TextEntry(args = args, tokenizer = tokenizer, 
                        context_f = context_f, triples_name = rdf_tripleset_unsorted, 
                        task_name = task_name, task_settings = task_settings, 
                        gemformatter = gemformatter, id = instance_idx, gemformat_done = True)
            
                try: xx.prep4input(direction = task_name)
                except: 
                    args.logger.print(f'ALLQ-CQ for Text2Data: Discarding {instance_idx}, \
                        not preppable for Text2Data format, likely that answer was unalignable.')
                    continue
                if len(xx.src) > 500 or len(xx.tgt) > 500:  
                    args.logger.print('SKIPPING', instance_idx)
                    continue 
                __data.append(trim_entry_obj(xx))
    
    return __data


def groupbyinput_D2T(data_merge, task_name):
    '''
    NOTE: difference with GBI for SQG/CQG is that keys here are sorted RDF subgraphs (tripleset)
    '''
    groupedbyinput = defaultdict(set)
    for instance_idx, ma_entry in data_merge.items(): 
        
        context_f = ma_entry.gen_sentence_en # NOTE: only En now
        triples_name = ma_entry.triples_name
        rdf_tripleset_sorted = tuple(tuple((t)) for t in sorted(triples_name))
        
        if task_name == 'data2text':
            groupedbyinput[rdf_tripleset_sorted].add((context_f, instance_idx))
        elif task_name == 'text2data': 
            groupedbyinput[context_f].add((rdf_tripleset_sorted, instance_idx))

    return groupedbyinput


def batch_create_TextNumFacts(data_merge_batch, args,  task_name, task_settings, **kwargs):
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)

    __data = []
    for __, (instance_idx, ma_entry) in enumerate(tqdm.tqdm(data_merge_batch.items())):
        context_f = ma_entry.gen_sentence
        num_facts = len(ma_entry.triples_name)
        xx = TextNxFactsEntry(args = args, tokenizer = tokenizer, 
                        context_f = context_f, num_facts = num_facts, 
                        task_name = task_name, task_settings = task_settings, id = instance_idx)
            
        xx.prep4input()
        __data.append(trim_entry_obj(xx))

    return __data


def batch_create_TextAnsSelect(data_merge_batch, args,  task_name, task_settings, **kwargs):
    tokenizer = AutoTokenizer.from_pretrained(args.settings['model'])
    tokenizer.add_tokens(sorted(args.add_tokens), special_tokens = True)
    c_linker = 'blink' in task_settings['tgt_format']
    EL_FUNC, linker_args = None, None
    if c_linker: 
        # 1. try to load blinktext data
        start_blink = False
        c_webnlg = any(['webnlg' in ds for ds in task_settings['datasets']])
        c_kelm = any(['kelm' in ds for ds in task_settings['datasets']])
        if c_webnlg:
            dp = 'datasets//dqe_webnlg/'
            if args.non_english:
                fp_blinktext_webnlg = dp + 'z11_dqe_gemwebnlg_blinktext_{}-MULTILINGUAL.jsonl'
            else: 
                fp_blinktext_webnlg = dp + 'z11_dqe_gemwebnlg_blinktext_{}.jsonl'
            if not os.path.exists(fp_blinktext_webnlg.format('train')) or \
            not os.path.exists(fp_blinktext_webnlg.format('validation')): start_blink = True
        # 2. else start BLINK and DUCKLING and run pipeline
        if start_blink: 
            import sys
            sys.path.append('../evaluation')
            from evaluation_coverage_utils import start_run_linker, assess_sqcq
            EL_FUNC, linker_args = start_run_linker(bsz = 800, linker = 'blink', faiss = 'flat')

    __data = []
    context_fs_dict = defaultdict(dict)
    for __, (instance_idx, ma_entry) in enumerate(tqdm.tqdm(data_merge_batch.items())):
        for lang in args.languages: 
            context_f = getattr(ma_entry, f'gen_sentence_{lang}', None)
            if not context_f: continue
            answerset = set()
            lang_key = f'_{lang}' if lang != 'en' else ''
            if c_linker: context_fs_dict[instance_idx][lang] = context_f
            for sqcq in [f'sq{lang_key}', f'cq{lang_key}']:
                for qa_pair in getattr(ma_entry, sqcq, []): 
                    if not qa_pair: continue
                    answerset.update([a[0].strip() for a in qa_pair[f'answer_readers{lang_key}'] if a])

            xx = TextAnswerSelectorEntry(args = args, tokenizer = tokenizer, 
                            context_f = context_f, src_type = 'text', answerset = answerset, 
                            task_name = task_name, task_settings = task_settings, id = instance_idx)
            xx.lang = lang
            __data.append(xx)

    if c_linker:
        if start_blink:
            print('Working on BLINK+DUCKLING to obtain mentionspans')
            blinktext_dict = assess_sqcq({instance_idx: {'text': context_f} for instance_idx, context_f
                            in context_fs_dict.items()} , EL_FUNC, linker_args)
            print('BLINK+DUCKLING mentionspans obtained... adding to answerset')
        else: 
            print('Saved BLINKTEXT jsonls found, proceeding to load and use...')
            if c_webnlg: 
                blinktext_dict = {lang: defaultdict(dict) for lang in args.languages}
                for split in ['train', 'validation']:
                    print('\t\tLOADING from:', fp_blinktext_webnlg)
                    with open(fp_blinktext_webnlg.format(split)) as f:
                        for l in f: 
                            line = json.loads(l)
                            gem_id = line.pop('id')
                            for lang in args.languages:
                                if lang == 'en': 
                                    add_answers = set([ms['text'] \
                                    for ms in line['mentionspans'] if ms['detect'] != 'sel'])
                                else: 
                                    add_answers = set()
                                    for ms in line['mentionspans']:
                                        ms_lang = ms.get(f'text_{lang}', '')
                                        if ms['detect'] != 'sel' and ms_lang: 
                                            add_answers.add(ms[f'text_{lang}'])
                                blinktext_dict[lang][gem_id]['entities'] = add_answers
            if c_kelm: raise NotImplementedError
            
        for i, xx in enumerate(__data):
            answerset = blinktext_dict[xx.lang][xx.id]['entities']
            if i == 0:
                print('context_f', xx.context_f)
                print('answerset BEFORE', xx.answerset) 
                print('answerset LINKER', answerset) 
            xx.answerset.update(answerset)
            if i == 0:
                print('answerset FINAL', xx.answerset)
                print()
    data = []
    for i, xx in enumerate(__data): 
        c1 = not getattr(args, 'multilingual_trained', getattr(args, 'multilingual')) and xx.lang != 'en'
        c2 = getattr(args, 'multilingual_trained', getattr(args, 'multilingual'))
        if (c1 or c2) and not xx.answerset: continue
        xx.prep4input(bypass_starts = True if (c1 or c2) else False)
        data.append(trim_entry_obj(xx))

    del EL_FUNC, linker_args
    return data


def groupbyinput_SQ(args, data_merge, src_type):
    '''
    data_merge (dict): keys: idxnums, values: questions
    '''
    # for use if loading dqeablate (which has SynQG data and SQuAD data that 
    # are not aligned/accompanies with t/g)
    dqeablate_bypass = args.dataset_key == 'dqeablate'
    
    # 1. get 1-triples first
    rdf_1triple_dict = defaultdict(list)
    retrieve4tripleset = []
    for instance_idx, ma_entry in data_merge.items(): 
        # NOTE: use English QA pairs to gather (g'_|1|, q, a)
        qa_pairs = getattr(ma_entry, 'sq_en', []) 

        # for the case of triplesets with no SQs, we set them aside
        # and try to retrieve once all the 1-triples are collected. 
        if not qa_pairs: retrieve4tripleset.append(ma_entry)

        for qid, qa_pair in enumerate(qa_pairs):
            # check it is originated from RDF
            if dqeablate_bypass and int(qa_pair['controls'][0])!=1: continue
            assert qa_pair['controls'] ==  ["1", "rdf"] or qa_pair['controls'] ==  ["1", "text"], qa_pair['controls']
            assert len(qa_pair['input']) == 1
            # for loading dqeablate data
            rdf1triple = (tuple(qa_pair['input'][0]), )

            reduced_qa_pair = {}
            for lang in args.languages:
                if lang == 'en': qa_pair_lang = qa_pair
                else: 
                    xx = getattr(ma_entry, f'sq_{lang}', [])
                    if not xx or xx[qid] is None: continue
                    qa_pair_lang = xx[qid]
                src_lang_str = f'_{lang}' if lang != 'en' else ''
                if f'question{src_lang_str}' not in qa_pair_lang: continue
                reduced_qa_pair[f'gen_sentence_{lang}'] = getattr(ma_entry, f'gen_sentence_{lang}')
                reduced_qa_pair[f'question_{lang}'] = qa_pair_lang[f'question{src_lang_str}']
                reduced_qa_pair[f'answer_org_{lang}'] = qa_pair_lang[f'answer_org{src_lang_str}']
                reduced_qa_pair[f'answer_readers_{lang}'] = qa_pair_lang[f'answer_readers{src_lang_str}']
                if 'controls' not in reduced_qa_pair: reduced_qa_pair['controls'] = qa_pair_lang['controls']
                if 'id' not in reduced_qa_pair: reduced_qa_pair['id'] = instance_idx

            rdf_1triple_dict[rdf1triple].append(reduced_qa_pair)

    # 2. begin group by input
    groupedbyinput = defaultdict(list)
    # for g2sq, we are done (unless there are retrieve4tripleset)
    if src_type == 'g2sq': 
        groupedbyinput = rdf_1triple_dict    
        for ma_entry in retrieve4tripleset:
            # NOTE: dqeablate includes SQuAD entries which don't have triples_name
            if dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name: continue
            rdf_tripleset_unsorted = tuple(tuple(t) for t in ma_entry.triples_name)
            rdf_tripleset_sorted = tuple(sorted(rdf_tripleset_unsorted))
            qa_pairs_dicts = []
            for t in rdf_tripleset_sorted: 
                qa_pairs_dicts.extend(rdf_1triple_dict[(t,)])
            # make sure to update "id" to the entry's, make a copy first 
            # the id contains "train"/"validation" which we use to control 
            # the train dev split later. 
            for idx, qa_pair_dict in enumerate(qa_pairs_dicts): 
                new_qa_pair_dict = copy.copy(qa_pair_dict)
                new_qa_pair_dict['id'] = ma_entry.id
                qa_pairs_dicts[idx] = new_qa_pair_dict
            
            groupedbyinput[rdf_tripleset_unsorted] = qa_pairs_dicts

    # for t2sq, we group by gen_sentence, loop through it graph g
    # grab the set of questions for all its triples. 
    elif src_type == 't2sq':
        actr, fctr = 0, 0
        for instance_idx, ma_entry in data_merge.items(): 
            gen_sentence_en = ma_entry.gen_sentence_en
            if dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name: 
                actr+=1
                if rdf_1triple_dict[((gen_sentence_en, ), )] == []: 
                    fctr+=1
                    print(f'\t\tSQUAD fail {fctr}/{actr}', gen_sentence_en, rdf_1triple_dict[gen_sentence_en])
                groupedbyinput[gen_sentence_en] =  rdf_1triple_dict[((gen_sentence_en, ), )]
            else: 
                rdf_tripleset_sorted = tuple(tuple(t) for t in sorted(ma_entry.triples_name))
                qa_pairs_dicts = []
                for t in rdf_tripleset_sorted: qa_pairs_dicts.extend(rdf_1triple_dict[(t,)])
                for idx, qa_pair_dict in enumerate(qa_pairs_dicts): 
                    new_qa_pair_dict = copy.copy(qa_pair_dict)
                    new_qa_pair_dict['id'] = ma_entry.id
                    qa_pairs_dicts[idx] = new_qa_pair_dict
                groupedbyinput[gen_sentence_en] = qa_pairs_dicts
    
    print(f'SQ prep: groupbyinput for ({src_type})... num groupedbyinput', 
            len(groupedbyinput), sum([len(v) for v in groupedbyinput.values()]))
    for i in range(3):
        samp_pick = random.choice(list(groupedbyinput.keys()))
        print('SQ prep: groupbyinput visual inspect', samp_pick, groupedbyinput[samp_pick], )
    print()
    return groupedbyinput


def groupbyinput_CQ(args, data_merge, src_type):
    '''
    data_merge (dict): keys: idxnums, values: questions
    dqeablate_bypass (bool): for use if loading dqeablate (which has SynQG data and SQuAD data that 
    are not aligned/accompanies with t/g)
    '''
    dqeablate_bypass = args.dataset_key == 'dqeablate'

    # 1. get input triplesets first
    rdf_tripleset_dict = defaultdict(list)
    for instance_idx, ma_entry in data_merge.items(): 
        # NOTE: use English QA pairs to gather (g'_|1|, q, a)
        qa_pairs = getattr(ma_entry, 'cq_en', [])

        for qid, qa_pair in enumerate(qa_pairs):
            # check it is originated from RDF
            if qa_pair['controls'][1] == "rdf": 
                rdf_tripleset_unsorted = tuple(tuple(t) for t in qa_pair['input'])
            elif dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name: 
                # NOTE: rdf_tripleset_unsorted is actually text 
                rdf_tripleset_unsorted = ma_entry.gen_sentence_en
            else: 
                rdf_tripleset_unsorted = tuple(tuple(t) for t in ma_entry.triples_name)

            reduced_qa_pair = {}
            for lang in args.languages:
                if lang == 'en': qa_pair_lang = qa_pair
                else: 
                    xx = getattr(ma_entry, f'cq_{lang}', [])
                    if not xx or xx[qid] is None: continue                    
                    qa_pair_lang = xx[qid]
                src_lang_str = f'_{lang}' if lang != 'en' else ''
                if f'question{src_lang_str}' not in qa_pair_lang: continue
                reduced_qa_pair[f'question_{lang}'] = qa_pair_lang[f'question{src_lang_str}']
                reduced_qa_pair[f'answer_org_{lang}'] = qa_pair_lang[f'answer_org{src_lang_str}']
                reduced_qa_pair[f'answer_readers_{lang}'] = qa_pair_lang[f'answer_readers{src_lang_str}']
                if 'controls' not in reduced_qa_pair: reduced_qa_pair['controls'] =  qa_pair_lang['controls']
                if 'id' not in reduced_qa_pair: reduced_qa_pair['id'] = instance_idx

            rdf_tripleset_dict[rdf_tripleset_unsorted].append(reduced_qa_pair)
    
    # 2. begin group by input
    groupedbyinput = defaultdict(list)
    
    # a. for g2cq, we are done
    if src_type == 'g2cq': groupedbyinput = rdf_tripleset_dict    
    
    # b. for t2cq, we group by gen_sentence, loop through its graph g
    # grab the set of questions for all its triples. 
    elif src_type == 't2cq':
        for instance_idx, ma_entry in data_merge.items(): 
            for lang in args.languages:
                gen_sentence_lang = getattr(ma_entry, f'gen_sentence_{lang}', '')
                if not gen_sentence_lang: continue
                if dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name: 
                    # NOTE: check_key_unsorted is actually text 
                    check_key_unsorted = gen_sentence_lang
                    groupedbyinput[f'{instance_idx} ^&^ {gen_sentence_lang} ^&^ [{lang}]']\
                                        .extend(rdf_tripleset_dict[check_key_unsorted])
                else: 
                    triples_name = sorted(ma_entry.triples_name)
                    for n_facts in [2,3,4]:
                        combis = list(itertools.combinations(triples_name, n_facts))
                        # NOTE: qwebnlg_EFU option (add graph the Q itself was conditioned on)
                        if args.qwebnlg_EFU: combis.append(triples_name)
                        for c in combis:
                            check_key_unsorted = tuple(tuple(t) for t in c)
                            try: groupedbyinput[f'{instance_idx} ^&^ {gen_sentence_lang} ^&^ [{lang}]']\
                                        .extend(rdf_tripleset_dict[check_key_unsorted])
                            except: pass

    print(f'CQ prep: groupbyinput for ({src_type})... num groupedbyinput', 
            len(groupedbyinput), sum([len(v) for v in groupedbyinput.values()]))
    for i in range(3):
        samp_pick = random.choice(list(groupedbyinput.keys()))
        print('CQ prep: groupbyinput visual inspect', samp_pick, groupedbyinput[samp_pick], )
    print()
    return groupedbyinput


def produce_sorted2unsorted_dict(data_merge, src_format, args, task_settings):
    '''
    helper to return a mapping from sorted RDF subgraph (triplesets) and unsorted.
    convert unsorted to gemformat if spec-ed
    '''
    dqeablate_bypass = args.dataset_key == 'dqeablate'
    sorted2unsorted_dict = {}
    c_gemformat = 'dqe_gem' in src_format
    gemformatter = None
    if c_gemformat: gemformatter, __ = _start_gem_formatter(args, task_settings)

    for __, (instance_idx, ma_entry) in enumerate(tqdm.tqdm(data_merge.items())):
        if dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name: continue
        rdf_tripleset_unsorted = tuple(tuple(t) for t in ma_entry.triples_name)
        rdf_tripleset_sorted = tuple(sorted(rdf_tripleset_unsorted))            
        if c_gemformat:
            try: rdf_tripleset_unsorted_gem = gemformatter(rdf_tripleset_unsorted)
            except: 
                args.logger.print(f'SKIPPING {instance_idx}, gem_formatter fail.') 
                continue
            sorted2unsorted_dict[rdf_tripleset_sorted] = rdf_tripleset_unsorted_gem
        else: 
            sorted2unsorted_dict[rdf_tripleset_sorted] = rdf_tripleset_unsorted

    return sorted2unsorted_dict


def collect_larger_factsets(data_merge, task_name, args, task_settings, d2t = False):
    ''''
    1. Given a fact, associate it with the set of facts (either in text or graph form) 
    that are larger than  it. i.e. it is a full subset of the other. 
    used for loading TextQA and KBQA.
    2. Given an RDF subgraph, return the mapping from sorted to unsorted. We use sorted 
    for efficient search. Unsorted is used as input to model (to avoid shifting dataset characteristic
    or causing distri shift between training and inference.). If gemformat required for model input, 
    also convert unsorted form to gemformat, to avoid repeated conversions later on. 
    
    NOTE: difference with GBI for SQG/CQG is that keys here are sorted RDF subgraphs (tripleset)
    '''
    dqeablate_bypass = args.dataset_key == 'dqeablate'
    print('INSIDE collect_larger_factsets')
    if task_name == 'textqa':
        factset_dict, gt_dict = defaultdict(set), {}
        g_s_e2ma_entry = {ma_entry.gen_sentence_en: ma_entry for instance_idx, ma_entry in data_merge.items()}
        for instance_idx, ma_entry in data_merge.items(): 
            if dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name: continue
            rdf_tripleset_sorted = tuple(tuple(t) for t in sorted(ma_entry.triples_name))
            gt_dict[rdf_tripleset_sorted] = ma_entry.gen_sentence_en
        for rdf_tripleset_sorted, gen_sentence_en in gt_dict.items(): 
            for n_facts in range(1, len(rdf_tripleset_sorted)):
                combis = itertools.combinations(range(len(rdf_tripleset_sorted)), n_facts)
                for c in combis:
                    cut = tuple(t for i, t in enumerate(rdf_tripleset_sorted) if i in c)
                    cut_sorted = tuple(sorted(cut))
                    # associate the larger factset to the cut
                    if cut_sorted in gt_dict:
                        smaller_gen_sentence_en = gt_dict[cut_sorted]
                        s_g_s_e_ma_entry = g_s_e2ma_entry[smaller_gen_sentence_en]
                        factset_dict[smaller_gen_sentence_en].add(s_g_s_e_ma_entry)

        print('TextQA factset_dict size: ', 
                len(factset_dict), sum(len(i) for i in factset_dict.values()))
        sorted2unsorted_dict = {} # empty 

    elif task_name == 'kbqa':
        factset_dict = defaultdict(set)
        sorted2unsorted_dict = {}
        src_format = task_settings['src_format']
        
        # 1. sorted2unsorted_dict serves 2 purposes
        # a. check for existence below
        # b. used for gemformat retrieval later (if spec-ed)
        
        print('INSIDE KBQA BRANCH... PRODUCING sorted2unsorted_dict')
        num_chunks, split_sz, max_workers = chunk_checker(use_mp = True, data_obj = data_merge, max_workers = 8)
        if num_chunks > 1:  
            all_keys = list(data_merge.keys())
            chunk_keys = [all_keys[split_sz*(nc):split_sz*(1+nc)] for nc in range(num_chunks)]
            proc_chunks = [{k:data_merge[k] for k in ck } for ck in chunk_keys]
            func_batch_create = partial(produce_sorted2unsorted_dict, args = copy.deepcopy(args), 
                            src_format = src_format, task_settings = copy.deepcopy(task_settings),)
            
            with ProcessPoolExecutor(max_workers = max_workers) as executor:     
                __data = executor.map(func_batch_create, proc_chunks)
            for chunk in __data: 
                for k,v in chunk.items(): sorted2unsorted_dict[k] = v
            del __data
        else: 
            sorted2unsorted_dict = produce_sorted2unsorted_dict(data_merge, src_format, args, task_settings)

        if d2t: return None, sorted2unsorted_dict 

        print('INSIDE KBQA BRANCH PRODUCING factset_dict')
        for __, (instance_idx, ma_entry) in enumerate(tqdm.tqdm(data_merge.items())): 
            if dqeablate_bypass and 'web_nlg' not in ma_entry.id and not ma_entry.triples_name: continue
            rdf_tripleset_unsorted = tuple(tuple(t) for t in ma_entry.triples_name)
            rdf_tripleset_sorted = tuple(sorted(rdf_tripleset_unsorted))
            
            for n_facts in range(1, len(rdf_tripleset_sorted)):
                combis = itertools.combinations(range(len(rdf_tripleset_sorted)), n_facts)
                for c in combis:
                    cut = tuple(t for i, t in enumerate(rdf_tripleset_sorted) if i in c)
                    if not check_connected(cut): continue 
                    cut_sorted = tuple(sorted(cut))
                    if cut_sorted not in sorted2unsorted_dict: continue

                    # associate the larger factset to the cut (sorted)
                    factset_dict[cut_sorted].add(rdf_tripleset_unsorted)

        print('KBQA factset_dict size: ', 
                len(factset_dict), sum(len(i) for i in factset_dict.values()))
    
    else: raise ValueError('Check task_name settings')

    for i in range(3):
        if len(factset_dict) > 0:
            samp_pick = random.choice(list(factset_dict.keys()))
            if task_name == 'textqa': 
                print('collect_larger_factsets visual inspect', samp_pick, 
                    getattr(factset_dict[samp_pick], 'gen_sentence_en', ''), )
            if task_name == 'kbqa': 
                print('collect_larger_facts visual inspect', samp_pick, factset_dict[samp_pick],)
        if task_name == 'kbqa' and len(sorted2unsorted_dict) > 0:
            samp_pick = random.choice(list(sorted2unsorted_dict.keys()))
            print('sorted2unsorted_dict visual inspect', samp_pick, sorted2unsorted_dict[samp_pick], )
    print()
    return factset_dict, sorted2unsorted_dict

def _inspect_entry_obj(lang, tokenizer, xx):
    print('LANG: ', lang)
    print('SRC: ', tokenizer.decode(xx.src))
    print('TGT: ', tokenizer.decode(xx.tgt))
    print()

def prepare_merge_cq_allq(args,  
            data_allq, data_cq, savefp, cq_no_reject = False, c_filter_nosubobj = False):
    '''
    given data_cq, fold all the questions into their 
    respective instance_idx entry in data_allq_cq
    '''
    gemformatter, spacy_pipeline = _start_gem_formatter(args, task_settings = None, bypass = True)
    data_merge = {}
    args.logger.print('Going through ALLQ - restore RDF subgraph to list of lists')
    for idx, line in enumerate(tqdm.tqdm(data_allq)):
        graph_orig = line
        for idx2, qa_pair in enumerate(line['sq']):
            # place all input triple into list of S,P,O
            triple = qa_pair['input'].strip('</s>').replace('[SUB]', '')
            for sep in ['PRP', 'OBJ']:
                triple = re.sub(rf'\s*\[{sep}\]\s*', ' | ', triple)
            
            line['sq'][idx2]['input'] = [triple.strip().split(' | ')]

        for idx2, qa_pair in enumerate(line['cq']):
            tripleset = qa_pair['input'].strip('</s>')
            if '[SUB]' in tripleset: 
                tripleset = tripleset.split('[SUB]')[1:]
                # place all input triple into list of S,P,O
                for idxt, triple in enumerate(tripleset):
                    for sep in ['PRP', 'OBJ', 'QOBJ']:
                        triple = re.sub(rf'\s*\[{sep}\]\s*', ' | ', triple)
                    triple = re.sub(rf'\s*\[REL\]\s*', ' ', triple)
                    tripleset[idxt] = triple.strip().split(' | ')
                line['cq'][idx2]['input'] = tripleset
            else: pass
            
        data_merge[line['id']] = MergeALLQ(**line)

    args.logger.print('Going through CQ - merging with ALLQ')
    for __, line in enumerate(tqdm.tqdm(data_cq)):
        if not line['qa_pairs']: continue 
        instance_idx = line['qa_pairs'][0]['id'][:-4]

        gen_sentence = line['gen_sentence']
        for qa in line['qa_pairs']: 
            qst, ans = qa['question'].strip(), qa['answer'].strip()
            # exclude "Is .... " questions that are not answerable by graph/extractive
            if qst[:3].lower() == 'is ': continue
            inp = qa['triples_name_prime']
            ans_pos = qa['ans_pos']
            
            try: span = re.search(rf'{re.escape(ans)}', gen_sentence)
            except: span = False

            qa_entry = \
            {'question': qst, 'controls': [len(inp), 'rdf'], 'input': inp,
            'answer_org': inp[ans_pos[0]][ans_pos[1]], # retrieve answer ent/val from triple
            # answer span, score (None), [start,end], reader
            'answer_readers': [[ans, None, 
                [*span.span()] if span else [None, None], 'roberta']]}
            if instance_idx in data_merge:
                data_merge[instance_idx].cq.append(qa_entry)
            else: 
                data_merge[instance_idx] = MergeALLQ(**{'gen_sentence': line['gen_sentence'],
                    'sq': [], 'cq': [qa_entry], 'id': instance_idx})
    args.logger.print('MERGE between ALLQ and CQ completed...')

    args.logger.print('Retrieving full RDF subgraph (name and code) for gen_sentence')
    if args.dataset_key == 'webnlg':
        fps = glob.glob('datasets/dqe_webnlg/dqe_gemwebnlg_haystack_qa-pairs*.jsonl')
        fps = [i for i in fps if '_cqg' not in i and 'test' not in i]
    else: raise NotImplementedError
    
    for fp in sorted(fps): 
        args.logger.print('Working on: ', fp)
        with open(fp, encoding = 'utf-8') as f:
            while True:
                try: line = json.loads(next(f))
                except StopIteration: break 
                if line['id'] in data_merge:
                    tname = line['triples_name']
                    if args.dataset_key == 'kelm':
                        tcode = line['triples_code']
                    if args.dataset_key == 'webnlg': 
                        tname = dqe_triple_preprocessing(tname, spacy_pipeline)
                        # TODO: add tcode ?
                        tcode = None
                    data_merge[line['id']].triples_name = tname
                    data_merge[line['id']].triples_code = tcode
    
    for instance_idx, v in data_merge.items(): 
        assert getattr(v, 'triples_name', False), f'{instance_idx} not paired with triples_name'
    args.logger.print('Full RDF subgraph retrieved for all gen_sentences.')

    args.logger.print('proceeding to save to:', savefp)
    with open(savefp, 'w+') as f:
        for instance_idx, ma_entry in data_merge.items():
            ma_entry.id = instance_idx
            f.write(json.dumps(ma_entry.__dict__) + '\n')
    args.logger.print('Merged ALLQ and CQ saved to:', savefp)
    
    return data_merge


def load_data_merge(args, task_settings,  negatives = False):
    '''
    params:
    negatives (bool): whether to return a version of data_merge with negative qa_pairs
    for textqa and kbqa settings.
    '''
    allq_cq_keys = [k for k in task_settings['datasets_filter'].keys() if 'allq-cq-' in k]
    cq_keys = [i for i in task_settings['datasets_filter'].keys() if i[:3] == 'cq-']
    c_filter, c_filter_cq = '', ''
    for k in allq_cq_keys:  c_filter = task_settings['datasets_filter'].get(k, '')
    for k in cq_keys:       c_filter_cq = task_settings['datasets_filter'].get(k, '')
    c_filter_nosubobj = 'nosubjobj' in c_filter or 'nosubjobj' in c_filter_cq
    if c_filter_nosubobj: 
        args.logger.print(f'ALLQ-CQ: using nosubobj version.')
    cq_no_reject = False 
    if c_filter_cq and c_filter_cq == 'no-rejects': 
        cq_no_reject = True
        args.logger.print(f'ALLQ-CQ filtering for {args.dataset_key} no-rejects specified... \
                            will remove rejects during merge.')

    cq_rjt = '_no-rejects' if cq_no_reject else ''
    subobj_str = '_nosubjobj' if  c_filter_nosubobj else ''
    if args.dataset_key == 'webnlg': dkey, fkey, fkey2 = 'dqe_webnlg', 'webnlg', 'WebNLG'
    elif args.dataset_key == 'dqeablate': dkey, fkey, fkey2 = 'dqe_webnlg', 'dqeablate', 'dqeablate'
    else:  raise NotImplementedError
    neg_key = '_negatives' if negatives else ''
    mling_key = '_MULTILINGUAL_Xinputgraph' if args.non_english else ''
    c_e_r_m = args.multilingual_ERM
    c_e_q_q = args.multilingual_EQQ
    savefp = f'datasets/{dkey}/z08_allq_cq_{fkey}{neg_key}_MERGED{cq_rjt}{subobj_str}{mling_key}.jsonl'
    print('savefp'.upper(), savefp)

    if os.path.exists(savefp):
        args.logger.print('A saved version of MERGED ALLQ and CQ found. proceeding to load... from:', savefp)
        data_merge = {}
        print('\t\t c_e_r_m:'.upper(), c_e_r_m)
        print('\t\t c_e_q_q:'.upper(), c_e_q_q)
        attrs = ['id', 'gen_sentence', 'cq', 'sq', 'triples_name', 'triples_code']
        ctr_c_lang, ctr_c_all = defaultdict(int), 0
        ctr_q_fail = {'cq': defaultdict(int), 'sq': defaultdict(int)}
        ctr_q_all  = {'cq': defaultdict(int), 'sq': defaultdict(int)}
        ctr_cqsq_by_nf = {lang: Counter() for lang in args.languages}
        with open(savefp, encoding='utf-8') as f:
            for i in f: 
                ctr_c_all+=1
                line = json.loads(i)
                instance_idx = line['id']
                
                # add En first 
                add_line = {f'{attr}': line[attr] for attr in attrs}
                    
                data_merge[instance_idx] = MergeALLQ(**add_line) 
        args.logger.print(f'MERGED ALLQ and CQ successfully loaded. # instances: {len(data_merge)}')
        args.logger.print("FAILURES CTX:", ctr_c_all, ctr_c_lang)
        args.logger.print("FAILURES CQ :", ctr_q_fail['cq'], ctr_q_all['cq'])
        args.logger.print("FAILURES SQ :", ctr_q_fail['sq'], ctr_q_all['sq'])
        args.logger.print('DISTRIBUTION OF Q by NF:', ctr_cqsq_by_nf)
    else: 
        args.logger.print('No saved version of MERGED ALLQ and CQ found. proceeding to merge from scratch.', savefp)
        if args.dataset_key == 'dqeablate':
            args.logger('USING load_data_merge_dqeablate_bypass')
            data_merge = load_data_merge_dqeablate_bypass(args, savefp, dkey)

        else: 
            fp = f'datasets/{dkey}/z08_allq_{fkey}{neg_key}{subobj_str}.jsonl'
            with open(fp, encoding = 'utf-8') as f:  data_allq = [json.loads(i) for i in f]
            
            fp = f'datasets/{dkey}/CQ_{fkey2}_master_x2reader.jsonl'
            if negatives: data_cq = [] # Important, don't mix other sole cq set in (non-negs there)
            else:
                with open(fp, encoding = 'utf-8') as f: data_cq = [json.loads(i) for i in f]
            
            # merge start 
            data_merge = prepare_merge_cq_allq(args,  
                            data_allq, data_cq, savefp, cq_no_reject, c_filter_nosubobj)
    
    return data_merge

def trim_multilingual_line_cqsq(line_cqsq, lang, gen_sentence, sq = False, 
                                    mling_keys = ['question', 'answer_org', 'answer_readers'], 
                                    bsc_cut = 0.7, gleu_cut = 0.3, qa_conf_cut = 0.7,
                                    ensure_readers_match = False, ensure_qac_quality = False,):
    new_line_cqsq = []
    for qid, qaline in enumerate(line_cqsq): 
        __ = {}
        for k in mling_keys:  
            # set En to its own keys (for use in cross-lingual answer in KBQA+KBQG)           
            __[f'{k}_en'] = qaline[k]
            # set xprime into keys
            __[f'{k}_{lang}'] = qaline[f'{k}_{lang}']
        __['controls'] = qaline['controls']
        if __['controls'][1] == 'text': 
            # follow format in z08_allq_cq_webnlg_MERGED_nosubjobj.jsonl
            if sq: __['input'] = [[gen_sentence]] 
            else: __['input'] = gen_sentence
        else: __['input'] = qaline['input']
        # if the reader_answers don't share at least 1 token overlap, exclude from training data 
        c1 = not qaline[f'answer_readers_match_{lang}']
        c2 = qaline[f'answer_org_{lang}_support']['method'] == 'misalign'
        
        if ensure_readers_match and (c1 or c2): 
            new_line_cqsq.append(None)
            continue 
        elif ensure_qac_quality:
            qst_bt_scores = qaline[f'qst_bt_scores_{lang}']
            c3 = any([i[1] < qa_conf_cut for i in qaline[f'answer_readers_{lang}']])
            c4 = qst_bt_scores['bsc'] < bsc_cut or qst_bt_scores['gleu'] < gleu_cut
            if (c1 or c2 or c3 or c4): 
                new_line_cqsq.append(None)
                continue 

        new_line_cqsq.append(__)

    return new_line_cqsq

def load_data_merge_dqeablate_bypass(args, savefp, dkey):
    '''
    bypass for load_data_merge in order to load the set of QG/QA data used by DQE
    - syn-gq; containing QA pairs generated on WebNLG using TextQA
    - squad-qg: containing QA pairs contained in SQUAD 2.0
    '''
    gemformatter, spacy_pipeline = _start_gem_formatter(args, task_settings = None, bypass = True)
    data_merge = {}

    fp = f'datasets/{dkey}/ABLATION_DATA_graphsynqg_validation-train_ANS_ALIGNED.jsonl'
    with open(fp, encoding = 'utf-8') as f: 
        data_allq = [json.loads(i) for i in f]
    
    data_webnlg = datasets.load_dataset('GEM/web_nlg', 'en')
    webnlg_idx2tname = {}
    for split in ['train', 'validation']:
        for line in data_webnlg[split]: webnlg_idx2tname[line['gem_id']] = line['input']
    
    for __, line in enumerate(tqdm.tqdm(data_allq)):
        idx = line['id']
        data_merge[idx] = MergeALLQ(**line)
        # recover full graph 
        tname = webnlg_idx2tname[idx]
        # process triple from DBpedia to Wikidata format 
        tname = dqe_triple_preprocessing(tname, spacy_pipeline)
        # TODO: add tcode ?
        tcode = None
        data_merge[idx].triples_name = tname
        data_merge[idx].triples_code = tcode
    

    fp_tmp = f'datasets/{dkey}/ABLATION_DATA_textsquad_*.jsonl'
    fps = glob.glob(fp_tmp)
    for fp in fps:
        with open(fp, encoding = 'utf-8') as f: data_allq = [json.loads(i) for i in f]
        for line in data_allq:
            idx = 'squad_' + line['id']
            data_merge[idx] = MergeALLQ(**line)

    args.logger.print('proceeding to save to:', savefp)
    with open(savefp, 'w+') as f:
        for instance_idx, ma_entry in data_merge.items():
            ma_entry.id = instance_idx
            f.write(json.dumps(ma_entry.__dict__) + '\n')
    args.logger.print('Merged ALLQ and CQ saved to:', savefp)

    return data_merge

class MergeALLQ:
    def __init__(self, id, gen_sentence, cq, sq, triples_name = None, triples_code = None, **kwargs):
        self.id = id
        self.gen_sentence_en = gen_sentence
        self.cq_en = cq
        self.sq_en = sq
        self.triples_name = triples_name
        self.triples_code = triples_code
        for k, v in kwargs.items(): setattr(self, k, v)