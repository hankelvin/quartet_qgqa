import unidecode, re, math, multiprocessing, os, string
import requests, subprocess, urllib, time, ast
from typing import List
from collections import defaultdict, Counter
from data_utils.data_utils_omnibus import OmnibusCQ, CQ_Entry
import pandas as pd

class Linearize_CQ_KELM_Input():
    '''
    adapted from QuestEval script 
    https://github.com/ThomasScialom/QuestEval/blob/main/questeval/utils.py
    qualified quadruples (e.g. from KELM (Wikidata)) are matched to their 'parent' S-O triple
    where they are nested after the O entity/value. Where a qual statement does not have 
    a parent S-O with their relation (many of these in KELM), a placeholder relation is instantiated. 
    e.g. 
    "entity [ Jose Antonio Camacho ], 
    member of sports team [ Real Madrid Castilla [ start time [ 01 January 1973 ] ] [ end time [ 01 January 1974 ] ] ]""
    for the RDF subgraph [[Jose Antonio Camacho, member of sports team,  Real Madrid Castilla], 
    [Jose Antonio Camacho, Real Madrid Castilla, start time, 01 January 1973]
    [Jose Antonio Camacho, Real Madrid Castilla, end time, 01 January 1974]]
    '''

    def __init__(
        self,
        spacy_pipeline,
        lowercase=False,
        format: str ='gem',
        c_dqebaseline: bool = False,
        tripend_char: str = None,
    ):
        """
        Linearize a CQ-KELM input for QuestEval.
        lowercase=True indicates that you want all strings to be lowercased.
        """

        self.lowercase = lowercase
        self.format = format
        self.spacy_pipeline = spacy_pipeline
        ### Modificaton start 
        self.c_dqebaseline = c_dqebaseline
        # defaults to ' , ', which is the separator used in DQE
        # intended use is to specify the tripend separator 
        self.tripend_char = tripend_char
        ### Modificaton end  

    def __call__(
        self,
        triples_name: List[List[str]],
    )-> str:

        if self.format != 'gem':
            raise ValueError(f'Unsupported format for now: {self.format}')

        if not isinstance(triples_name, (list, tuple)):
            raise WrongWebNlgFormat(triples_name)

        triples = [Triple(triple,
                          spacy_pipeline=self.spacy_pipeline,
                          lower=self.lowercase, c_dqebaseline = self.c_dqebaseline)
                   for triple in triples_name]

        # [ADDITION] match qual_objs to the right triple
        qual_match, qual_nomatch = defaultdict(list), defaultdict(list)
        for idx_qual_t, triple in enumerate(triples):
            if triple.qual_obj is not None:
                s, o = triple.sbj, triple.obj
                found = False
                for idx_t, triple in enumerate(triples): 
                    if s == triple.sbj and o == triple.obj and triple.qual_obj is None: 
                        found  = True
                        break
                if found: qual_match[idx_t].append(idx_qual_t)
                else: qual_nomatch[(s,o)].append(idx_qual_t)

        table = dict()
        for idx_t, triple in enumerate(triples):
            table.setdefault(triple.sbj, list())
            if triple.qual_obj is not None: continue
            
            obj_line, prp_line = triple.obj, triple.prp
            # [ADDITION] start
            # adding qual_objs to their S-O 'parent'
            if idx_t in qual_match:
                for idx_qual_t in qual_match[idx_t]:
                    qual_trip = triples[idx_qual_t]
                    qual_line = f' [ {qual_trip.prp} [ {qual_trip.qual_obj} ] ]'     
                    obj_line += qual_line
            # [ADDITION] end
                
            table[triple.sbj].append((obj_line, prp_line))
        
        # [ADDITION] start
        # for certain KELM entries, qual-ed statements do not come with their S-O parent
        # adding placeholder '##F_PRP' for these 
        for s_o, idx_qual_ts in qual_nomatch.items():
            obj_line = triple.obj
            prp_line = '##F_PRP'
            qual_line = ''
            for idx_qual_t in idx_qual_ts:
                triple = triples[idx_qual_t]
                qual_line += f' [ {triple.prp} [ {triple.qual_obj} ] ]'
            
            obj_line += qual_line
            table[triple.sbj].append((obj_line, prp_line))
        # [ADDITION] end

        ret = list()
        for entidx, (entname, entlist) in enumerate(table.items(), 1):
            ret.append(f'entity [ {entname} ]')
            for values, key in entlist:
                ret.append(f'{key} [ {values} ]')
            
            # [ADDITION] start
            # add tend to the last element of the set of facts about current entity
            if self.tripend_char is not None: ret[-1] += self.tripend_char
            # [ADDITION] end

        return ' , '.join(ret)


class Triple:
    def __init__(
        self,
        triples_name: list,
        spacy_pipeline,
        lower: bool = False,
        c_dqebaseline: bool = True,
    ):
        if len(triples_name) == 4: sbj, obj, prp, qual_obj = triples_name
        elif len(triples_name) == 3: (sbj, prp, obj), qual_obj = triples_name, None
        else: raise NotImplementedError(f"{triples_name} provided...")
        if qual_obj: 
            qual_obj = ' '.join([t.text for t in spacy_pipeline(self.clean_obj(qual_obj.strip(), lc=lower))])
        else: qual_obj = None
        obj = ' '.join([t.text for t in spacy_pipeline(self.clean_obj(obj.strip(), lc=lower))])
        prp = self.clean_prp(prp.strip(), c_dqebaseline = c_dqebaseline)
        sbj = ' '.join([t.text for t in spacy_pipeline(self.clean_obj(sbj.strip(), lc=lower))])
        if prp == 'ethnicgroup':
            obj = obj.split('_in_')[0]
            obj = obj.split('_of_')[0]

        self.sbj = sbj
        self.obj = obj
        self.prp = prp.replace('_', ' ')
        self.qual_obj = qual_obj


    @staticmethod
    def clean_obj(
        s,
        lc: bool = False
    ):
        s = unidecode.unidecode(s)
        if lc: s = s.lower()
        s = re.sub('^"|"$', "", s)  # remove useless quotesigns
        s = re.sub('_', ' ', s)     # turn undescores to spaces
        return s

    @staticmethod
    def clean_prp(
        s: str,
        lc: bool=False,
        c_dqebaseline: bool=False,
    ) -> str:
        s = unidecode.unidecode(s)
        if lc: s = s.lower()
        s = re.sub('^"|"$', "", s)  # remove useless quotesigns
        ### [MODIFICATION] START: 
        if c_dqebaseline:
            s = re.sub('\s+', '_', s)  # turn spaces to underscores
        else:
            # turn spaces to single whitespace
            s = re.sub('\s+', ' ', s) 
            # remove camelCase (lower caps, introduce whitespace)
            # align with how we represent triples in Wikidata (closer to NL)  
            s = re.sub(r"([a-z])([A-Z])", "\g<1> \g<2>", s).lower()
        ### [MODIFICATION] END: 
        s = re.sub('\s+\(in metres\)', '_m', s)
        s = re.sub('\s+\(in feet\)', '_f', s)
        s = re.sub('\(.*\)', '', s)
        return s.strip()

class WrongWebNlgFormat(Exception):
    def __init__(self, obj):
        err = """
            It seems you passed an objected weirdly formatted.
            For KELM, please give a list of triplets, where each
            triplet is a list with 3 or 4 elements.
            For instance:
                input = [
                    ["(15788)_1993_SB", "discoverer", "Donal_O'Ceallaigh"],
                    ["(15788)_1993_SB", "epoch", "2006-03-06"]
                ]
            Your object was: {} of type {}
        """
        super().__init__(err.format(obj, type(obj)))


class QTypesPredictor:
    '''
    modified from:
    https://gitlab.inria.fr/hankelvin/wikidataqg/-/blob/main/01_data/02_datasets/02_prepare_diffqtypes_traindevsets.py
    '''
    def __init__(self, args, topk = 3):
        import pickle
        import torch, torch.nn as nn
        from transformers import BertForSequenceClassification, BertTokenizer
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        print('QT Predictor device available: ', self.device)

        ### DATA AND MODEL PRELIMS ###
        # 1. load the label encoders to configure final layer, also vocab (qfocus position tokens)
        self.wkdtqg_path = 'datasets/wikidataqg/'
        with open(f'{self.wkdtqg_path}labelencoder_q_sqdbpqa.pkl', 'rb') as f: 
            self.label_enc = pickle.load(f)
        with open(f'{self.wkdtqg_path}labelencoder_asubj_sqdbpqa.pkl', 'rb') as f: 
            self.asubj_label_enc = pickle.load(f)

        self.classes = [qt.replace('[', '').replace(']', '').lower() \
            for qt in self.label_enc.classes_.tolist()]
        self.topk = topk
        self.args = args

        # 2. load BERT model for qtype classifier
        model_path = 'bert-base-uncased'
        self.model = BertForSequenceClassification.from_pretrained(model_path)
        self.tokenizer = BertTokenizer.from_pretrained(model_path)
        self.tokenizer.add_tokens(self.asubj_label_enc.classes_.tolist())
        self.model.resize_token_embeddings(len(self.tokenizer))

        # 3. change classification head to number of labels
        self.model.classifier = nn.Linear(self.model.config.hidden_size, len(self.label_enc.classes_))
        self.model.num_labels = len(self.label_enc.classes_)

        # 4. qtype_qtype_distribution_model_tested_epoch_3 was trained with SQ, WQ and ZQ train and dev sets, with qftype filtered
        model_fp = f'{self.wkdtqg_path}02_modelbuilding/qtype_classifier/models/qtype_qtype_distribution_sqwqzq-asubj_model_tested_epoch_.pt'
        model_state_dict = torch.load(model_fp, map_location=torch.device('cpu')) 
                    
        # 5. load weights
        self.model.load_state_dict(model_state_dict['model_state_dict'], strict = False)
        self.model.to(self.device)

    def select_qtypes_set(self, predictions):
        '''
        Helper function, given a set of qtype distri predictions, qtype labels,
        the original qtype... return a set of other qtypes 
        (qtype with probability > 0, but not current qtype) 
        '''
        if self.topk < 0: self.topk = predictions.shape[-1] # topk = all
        # 1. set negative logits to -inf
        predictions[(predictions<0)] = -float('inf')
        # 2. softmax to get probs
        predictions = predictions.softmax(-1)
        
        top_k_preds = predictions.topk(self.topk)
        # 3. ensure all topks are > 0 prob... top_k_preds.indices[0] since going 1 by 1 for predict()
        l_pos = top_k_preds.indices[0]
        filtered_ranked = {p.item(): self.classes[l_pos[i2]] for i2, p\
            in enumerate(top_k_preds.values[0]) if p > 0}
        
        # 4. check to remove impossible combis - 2 or more of {who, where, when, how}
        filtered_ranked = self.remove_imposs(filtered_ranked)
            
        return filtered_ranked

    def remove_imposs(self, filtered_ranked):
        '''
        helper function to check for impossible combis, if found
        return the most prob of the subset. 
        '''
        imposs = ['who', 'where', 'when', 'how']
        check = {p:qt for p, qt in filtered_ranked.items() if qt in imposs}
        # keep the most probable one
        if len(check) > 1:
            keep = check[max(check)]
            imposs.remove(keep)
            
            return {p: qt for p, qt in filtered_ranked.items() if qt not in imposs}
        else: return filtered_ranked


    def predict(self, a_semtypes, apos_s_o):
        '''
        given a single entity semantic type, return the set of question types.
        '''
        if apos_s_o == self.args.trp_seps['s']: pos_str = '[ASUBJ]'
        elif apos_s_o == self.args.trp_seps['o']: pos_str = '[AOBJ]'
        else: raise ValueError('check apos_s_o')
        toks = self.tokenizer.encode(f'{a_semtypes} {pos_str}', padding = True, return_tensors='pt')
        predictions = self.model(toks.to(self.device)).logits

        new_qtypes = self.select_qtypes_set(predictions)

        return new_qtypes.values()
        
def _start_gem_formatter(args, task_settings = None, c_dqebaseline = False, bypass = False, language = 'en'):
    gemformatter, spacy_pipeline = None, None
    if (task_settings is not None and 'dqe_gem' in task_settings['src_format']) \
        or bypass:
        import spacy
        model_map = {'en': 'en_core_web_sm', 'ptbr': 'pt_core_news_sm', 'ru':'ru_core_news_sm'}
        spacy_pipeline = spacy.load(model_map[language])

        c_use_end = any(['_usetripend' in v['src_format'] for v in args.settings['tasks'].values()])
        print('\n\n\n C_USE_END, ', c_use_end)
        if c_use_end:

            tripend_char = args.trp_seps['tripend']
            print('\n\n\n tripend_char, ', tripend_char)
            gemformatter = Linearize_CQ_KELM_Input(spacy_pipeline = spacy_pipeline,
                                                c_dqebaseline = c_dqebaseline, tripend_char = tripend_char)
        else: gemformatter = Linearize_CQ_KELM_Input(spacy_pipeline = spacy_pipeline,
                                                c_dqebaseline = c_dqebaseline)
    return gemformatter, spacy_pipeline


def get_NE_NP(text, spacy_pipeline, language = 'en',
              nominal_deprels = set(['nsubj', 'obj', 'iobj', 'pobj',
                    'obl', 'vocative', 'expl', 'dislocated',
                    'nmod', 'appos','nummod']),
              trans_table = ''.maketrans('', '', string.punctuation+' ')):
    spacy_doc = spacy_pipeline(text)
    answers = set(a for a in spacy_doc.ents)
    answers = set(e.text for e in answers if (len(list(e.subtree)) > 1 \
                or list(e.subtree)[0].pos_ != 'PRON'))
    if language in ['ru', 'ptbr']: 
        for sent in spacy_doc.sents:
            for word in sent:
                # look for head words holding nominal deprels
                if word.dep_ in nominal_deprels: 
                    # get subtree of these nominal head words 
                    ans_cand, subtree, first = [], list(word.subtree), False
                    for i, tok in enumerate(subtree):
                        if (i == 0 or first) and tok.pos_ in ['PUNCT', 'ADP', 'DET', 'PRON']: 
                            first = True
                            continue
                        first = False
                        if tok.pos_ == 'VERB' and i+1<len(subtree) \
                            and subtree[i+1].pos_ == 'ADP': break
                        if tok.pos_ in ['SCONJ', 'CCONJ']:
                            answers.add(' '.join(ans_cand))
                            ans_cand = []                        
                        else: ans_cand.append(tok.text)
                    ans_cand = ' '.join(ans_cand)
                    # remove answer candidates that are only punct and/or whitespace
                    if not ans_cand.translate(trans_table): continue
                    answers.add(ans_cand)
    else:
        __ = set(a for a in spacy_doc.noun_chunks) 
        __ = set(e.text for e in __ if (len(list(e.subtree)) > 1 \
                or list(e.subtree)[0].pos_ != 'PRON'))
        answers.update(__)
    return answers

def chunk_checker(use_mp, data_obj, max_workers = None):
    '''
    helper to compute stats need for multiprocessing
    '''
    if use_mp and max_workers is None: 
        num_chunks = max_workers = int(multiprocessing.cpu_count() * 0.5)
    elif use_mp and max_workers > 1: \
        num_chunks = max_workers = min([int(multiprocessing.cpu_count() * 0.5), max_workers])
    else: num_chunks = max_workers = 1
    split_sz = int(math.ceil(len(data_obj)/num_chunks))

    return num_chunks, split_sz, max_workers

def make_one_omnibus_entry(args, tokenizer, context_f, question, answer, 
    triples_code_prime, triples_name_prime, triples_code, triples_name, 
    ans_pos, qtypes, a_semtypes, cq_id, task_name, task_settings,
    gemformatter):

    ### 2. Create CQ_Entry
    __cq_entry = CQ_Entry(
        args = args, 
        question = question, answer = answer, 
        triples_code = triples_code_prime,  
        triples_name = triples_name_prime, 
        ans_pos = ans_pos, 
        qtypes = qtypes if qtypes else ['other'], 
        a_semtypes = [a_semtypes], 
        id = None)

    ### 3. Instantiate OmnibusCQ
    # 1 input to 1 question 
    __ = OmnibusCQ(args, tokenizer, __cq_entry, 
            context_f = context_f,
            triples_code = triples_code, triples_name = triples_name,
            task_name = task_name, task_settings = task_settings,
            id = cq_id, gemformatter = gemformatter)
    
    return __


def dqe_triple_preprocessing(triples_name, spacy_pipeline, c_dqebaseline = False):
    '''
    helper func to preprocess WebNLG triple so that it is the same as that  
    in the DQE experiments.
    '''
    proc_triples_name = []
    for t in triples_name: 
        _t = Triple(t.split(' | '), spacy_pipeline = spacy_pipeline, c_dqebaseline = c_dqebaseline)
        proc_triples_name.append([_t.sbj,  _t.prp, _t.obj])

    return proc_triples_name


def check_connected(subgraph_name):
    '''
    helper func to check if subgraph is connected. returns False if not 
    used for cases where we loop through combinations of subgraph to obtain subsets 
    and applies to case where subgraph triples do not necessarily 
    all share the same SBJ (e.g. case of WebNLG)
    '''
    for idx, t in enumerate(subgraph_name):
        other_ts = subgraph_name[:idx] + subgraph_name[idx+1:]
        if len(t) == 3: 
            other_ents = [t3 for t2 in [t[::2] for t in other_ts] for t3 in t2]
        elif len(t) == 4:
            other_ents = [t3 for t2 in [t[0:1] + t[2:] for t in other_ts] for t3 in t2]
        else: 
            print('Subgraph contains triples of size > 4', subgraph_name)
            return False
        
        # check that curr triple's set of ents and other ents is not empty set
        if len(set(t[::2]).intersection(other_ents)) == 0: return False
    
    return True

def webnlg_pt_test():
    import os, requests, json
    '''
    helper func to return WebNLG-PTBR test set 
    NOTE: changes here must also be made at 01_data/dqe_webnlg/produce_qgqa.py
    '''
    fp_webnlg_pt_test = 'webnlgPT_release_v1_test.json'
    if not os.path.exists(fp_webnlg_pt_test):
        url_webnlg_pt_test = \
            'https://raw.githubusercontent.com/felipealco/webnlg-pt/main/corpus/webnlgPT_release_v1_test.json'
        __ = json.loads(requests.get(url_webnlg_pt_test).text)
        with open(fp_webnlg_pt_test, encoding = 'utf-8', mode = 'w+') as f: json.dump(__, f)
    else: 
        with open(fp_webnlg_pt_test, encoding = 'utf-8') as f: __ = json.load(f)
    
    data = {'test': []}
    for entry  in __['entries']:
        for i, line in entry.items():
            gem_id = f'web_nlg_ptbr-test-{i}'
            inp = [' | '.join([i['object'], i['property'], i['subject']]) for i in line['modifiedtripleset']]
            for lid1, lexline in enumerate(line['lexicalisations']): 
                if lid1 > 0: continue # pick 1st En lexicalisation 
                lex_pts = []
                pool = [l for l in lexline['portuguese'] if l['source'] != 'machine']
                keys = ['very good', 'good', 'medium', 'poor', 'very poor']
                while not lex_pts:
                    key = keys.pop(0)
                    lex_pts = [l for l in pool if l['comment'] == key]
                for lid2, lex_pt in enumerate(lex_pts):
                    if lid2 > 0: continue # pick 1st PtBr lexicalisation 
                    data['test'].append({'input': inp, 
                                            'target':lex_pt['pt_lex'], 
                                            'gem_id': f'{gem_id}_{lid1}_{lid2}'})
                
    return data

        
def _semtypes_distri(fp_template):
    '''
    helper func to return the counts of entity semantic types in the data split. For picking the most common
    '''
    data = pd.read_csv(fp_template.format('train'))
    data['a_semtypes'] = data.apply(lambda x: x['o_types'] if x['asubj'] == '[AOBJ]' else x['s_types'], axis=1)
    data['a_semtypes'] = data['a_semtypes'].apply(eval)

    # flatten 1
    types_distri = []
    for ty in data['a_semtypes'].tolist():
        for ty2 in ty: 
            if ';' in ty2: types_distri.extend(ty2.split('; '))
            else: types_distri.append(ty2)
    # flatten 2
    types_distri = Counter(ty2 for ty in types_distri for ty2 in ty)

    return types_distri

### Wikidata SPARQL related ###
WIKIDATA_ENDPOINT = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'
SLEEP = 1
HEADERS = {'User-Agent': '(academic research project)',
            'From': ''} 
SPARQL_query_answer_semtype = '''
SELECT DISTINCT ?xLabel 

(GROUP_CONCAT(DISTINCT(?type); 
separator = "; ") AS ?type_list)

(GROUP_CONCAT(DISTINCT(?typeLabel); 
separator = "; ") AS ?typeLabel_list)

(GROUP_CONCAT(DISTINCT(?supertype); 
separator = "; ") AS ?supertype_list)

(GROUP_CONCAT(DISTINCT(?supertypeLabel); 
separator = "; ") AS ?supertypeLabel_list)


WHERE {{ 
  wd:{0} rdfs:label ?xLabel .
  {{wd:{0} wdt:P31 ?type}}              # P31 is 'instance of'
  UNION
  {{wd:{0} wdt:P279 ?type}}                 # P279 is 'subclass of'
                                            # some entities do not have P31 instantiated. see Q902104
  ?type rdfs:label ?typeLabel . 
  ?type wdt:P279 ?supertype .               
                                            # the class should be a subclass of another/other class(es)
  ?supertype rdfs:label ?supertypeLabel .
  FILTER (LANG(?xLabel) = "en")
  FILTER (LANG(?typeLabel) = "en")
  FILTER (LANG(?supertypeLabel) = "en")
}}
GROUP BY ?xLabel 
'''

PREFIXES = '''
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
'''

def get_answer_semtype(ent_code, endpoint = WIKIDATA_ENDPOINT, graphdb_avail = False, 
            ip_address = 'localhost', repo_name = 'Wikidata', offset = None):
    
    query = SPARQL_query_answer_semtype.format(ent_code)

    if graphdb_avail: 
        query = PREFIXES + query
        query = urllib.parse.quote(query.encode('utf'))
        curl = """curl -G -H "Accept:application/sparql-results+json" -d query={0} """.format(query) + \
        f""" http://{ip_address}:7200/repositories/{repo_name} """

        results_out = subprocess.check_output(curl, shell=True, 
        stderr=subprocess.DEVNULL, encoding='utf-8')
        try: results_dict = ast.literal_eval(results_out)['results']['bindings']
        except: results_dict = None
    else: 
        params = {'format': 'json', 'query': query}
        r = requests.get(endpoint, headers = HEADERS, params=params)
        results_dict = r.json()['results']['bindings']
        time.sleep(0.5)
    
    result = None 
    if results_dict: 
        keys = ['type_list', 'typeLabel_list', 'supertype_list', 'supertypeLabel_list']
        result = [[os.path.basename(i[key]['value']) for key in keys] for i in results_dict]
            
        if result: result = result[0]
        else: result = None

    return result 

GRAPHDB_COMMANDS = [
            'cp -r /tools/graphdb-free-9.10.0 /tmp/graphdb-free-9.10.0/',
            'cp -r /graphdb-se-9.10.1 /tmp/graphdb-se-9.10.1/', 
            'tmux new -s ontotext -d', 
            'tmux send-keys -t ontotext "cd ~ && cd /tmp/graphdb-se-9.10.1/bin && ./graphdb -d -Xms80g -Xmx100g" C-m', 
            ### NOTE: use below if licence not avail
            # 'tmux send-keys -t ontotext "cd ~ && cd /tmp/graphdb-free-9.10.0/bin && ./graphdb -d -Xms80g -Xmx100g" C-m', 
            ]


def startstop_graphdb(action = 'start', ip_address = 'localhost', repo_name = 'Wikidata'):
    graphdb_avail = False
    assert action in ['start', 'stop']
    if action == 'start':
        # check if an existing tmux 'ontotext' window exists

        if ip_address == 'localhost':
            try: 
                check_exist = subprocess.check_output('tmux ls', shell=True)
                if b'ontotext: 1 windows' in check_exist: return True
            except subprocess.CalledProcessError: pass
            
            for command in GRAPHDB_COMMANDS:
                check = subprocess.run(command, shell=True, check=True)
                if check.returncode != 0: raise ProcessLookupError(f'{command} failed')
            time.sleep(10)
            graphdb_avail = True
            print('graphdb successfully launched')
        else: 
            query = '''
            PREFIX wd: <http://www.wikidata.org/entity/>
            PREFIX wdt: <http://www.wikidata.org/prop/direct/>
            SELECT DISTINCT * WHERE {
                    wd:Q4551382 ?r wd:Q7934227.}
            '''
            query = urllib.parse.quote(query.encode('utf'))
            curl = """curl -G -H "Accept:application/sparql-results+json" -d query={0} """.format(query) + \
            f""" http://{ip_address}:7200/repositories/{repo_name} """
            
            check_exist = subprocess.check_output(curl, shell=True)
            
            check_exist = ast.literal_eval(check_exist.decode())
            if check_exist['results']['bindings'][0]['r']['value'] \
                == "http://www.wikidata.org/prop/direct/P50":
                graphdb_avail = True
                print(f'graphdb found present and successfully queried at: {ip_address}') 
    
    elif action == 'stop':
        command = 'tmux kill-session -t ontotext'
        check = subprocess.run(command, shell=True, check=True)
        if check.returncode != 0: raise ProcessLookupError(f'{command} failed')
        graphdb_avail = False
        print('graphdb successfully terminated')
    
    return graphdb_avail