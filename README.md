# QUARTET (__QTT__): QG and QA from Text and from Knowledge Graph

This repo contains the scripts to train and evaluate the following model from the IJCNLP-AACL 2023 paper [Generating and Answering Simple and Complex Questions from Text and from Knowledge Graphs]().

### 0. Preliminaries 
Create conda environments:

##### a. For __QTT__ training and QA-QG inference 
```
conda env create -f conda_quartet_qgqa.yml
conda activate quartet_qgqa
python -m pip install --upgrade pip
# (you may need "sudo apt-get update && sudo apt-get install python3.8-dev default-libmysqlclient-dev build-essential")
CUDA="cu113"
python -m pip install torch==1.12.1+${CUDA} torchvision==0.13.1+${CUDA} torchaudio==0.12.1 --extra-index-url https://download.pytorch.org/whl/${CUDA}
python -m spacy download en_core_web_sm
```

##### b. For downtream evaluation with Data-QuestEval
```
conda deactivate
conda create -n questeval python=3.7 -y
conda activate questeval
cd tools/QuestEval
python -m pip install -r requirements.txt
python -m pip install -e .
CUDA="cu113"
python -m pip install torch==1.12.1+${CUDA} torchvision==0.13.1+${CUDA} torchaudio==0.12.1 --extra-index-url https://download.pytorch.org/whl/${CUDA}
python -m pip install spacy==3.3.1 --no-cache-dir
python -m spacy download en_core_web_sm
python -m pip install transformers==4.10.0 --no-cache-dir
```

##### c. For downtream evaluation with FiD
```
conda deactivate
conda env create -f ./tools/FiD/conda_fid.yml
conda activate fid
cd tools/FiD
python -m pip install -r requirements.txt
```

### 1. Datasets
- __Q-KELM:__ synthetic QA-QG data that was generated on top of a filtered subset of ($g, t$) pairs from [KELM](https://github.com/google-research-datasets/KELM-corpus) (Agarwal et al, 2021). This is the data used to train our controllable QG models (CQ-KELM and SQ-KELM in our paper): 
    - `datasets/KELM/CQ_KELM_master.jsonl`
    - `datasets/KELM/SQ_KELM_master.jsonl`
- __Q-WebNLG:__ (used to train our __QTT__ model, see below):                 
    - `datasets/dqe_webnlg/z08_allq_cq_webnlg_MERGED_nosubjobj.jsonl` 
- These datasets, together with the data required to run our evaluation (see below) can be downloaded from here: [datasets.zip](https://mega.nz/file/2yZHmKQR#T2mJbUr5xuksnaroKmzQc4nAo_VnrG4FSd7c3ceErgg). Download the file, unzip it and move its contents into the `datasets` folder.

### 2. Model training / saved checkpoint
- __QTT__: our multi-modal question generation and question answering model that can generate and answer questions from text or from graph. QTT's training included 4 main tasks (TextQG, KBQG, TextQA and KBQA) trained jointly with 4 auxiliary tasks. This is enabled by the __Q-WebNLG__ dataset we generated (see above). 

- The checkpoint for our trained QTT model can be downloaded from here: [omnibus_model.zip](https://mega.nz/file/y2Y1HAQJ#q_gdargAkRiRwXBYPn65V-P01o5K656XitRNmhrIHus). Download the file, unzip it and move its contents into the `results/omnibus_model` folder. 
    - NOTE: This checkpoint is usable for the Data-QuestEval metric.  
    - To utilise it, you will need the version of the QuestEval code that we modified (see _"Modifications to QuestEval and FiD"_ below)
    - Use the questeval conda environment above. Follow the setps in `scripts/5_run_human_eval_correlations.sh` script (under _"Downstream evaluation with Data-QuestEval"_ below)

### 3. Evaluation
- For training __QTT__ and the 3 other ablation models:                     
    - run `./scripts/1_train___QTT__.sh`
- For question generation (CQ and SQ generation) from __QTT__ and ablation models:  
    - run `./scripts/2_qst_inference_qtt.sh`
- For evaluating __QTT__ versus the baseline models in Data-QuestEval (Rebuffel et al, 2021)
    - _Coverage_:       
        - run `./scripts/3_run_coverage.sh`
    - _QA consistency_: 
        - run `./scripts/4_run_qa_consistency.sh`
    - _Downstream evaluation with Data-QuestEval_: 
        - run `./scripts5_run_human_eval_correlations.sh`
    - _Downstream evaluation with FiD_: run the following from home directory 
        - data preparation:            
            - `python tools/FiD/utils_finetune_fid.py`
        - download FiD checkpoints:    
            - `./tools/FiD/get-model.sh -m "tqa_reader_base"`
        - training:                    
            - `./scripts/6a_run_fid_training.sh`
        - evaluation:                  
            - `./scripts/6b_run_fid_evaluation.sh`

### Modifications to QuestEval and FiD
We modified QuestEval's codebase (where Data-QuestEval is contained) in order to extend Data-QuestEval to load and use our models (including handling our model's generation of multiple questions per input), as well as to accomodate the consistency filtering settings we describe in our paper. Where modifications have been made, they are marked in the code between `## CHANGE START` and `## CHANGE END`.

We also modified the FiD codebase to add a data processing script. Minor modifications to train_reader.py to add a zeroth-step optimizer to replace the "missing" optimizer states that are not part of the public release for the `tqa_reader_base` checkpoint. Similar to QuestEval, where modifications have been made, they are marked in the code between `## CHANGE START` and `## CHANGE END`.


### Citation
If you find our work useful, please cite our publication:

```
@inproceedings{han-gardent-2023-generating,
    title = "Generating and Answering Simple and Complex Questions from Text and from Knowledge Graphs",
    author = "Han, Kelvin and Gardent, Claire",
    booktitle = "Proceedings of the The 13th International Joint Conference on Natural Language Processing and the 3rd Conference of the Asia-Pacific Chapter of the Association for Computational Linguistics",
    month = nov,
    year = "2023",
    address = "Online and Bali, Indonesia",
    publisher = "Association for Computational Linguistics",
}
```
