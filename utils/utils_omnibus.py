import os, datetime, re, json
PARENTDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# kept 
['cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter',
 'quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters',
 'quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters',
 'quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters',
 'quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw',
 'quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter',
 'quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks',
 'quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate',
 'quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink',
 'sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter',
 'textansselect_t5base_gen_ce_EPR_uncased_infer',
 'textansselect_t5base_gen_ce_uncased',
 'textansselect_t5base_gen_ce_uncased_infer',]

class Settings: 
    
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter = {}
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['model'] = 't5-small'
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['tasks'] = {'cqg': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['g2cq', 't2cq'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['use_saved_data'] = True
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['save_processed_data'] = True
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['diet_gpu'] = ['deepspeed']
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['deepspeed_stage'] = '_stage_2_offload'
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['load_ckpt'] = []
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['bsz'] = 192
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['lr'] = 0.0001
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['max_epochs'] = 10
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['accumulate_grad_batches'] = 4
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['linear_warmup_ratio'] = 0.1
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['trp_sep_scheme'] = 2
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['subobj_tok'] = False
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['src_seps'] = {'input': 'input', 'ans': 'ans'}
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['tokenization'] = {'truncation': True, 'max_length': 512}
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    cqg_t5small_t2cq_g2cq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['bert_score_model'] = 'bert-base-multilingual-cased'

    
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters = {}
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['model'] = 't5-small'
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['tasks'] = {'kbqa': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['kbqa'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['use_saved_data'] = True
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['save_processed_data'] = True
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['diet_gpu'] = ['deepspeed']
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['deepspeed_stage'] = '_stage_2_offload'
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['load_ckpt'] = []
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['bsz'] = 48
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['lr'] = 0.0001
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['max_epochs'] = 10
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['accumulate_grad_batches'] = 4
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['linear_warmup_ratio'] = 0.1
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['trp_sep_scheme'] = 2
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['subobj_tok'] = True
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['src_seps'] = {'input': 'input', 'ans': 'ans'}
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['tokenization'] = {'truncation': True, 'max_length': 512}
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    quartet_kbqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['bert_score_model'] = 'bert-base-multilingual-cased'


    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw = {}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['model'] = 't5-small'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['tasks'] = {'quartet': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa', 'data2text', 'text2data', 'g2el', 't2el'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['use_saved_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['save_processed_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['diet_gpu'] = ['deepspeed']
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['deepspeed_stage'] = '_stage_2_offload'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['load_ckpt'] = []
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['bsz'] = 32
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['lr'] = 5e-05
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['max_epochs'] = 10
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['accumulate_grad_batches'] = 1
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['linear_warmup_ratio'] = 0.0
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['trp_sep_scheme'] = 2
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['subobj_tok'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['src_seps'] = {'input': 'input', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['tokenization'] = {'truncation': True, 'max_length': 512}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw['bert_score_model'] = 'bert-base-multilingual-cased'


    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter = {}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['model'] = 't5-small'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['tasks'] = {'quartet': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa', 'data2text', 'text2data', 'g2el', 't2el'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['use_saved_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['save_processed_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['diet_gpu'] = ['deepspeed']
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['deepspeed_stage'] = '_stage_2_offload'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['load_ckpt'] = []
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['bsz'] = 32
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['lr'] = 5e-05
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['max_epochs'] = 10
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['accumulate_grad_batches'] = 1
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['linear_warmup_ratio'] = 0.0
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['trp_sep_scheme'] = 2
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['subobj_tok'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['src_seps'] = {'input': 'input', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['tokenization'] = {'truncation': True, 'max_length': 512}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xadapter['bert_score_model'] = 'bert-base-multilingual-cased'


    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks = {}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['model'] = 't5-small'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['tasks'] = {'quartet': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['use_saved_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['save_processed_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['diet_gpu'] = ['deepspeed']
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['deepspeed_stage'] = '_stage_2_offload'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['load_ckpt'] = []
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['bsz'] = 32
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['lr'] = 5e-05
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['max_epochs'] = 10
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['accumulate_grad_batches'] = 1
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['linear_warmup_ratio'] = 0.0
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['trp_sep_scheme'] = 2
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['subobj_tok'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['src_seps'] = {'input': 'input', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['tokenization'] = {'truncation': True, 'max_length': 512}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xauxtasks['bert_score_model'] = 'bert-base-multilingual-cased'


    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate = {}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['model'] = 't5-small'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['tasks'] = {'quartet': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa', 'data2text', 'text2data', 'g2el', 't2el'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['use_saved_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['save_processed_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['diet_gpu'] = ['deepspeed']
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['deepspeed_stage'] = '_stage_2_offload'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['load_ckpt'] = []
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['bsz'] = 32
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['lr'] = 5e-05
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['max_epochs'] = 10
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['accumulate_grad_batches'] = 1
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['linear_warmup_ratio'] = 0.0
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['trp_sep_scheme'] = 2
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['subobj_tok'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['src_seps'] = {'input': 'input', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['tokenization'] = {'truncation': True, 'max_length': 512}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xdqeablate['bert_score_model'] = 'bert-base-multilingual-cased'


    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink = {}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['model'] = 't5-small'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['tasks'] = {'quartet': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['g2sq', 't2sq', 'g2cq', 't2cq', 'textqa', 'kbqa', 'data2text', 'text2data'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['use_saved_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['save_processed_data'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['diet_gpu'] = ['deepspeed']
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['deepspeed_stage'] = '_stage_2_offload'
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['load_ckpt'] = []
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['bsz'] = 32
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['lr'] = 5e-05
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['max_epochs'] = 10
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['accumulate_grad_batches'] = 1
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['linear_warmup_ratio'] = 0.0
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['trp_sep_scheme'] = 2
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['subobj_tok'] = True
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['src_seps'] = {'input': 'input', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['tokenization'] = {'truncation': True, 'max_length': 512}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    quartet_withD2T_t5small_t2sqcq_g2sqcq_textqa_kbqa_usetripend_adamw_ablation_xentlink['bert_score_model'] = 'bert-base-multilingual-cased'
    

    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter = {}
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['model'] = 't5-small'
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['tasks'] = {'sqg': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['g2sq', 't2sq'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['use_saved_data'] = True
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['save_processed_data'] = True
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['diet_gpu'] = ['deepspeed']
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['deepspeed_stage'] = '_stage_2_offload'
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['load_ckpt'] = []
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['bsz'] = 192
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['lr'] = 0.0001
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['max_epochs'] = 10
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['accumulate_grad_batches'] = 4
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['linear_warmup_ratio'] = 0.1
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['trp_sep_scheme'] = 2
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['subobj_tok'] = True
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['src_seps'] = {'input': 'input', 'ans': 'ans'}
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['tokenization'] = {'truncation': True, 'max_length': 512}
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    sqg_t5small_t2sq_g2sq_usetripend_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapter['bert_score_model'] = 'bert-base-multilingual-cased'
    

    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters = {}
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['model'] = 't5-small'
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['tasks'] = {'sqgcqg': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['sqgcqg'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['use_saved_data'] = True
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['save_processed_data'] = True
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['diet_gpu'] = ['deepspeed']
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['deepspeed_stage'] = '_stage_2_offload'
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['bsz'] = 48
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['lr'] = 0.0001
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['max_epochs'] = 10
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['accumulate_grad_batches'] = 4
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['linear_warmup_ratio'] = 0.1
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['trp_sep_scheme'] = 2
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['subobj_tok'] = True
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['src_seps'] = {'input': 'input', 'ans': 'ans'}
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['tokenization'] = {'truncation': True, 'max_length': 512}
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    quartet_sqgcqg_t5small_t2sqcq_g2sqcq_usetripend_infer_dqewebnlgtest_10_model3_ablation_xadapters['bert_score_model'] = 'bert-base-multilingual-cased'


    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters = {}
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['model'] = 't5-small'
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['tasks'] = {'textqa': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['textqa'], 'prompt_strategies': 'text_num_facts', 'src_format': 'dqe_gem_usetripend', 'tgt_format': 'dqe_generate', 'num_labels': None, 'src_setting': ['single-sent', 'ans_included'], 'tgt_setting': ['ans_included'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['use_saved_data'] = True
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['save_processed_data'] = True
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['diet_gpu'] = ['deepspeed']
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['deepspeed_stage'] = '_stage_2_offload'
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['load_ckpt'] = []
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['bsz'] = 48
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['lr'] = 0.0001
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['max_epochs'] = 10
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['accumulate_grad_batches'] = 4
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['linear_warmup_ratio'] = 0.1
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['trp_sep_scheme'] = 2
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['subobj_tok'] = True
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['src_seps'] = {'input': 'input', 'ans': 'ans'}
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['tokenization'] = {'truncation': True, 'max_length': 512}
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['gen_args'] = {'max_length': 512, 'min_length': 5, 'num_beams': 1}
    quartet_textqa_t5small_gen_ce_quartet_infer_dqewebnlgtest_10_model3_ablation_xadapters['bert_score_model'] = 'bert-base-multilingual-cased'

    textansselect_t5base_gen_ce_uncased = {}
    textansselect_t5base_gen_ce_uncased['model'] = 't5-base'
    textansselect_t5base_gen_ce_uncased['tasks'] = {'textansselect': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['textansselect'], 'prompt_strategies': '', 'src_format': 'trp_sep_scheme2', 'tgt_format': 'dqe_generate:blink', 'src_setting': ['single-sent', 'uncased'], 'tgt_setting': ['uncased'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    textansselect_t5base_gen_ce_uncased['use_saved_data'] = True
    textansselect_t5base_gen_ce_uncased['save_processed_data'] = True
    textansselect_t5base_gen_ce_uncased['diet_gpu'] = ['deepspeed']
    textansselect_t5base_gen_ce_uncased['deepspeed_stage'] = '_stage_2_offload'
    textansselect_t5base_gen_ce_uncased['load_ckpt'] = []
    textansselect_t5base_gen_ce_uncased['bsz'] = 32
    textansselect_t5base_gen_ce_uncased['lr'] = 0.0002
    textansselect_t5base_gen_ce_uncased['max_epochs'] = 10
    textansselect_t5base_gen_ce_uncased['accumulate_grad_batches'] = 4
    textansselect_t5base_gen_ce_uncased['linear_warmup_ratio'] = 0.1
    textansselect_t5base_gen_ce_uncased['trp_sep_scheme'] = 2
    textansselect_t5base_gen_ce_uncased['subobj_tok'] = False
    textansselect_t5base_gen_ce_uncased['src_seps'] = {'input': 'input', 'ans': 'ans'}
    textansselect_t5base_gen_ce_uncased['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    textansselect_t5base_gen_ce_uncased['tokenization'] = {'truncation': True, 'max_length': 512}
    textansselect_t5base_gen_ce_uncased['gen_args'] = {'max_length': 512, 'min_length': 1, 'num_beams': 1}
    textansselect_t5base_gen_ce_uncased['bert_score_model'] = 'bert-base-multilingual-cased'

    textansselect_t5base_gen_ce_uncased_infer = {}
    textansselect_t5base_gen_ce_uncased_infer['model'] = 't5-base'
    textansselect_t5base_gen_ce_uncased_infer['tasks'] = {'textansselect': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['textansselect'], 'prompt_strategies': '', 'src_format': 'trp_sep_scheme2', 'tgt_format': 'dqe_generate:blink', 'src_setting': ['single-sent', 'uncased'], 'tgt_setting': ['uncased'], 'losses': ['ce'], 'auto_scores': ['bertscore']}}
    textansselect_t5base_gen_ce_uncased_infer['use_saved_data'] = True
    textansselect_t5base_gen_ce_uncased_infer['save_processed_data'] = True
    textansselect_t5base_gen_ce_uncased_infer['diet_gpu'] = ['deepspeed']
    textansselect_t5base_gen_ce_uncased_infer['deepspeed_stage'] = '_stage_2_offload'
    textansselect_t5base_gen_ce_uncased_infer['load_ckpt'] = ['', f'{PARENTDIR}/results/omnibus_model/textansselect_t5base_gen_ce_uncased_allq-cq-webnlg_lrwarm0.1_ep10/epoch=9-step=1830_tested.ckpt']
    textansselect_t5base_gen_ce_uncased_infer['bsz'] = 32
    textansselect_t5base_gen_ce_uncased_infer['lr'] = 0.0002
    textansselect_t5base_gen_ce_uncased_infer['max_epochs'] = 10
    textansselect_t5base_gen_ce_uncased_infer['accumulate_grad_batches'] = 4
    textansselect_t5base_gen_ce_uncased_infer['linear_warmup_ratio'] = 0.1
    textansselect_t5base_gen_ce_uncased_infer['trp_sep_scheme'] = 2
    textansselect_t5base_gen_ce_uncased_infer['subobj_tok'] = False
    textansselect_t5base_gen_ce_uncased_infer['src_seps'] = {'input': 'input', 'ans': 'ans'}
    textansselect_t5base_gen_ce_uncased_infer['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    textansselect_t5base_gen_ce_uncased_infer['tokenization'] = {'truncation': True, 'max_length': 512}
    textansselect_t5base_gen_ce_uncased_infer['gen_args'] = {'max_length': 512, 'min_length': 1, 'num_beams': 1}
    textansselect_t5base_gen_ce_uncased_infer['bert_score_model'] = 'bert-base-multilingual-cased'

    

    textnxfacts_t5small_infer_dqewebnlgtest_10 = {}
    textnxfacts_t5small_infer_dqewebnlgtest_10['model'] = 't5-small'
    textnxfacts_t5small_infer_dqewebnlgtest_10['tasks'] = {'textnxfacts': {'datasets': [], 'datasets_filter': {}, 'input_strategies': ['textnxfacts'], 'prompt_strategies': '', 'src_format': 'NA', 'tgt_format': 'dqe_generate', 'num_labels': 5, 'src_setting': ['single-sent'], 'tgt_setting': [], 'losses': ['ce'], 'auto_scores': []}}
    textnxfacts_t5small_infer_dqewebnlgtest_10['use_saved_data'] = True
    textnxfacts_t5small_infer_dqewebnlgtest_10['save_processed_data'] = True
    textnxfacts_t5small_infer_dqewebnlgtest_10['diet_gpu'] = ['deepspeed']
    textnxfacts_t5small_infer_dqewebnlgtest_10['deepspeed_stage'] = '_stage_2_offload'
    textnxfacts_t5small_infer_dqewebnlgtest_10['load_ckpt'] = ['', f'{PARENTDIR}/results/omnibus_model/textnxfacts_t5small_dqe-webnlg_lrwarm0.1_ep10/epoch=8-step=2097_tested.ckpt']
    textnxfacts_t5small_infer_dqewebnlgtest_10['bsz'] = 192
    textnxfacts_t5small_infer_dqewebnlgtest_10['lr'] = 0.0002
    textnxfacts_t5small_infer_dqewebnlgtest_10['max_epochs'] = 10
    textnxfacts_t5small_infer_dqewebnlgtest_10['accumulate_grad_batches'] = 4
    textnxfacts_t5small_infer_dqewebnlgtest_10['linear_warmup_ratio'] = 0.1
    textnxfacts_t5small_infer_dqewebnlgtest_10['trp_sep_scheme'] = 2
    textnxfacts_t5small_infer_dqewebnlgtest_10['subobj_tok'] = False
    textnxfacts_t5small_infer_dqewebnlgtest_10['src_seps'] = {'input': 'input', 'ans': 'ans'}
    textnxfacts_t5small_infer_dqewebnlgtest_10['tgt_seps'] = {'input': 'input', 'qst': 'qst', 'cq': 'qstend', 'ans': 'ans'}
    textnxfacts_t5small_infer_dqewebnlgtest_10['tokenization'] = {'truncation': True, 'max_length': 512}
    textnxfacts_t5small_infer_dqewebnlgtest_10['gen_args'] = {'max_length': 512, 'min_length': 1, 'num_beams': 1}
    textnxfacts_t5small_infer_dqewebnlgtest_10['bert_score_model'] = 'bert-base-multilingual-cased'

    

def deepspeed_config_for_t5(ds_stage = 2):
    # modified from https://github.com/huggingface/transformers/blob/main/tests/deepspeed/ds_config_zero2.json
    ds_config = { 
    "bf16": {
        "enabled": True
    },

    "zero_optimization": {
        "stage": 2,
        "offload_optimizer": {
            "device": "cpu",
            "pin_memory": True
        },
        "allgather_partitions": True,
        "allgather_bucket_size": 2e8,
        "overlap_comm": True,
        "reduce_scatter": True,
        "reduce_bucket_size": 2e8,
        "contiguous_gradients": True
    },
    "zero_allow_untested_optimizer": True,
    "steps_per_print": 2000,
    "wall_clock_breakdown": False}
    if ds_stage == 2: pass
    elif ds_stage == 3: 
        ds_config['zero_optimization']['stage'] = 3
        ds_config['zero_optimization']['offload_param'] = {
            "device": "cpu",
            "pin_memory": True
        }

        print('DEEP SPEED 3 OFFLOAD CONFIG READY...')


    return ds_config

def set_savepath(args, ):
    SAVEPATH = f'results/omnibus_model/{args.settings_code}'
    datasets = {ds: settings['datasets_filter'].get(ds, None) \
                for __, settings in args.settings['tasks'].items() \
                for ds in settings['datasets']}
    print('datasets', datasets)
    for ds in sorted(datasets.keys()): 
        SAVEPATH += f'_{ds}'
        if datasets[ds] is not None: SAVEPATH += f'_{datasets[ds]}'
    if args.load_ckpt:
        ckpt_output_path, ckpt_save_path = args.load_ckpt
        if ckpt_output_path != '': 
            date = re.search(r'[0-9]{4}-[0-9]{2}-[0-9]{2}', ckpt_output_path).group()
            SAVEPATH += f'-loaded_{date}'
    
    if args.test_only: 
        if args.test_only == True: 
            assert args.load_ckpt != [], 'If testing only, specify the checkpoint to load'
        SAVEPATH += '-test_only'
    SAVEPATH += f'_lrwarm{args.settings["linear_warmup_ratio"]}'
    txtanssel_str = "_TAS" if args.textansselector else ""
    SAVEPATH += f'_ep{args.settings["max_epochs"]}{args.exp_code_str}{txtanssel_str}/'
    print('SAVEPATH: ', SAVEPATH)
    return SAVEPATH

def check_save2loadmodel(args):
    key = 0 if args.load_ckpt[0] else 1
    fp = os.path.dirname(args.load_ckpt[key]) + '/model_locked.status'
    if os.path.exists(fp): 
        args.logger.print('Model state currently being used for loading elsewhere... Retrying in 5...')
        return False
    else: 
        with open(fp, 'w+') as f: f.write(f'Locked: {str(datetime.datetime.now())}')
        return True

def unlock4loadmodel(args):
    key = 0 if args.load_ckpt[0] else 1
    fp = os.path.dirname(args.load_ckpt[key]) + '/model_locked.status'
    if os.path.exists(fp): os.remove(fp)
    args.logger.print('Model state being freed for use...')


def backward_compat_args(args):
    args.graphdb_ip_add = False
    args.add_task_tokens = False
    args.main_task_token = None
    args.main_task_token_id_enc = None 
    args.main_task_token_id_dec = None 
    args.trp_sep_scheme = None
    args.trp_seps = None
    args.triple_repr = None
    args.src_filepath = 'NA_KELM_CQ' # for process_nl2kb.py
    args.graphdb_ip_add = 'localhost'
    return args

def visual_inspect_saveout(p_lm_args, datamodule, tokenizer, logger):
    if p_lm_args.test_only: 
        __bx = next(iter(datamodule.test_dataloader()))
    else: 
        __bx = next(iter(datamodule.train_dataloader()))
    for task_name, data in __bx.items():
        logger.print('TASK:', task_name.upper())
        s, __, t, *__ = data
        # max_size = max(len(s), 2000)
        max_size = max(len(s), 200000)
        s_dec = tokenizer.batch_decode(s[0:max_size])
        s_dec += tokenizer.batch_decode(s[-max_size:])
        s_dec = [ss.replace(tokenizer.pad_token, '') for ss in s_dec]

        t_dec = tokenizer.batch_decode(t[0:max_size])
        t_dec += tokenizer.batch_decode(t[-max_size:])
        t_dec = [tt.replace(tokenizer.pad_token, '') for tt in t_dec]
        
        collect = []
        for __, (ss, tt) in enumerate(zip(s_dec,t_dec)):
            if __ <10 or __ > len(s_dec)-10:
                logger.print('SRC:', ss) 
                logger.print('TGT:', tt) 
                logger.print()
            collect.append({'src': ss, 'tgt': tt})
            
    with open(os.path.join(p_lm_args.savepath, 'inputs_targets_samples.jsonl'),
            encoding = 'utf-8', mode = 'w+') as f: 
            for l in collect: f.write(json.dumps(l) + '\n')