import logging, torch, json, copy, sys, os
LOGGING_LEVEL = logging.INFO

##### logging tools ##### 
class QTTLogger:

    def __init__(self, foldername, main_process):
        '''
        Creates a logging object and begins saving to a log file.
        '''
        self.logger = None
        self.main_process = main_process
        if self.main_process:
            self.logger = logging.getLogger()
            fhandler = logging.FileHandler(filename=foldername+'errorlogging.log', mode='a')
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            fhandler.setFormatter(formatter)
            self.logger.addHandler(fhandler)
            self.logger.setLevel(LOGGING_LEVEL)

    def print(self, *message):
        '''
        Helper function to print and log messages. 
        '''
        if self.main_process:
            # print(*message)
            self.logger.log(LOGGING_LEVEL, message)


def save_out(test_out_savepath, idxes, gens, tgt, src, logger):
    '''
    helper to save and print model outputs.
    '''
    logger.print('GEN :', gens[0:1])
    logger.print('TGT :', tgt[0:1])
    logger.print('SRC :', src[0:1])
    logger.print('IDX :', idxes[0:1])

    with open(test_out_savepath+'tested_gens.txt', 'w+') as f:
        [f.write(f'{idx}\t{i}\n') for idx, i in zip(idxes, gens)]
    with open(test_out_savepath+'tested_tgt.txt', 'w+') as f:
        [f.write(f'{idx}\t{i}\n') for idx, i in zip(idxes, tgt)]
    with open(test_out_savepath+'tested_src.txt', 'w+') as f:
        [f.write(f'{idx}\t{i}\n') for idx, i in zip(idxes, src)]
    
    logger.print()

def decode_gens_tgts(gens, tgt, src, idxes, tokenizer):
    '''
    helper function to decode the generated, target and source. to retain separator tokens 
    for splitting, we manually replace the pad tokens with '' (instead of skip_special_tokens.)
    '''
    pad_token = tokenizer.pad_token
    gens_dec = [elem.replace(pad_token, '') for elem in tokenizer.batch_decode(gens)]
    tgt_dec  = [elem.replace(pad_token, '') for elem in tokenizer.batch_decode(tgt)]
    src_dec  = [elem.replace(pad_token, '') for elem in tokenizer.batch_decode(src)]
    idx_dec  = [elem.replace(pad_token, '') for elem in tokenizer.batch_decode(idxes)]

    return gens_dec, tgt_dec, src_dec, idx_dec

class TextAnsSelector:
    def __init__(self, args):
        from modeling_omnibus import load_model_tokenizer, load_save_model
        sys.path.append('../data_utils')
        from data_utils_textnxfacts import TextAnswerSelectorEntry
        
        self.tas_args = copy.deepcopy(args)
        assert self.tas_args.textansselector_ckpt, f"tas_args.load_ckpt must be set... {self.tas_args.textansselector_ckpt}"
        fp_exp_config = os.path.join(os.path.dirname(self.tas_args.textansselector_ckpt), 'exp_configs.json')
        with open(fp_exp_config) as f: self.exp_config = json.load(f)
        for k,v in self.exp_config.items():
            if k == 'textansselector_ckpt': continue
            setattr(self.tas_args, k, v)
        self.tas_args.load_ckpt = ['', self.tas_args.textansselector_ckpt]
        print('\n\n!!! LOADING TEXT ANS SELECTOR MODEL AND TOKENIZER', self.tas_args.load_ckpt)
        self.model, self.tokenizer = load_model_tokenizer(self.tas_args, test_phase = True)
        self.model = load_save_model(*self.tas_args.load_ckpt, self.tas_args, test_phase = True)
        print('!!! LOADING TEXT ANSWER SELECTOR MODEL AND TOKENIZER DONE \n\n')
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model.to(self.device)
        self.model.eval()
        self.task_settings = self.tas_args.settings['tasks']['textansselect']

    def predict_answerset(self, text, lang = 'en'):

        entry = TextAnswerSelectorEntry(self.tas_args, self.tokenizer, text, 
                                        'text', set(['NONE']), task_name = 'textansselect', 
                                        task_settings = self.task_settings, id = 'None')
        entry.lang = lang
        entry.prep4input(bypass_tgt = True)


        with torch.no_grad(): 
            gen = self.model.model.generate(input_ids = torch.LongTensor([entry.src]).to(self.device),
                                **self.tas_args.settings['gen_args'])

        pred_answerset = self.tokenizer.decode(gen[0]).replace(self.tokenizer.pad_token, '')\
            .replace(self.tokenizer.eos_token, '')
        pred_answerset = pred_answerset.split(self.tas_args.trp_seps['ans'])
        pred_answerset = set(a.strip() for a in pred_answerset)
        # remove '', ensure a in text, also ensure ans not longer than 1/2 of text
        pred_answerset = [a for a in pred_answerset if a and a in text and len(a)/len(text)<0.5]
        return pred_answerset