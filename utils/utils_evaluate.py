import datasets, torch, copy, random

def evaluate(references, generated, 
            tgt_strategy = 'haystack-t5-e2e-qg', 
            gentypes = ['qtype_strict', 'qtype_partial'], 
            reftypes = ['single'], pfx = '',
            auto_scores = ['bertscore'],
            bert_score_model = 'bert-base-multilingual-cased', 
            bert_score_rescale_with_baseline = True,
            eos_token = '</s>'):
    '''
    Given a set of generated questions score on BERTScore
    
    Params: 
    - pfx (str): a string prefix to distinguish different outputs (e.g. sq for simpler questions, 
    cq for complex questions)
    - gentypes (list, str): 'normal' is an option too. 
    >> qtype_partial
    >> qtype_strict: if the generated set of questions does not 
    - reftypes (list, str): 'multi' is an option too.
    '''

    bertscore = datasets.load_metric('bertscore', experiment_id=random.randint(1,1000))
        
    score, stage = {}, 'test'
    assert len(references) == len(generated)
    score['count'] = len(references)
    # ensure eos token at end is removed (may be lingering for some settings,
    # where detok keeps special tokens so as to split on them, e.g. for QG)
    if type(references[0]) == list: references = [[r2.replace(eos_token, '') for r2 in r] for r in references]
    else: references = [r.replace(eos_token, '') for r in references]
    if type(generated[0]) == list: [[g2.replace(eos_token, '') for g2 in g] for g in generated]
    else: generated = [g.replace(eos_token, '') for g in generated]
    
    for score_name in auto_scores:
        for reftype in reftypes:    
            for gentype in gentypes:
                qtype_recall = 0
                __references, __generated = copy.deepcopy(references), copy.deepcopy(generated)
            
                # for haystack-t5-e2e-qg or similar, (multi Q for 1 input context)
                # also, extend references to give 1 output - 1 ref scoring 
                if tgt_strategy == 'dqe_compare' and 'multi' in reftypes:  
                    # NOTE: for handling coverage cross-comparison for DQE and our's  
                    # multi-ref setting. flatten generated. 
                    # for each entry, create as many sets of refs as # refs per entry
                    __references = [__references[idx] for idx, g in enumerate(__generated) for g2 in g]
                    __generated = [g2.strip() for g in __generated for g2 in g]
                
                for ix in [0,-11]:
                    print('CHECK: __generated[0]', __generated[ix])
                    print('CHECK: __references[0]', __references[ix])
                    print()
                
                score[stage + f'_qtyperecall_{reftype}_{gentype}_{pfx}'] = qtype_recall/len(references)
                
                if score_name == 'bertscore':
                    # set __references 
                    bertscoref1_all = bertscore.compute(predictions = __generated, references = __references, 
                                    lang = 'en', model_type = bert_score_model, 
                                    rescale_with_baseline = bert_score_rescale_with_baseline, #use_fast_tokenizer = True, 
                                    idf = False, device = torch.device('cuda') \
                                    if torch.cuda.is_available() else torch.device('cpu'))['f1'] 
                    bertscoref1_avg = sum(bertscoref1_all)/len(__generated)
                    score[stage+f'_bertscoref1_{reftype}_{gentype}_{pfx}'] = float(bertscoref1_avg)
                    print('BERTScore done:', float(bertscoref1_avg))

                    del bertscoref1_all, bertscoref1_avg # prevent OOM

                else: raise NotImplementedError

    return score
