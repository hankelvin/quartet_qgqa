from pytorch_lightning import LightningDataModule
import pickle, random, os, pickle, re, sys, torch
# sys.path.append('../dataset_handlers')
from dataset_handlers import webnlg, allq_cq
from sklearn.model_selection import train_test_split
from pytorch_lightning.trainer.supporters import CombinedLoader
from torch.utils.data import DataLoader
from collections import Counter

class OmnibusDataModule(LightningDataModule):

    def __init__(self, args, tokenizer, ipstrat2idx):
        super().__init__()
        self.args = args
        self.tokenizer = tokenizer
        self.webnlg_full_train = None
        self.ipstrat2idx = ipstrat2idx

        c_use_saved = args.settings.get('use_saved_data', False)
        c_datasets_filter = [ds for k,v in args.settings['tasks'].items() for ds in v['datasets_filter']]
        c_webnlg_filter = 'dqe-webnlg' in c_datasets_filter or 'gem-webnlg' in c_datasets_filter
        if c_webnlg_filter: c_use_saved = False
        c_save_proced = args.settings.get('save_processed_data', False)
        if type(c_use_saved) == str: data_pkl_fp = c_use_saved
        else: 
            data_pkl_fp = os.path.basename(args.savepath[:-1]) # remove "/""
            data_pkl_fp = re.match(r'(.+)(?:\_ep([0-9]+|None))', data_pkl_fp).group(1)
            # NOTE: ensure that if filtered setting used, load and save the right versions
            filter_str = \
            re.search(r'(no-rejects|all-[01]\.[0-9]{1,2}|1t-[01]\.[0-9]{1,3})', args.savepath)
            if filter_str: data_pkl_fp += f'_{filter_str.group()}'
            if args.multilingual: 
                data_pkl_fp += f'_MULTILINGUAL'
                for lang in args.languages: data_pkl_fp += f'-{lang}'
        data_pkl_fp = f'datasets/tqg_proc_data/{data_pkl_fp}.pkl'
        c_data_pkl = os.path.exists(data_pkl_fp)
        if c_use_saved and c_data_pkl:
            self.args.logger.print(f'Loading processed data from {data_pkl_fp}')
            with open(data_pkl_fp, 'rb') as f:datasets_dict = pickle.load(f)
            self.args.logger.print(f'Processed data loaded from {data_pkl_fp}')
            c_save_proced = False
            for task_name, ddict in datasets_dict.items(): 
                for split, dlist in ddict.items():
                    self.args.logger.print(f'for split {split}, the data size is:', len(dlist))

            if args.test_only: # ensure no other splits around
                for task_name, ddict in datasets_dict.items(): 
                    datasets_dict[task_name] = {'test': ddict['test']}

        else: 
            self.args.logger.print(f'Begin processing of data from scratch.')
            datasets_dict, do_split = {}, False
            ALLQ_CQ_CODES = set()
            skel = 'allq-cq-{}{}{}{}'
            for d1 in ['webnlg', 'kelm']:
                for d2 in ['', '-negsamp']:
                    if d2 == '-negsamp':
                        for d3 in ['', '2', '3', '4']:
                            for d4 in ['', '-hardnegs']:
                                ALLQ_CQ_CODES.add(skel.format(d1, d2, d3, d4))
                    else: 
                        d3, d4 = '', ''
                        ALLQ_CQ_CODES.add(skel.format(d1, d2, d3, d4))
                
            for task_name, task_settings in args.settings['tasks'].items():
                data_test, data_train, data_dev, data_traindev = [], [], [], []
                for dataset in task_settings['datasets']:
                    if 'gem-webnlg' in dataset or 'dqe-webnlg' in dataset and 'allq-webnlg' not in dataset:
                        self.args.logger.print('Loading WebNLG dataset')
                        if args.test_only: splits = ['test'] 
                        elif args.settings_code in ['textnxfacts_t5small']: 
                            splits = ['train', 'validation']
                        else: splits = ['train', 'validation', 'test']
                        __data, unfiltered_train_size = \
                            webnlg.load_webnlg(self.args, self.tokenizer, task_name, task_settings,
                                                    dataset_version = dataset, splits = splits)
                        self.webnlg_full_train = unfiltered_train_size
                        # HACK: all the entries were being put into 'train' and 'validation'
                        if args.settings_code in ['textnxfacts_t5small']:
                            data_test.extend(__data['test'])
                            data_train.extend(__data['test'][:20])
                            data_dev.extend(__data['test'][:20])
                        else:     
                            if 'test' in __data: data_test.extend(__data['test'])
                            if 'train' in __data: data_train.extend(__data['train'])
                            if 'validation' in __data: data_dev.extend(__data['validation'])

                    elif 'allq-cq' in dataset:
                        self.args.logger.print('Loading ALLQ-CQ dataset')
                        __data = allq_cq.load_allq_cq(self.args, task_name, task_settings)
                        random.shuffle(__data)
                        c_test_only = task_name in ['textnxfacts', 'cqg'] and 'test_only' in dataset
                        if c_test_only and dataset not in ALLQ_CQ_CODES: 
                            data_test.extend(__data)
                            data_train.extend(__data[:10])
                            data_dev.extend(__data[:10])
                        elif dataset in ALLQ_CQ_CODES:
                            # for handling squad samples added
                            __data_squad = [e for e in __data if 'webnlg' not in e.id and \
                                            not re.search(r'[0-9]{3}_[0-9]{3}_+', e.id)]
                            __data = [e for e in __data if 'webnlg' in e.id or \
                                    re.search(r'[0-9]{3}_[0-9]{3}_+', e.id)]

                            if 'webnlg' in dataset:
                                __data_train = [e for e in __data if ('train' in e.id and 'webnlg' in e.id)]
                                __data_devtest = [e for e in __data if ('train' not in e.id and 'webnlg' in e.id)]
                            else:
                                __data_train, __data_devtest  = train_test_split(__data, 
                                            train_size = 0.85, random_state = 54506)
                            
                            # for handling squad samples added
                            if __data_squad:
                                __data_squad_train, __data_squad_devtest = \
                                    train_test_split(__data_squad, train_size = 0.8, random_state = 54506)
                                __data_train += __data_squad_train
                                __data_devtest += __data_squad_devtest

                            self.args.logger.print('__data_devtest SIZE', len(__data_devtest))
                            self.args.logger.print('__data_train SIZE', len(__data_train))

                            if len(__data_devtest)/len(__data_train) < 0.2:
                                __data_train, _dotherplus = \
                                    train_test_split(__data_train, 
                                                train_size = 1-(0.2-len(__data_devtest)/len(__data_train)), 
                                                random_state = 54506)
                                __data_devtest += _dotherplus
                            
                            __data_test, __data_dev = \
                                train_test_split(__data_devtest, train_size = 0.5, 
                                                random_state = 54506)
                            
                            data_train.extend(__data_train)
                            data_test.extend(__data_test)
                            data_dev.extend(__data_dev)
                        else:
                            __data_traindev, __data_test  = train_test_split(__data, 
                                            train_size = 0.85, random_state = 54506)
                            data_test.extend(__data_test)
                            data_traindev.extend(__data_traindev)
                            do_split = True
                        
                if do_split:
                    random.shuffle(data_traindev)
                    data_train, data_dev = train_test_split(data_traindev, train_size = (0.7/0.85), random_state = 54506)
                self.args.logger.print(f'for {task_name}... train, dev, test sizes:', len(data_train), len(data_dev), len(data_test))
                datasets_dict[task_name] = {'train': data_train, 'dev': data_dev, 'test': data_test}
                self.args.logger.print('')
        
        for task_name, ddict in datasets_dict.items(): 
            for split in ['train', 'dev', 'test']:
                self.args.logger.print(f'DISTRIBUTION OF {split} SPLIT FOR {task_name} TASK:', 
                            Counter([f"{getattr(i, 'ipstrat', None)}_negative{getattr(i, 'negative', None)}" for i in ddict.get(split, [])]))
        
        ### COLLATE func ###
        self.collate_fn = Collate(bos_token_id = self.tokenizer.bos_token_id, 
                    pad_token_id = self.tokenizer.pad_token_id, 
                    eos_token_id = self.tokenizer.eos_token_id,
                    ipstrat2idx = self.ipstrat2idx).collate_fn
        
        ### to attr for access ###
        self.datasets_dict = datasets_dict

        if c_save_proced:
            self.args.logger.print(f'Saving processed data to {data_pkl_fp}')
            with open(data_pkl_fp, 'wb+') as f: pickle.dump(datasets_dict, f)
            self.args.logger.print(f'Processed data saved to {data_pkl_fp}')

    def train_dataloader(self):
        dataloaders, key, empty = {}, 'train', True
        for task, datasets in self.datasets_dict.items():
            if datasets[key]:
                dataloaders[task] = DataLoader(datasets[key], batch_size = self.args.settings['bsz'], 
                    collate_fn = self.collate_fn, num_workers = 2, pin_memory = False, shuffle = True)
                empty = False
        # using CombinedLoader
        # https://github.com/Lightning-AI/lightning/issues/10809
        if not empty: return CombinedLoader(dataloaders, mode = 'max_size_cycle')
        else: return None

    def val_dataloader(self):
        dataloaders, key, empty = {}, 'dev', True
        for task, datasets in self.datasets_dict.items():
            if datasets[key]:
                dataloaders[task] = DataLoader(datasets[key], batch_size = self.args.settings['bsz'], 
                    collate_fn = self.collate_fn, num_workers = 2, pin_memory = False)
                empty = False
            
        if not empty: return CombinedLoader(dataloaders, mode = 'max_size_cycle')
        else: return None

    def test_dataloader(self):
        dataloaders, key, empty = {}, 'test', True
        for task, datasets in self.datasets_dict.items():
            if datasets[key]:
                dataloaders[task] = DataLoader(datasets[key], batch_size = min(256, self.args.settings['bsz']*20), 
                    collate_fn = self.collate_fn, num_workers = 2, pin_memory = False)
                empty = False
            
        if not empty: return CombinedLoader(dataloaders, mode = 'max_size_cycle')
        else: return None

class Collate:
    '''
    Custom collate function for dataloading in this script.
    '''
    def __init__(self, bos_token_id = None, pad_token_id = None, eos_token_id = None, 
                ipstrat2idx = None):
        self.bos_token_id, self.eos_token_id = bos_token_id, eos_token_id
        self.pad_token_id, self.ipstrat2idx = pad_token_id, ipstrat2idx

    def pad2max(self, batch_elem, make_mask = False):

        max_len = max([len(s) for s in batch_elem])
        new_batch_elem, batch_elem_mask = [], []
        for s in batch_elem: 
            orig_len, pad_size = len(s), max_len - len(s)
            s.extend([self.pad_token_id] * pad_size)
            new_batch_elem.append(s)
            if make_mask:
                batch_elem_mask.append([1] * orig_len + [0] * pad_size)

        return new_batch_elem, batch_elem_mask 

    def collate_fn(self, batch):
        ipstrat_enc = torch.LongTensor([self.ipstrat2idx[entry.ipstrat] for entry in batch])

        # 1. prepare tgt
        tgt_pad = self.pad_token_id
        
        tgt = [entry.tgt for entry in batch]
        # check case of kbqa with token_indices
        if type(tgt[0]) == tuple: assert all(len(tt) for tt in tgt)
        elif type(tgt[0]) == int: pass # for textnxfacts
        else: tgt, tgt_mask = self.pad2max(tgt, make_mask = True)
        tgt = torch.LongTensor(tgt)
        tgt_mask = torch.LongTensor(tgt_mask)
        tgt[(tgt==self.pad_token_id)] = tgt_pad 
        
        src, src_mask = [entry.src for entry in batch], []

        # 2. prepare src 
        # pad to maxlen in batch, also create mask
        src, src_mask = self.pad2max(src, make_mask = True)
        src, src_mask = torch.LongTensor(src), torch.LongTensor(src_mask)
        
        id_enc, __ = self.pad2max([entry.id_enc for entry in batch], make_mask = False)
        id_enc = torch.LongTensor(id_enc)

        return src, src_mask, tgt, tgt_mask, id_enc, ipstrat_enc
