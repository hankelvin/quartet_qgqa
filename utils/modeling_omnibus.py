import json 
from data_utils.shared import QTypesDetect
REL_SEP, INPUT_SEP = '[REL] ', '[INP] '

def load_model_tokenizer(args, test_phase = False):
    try: print_func = args.logger.print
    except: print_func = print
    
    from transformers import AutoModelForSeq2SeqLM, AutoTokenizer
    ### 1. Tokenizer prep
    TOKENIZER = AutoTokenizer.from_pretrained(args.settings['model'], use_fast = True,)
    args.trp_seps = trip_sep_loader(args.settings['trp_sep_scheme'])
    args.trp_seps.update(trip_sep_loader_qa(args.settings['trp_sep_scheme']))
    add_tokens = [st for st in args.trp_seps.values()]         
    qtypes = [f'[{qt}]' for qt in QTypesDetect().question_words] + ['[other]']
    add_tokens += qtypes
    if getattr(args, 'multilingual_trained', getattr(args, 'multilingual', None)):
        __langs = getattr(args, 'languages_trained', getattr(args, 'languages', None))
        if __langs and len(__langs) >1:  
            for lang in __langs: 
                lang_token = f'[{lang}]'
                print('LANG TOKEN ADDED:', lang_token)
                add_tokens.append(lang_token)
    args.add_tokens = sorted(add_tokens) # for use in dataset loading with mp
    print_func(f'These tokens being added {args.add_tokens}') 
    TOKENIZER.add_tokens(sorted(add_tokens), special_tokens = True)
    if not getattr(TOKENIZER, 'vocab', False): setattr(TOKENIZER, 'vocab', TOKENIZER.get_vocab())
    print_func('TOKENIZER SIZE: ', len(TOKENIZER.vocab))

    ipstrats = set(i for __, v in args.settings['tasks'].items() for i in v['input_strategies'])
    ipstrat2idx = {ipstrat: i for i, ipstrat in enumerate(sorted(ipstrats))}
    
    ### 2. Model prep 
    MODEL = AutoModelForSeq2SeqLM.from_pretrained(args.settings['model'])
    MODEL.ipstrat2idx = ipstrat2idx
    print_func('!!! args.settings_code!!!!',  args.settings_code)
    if len(TOKENIZER.vocab) > MODEL.config.vocab_size: 
        print_func('resizing embedding to:', max(len(TOKENIZER.vocab), MODEL.config.vocab_size))
        MODEL.resize_token_embeddings(max(len(TOKENIZER.vocab), MODEL.config.vocab_size))
    
    MODEL.args = args
    return MODEL, TOKENIZER


def load_save_model(ckpt_output_path, ckpt_save_path, args, test_phase = False, load4dqe = False):
    import sys
    sys.path.append('../')
    from omnibus import OmnibusModel
    import torch, os

    # NOTE: issue between Transformer/Deepspeed... certain weights not saved by 
    # convert_zero_checkpoint_to_fp32_state_dict
    # 'model.encoder.embed_tokens.weight', 'model.decoder.embed_tokens.weight', 'model.lm_head.weight'
    # i. see https://github.com/microsoft/DeepSpeed/issues/1893
    # ii. see https://github.com/microsoft/DeepSpeed/issues/1896
    try: print_func = args.logger.print
    except: print_func = print
    full_state_dict = load_best_ckpt(ckpt_save_path)
    tmp = False
    c_deepspeed_bf16 = args.ds_config['bf16']['enabled']
    if ckpt_output_path != '' and not c_deepspeed_bf16:
        print_func('\t\t 1. Either "ckpt_output_path" specified, or "post_train_testing" specified.')
        print_func('\t\t Loading saved model.pt from here:', ckpt_output_path)
        save_model_pt = torch.load(ckpt_output_path, map_location=torch.device('cpu'))['state_dict']
        for k,v in save_model_pt.items():
            assert torch.allclose(full_state_dict[k].float().ceil(), v.float().ceil()), \
                ("Saved weights don't match for: ", k, 
                full_state_dict[k].float().half()[0], v.float().half()[0], 
                full_state_dict[k].float().half()[0].shape, v.float().half()[0].shape)
        print_func("\t\t 2. Saved weights match! ")
    else:
        import tempfile 
        if not os.path.exists('tmp'): os.makedirs('tmp')
        tmp = tempfile.NamedTemporaryFile(delete = True, dir = 'tmp')
        save_model_pt, ckpt_output_path = {}, tmp.name

    print_func("Weights (where saved by fp32 conversion script) match best \
                        checkpoint's... proceeding to overwrite.")
    save_model_pt['state_dict'] = full_state_dict
    torch.save(save_model_pt, ckpt_output_path)
    print_func('Saving to tmp ckpt_output_path: ', ckpt_output_path)

    # load the saved args
    exp_configs_path = os.path.join(os.path.dirname(ckpt_save_path), 'exp_configs.json')
    with open(exp_configs_path) as f: exp_config = json.load(f)
    use_saved_data = args.settings['use_saved_data']
    linear_warmup_ratio = args.settings['linear_warmup_ratio']
    tasks = args.settings['tasks']
    max_epochs = args.settings['max_epochs']
    settings_code = args.settings_code
    override = ['logger', 'load_ckpt', 'test_only', 'load_pkl_dataset', 'exp_code_str', 'bsz',
                'max_epochs', 'datasets', 'datasets_filter', 'partial_freeze_mamtuning', 
                'save_all_epochs', 'savepath', 'test_out_savepath', 'ds_config', 'training_steps',
                'patience', 'accelerator', 'devices', 'num_nodes']
    if load4dqe: override.extend(['settings', 'settings_code'])
    for k,v in exp_config.items():
        if k in override: continue
        setattr(args, k, v)

    pl_model = OmnibusModel.load_from_checkpoint(checkpoint_path = ckpt_output_path, 
            args = args, test_phase = test_phase, strict = True, map_location=torch.device('cpu'))
    args.settings_code = settings_code
    args.settings['tasks'] = tasks
    args.settings['max_epochs'] = max_epochs
    args.settings['use_saved_data'] = use_saved_data
    args.settings['linear_warmup_ratio'] = linear_warmup_ratio

    print_func('Loading from tmp ckpt_output_path: ', ckpt_output_path)
    pl_model.args = args
    
    if tmp: 
        try: os.remove(tmp.name) # delete tmp file
        except: pass
    return pl_model

def load_best_ckpt(best_ckpt):
    '''
    solves an issue of lm_head not being saved out by deepspeed's 
    convert_zero_checkpoint_to_fp32_state_dict when using zero2_offload. 
    Loads the mp_rank_00_model_states.pt for best checkpoint and 
    extracts lm_head.weight.
    '''
    import torch
    state_dict = torch.load(f'{best_ckpt}/checkpoint/mp_rank_00_model_states.pt', 
                        map_location=torch.device('cpu'))
    full_state_dict = {}
    for k,v in state_dict['module'].items():
        full_state_dict[k.replace('module.', '')] = v

    return full_state_dict

def save_exp_configs(p_lm_args, pl_model, logger):

    setattr(p_lm_args, 'ipstrat2idx', pl_model.model.ipstrat2idx)
    exp_config = vars(p_lm_args)
    exp_config['logger'] = 'removed'
    with open(f'{p_lm_args.savepath}exp_configs.json', 'w+') as f:
        json.dump(exp_config, f)
    logger.print('Exp config saved!!')

def trip_sep_loader(trp_sep_scheme):
    if trp_sep_scheme == 1: 
        trp_seps = {'s': '', 'p':'| ', 'o': '| ', 
                    'qp': REL_SEP, 'qo': '| ', 
                    'tripend': '', 'tripsetend': '', 
                    'input': INPUT_SEP, 'spare1': '',
                    'spare2': '', 'spare3': ''} 
    elif trp_sep_scheme == 2: 
        trp_seps = {'s': '[SUB] ', 'p':'[PRP] ', 'o': '[OBJ] ', 
                    'qp': REL_SEP, 'qo': '[QOBJ] ', 
                    'tripend': '[TEND] ', 'tripsetend': '[TSEND] ', 
                    'input': INPUT_SEP, 'spare1': '[Sp1]',
                    'spare2': '[Sp2]', 'spare3': '[Sp3]'} 
    return trp_seps

def trip_sep_loader_qa(trp_sep_scheme):
    if trp_sep_scheme == 1: 
        raise ValueError
    elif trp_sep_scheme == 2: 
        trp_seps = {'qst': '[QST] ', 'ans':'[ANS] ',
                    'qstend': '[QEND] ', 'ansend': '[AEND] ',
                    'spare1': '[Sp1] ','spare2': '[Sp2] ', 
                    'spare3': '[Sp3] '} 
    return trp_seps
 