import json, math, copy, sys
from collections import defaultdict
import pytorch_lightning as pl
from deepspeed.ops.adam import DeepSpeedCPUAdam
from deepspeed.runtime.lr_schedules import WarmupDecayLR
from utils.utils_evaluate import evaluate
from utils.modeling_omnibus import load_model_tokenizer
from utils.utils import save_out, decode_gens_tgts

class OmnibusModel(pl.LightningModule):
    
    def __init__(self, args, test_phase = False):
        super().__init__()
        self.test_phase = test_phase
        self.model, self.tokenizer = load_model_tokenizer(args, test_phase = self.test_phase)
        self.args = self.model.args
        self.gen_args = self.args.settings['gen_args']

        try: self.print_func = self.args.logger.print
        except: self.print_func = print

    def configure_optimizers(self):
        warmup_ratio = self.args.settings["linear_warmup_ratio"]
        warmup_num_steps = math.ceil(warmup_ratio*self.args.training_steps)
        lrate = self.args.settings['lr']
        self.print_func(f'OPTIMIZER: using DeepSpeedCPUAdam with linear warm-up of {warmup_ratio} ({warmup_num_steps} steps) of total training steps. LR: {lrate}')
        optimizer = DeepSpeedCPUAdam(self.model.parameters(), bias_correction = True, lr = lrate) 
        scheduler = WarmupDecayLR(optimizer = optimizer, total_num_steps = self.args.training_steps, 
                                    warmup_min_lr = 0,  warmup_max_lr = lrate, 
                                    warmup_num_steps = warmup_num_steps, warmup_type = 'linear')
        return {'optimizer': optimizer, 'lr_scheduler': {"scheduler": scheduler, "interval": "step"}}


    def lr_scheduler_step(self, scheduler, optimizer_idx, metric):
        scheduler.step()

    def training_step(self, batch, batch_idx, log = True):
        train_loss = 0.0
        for task_name, data in batch.items():
            src, src_mask, tgt, tgt_mask, id_enc, ipstrat_enc = data
            pred, gens = None, None
            out = self.model(input_ids = src, attention_mask = src_mask, labels = tgt)
            train_loss += out.loss
        train_loss = train_loss/len(batch)
        pbar = {'train_loss': train_loss.detach()}
        if log: 
            self.log("train_loss", train_loss, on_step = True, on_epoch = True, prog_bar = True, logger = True)
        return {'loss': train_loss, 'progress_bar': pbar, 'pred': pred, 'gens': gens}

    def train_epoch_end(self, train_step_outputs):
        train_loss_epoch = \
            sum([x['progress_bar']['train_loss'] for x in train_step_outputs])/len(train_step_outputs)
        pbar = {'train_loss_epoch': train_loss_epoch.detach()}
        return {'loss': train_loss_epoch, 'progress_bar': pbar}

    def validation_step(self, batch, batch_idx, log = True):
        results = self.training_step(batch, batch_idx, log = False)
        results_copy = copy.deepcopy(results)
        to_del = []
        for k,v in results['progress_bar'].items():
            new_k = k.replace('train', 'validation')
            results_copy['progress_bar'][new_k] = copy.deepcopy(v)
            to_del.append(k)
            if log: self.log(new_k, v, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        for k in to_del: del results_copy['progress_bar'][k]
        return results_copy

    def validation_epoch_end(self, val_step_outputs):
        validation_loss_epoch = \
            sum([x['progress_bar']['validation_loss'] for x in val_step_outputs])/len(val_step_outputs)
        pbar = {'validation_loss_epoch': validation_loss_epoch.detach()}
        return {'loss': validation_loss_epoch, 'progress_bar': pbar}

    def test_step(self, batch, batch_idx, log = True):
        out_dict = {}
        for task_name, data in batch.items():
            src, src_mask, tgt, tgt_mask, id_enc, ipstrat_enc = data
            gens = self.model.generate(src, **self.args.settings['gen_args'])
            out_dict[task_name] = {'gens': gens, 'tgt': tgt, 'src': src, 'id_enc': id_enc}
        return out_dict

    def test_epoch_end(self, test_step_outputs, log = True):
        test_scores_dict = {}
        
        # flatten across batches 
        test_data = {key: defaultdict(list) for key in test_step_outputs[0]}
        for __ in test_step_outputs: 
            for task_name, data_dict in __.items(): 
                for elem, data in data_dict.items():
                    test_data[task_name][elem].extend(data)
        
        for task_name, data in test_data.items():
            auto_scores = self.args.settings['tasks'][task_name]['auto_scores']
            bert_score_model = self.args.settings['bert_score_model']
            
            allgens, alltgt, allsrc, allidx = data['gens'], data['tgt'], data['src'], data['id_enc']
            gens, tgt, src, idxes = decode_gens_tgts(allgens, alltgt, allsrc, allidx, self.tokenizer)
            
            save_out(self.args.test_out_savepath, idxes, gens, tgt, src, self.args.logger)

            tgt_setting = self.args.settings['tasks'][task_name]['tgt_setting']
            test_score = evaluate(tgt, gens, gentypes = ['normal'], 
                                    reftypes = ['single'], tgt_strategy = tgt_setting,
                                    auto_scores = auto_scores, bert_score_model = bert_score_model,
                                    eos_token = self.tokenizer.eos_token)
        
            self.print_func('TEST SCORE', test_score)
            test_scores_dict[task_name] = test_score
        
        with open(f'{self.args.test_out_savepath}test_scores.json', 'w+') as f: 
            json.dump(test_scores_dict, f)
        return {'test_score': test_scores_dict}
