class TextQAEntry:
    '''
    For answering questions from text. 
    '''
    def __init__(self, args, tokenizer, question, answer,
                context_f, task_name, task_settings, 
                ans_pos = None, input_sep_token_id = None, 
                id = None, triples_ents = None, negative = False, **kwargs):
        self.ipstrat = 'textqa'
        self.context_f = context_f # this is the text 
        self.question, self.answer = question, answer
        self.ans_pos = ans_pos
        self.args, self.tokenizer, self.id = args, tokenizer, id
        self.is_quartet = 'quartet' in args.settings['tasks'] or 'quartet_infer' in args.settings_code
        
        self.max_length = args.settings['tokenization']['max_length']
        self.truncation = args.settings['tokenization']['truncation']
        self.task_name, self.task_settings = task_name, task_settings
        self.input_sep_token_id = input_sep_token_id
        self.triples_ents = triples_ents

        self.trp_sep_scheme = args.settings['trp_sep_scheme']
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)

        self.prmpt = task_settings['prompt_strategies']
        self.enc_pfx, self.dec_pfx = '', ''
        if self.prmpt: 
            if self.prmpt == 'verbal': 
                # {0}: # question, {1}: separator; {2}: context
                self.enc_pfx = f'answer TextQA {self.spare1_sep} '
            elif self.prmpt in ['', 'text_num_facts']: pass
            else: raise NotImplementedError

        self.src_ctx, self.src_qst, self.src_ans = None, None, None
        self.negative = negative
        self.lang = None

    def prep4input(self, tgt_format = 'dqe_generate', multilingual = False, **kwargs):
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)
        self.ipstrat = 'textqa' 

        if multilingual: 
            assert self.lang
            mling_str = f'[{self.lang}] '
        else: mling_str = ''
        if self.is_quartet and self.prmpt != 'verbal': 
            enc_verbal_pfx = f'textqa task {mling_str}{self.spare1_sep} ' 
            dec_verbal_pfx = f'textqa task {mling_str}{self.spare1_sep} '
        elif self.prmpt != 'verbal': 
            enc_verbal_pfx = self.enc_pfx + mling_str
            dec_verbal_pfx = self.dec_pfx + mling_str

        ### SRC 
        context = self.context_f
        src = enc_verbal_pfx + f'{self.question} {self.input_sep} {context}'
        self.src = self.tokenizer.encode(src, truncation = self.truncation, max_length = self.max_length)

        ### TGT 
        if 'dqe_generate' in tgt_format: 
            tgt = self.answer
            self.tgt = self.tokenizer.encode(dec_verbal_pfx + tgt, 
                        truncation = self.truncation, max_length = self.max_length)
        else: raise NotImplementedError