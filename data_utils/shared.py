import spacy, itertools, string, torch, re
import pandas as pd
from collections import OrderedDict
from nltk import edit_distance
from typing import List

class Entry:
    '''
    Base class for Entry-like objects
    '''
    def __init__(self, args, triple_name = None, triple_code = None, graphdb_avail = False):
        REL_SEP, INPUT_SEP = '[REL] ', '[INP] '
        self.graphdb_avail = graphdb_avail
        self.graphdb_ip_add = getattr(args, 'graphdb_ip_add', None)
        self.args = args

        if args.trp_sep_scheme is None: self.trp_sep_scheme = 1
        else: self.trp_sep_scheme = args.trp_sep_scheme
        
        if args.trp_seps is None: 
            self.trp_seps = {'s': '', 'p': '| ', 'o': '| ', 
                    'qp': REL_SEP, 'qo': '| ', 'input': INPUT_SEP} 
        else: self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)

        if self.trp_sep_scheme == 1:
            for sep in ['o_sep', 'qo_sep']: 
                if getattr(self, sep, None) is None: setattr(self, sep, self.p_sep)
            if getattr(self, 'input_sep', None) is None: self.input_sep = INPUT_SEP
            if getattr(self, 'qp_sep', None) is None: self.qp_sep = REL_SEP
            
        self.triple_repr = getattr(args, 'triple_repr', 'spo')
        if triple_name is not None: 
            # assert triple_code is not None
            if len(triple_name) == 3: 
                self.pick_s_name, self.pick_p_name, \
                self.pick_o_name = triple_name
                self.qual_obj_name = None
                
                if triple_code is not None: 
                    self.pick_s_code, self.pick_p_code, \
                    self.pick_o_code = triple_code
                    self.qual_obj_code = None
            
            elif len(triple_name) == 4: 
                # e.g. 'Valentin Lavigne', 'FC Lorient', 'start time', '01 January 2014'
                self.pick_s_name, self.qual_obj_name, \
                    self.pick_p_name, self.pick_o_name = triple_name

                if triple_code is not None: 
                    self.pick_s_code, self.qual_obj_code, \
                        self.pick_p_code, self.pick_o_code = triple_code

            else: 
                raise NotImplementedError('handling of quintruple and above not yet implemented.')
        else:
            self.pick_s_name, self.qual_obj_name, \
                    self.pick_p_name, self.pick_o_name = None, None, None, None
            self.pick_s_code, self.qual_obj_code, \
                    self.pick_p_code, self.pick_o_code = None, None, None, None

class G2QEntry:
    def __init__(self, args, tokenizer, context_s = None, context_f = None, question = None, 
                answers = None, q_type = None, a_type = None, gen_questions = None, 
                gen_answers = None, triples_name = None,
                triples_code = None, source = None, id = None):
        self.context_s = context_s # short
        self.context_f = context_f # full
        self.question, self.answers = question, answers
        self.source, self.id = source, id
        self.q_type, self.a_type = q_type, a_type
        self.gen_questions, self.gen_answers = [], []
        self.triples_name, self.triples_code = triples_name, triples_code
        self.gen_questions, self.gen_answers = gen_questions, gen_answers
        self.tokenizer = tokenizer
        
        self.args = args
        self.trp_sep_scheme = args.trp_sep_scheme
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)

    def prep4input(self, **kwargs):
        tset = self.triples_name
        tset_string =  f'{self.args.s2s_input_prompt}' 
    
        for t in tset: 
            trip_elems = make_trip_elems(t)
            tset_string += f'{self.create_triple(trip_elems)} '

        self.src = self.tokenizer.encode(tset_string, truncation = True, max_length = self.args.truncate_length+50)
        
        q_string = self.qst_sep.join(self.gen_questions)
        
        self.tgt = self.tokenizer.encode(q_string, truncation = True, max_length = self.args.truncate_length+50)
        self.graph = self.tokenizer.encode(str(self.triples_name), truncation = True, max_length = self.args.truncate_length+50)
        self.text = self.tokenizer.encode(self.context_f, truncation = True, max_length = self.args.truncate_length+50)
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)
    
    def _scores_table(self, ans_set, ents_set):
        '''
        given a set of entities (from the graph)
        and a set of answers (generated from text),
        return a table of pair-wise edit distances.
        edit scores are normalised by #char in entity name
        '''
        table = pd.DataFrame(columns = ans_set, index = ents_set)
        pairs = itertools.product(ents_set, ans_set)
        for p in pairs: # normalise by length of the enitites
            e, a = p
            score = round(edit_distance(e, a)/len(e), 2)
            
            table.loc[p] = score
            
        return table 

    def _align_ans_ents(self, gen_answers, inv_nameslexicon):
        '''
        given a set of generated questions and answers (from text),
        align answers with entities in graph.
        Params:
        - inv_nameslexicon (dict): keys are entities, values are variables
        '''
        self.other_var = self.spare1_sep # use a spare token to rep "other"
        # add to nameslexicon, used for sorting in prep4input
        self.nameslexicon[self.other_var] = self.other_var
        # keep only ents. here k: entity name, v: entity var 
        ent_lexicon = {k:v for k,v in inv_nameslexicon.items() if 'PRP' not in v}
        max_len = max([len(ent) for ent in ent_lexicon.keys()])
        
        ans_set = list(set(a2 for a in gen_answers for a2 in a))
        ents_set = list(set(ent_lexicon.keys()))
        
        ans_ent_dict = {}
        df_scores = self._scores_table(ans_set, ents_set)
        for a in ans_set:
            if len(a) > max_len: 
                ans_ent_dict[a] = self.other_var
                continue
            
            # a. search for 0 score (no edit) first
            s = df_scores[df_scores[a]==0]
            # if present accept
            if s.shape[0] > 0: 
                ans_ent_dict[a] = ent_lexicon[s.index[0]]
                continue

            # search for min score, if > 1, set to other
            closest_ratio = df_scores[a].min()
            if closest_ratio > 1:
                ans_ent_dict[a] = self.other_var
                continue
            s = df_scores[df_scores[a]==closest_ratio]
            if s.shape[0] > 0: 
                ans_ent_dict[a] = ent_lexicon[s.index[0]]
                continue

            ans_ent_dict[a] = self.other_var
        
        return ans_ent_dict

    def create_triple(self, trip_elems = None):
        '''
        creates a triple from the s,p,o attributes in instance. 
        '''
        pick_s_name, pick_p_name  = trip_elems['pick_s_name'], trip_elems['pick_p_name']
        pick_o_name, qual_obj_name = trip_elems['pick_o_name'], trip_elems['qual_obj_name']            

        if self.trp_sep_scheme == 1:
            triple = f'{pick_p_name} {self.o_sep}{pick_o_name}'
            if qual_obj_name is None:
                triple = f'{pick_s_name} {self.p_sep}' + triple
            else: 
                # e.g. 'Valentin Lavigne', 'FC Lorient', 'start time', '01 January 2014'
                # will be 'Valentin Lavigne [REL] FC Lorient | start time | 01 January 2014'
                triple = \
                f'{pick_s_name} {self.qp_sep}{qual_obj_name} {self.o_sep}' + triple
        
        elif self.trp_sep_scheme == 2:
            # '<PRP> member of sport team <OBJ> FC Lorient':
            triple = f'{self.p_sep}{pick_p_name} {self.o_sep}{pick_o_name}'
            if qual_obj_name is None:
                # '<SUB> Valentin Lavigne <PRP> member of sport team <OBJ> FC Lorient':
                triple = f'{self.s_sep}{pick_s_name} ' + triple
            else: 
                # e.g. 'Valentin Lavigne', 'FC Lorient', 'start time', '01 January 2014'
                # will be '<SUB> Valentin Lavigne [REL] <qOBJ> FC Lorient <PRP> start time <OBJ> 01 January 2014'
                triple = \
                f'{self.s_sep}{pick_s_name} {self.qp_sep}{self.qo_sep}{qual_obj_name} ' + triple

        if trip_elems: return triple
        else: self.triple = triple


class T2QEntry:
    def __init__(self, context_s = None, context_f = None, question = None, 
                answers = None, q_type = None, a_type = None, triples_name = None,
                triples_code = None, gen_questions = [], 
                gen_answers = [], source = None, id = None):
        self.context_s = context_s # short
        self.context_f = context_f # full
        self.question, self.answers = question, answers
        self.source, self.id = source, id
        self.q_type, self.a_type = q_type, a_type
        self.gen_questions, self.gen_answers = gen_questions, gen_answers
        self.triples_name, self.triples_code = triples_name, triples_code


def make_trip_elems(triple_name): 
    if len(triple_name) == 4:
        trip_elems = {'pick_s_name': triple_name[0], 'pick_p_name': triple_name[2],
                'pick_o_name': triple_name[3], 'qual_obj_name': triple_name[1]}
    else: 
        trip_elems = {'pick_s_name': triple_name[0], 'pick_p_name': triple_name[1],
                'pick_o_name': triple_name[2], 'qual_obj_name': None}
    return trip_elems


def align_answer(gen_answer, answer_ms_data, triples_name, no_match_val = 999): 
    '''
    helper function to align the Reader-detected answer to an entity/value in the tripleset
    '''
    ans_text = None
    if answer_ms_data is not None: ans_text = answer_ms_data['text'].lower()   
    triples_name_lwr = [[_strip_punct_ws(i.lower()) for i in t] for t in triples_name]
    # answer from Reader... lowercase, strip punct, remove +ws to better match
    gen_answer_lwr = _strip_punct_ws(gen_answer.lower())  
    
    # 1. check if ans_text contained/exact-match in Subject or any object
    results = {}
    todos = [gen_answer_lwr, ans_text] if answer_ms_data is not None else [gen_answer_lwr]
    for ans in todos:
        ans_pos, score = _check_subobj(ans, triples_name_lwr,
                                        check = 'contain', no_match_val = no_match_val)
        
        if ans_pos is not None: results[score] = ans_pos
    
    # 2. if no result (else)... check for least edit distance with ans_text
    if results:
        ans_pos = results[min(results.keys())]
    else: 
        if ans_text is None: ans_text = gen_answer_lwr 
        ans_pos, score = _check_subobj(ans_text, triples_name_lwr, 
                                       check = 'edit_dist', no_match_val = no_match_val)
        
    return ans_pos

def _strip_punct_ws(text, ttable = str.maketrans('','', string.punctuation)):
    text = text.translate(ttable)
    return re.sub('\s{2,}', ' ', text)

def _check_subobj(ans_text, triples_name_lwr, check = 'contain', no_match_val = 999):
    '''
    params:
    - check (str): 'contain 'check if answer text contained in either 
    Subject or one of the objects. 'edit_dist' check for either 
    Subject or one of the objects that has the least edit distance to answer text 
    returns:
    (tuple): 1st elem is a tuple of (triple index #, entity/value index position in triple)
    2nd elem is the min value that led to selection.
    '''
    edit_ls = []
    for idx, t in enumerate(triples_name_lwr):
        keys = [0, -1] if idx == 0 else [-1] # check Subj during 1st triple
        for k in keys:
            if check == 'contain':
                lat = len(ans_text) if ans_text else 0
                if ans_text in t[k]: 
                    if lat: edit_ls.append(len(t[k])/lat) 
                    else: edit_ls.append(0)
                else: edit_ls.append(no_match_val)
            elif check == 'edit_dist':
                edit_ls.append(edit_distance(ans_text, t[k]))
            else: raise ValueError
    
    if set(edit_ls) == set([no_match_val]): return None, None
    best = edit_ls.index(min(edit_ls))

    # if 0, match is with Subj. if > 0, match is with Objects (-1 to account for Subj check)
    return (0, 0) if best == 0 else (best-1, -1), min(edit_ls)

def trim_entry_obj(entry_obj, keep_attrs = []):
    '''
    used once an entry has prep4input applied (and does not need all the remaining
    attributes except a subset for use in collate_fn). 
    '''
    keys = entry_obj.__dict__
    for k in list(keys):
        if k in ['id', 'tgt', 'src', 'id_enc', 'graph', 
        'text', 'ipstrat', 'src_ctx', 'src_qst', 'src_ans', 'negative'] + keep_attrs: continue
        delattr(entry_obj, k)
    return entry_obj

##### Question Type Detection #####
# question type detector, surface realisation-based, anywhere in question
# adapted heavily from https://github.com/GEM-benchmark/NL-Augmenter/blob/main/filters/question_filter/filter.py
# credits to the authors

class QTypesDetect():

    def __init__(self, keywords: List[str] = ['where', 'what', 'who', 'which', 'when', 'how', 'why']):
        # NOTE: including 'how' (quantity seeking) and 'why' questions. The latter is present in SQuAD and
        # other textQA datasets.
        super().__init__()
        self.question_words = set(k.lower() for k in keywords)
        self.keywords = keywords
        self.nlp = spacy.load("en_core_web_sm")

    def detect_qtypes(self, context:str = None, 
                            question: str = None, 
                            answers: List[str]= None):
        '''Determines if some keyword is present in the given question or not'''
        tokenized = self.nlp(question)
        tokenized = [token.text.lower() for token in tokenized]

        # qword check
        r_dict = OrderedDict()
        for qword in self.question_words:
            r_dict[qword] = qword in tokenized

        # ans ent type check
        r_dict.update({'numeric': False, 'date': False, 'person': False, 
                        'location': False, 'common_noun': False, 'verb_phrase': False})

        if answers is None: answers = []
        for ans in answers:
            ans_tokenized = self.nlp(ans)
            ans_tags = [i.pos_ for i in ans_tokenized]
            ans_ner = [i.label_ for i in ans_tokenized.ents]
            ans_phrases = [i for i in ans_tokenized.noun_chunks]
            # NumericQuestion
            if 'NUM' in ans_tags and 'DATE' not in ans_ner:
                r_dict['numeric'] = True
            # DateQuestion
            if 'NUM' in ans_tags and 'DATE' in ans_ner:
                r_dict['date'] = True

            if not r_dict['numeric']:
                # PersonQuestion
                if 'PERSON' in ans_ner:
                    r_dict['person'] = True
                # LocationQuestion
                if 'LOC' in ans_ner or 'GPE' in ans_ner:
                    r_dict['location'] = True
                # CommonNounPhraseQuestion
                if 'PROPN' not in ans_tags and 'VERB' \
                    not in ans_tags and 'ADJ' not in ans_tags and ans_phrases !=[]:
                    r_dict['common_noun'] = True
                # VerbPhraseQuestion
                if 'PROPN' not in ans_tags and 'VERB' in ans_tags and 'ADJ' not in ans_tags:
                    r_dict['verb_phrase'] = True

        if sum(r_dict.values()) == 0:
            r_dict['other'] = True
        else: r_dict['other'] = False
        
        return r_dict, ' '.join(tokenized)
