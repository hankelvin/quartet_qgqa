from data_utils.shared import Entry, T2QEntry, G2QEntry, make_trip_elems

##### Complex Questions related #####
class CQ_Entry(Entry):
    '''
    Holds information about a complex QA pair. 
    '''
    def __init__(self, args, question, answer, triples_code, triples_name, ans_pos,
                qtypes = None, a_semtypes = None, id = None, method = 'trip_baseline',
                negative = False, graphdb_avail = False):       
        super().__init__(args, graphdb_avail = graphdb_avail)
        self.negative = negative
        self.question = question
        self.answer = answer
        self.ans_pos = ans_pos
        self.assrt = None
        self.text = question # for use with duckling (only on question part)
        self.triples_code = triples_code # to complete
        self.triples_name = triples_name # to complete
        self.id = id
        self.qtypes = qtypes
        self.a_semtypes = a_semtypes
        self.args = args
        self.graphdb_avail = graphdb_avail


class OmnibusCQ(G2QEntry, T2QEntry):
    '''
    Holds information about a CQ and support information (context etc) for CQG.
    '''
    def __init__(self, args, tokenizer, cq_entry, context_f, 
                triples_code, triples_name, task_name, task_settings, id = None,
                gemformatter = None, negative = False):
        super().__init__(args, tokenizer)
        self.cq_entry = cq_entry
        self.context_f = context_f
        self.triples_code = triples_code 
        self.triples_name = triples_name 
        self.id = id
        self.args = args
        self.tokenizer = tokenizer
        self.is_quartet = 'quartet' in args.settings['tasks'] or 'quartet_infer' in args.settings_code
        self.max_length = args.settings['tokenization']['max_length']
        self.truncation = args.settings['tokenization']['truncation']
        self.task_name, self.task_settings = task_name, task_settings
        self.gemformatter = gemformatter
        self.prmpt = task_settings['prompt_strategies']
        self.enc_pfx, self.dec_pfx = '', ''
        if self.prmpt or self.prmpt == '': 
            if self.prmpt == 'verbal': 
                # {0}: # questions, {1}: qtype; {2}: question(s) ; {3} input type; {4}: n_facts
                self.enc_pfx = 'generate {}{} complex question{} of {} facts from {}'
                # {0}: # questions, {1} input type; {2}: n_facts
                self.dec_pfx = 'cq {} {} {}'
            elif self.prmpt in ['text_num_facts']:
                self.enc_pfx = '{} {}' # src_type, num_facts
                self.dec_pfx = '{} {}' # src_type, num_facts
            elif self.prmpt in ['']: pass
            else: raise NotImplementedError(f'({self.prmpt})')

        self.trp_sep_scheme = args.settings['trp_sep_scheme']
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)

        self.tgt_type_list, self.n_facts_list = [], []

        self.src_ctx, self.src_qst, self.src_ans = None, None, None
        self.negative = negative
        self.lang = None

    def prep4input(self, src_type = 'g2cq', n_facts = None, multilingual = False, **kwargs):
        self.tgt_type_list.append(type)
        self.n_facts_list.append(n_facts)
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)
        c_ans_included_SRC = 'ans_included' in self.task_settings['src_setting']
        c_ans_included_TGT = 'ans_included' in self.task_settings['tgt_setting']
        c_qtype_included = 'qtype_included' in self.task_settings['src_setting']
        c_a_semtype_included = 'a_semtype_included' in self.task_settings['src_setting']
        self.ipstrat = src_type
        if src_type == 't2cq': 
            input_type = 'text'

            src = self.context_f
            src_ans_str = self.ans_sep + self.cq_entry.answer if c_ans_included_SRC else ''
        
        elif src_type == 'g2cq': 
            input_type = 'rdf'

            triples_name = self.cq_entry.triples_name
            if 'dqe_gem' in self.task_settings['src_format']:
                src = self.gemformatter(triples_name)
            elif 'trp_sep_scheme' in self.task_settings['src_format']:
                src = ''
                for t in triples_name:
                    t = make_trip_elems(t)
                    src += self.create_triple(t)
            if self.cq_entry.ans_pos is not None:
                answer = self._get_ans_from_graph(self.cq_entry) 
            else:
                assert self.cq_entry.answer is not None 
                answer = self.cq_entry.answer
            src_ans_str = self.ans_sep + answer if c_ans_included_SRC else ''
        
        # add qtype, ans sem type to src/tgt if spec-ed
        src_ans_str = src_ans_str + self.qst_sep + self.cq_entry.qtypes[0] if c_qtype_included else src_ans_str
        src_ans_str = src_ans_str + self.spare2_sep + self.cq_entry.a_semtypes[0] if c_a_semtype_included else src_ans_str
        tgt_ans_str = self.ans_sep + self.cq_entry.answer if c_ans_included_TGT else ''

        # controllability
        num_q, n_facts = 1, len(self.cq_entry.triples_name) if n_facts is None else n_facts
        qtype, q_plural = '', '' if num_q == 1 else 's'
        
        ### SRC 
        enc_verbal_pfx, dec_verbal_pfx = '', ''
        if self.prmpt == 'verbal': 
            enc_verbal_pfx = self.enc_pfx.format(num_q, qtype, q_plural, n_facts, input_type) 
            dec_verbal_pfx = self.dec_pfx.format(num_q, n_facts, input_type) 
        elif self.prmpt in ['text_num_facts']:# and type == 't2cq': 
            assert n_facts is not None
            enc_verbal_pfx = self.enc_pfx.format(input_type, n_facts) 
            dec_verbal_pfx = self.dec_pfx.format(input_type, n_facts) 
        if multilingual: 
            assert self.lang
            mling_str = f'[{self.lang}] '
        else: mling_str = ''
        if self.is_quartet: 
            enc_verbal_pfx = f'cqg task {mling_str}{self.spare1_sep} ' + enc_verbal_pfx
            dec_verbal_pfx = f'cqg task {mling_str}{self.spare1_sep} ' + dec_verbal_pfx
        else: # else add mling to verbal pfx
            enc_verbal_pfx+=mling_str
            dec_verbal_pfx+=mling_str
            
        src = enc_verbal_pfx + src_ans_str + self.input_sep + src
        self.src = self.tokenizer.encode(src, truncation = self.truncation, max_length = self.max_length)

        ### TGT
        tgt = dec_verbal_pfx + tgt_ans_str + self.qst_sep + self.cq_entry.question
        self.tgt = self.tokenizer.encode(tgt, truncation = self.truncation, max_length = self.max_length)

    def _get_ans_from_graph(self, cq_entry):
        triples_name, ans_pos = cq_entry.triples_name, cq_entry.ans_pos
        return triples_name[ans_pos[0]][ans_pos[1]]

##### Simple Questions #####
class SQEntry(G2QEntry):

    def __init__(self, args, tokenizer, question, answer, triples_name, triples_code, 
                qtypes, a_semtypes, task_name, task_settings, id, gemformatter, 
                context_f = None, negative = False):
        self.question, self.answer = question, answer
        self.triples_name, self.triples_code = triples_name, triples_code
        self.context_f = context_f

        self.args, self.tokenizer, self.id = args, tokenizer, id
        self.is_quartet = 'quartet' in args.settings['tasks'] or 'quartet_infer' in args.settings_code
        self.gemformatter = gemformatter

        self.max_length = args.settings['tokenization']['max_length']
        self.truncation = args.settings['tokenization']['truncation']
        self.task_name, self.task_settings = task_name, task_settings
        self.qtypes, self.a_semtypes = qtypes, a_semtypes

        self.trp_sep_scheme = args.settings['trp_sep_scheme']
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)
        
        self.prmpt = task_settings['prompt_strategies']
        self.enc_pfx, self.dec_pfx = '{}', '{}' # NOTE: we always demarcate the type of the input (rdf/text)
        if self.prmpt or self.prmpt == '': 
            if self.prmpt == 'verbal': 
                # {0}: # questions, {1}: qtype; {2}: question(s) ; {3} input type; {4}: n_facts
                self.enc_pfx = 'generate 1 simple question of 1 fact from {}'
                # {0}: # questions, {1} input type; {2}: n_facts
                self.dec_pfx = 'sq 1 {} 1'
            elif self.prmpt in ['text_num_facts']:
                self.enc_pfx = '{} {}'
                self.dec_pfx = '{} {}'
            elif self.prmpt in ['']: pass
            else: raise NotImplementedError(self.prmpt)

        self.src_ctx, self.src_qst, self.src_ans = None, None, None
        self.negative = negative
        self.lang = None

    def prep4input(self, src_type = 'g2sq', multilingual = False, **kwargs):
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)
        c_ans_included_SRC = 'ans_included' in self.task_settings['src_setting']
        c_ans_included_TGT = 'ans_included' in self.task_settings['tgt_setting']
        c_qtype_included = 'qtype_included' in self.task_settings['src_setting']
        c_a_semtype_included = 'a_semtype_included' in self.task_settings['src_setting']
        self.ipstrat = src_type
        if src_type == 'g2sq': 
            input_type = 'rdf'
            assert self.triples_name is not None
            triples_name = self.triples_name
            if 'dqe_gem' in self.task_settings['src_format']:
                src = self.gemformatter(triples_name)
            elif 'trp_sep_scheme' in self.task_settings['src_format']:
                src = ''
                for t in triples_name:
                    t = make_trip_elems(t)
                    src += self.create_triple(t)
        elif src_type == 't2sq': 
            input_type = 'text'
            assert self.context_f is not None
            src = self.context_f
        else: raise NotImplementedError
        
        ### SRC 
        src_ans_str = self.ans_sep + self.answer if c_ans_included_SRC else ''
        src_ans_str = src_ans_str + self.qst_sep + self.qtypes[0] if c_qtype_included else src_ans_str
        src_ans_str = src_ans_str + self.spare2_sep + self.a_semtypes if c_a_semtype_included else src_ans_str
        
        if self.prmpt == 'verbal': 
            enc_verbal_pfx = self.enc_pfx.format(input_type) 
            dec_verbal_pfx = self.dec_pfx.format(input_type) 
        elif self.prmpt in ['text_num_facts']:
            enc_verbal_pfx = self.enc_pfx.format(input_type, '1') 
            dec_verbal_pfx = self.dec_pfx.format(input_type, '1') 
        if multilingual: 
            assert self.lang
            mling_str = f'[{self.lang}] '
        else: mling_str = ''
        if self.is_quartet: 
            enc_verbal_pfx = f'sqg task {mling_str}{self.spare1_sep}' + enc_verbal_pfx
            dec_verbal_pfx = f'sqg task {mling_str}{self.spare1_sep}' + dec_verbal_pfx
        else: # else add mling to verbal pfx
            enc_verbal_pfx+=mling_str
            dec_verbal_pfx+=mling_str
        src = enc_verbal_pfx + src_ans_str + self.input_sep + src
        self.src = self.tokenizer.encode(src, truncation = self.truncation, max_length = self.max_length)

        ### TGT
        tgt_ans_str = self.ans_sep + self.answer if c_ans_included_TGT else ''
        tgt = dec_verbal_pfx + tgt_ans_str + self.qst_sep + self.question
        self.tgt = self.tokenizer.encode(tgt, truncation = self.truncation, max_length = self.max_length)