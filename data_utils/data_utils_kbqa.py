import re, spacy
from data_utils.shared import T2QEntry, G2QEntry, make_trip_elems
from dataset_handlers.utils import Triple
spacy_pipeline = spacy.load('en_core_web_sm')

class KBQAEntry(T2QEntry, G2QEntry):
    '''
    For answering questions from graph.
    '''
    def __init__(self, args, tokenizer, question, answer,
                context_f, triples_name, task_name, task_settings, 
                ans_pos = None, trip_start_token_id = None, id = None, 
                negative = False, **kwargs):
        self.ipstrat = 'kbqa'
        self.context_f = context_f # this is the RDF subgraph; graph_prime
        self.question, self.answer = question, answer
        self.triples_name, self.ans_pos = triples_name, ans_pos
        self.args, self.tokenizer, self.id = args, tokenizer, id
        self.is_quartet = 'quartet' in args.settings['tasks'] or 'quartet_infer' in args.settings_code
        self.triples_ents = set()
        for t in self.triples_name:
            if len(t) == 3: self.triples_ents.update(t[::2])
            elif len(t) == 4: self.triples_ents.update(t[0:1] + t[2:])

        self.max_length = args.settings['tokenization']['max_length']
        self.truncation = args.settings['tokenization']['truncation']
        self.task_name, self.task_settings = task_name, task_settings

        self.trp_sep_scheme = args.settings['trp_sep_scheme']
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)
        
        self.trip_start_token_id = trip_start_token_id
        self.prmpt = task_settings['prompt_strategies']
        self.enc_pfx, self.dec_pfx = '', ''
        if self.prmpt: 
            if self.prmpt == 'verbal': 
                # {0}: # question, {1}: separator; {2}: context
                self.enc_pfx = f'answer KBQA {self.spare1_sep} '
            elif self.prmpt in ['', 'text_num_facts']: pass
            else: raise NotImplementedError

        self.src_ctx, self.src_qst, self.src_ans = None, None, None
        self.negative = negative
        self.lang = None

    def prep4input(self, src_format = 'dqe_gem', tgt_format = 'dqe_generate', kelm = False, 
                   multilingual = False, **kwargs):
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)
        self.ipstrat = 'kbqa' # HACK: to allow counter and upsamp
        ### SRC 
        if 'dqe_gem' in src_format:
            # from DQE, context_f is a str (linearised triples)
            assert type(self.context_f) == str, f'context_f should be a string, instead it was: {type(self.context_f)}'
            context = self.context_f
        elif 'trp_sep_scheme' in src_format: 
            # from GEM, context_f is a list of lists (list of triples)
            if type(self.context_f) == list or type(self.context_f) == tuple: 
                context = ''
                for t in self.triples_name:
                    t = make_trip_elems(t)
                    context += self.create_triple(t)
            # from DQE, context_f is a str (linearised triples)
            elif type(self.context_f) == str: context = self.context_f 
            else: raise ValueError('Check type of self.context_f')
        else: raise NotImplementedError

        if multilingual: 
            assert self.lang
            mling_str = f'[{self.lang}] '
        else: mling_str = ''
        if self.is_quartet and self.prmpt != 'verbal': 
            enc_verbal_pfx = f'kbqa task {mling_str}{self.spare1_sep} ' 
            dec_verbal_pfx = f'kbqa task {mling_str}{self.spare1_sep} '
        elif self.prmpt != 'verbal': 
            enc_verbal_pfx = self.enc_pfx + mling_str
            dec_verbal_pfx = self.dec_pfx + mling_str

        src = enc_verbal_pfx + f'{self.question} {self.input_sep} {context}'
        self.src = self.tokenizer.encode(src, truncation = self.truncation, max_length = self.max_length)

        ### TGT 
        if tgt_format == 'dqe_generate': 
            # ensure _ removed.... follow DQE treatment of entity/values
            if not kelm: 
                tgt = ' '.join([t.text for t in spacy_pipeline(Triple.clean_obj(self.answer.strip(), lc=False))])
            else: tgt = self.answer
            self.tgt = self.tokenizer.encode(dec_verbal_pfx + tgt, 
                        truncation = self.truncation, max_length = self.max_length)
            
        else: raise NotImplementedError

    def check_nexts(self, answer_run, running, ans_indices, src, idx, ws_check_len, chunk_len):
        chunk_next = self.tokenizer.decode(src[idx+1])
        check_span = answer_run[ws_check_len:chunk_len+ws_check_len+5]
        
        if chunk_next in ['', '</s>', '.', '`', "'"]: # if empty string is next (idx was end of word), include idx
            ans_indices.append(idx)
            answer_run, running = answer_run[ws_check_len+1:], True
        
        elif chunk_next in check_span:
            ans_indices.append(idx)
            cut = re.search(rf'{re.escape(chunk_next)}', check_span).end()-len(chunk_next)
            answer_run, running = answer_run[ws_check_len+cut:], True
        return answer_run, running, ans_indices

    
    
    
    
        
        
        
            
            
        
            
            
            
        