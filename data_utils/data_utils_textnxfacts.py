import re
from data_utils.shared import make_trip_elems, Entry

class TextNxFactsEntry:
    '''
    For predicting the number of facts present in a given text. 
    Possible predictions between {1, 2, 3, 4, 5 or more}
    '''

    def __init__(self, args, tokenizer, context_f, num_facts, task_name, task_settings, id = None):
        
        self.context_f, self.num_facts = context_f, num_facts
        self.args, self.tokenizer = args, tokenizer
        self.task_settings, self.task_name = task_settings, task_name
        self.id = id

        self.max_length = args.settings['tokenization']['max_length']
        self.truncation = args.settings['tokenization']['truncation']
        
        self.trp_sep_scheme = args.settings['trp_sep_scheme']
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)

        self.prmpt = task_settings['prompt_strategies']
        self.enc_pfx, self.dec_pfx = '', ''
        if self.prmpt: 
            if self.prmpt == ['verbal', 'text_num_facts']: 
                # {0}: # question, {1}: separator; {2}: context
                self.enc_pfx = f'predict number of facts in the text {self.spare1_sep} '
                self.dec_pfx = f'number of facts: '
            elif self.prmpt in ['', ]: pass
            else: raise NotImplementedError
            
        self.src_ctx, self.src_qst, self.src_ans = None, None, None
        self.negative = False


    def prep4input(self, **kwargs):
        self.ipstrat = 'textnxfacts'
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)
        ### SRC 
        src = self.enc_pfx + self.context_f
        self.src = self.tokenizer.encode(src, truncation = self.truncation, max_length = self.max_length)

        ### TGT 
        # 0-indexed
        # for all num_facts > 4: set to single label , i.e. more than 4"
        self.tgt = self.num_facts if self.num_facts<=4 else 4
        if self.args.settings['tasks']['textnxfacts']['tgt_format'] == 'dqe_generate':
            self.tgt = self.tokenizer.encode(self.dec_pfx + str(self.tgt))
        

class TextAnswerSelectorEntry(Entry):
    '''
    For selecting answer spans from text.
    '''
    def __init__(self, args, tokenizer, context_f, src_type, answerset, task_name, task_settings, id = None):
        
        self.context_f, self.answerset = context_f, answerset # answerset is of type list
        self.args, self.tokenizer = args, tokenizer
        self.task_settings, self.task_name = task_settings, task_name
        self.id = id
        self.src_type = src_type

        self.max_length = args.settings['tokenization']['max_length']
        self.truncation = args.settings['tokenization']['truncation']
        
        self.trp_sep_scheme = args.settings['trp_sep_scheme']
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)

        self.prmpt = task_settings['prompt_strategies']
        self.uncased_src = 'uncased' in task_settings['src_setting']
        self.uncased_tgt = 'uncased' in task_settings['tgt_setting']
        self.enc_pfx, self.dec_pfx = '', ''
        if self.prmpt: 
            if self.prmpt == 'verbal': 
                # {0}: # question, {1}: separator; {2}: context
                self.enc_pfx = f'select answer spans in the {src_type} {self.spare1_sep} '
                self.dec_pfx = f'answer spans: '
            elif self.prmpt in ['', ]: pass
            else: raise NotImplementedError

        self.src_ctx, self.src_qst, self.src_ans = None, None, None
        self.negative = False

    def prep4input(self, bypass_tgt = False,  bypass_starts = False, **kwargs):
        self.ipstrat = 'textansselect'
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)
        
        ### SRC 
        if self.src_type == 'rdf':
            if 'trp_sep_scheme' in self.task_settings['src_format']:
                src_context_f = ''
                for t in self.triples_name:
                    t = make_trip_elems(t)
                    src_context_f += self.create_triple(t)
            else: raise NotImplementedError
        else: src_context_f = self.context_f
        if self.uncased_src: src_context_f = src_context_f.lower()
        lang_str = f'[{self.lang}]' if self.args.multilingual else ''
        src = lang_str + self.enc_pfx + src_context_f
        self.src = self.tokenizer.encode(src, truncation = self.truncation, max_length = self.max_length)

        ### TGT 
        self.tgt = [0] # HACK: filler for when bypass_tgt
        if not bypass_tgt:
            if 'dqe_generate' in self.args.settings['tasks']['textansselect']['tgt_format']:
                ans_sep = self.trp_seps['ans']
                answerset = list(self.answerset)
                if self.uncased_src: answerset = [a.lower() for a in answerset]
                starts = []
                ctr_bypass_start = 9999
                for a in answerset:
                    try: starts.append(re.search(re.compile(re.escape(a)), src_context_f).span()[0])
                    except: 
                        if bypass_starts: 
                            starts.append(ctr_bypass_start)
                            ctr_bypass_start-=1
                            print(f'\t\t FAIL (BYPASS): starts for.... {src_context_f} <> {a}')
                        else: print(f'\t\t FAIL: starts for.... {src_context_f} <> {a}')

                tgt = ans_sep 
                locs = {start: a for start, a in zip(starts, answerset)}
                for loc in sorted(locs.keys()):
                    ans = locs[loc]
                    if self.uncased_tgt: ans = ans.lower()
                    tgt += f'{ans_sep}{ans}' # keep original setting
                self.tgt = self.tokenizer.encode(self.dec_pfx + tgt)
            
            else: raise NotImplementedError


class EntLinkEntry(Entry):
    '''
    For predicting entity mentions and types in input.
    '''
    def __init__(self, args, tokenizer, context_f, triples_name, text_mentionspans, graph_mentionspans,
                task_name, task_settings, gemformatter, id = None, gemformat_done = False,
                trip_start_token_id = None, negative = False):
        
        self.context_f = context_f
        self.text_mentionspans, self.graph_mentionspans = text_mentionspans, graph_mentionspans
        self.triples_name = triples_name
        self.args, self.tokenizer, self.id = args, tokenizer, id
        self.task_settings, self.task_name = task_settings, task_name
        self.is_quartet = 'quartet' in args.settings['tasks'] or 'quartet_infer' in args.settings_code
        
        self.max_length = args.settings['tokenization']['max_length']
        self.truncation = args.settings['tokenization']['truncation']
        self.task_name, self.task_settings = task_name, task_settings

        self.trp_sep_scheme = args.settings['trp_sep_scheme']
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)
        
        self.trip_start_token_id = trip_start_token_id
        self.prmpt = task_settings['prompt_strategies']
        self.enc_pfx, self.dec_pfx = '', ''
        if self.prmpt: 
            if self.prmpt == 'verbal': 
                # {0}: # question, {1}: separator; {2}: context
                self.enc_pfx = f'predict entities and types {self.spare1_sep} '
            elif self.prmpt in ['', 'text_num_facts']: pass
            else: raise NotImplementedError

        self.src_ctx, self.src_qst, self.src_ans = None, None, None
        self.gemformatter = gemformatter
        self.negative = negative

    def prep4input(self, src_type = 't2el'):
        self.ipstrat = src_type 
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)

        if src_type == 'g2el': 
            assert self.triples_name is not None
            triples_name = self.triples_name
            if 'dqe_gem' in self.task_settings['src_format']:
                src = self.gemformatter(triples_name)
            elif 'trp_sep_scheme' in self.task_settings['src_format']:
                src = ''
                for t in triples_name:
                    t = make_trip_elems(t)
                    src += self.create_triple(t)
            
            tgt = ''
            locs = {re.search(re.escape(g_en), src).start(): (g_en, etypes) \
                    for g_en, etypes in self.graph_mentionspans.items()}

            for loc in sorted(locs.keys()):
                g_en, etypes = locs[loc]
                tgt += f'{self.input_sep} {g_en} {self.spare3_sep} {etypes}'
        elif src_type == 't2el': 
            assert self.context_f is not None
            src = self.context_f

            tgt = ''
            locs = {ms['start_pos'] if 'start_pos' in ms else ms['start_char']: \
                    (ms['text'], ms['enttype']) for ms in self.text_mentionspans}
            for loc in sorted(locs.keys()):
                text, etypes = locs[loc]
                tgt += f'{self.input_sep} {text} {self.spare3_sep} {etypes}'
        else: raise NotImplementedError

        if self.is_quartet and self.prmpt != 'verbal': 
            enc_verbal_pfx = f'entlink task {self.spare1_sep} ' 
            dec_verbal_pfx = f'entlink task {self.spare1_sep} '
        elif self.prmpt != 'verbal': 
            enc_verbal_pfx = self.enc_pfx
            dec_verbal_pfx = self.dec_pfx

        ### SRC 
        self.src = self.tokenizer.encode(enc_verbal_pfx + src, 
                    truncation = self.truncation, max_length = self.max_length)

        ### TGT 
        self.tgt = self.tokenizer.encode(dec_verbal_pfx + tgt, 
                    truncation = self.truncation, max_length = self.max_length)