from data_utils.shared import make_trip_elems, Entry

class Data2TextEntry(Entry):
    '''
    For producing text from data (D2T) or parsing text into data (T2D)
    '''
    def __init__(self, args, tokenizer, context_f, triples_name, task_name, task_settings, 
                gemformatter, id = None, gemformat_done = False):
        
        self.context_f = context_f
        self.gemformatter = gemformatter
        self.args, self.tokenizer = args, tokenizer
        self.task_settings, self.task_name = task_settings, task_name
        self.id = id

        self.trp_sep_scheme = args.settings['trp_sep_scheme']
        self.trp_seps = args.trp_seps
        for k, v in self.trp_seps.items():
            setattr(self, f'{k}_sep', v)
        
        if 'dqe_gem' in self.task_settings['src_format'] and not gemformat_done:
            self.triples_name = self.gemformatter(triples_name)
        elif 'trp_sep_scheme' in self.task_settings['src_format']:
            self.triples_name = ''
            for t in triples_name:
                t = make_trip_elems(t)
                self.triples_name += self.create_triple(t)
        else: self.triples_name = triples_name

        self.max_length = args.settings['tokenization']['max_length']
        self.truncation = args.settings['tokenization']['truncation']
        self.is_quartet = 'quartet' in args.settings['tasks'] or 'quartet_infer' in args.settings_code

        self.prmpt = task_settings['prompt_strategies']
        self.enc_pfx, self.dec_pfx = '', ''
        if self.prmpt: 
            if self.prmpt == 'verbal': raise NotImplementedError
            elif self.prmpt in ['', 'text_num_facts']: pass
            else: raise NotImplementedError

        self.src_ctx, self.src_qst, self.src_ans = None, None, None
        self.negative = True 

    def prep4input(self, direction = 'data2text', **kwargs):
        self.id_enc = self.tokenizer.encode(self.id, add_special_tokens=False)
        assert self.triples_name is not None
        self.ipstrat = direction
        
        ### SRC 
        if self.is_quartet: 
            assert direction in ['data2text', 'text2data']
            enc_verbal_pfx = f'{direction} task {self.spare1_sep}' 
            dec_verbal_pfx = f'{direction} task {self.spare1_sep}' 

        else: raise NotImplementedError
        src = enc_verbal_pfx + self.context_f if direction == 'text2data' else enc_verbal_pfx + self.triples_name
        self.src = self.tokenizer.encode(src, truncation = self.truncation, max_length = self.max_length)

        ### TGT 
        self.tgt = self.tokenizer.encode(dec_verbal_pfx + self.context_f if direction == 'data2text' \
                                    else dec_verbal_pfx + self.triples_name)
        
    def create_triple(self, trip_elems = None):
        '''
        creates a triple from the s,p,o attributes in instance. 
        '''
        pick_s_name, pick_p_name  = trip_elems['pick_s_name'], trip_elems['pick_p_name']
        pick_o_name, qual_obj_name = trip_elems['pick_o_name'], trip_elems['qual_obj_name']            

        if self.trp_sep_scheme == 1:
            triple = f'{pick_p_name} {self.o_sep}{pick_o_name}'
            if qual_obj_name is None:
                triple = f'{pick_s_name} {self.p_sep}' + triple
            else: 
                # e.g. 'Valentin Lavigne', 'FC Lorient', 'start time', '01 January 2014'
                # will be 'Valentin Lavigne [REL] FC Lorient | start time | 01 January 2014'
                triple = \
                f'{pick_s_name} {self.qp_sep}{qual_obj_name} {self.o_sep}' + triple
        
        elif self.trp_sep_scheme == 2:
            # '<PRP> member of sport team <OBJ> FC Lorient':
            triple = f'{self.p_sep}{pick_p_name} {self.o_sep}{pick_o_name}'
            if qual_obj_name is None:
                # '<SUB> Valentin Lavigne <PRP> member of sport team <OBJ> FC Lorient':
                triple = f'{self.s_sep}{pick_s_name} ' + triple
            else: 
                # e.g. 'Valentin Lavigne', 'FC Lorient', 'start time', '01 January 2014'
                # will be '<SUB> Valentin Lavigne [REL] <qOBJ> FC Lorient <PRP> start time <OBJ> 01 January 2014'
                triple = \
                f'{self.s_sep}{pick_s_name} {self.qp_sep}{self.qo_sep}{qual_obj_name} ' + triple

        if trip_elems: return triple
        else: self.triple = triple